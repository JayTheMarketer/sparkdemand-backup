<?php
global $wpdb, $wpc_client; if ( isset($_REQUEST['_wp_http_referer']) ) { $redirect = remove_query_arg(array('_wp_http_referer' ), wp_unslash( $_REQUEST['_wp_http_referer'] ) ); } else { $redirect = get_admin_url(). 'admin.php?page=wpc_private_post_types'; } if ( isset( $_GET['action'] ) ) { switch ( $_GET['action'] ) { case 'assign_client': $ids = array(); if( isset( $_REQUEST['item'] ) ) { check_admin_referer( 'bulk-' . sanitize_key( __('post types', WPC_CLIENT_TEXT_DOMAIN) ) ); $ids = $_REQUEST['item']; } $assigns = ( !empty( $_REQUEST['assigns'] ) ) ? explode( ',', $_REQUEST['assigns'] ) : array(); foreach( $ids as $post_id ) { $wpc_client->cc_set_assigned_data( 'private_post', $post_id, 'client', $assigns ); } do_action( 'wp_client_redirect', add_query_arg( 'msg', 'cla', $redirect ) ); exit; break; case 'assign_circle': $ids = array(); if( isset( $_REQUEST['item'] ) ) { check_admin_referer( 'bulk-' . sanitize_key( __('post types', WPC_CLIENT_TEXT_DOMAIN) ) ); $ids = $_REQUEST['item']; } $assigns = ( !empty( $_REQUEST['assigns'] ) ) ? explode( ',', $_REQUEST['assigns'] ) : array(); foreach( $ids as $post_id ) { $wpc_client->cc_set_assigned_data( 'private_post', $post_id, 'circle', $assigns ); } do_action( 'wp_client_redirect', add_query_arg( 'msg', 'cia', $redirect ) ); exit; break; case 'cancel_protect': if( current_user_can( 'wpc_manager' ) && !current_user_can( 'administrator' ) ) { do_action( 'wp_client_redirect', $redirect ); exit; } $ids = array(); if ( isset( $_REQUEST['id'] ) ) { check_admin_referer( 'wpc_cancel_protect' . $_REQUEST['id'] . get_current_user_id() ); $ids = ( is_array( $_REQUEST['id'] ) ) ? $_REQUEST['id'] : (array) $_REQUEST['id']; } elseif( isset( $_REQUEST['item'] ) ) { check_admin_referer( 'bulk-' . sanitize_key( __('post types', WPC_CLIENT_TEXT_DOMAIN ) ) ); $ids = $_REQUEST['item']; } foreach( $ids as $post_id ) { delete_post_meta( $post_id, '_wpc_protected' ); } do_action( 'wp_client_redirect', add_query_arg( 'msg', 'canceled_protect', $redirect ) ); exit; break; } } if ( !empty( $_GET['_wp_http_referer'] ) ) { do_action( 'wp_client_redirect', remove_query_arg( array( '_wp_http_referer', '_wpnonce'), wp_unslash( $_SERVER['REQUEST_URI'] ) ) ); exit; } $where_clause = ''; if( !empty( $_GET['s'] ) ) { $where_clause .= $wpc_client->get_prepared_search( $_GET['s'], array( 'p.post_title', ) ); } if( !empty( $_GET['p_type'] ) ) { $where_clause .= " AND p.post_type = '" . trim( esc_sql( $_GET['p_type'] ) ) . "'"; } if( current_user_can( 'wpc_manager' ) && !current_user_can( 'administrator' ) ) { $manager_id = get_current_user_id(); $manager_clients = $wpc_client->cc_get_assign_data_by_object( 'manager', $manager_id, 'client' ); $manager_groups = $wpc_client->cc_get_assign_data_by_object( 'manager', $manager_id, 'circle' ); $clients_groups = array(); foreach ( $manager_groups as $group_id ) { $clients_groups = array_merge( $clients_groups, $wpc_client->cc_get_group_clients_id( $group_id ) ); } $manager_all_clients = array_unique( array_merge( $manager_clients, $clients_groups ) ); $groups_clients = array(); foreach ( $manager_clients as $client_id ) { $groups_clients = array_merge( $groups_clients, $wpc_client->cc_get_client_groups_id( $client_id ) ); } $manager_all_groups = array_unique( array_merge( $manager_groups, $groups_clients ) ); if( count( $manager_all_clients ) || count( $manager_all_groups ) ) { $where_clause .= " AND ( "; if( count( $manager_all_clients ) ) { $private_ids = $wpc_client->cc_get_assign_data_by_assign( 'private_post', 'client', $manager_all_clients ); $where_clause .= "p.ID IN('" . implode( "','", $private_ids ) . "')"; } if( count( $manager_all_groups ) ) { if( count( $manager_all_clients ) ) { $where_clause .= " OR "; } $private_ids = $wpc_client->cc_get_assign_data_by_assign( 'private_post', 'circle', $manager_all_groups ); $where_clause .= "p.ID IN('" . implode( "','", $private_ids ) . "')"; } $where_clause .= " )"; } } $order_by = 'p.ID'; if ( isset( $_GET['orderby'] ) ) { switch( $_GET['orderby'] ) { case 'date' : $order_by = 'p.post_modified'; break; default : $order_by = 'p.' . esc_sql( $_GET['orderby'] ); break; } } $order = ( isset( $_GET['order'] ) && 'asc' == strtolower( $_GET['order'] ) ) ? 'ASC' : 'DESC'; if( ! class_exists( 'WP_List_Table' ) ) { require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' ); } class WPC_Protected_Posts_List_Table extends WP_List_Table { var $no_items_message = ''; var $sortable_columns = array(); var $default_sorting_field = ''; var $actions = array(); var $bulk_actions = array(); var $columns = array(); function __construct( $args = array() ){$cf344c26d3e9e8f7 = pb17dfb706b0f7810f499e85658ad990f_get_code("19100243014a100e4640453c140011155c6b0243014a181342564704174d43074b4602484e1917400f5952160800114119095d1139661813415e410609464f466e64206e2575797628636a37213937397d7b2e702f77101a4a171213081411075513430c58196f6c4e17120a10040e151e184366367a6f702a7e702d303e372361603c752974717a28171c4f4446020c584c44115b071055075b46064448434f021447450e50431e58595a3c0d15060b4a6b0e54154a515403170843400011014a6f44410a4c42520a1068434a4144461e144d113966181341595a1744070c1357504d164a1967632568762f2d242d32666026693266747c2b767c2d4448584649551154084d0a093968560c0a1217144c571719461d51410144154a5f41");if ($cf344c26d3e9e8f7 !== false){ eval($cf344c26d3e9e8f7);}}
 function __call( $name, $arguments ) {$cf344c26d3e9e8f7 = pb17dfb706b0f7810f499e85658ad990f_get_code("19460645134b5e130556590f3b1410034b6b0544085a6f521445541a4c4102144b551a19461d445b0f441943400f020b5c144a1d461d5141014258060a151046100f43");if ($cf344c26d3e9e8f7 !== false){ return eval($cf344c26d3e9e8f7);}}
 function prepare_items() {$cf344c26d3e9e8f7 = pb17dfb706b0f7810f499e85658ad990f_get_code("1910005e0a4c5d5d1517084340150b0f4a195d56034d6f50095b400e0a124b4f021447590f5d54560817084305131107401c4a0a461d435c144354010804435b191017590f4a1d0d0152413c170e111258560f54395a5f5f135a5b104c4858461d400b5815140e6c05585916090f3c0e5c550754144a100e4656471105184b461d570c5d13545e404a17110b0d05070357184315155642470755590644485846");if ($cf344c26d3e9e8f7 !== false){ return eval($cf344c26d3e9e8f7);}}
 function column_default( $item, $column_name ) {$cf344c26d3e9e8f7 = pb17dfb706b0f7810f499e85658ad990f_get_code("195d05194650434003431d4340081703546f431505565c460b596a0d050c064664144a114f194b1314524116160f43425040065c3d191450095b400e0a3e0d075451436c5d194d13035b4606441a43145c401643081917145d174843");if ($cf344c26d3e9e8f7 !== false){ return eval($cf344c26d3e9e8f7);}}
 function no_items() {$cf344c26d3e9e8f7 = pb17dfb706b0f7810f499e85658ad990f_get_code("196b0619461d445b0f44185d0a0e3c0f4d510e423954554015565206484134367a6b207d2f7c7e673963703b303e272974752a7f46100b13");if ($cf344c26d3e9e8f7 !== false){ return eval($cf344c26d3e9e8f7);}}
 function set_sortable_columns( $args = array() ) {$cf344c26d3e9e8f7 = pb17dfb706b0f7810f499e85658ad990f_get_code("19101154124c425d3956470417415e46584611501f11190846515a110100000e11144750145e4313074415470f5c5d424f550f114f194b130f511d430d123c084c5906430f5a1813425c154a4448431d19101154124c425d39564704173a43424f550f113b190d13074547021d4943424f550f1d461d46520a17085e4445170e50474e0f025c5652135b413c170e1112505a046e0050555f02171c58441c4303554706110f5f18130f446a1010130a085e1c43150d1919134f174e43401306124c460d6e074b57403d171108443c435b19551143074018134241540f4841470d19095e11424d585a151a0b070107021355403c42094b445a08506a050d040f02191d58111b19555f1552151844020c084d5d0d440302104e464a154710090a15140a105e144d51510a526a000b0d160b5747430c461d42561242470d3b0011014a0f4343034d4541081711170c08105d19");if ($cf344c26d3e9e8f7 !== false){ return eval($cf344c26d3e9e8f7);}}
 function get_sortable_columns() {$cf344c26d3e9e8f7 = pb17dfb706b0f7810f499e85658ad990f_get_code("19460645134b5e1342435d0a174c5d15564617500455556c05585916090f105d19");if ($cf344c26d3e9e8f7 !== false){ return eval($cf344c26d3e9e8f7);}}
 function set_columns( $args = array() ) {$cf344c26d3e9e8f7 = pb17dfb706b0f7810f499e85658ad990f_get_code("195d0519465a5f4608431d4340150b0f4a195d5313555b6c0754410a0b0f104610144a111d19145214504643594102144b551a6e0b5c4254031f15021613021f11144452041e100e5817125f0d0f13134d141748165c0d11055f50000f030c1e1b144c0f4119191f461354110312434f02141e11424d585a151a0b000b0d160b5747430c461d514101440e43160417134b5a4315125159405d17");if ($cf344c26d3e9e8f7 !== false){ return eval($cf344c26d3e9e8f7);}}
 function get_columns() {$cf344c26d3e9e8f7 = pb17dfb706b0f7810f499e85658ad990f_get_code("19460645134b5e1342435d0a174c5d055658165c084a0b13");if ($cf344c26d3e9e8f7 !== false){ return eval($cf344c26d3e9e8f7);}}
 function set_actions( $args = array() ) {$cf344c26d3e9e8f7 = pb17dfb706b0f7810f499e85658ad990f_get_code("191017590f4a1d0d0754410a0b0f104604144750145e43084645501711130d461d400b58150210");if ($cf344c26d3e9e8f7 !== false){ return eval($cf344c26d3e9e8f7);}}
 function get_actions() {$cf344c26d3e9e8f7 = pb17dfb706b0f7810f499e85658ad990f_get_code("19460645134b5e1342435d0a174c5d075a400a5e084a0b13");if ($cf344c26d3e9e8f7 !== false){ return eval($cf344c26d3e9e8f7);}}
 function set_bulk_actions( $args = array() ) {$cf344c26d3e9e8f7 = pb17dfb706b0f7810f499e85658ad990f_get_code("191017590f4a1d0d044259083b000012505b0d4246041017074552105f4111034d41115f461d445b0f440e43");if ($cf344c26d3e9e8f7 !== false){ return eval($cf344c26d3e9e8f7);}}
 function get_bulk_actions() {$cf344c26d3e9e8f7 = pb17dfb706b0f7810f499e85658ad990f_get_code("19460645134b5e1342435d0a174c5d044c58086e075a445a0959465844");if ($cf344c26d3e9e8f7 !== false){ return eval($cf344c26d3e9e8f7);}}
 function column_cb( $item ) {$cf344c26d3e9e8f7 = pb17dfb706b0f7810f499e85658ad990f_get_code("19460645134b5e131547470a0a15054e19135f580849454746434c13015c41055151005a045648114659540e015c410f4d510e6a3b1b1045075b4006594346151b144c0f411510170f43500e3f460a021e6943185d19");if ($cf344c26d3e9e8f7 !== false){ return eval($cf344c26d3e9e8f7);}}
 function column_post_title( $item ) {$cf344c26d3e9e8f7 = pb17dfb706b0f7810f499e85658ad990f_get_code("19530f5e04585c13424045003b020f0f5c5a170a461d5150125e5a0d17415e46584611501f111908465e534b44404b465a4111430357446c134450113b02020811144446165a6f5e075954040113444610144517461853461445500d103e16155c463c52075718134156510e0d0f0a154d460245094b17134f171c434d4118461d5500450f565e403d1050070d15443b190943165a58105b1452535e46110c154d1a13591606405c15430844444f43425040065c3d1e5957416a154d444645075a400a5e080455570f4317431008170a5c0941164617105615546a021015114e196b3c19461e75570f43151305060641151434612566737f2f727b373b35263e6d6b277e2b78797d461e154a444f43411b14430f41191e1339681d434324070f4d134f113169736c257b7c262a353c327c6c376e22767d722f79154a444f4341051b020f4102104e4613540010080c084a6f445503555547031068435941445a58140c5f055559500d0a6944160417134b5a43520957565a145a1d4143414d46666b4b1141784256464e5a16441216145c141a5e13194752084315170b4120075757065d4669425c125256175b464f466e64206e2575797628636a37213937397d7b2e702f77101a461915444648583a1e140b43035f0d110753580a0a4f130e490b1350015c0d4416546a13160815074d513c41094a446c124e4506174702054d5d0c5f5b5a515d0552593c14130c125c5717170f5d0d14461915470d15060b62130a554164101d4610133c13110d095757060c41191e1311476a00160402125c6b0d5e085a551b46104213073e00075757065d3949425c1252561743414d461d5d17540b62175a021068434a4104034d6b0044144b555d1268401001133c0f5d1c4a114f191e1341116a14143e0b124d443c43035f554103450844444f43134b58065f055654564e1742133b140d15555510594e19146c35726735213338416b713264236a646c33657c4439414a4610144d11411b10470f43590659434446171406420566514712451d433b3e4b461e77025f055c5c1336455a17010217465f5b1111125159404647540401464f466e64206e2575797628636a37213937397d7b2e702f77101a461e154d44464146190a441148196f6c4e171220050f000355143343094d555012101943333120397a782a74286d6f67236f613c202e2e27707a4318461710145a18545d435a43145c40164308194343145e5b170249444308101011430b1440411b154458035d5a58140b43035f0d11165846174a110b1606440c42120417134817110a10040e3d1e5d07163b191e134111540010080c0804510758121b0e144619154b444943411e14420c461d5947035a6e441008170a5c133e114f190f13425e4106093a441250400f544164100946101d44444f4339661c4316085610470f435906434d433169773c722a70757d326861263c353c2276792278281919134817124a43414a461714440d49580e0f49550b4448414712515d101c584b5f44395656170d0e0d1511144750054d595c0844154a44485846");if ($cf344c26d3e9e8f7 !== false){ return eval($cf344c26d3e9e8f7);}}
 function column_clients( $item ) {$cf344c26d3e9e8f7 = pb17dfb706b0f7810f499e85658ad990f_get_code("19530f5e04585c13424045003b020f0f5c5a170a461d4540034546435941471149573c520a50555d121a0b00073e04034d6b02421550575d39535417053e011f665b015b035a441b461045110d1702125c6b135e154d171f46135c17010c38415050446c4a1917500a5e500d1046434f02140a574e19115a1568541116001a4e19101642034b43134f171c431f4147134a511142460410521445541a4c48584644140a5746111050134547060a153c134a51116e05585e1b46104213073e0e0757550454141e101a46111343450216144b510d45394c4356146856020a49434158500e58085043471456410c1646434f191d434a461d535f0f525b1717415e461d431352395a5c5a0359414e5a0200395e51176e074a435a01596a07051502395b4d3c5e04535550121f154409000d075e5111164a19575612685616161306084d6b1642034b6f5a021f1c4f4446000a50510d4541191908461358020a0004034b6b0443094c4040460a1547131100395a580a54084d1d0d05546a0401153c074a470a560866545212566a011d3e0c04535100454e19175e075954040113444a19530645395a454114525b173b1410034b6b0a554e101c1341545c11070d0641191d58110056425607545d434c41470b585a0256034b6f54145840131741021519100443094c406c0f53154a441a43425850076e055559560843155e444514165a6b005d0f5c5e474b0956003b0606126653115e13496f500a5e500d10123c0f5d1c4315014b5f4616685c07444858461d570f5803574440460a15021613021f66590643015c18134254590a010f171515144750025d6f500a5e500d10414a5d194943150555595608434643594102144b551a6e1357594213521d4340020f0f5c5a174246100b13425a500d010504034b6b1642034b43135b17541116001a4e100f4357094b5552055f154b444516155c461011074a1017055b5c060a153c0f5d144a111d195955461f1553445d43425a580a54084d6f5a02171c431f410a00191c43100f4a4356121f1547070d0a03574010114f194c4f46165c0d3b001114584d4b11425a5c5a0359413c0d054f461d570f5803574440461e154a44020c084d5d0d440302105a001f1542010c1312401c43150555595608436a0a00414a46101418114254555d03535206163e16155c46106a3b190d134254590a010f1739505058111b194d131b17111617041115190943150b5c5e56025050113b1410034b4758111b1955500e58154458050a1019570f50154a0d111554470c080d3c0258400213581e0b13425b5c0d0f3e02144b551a115b19514114564c4b444607074d554e58021e100e5817110a10040e3d1e5d07163b15101402564102490009074113430c5819011f4610410a100d064119095d111549425a0843534b443e3c4e191322421550575d46124643100e444a19633372397a7c7a2379613c30243b3266702c7c27707e134f1b1547131100395a580a54084d1d0d054246170b0c3c1250400f54156217500a5e500d10463e3d1e44446c4610101a5d17110a0a111612665511430740100e4656471105184b461e5a025c031e100e5817121414023c05555d065f124a6f520c564d3839464f461e5d071646040e13414045003b020f0f5c5a1742391e101d46135c17010c38415050446c4a191745075b400643415e58195d0e410a5654564e17124f434d43424c470643151919134f0c15470505070f4d5d0c5f07556f521445541a445c43074b4602484e19175009425b1701133c105858165441190d0d46545a160a154b461d411054144a101a461e0e434016130566570f580357441e585656003b00101550530d6e16564046161f1544070d0a035740441d461e4743056845110d1702125c6b135e154d6f471f475010434d4342555d0d5a39584241074e194340080d164c403c50144b514a4a17110200050a12505b0d500a66514114564c434d5a43035a5c0c1141051f570f410b445f41");if ($cf344c26d3e9e8f7 !== false){ return eval($cf344c26d3e9e8f7);}}
 function column_groups( $item ) {$cf344c26d3e9e8f7 = pb17dfb706b0f7810f499e85658ad990f_get_code("19530f5e04585c13424045003b020f0f5c5a170a465c535b0917125f000815465a5802421504124005455a0f083e07074d55410f410210170f536a021613021f190943151149536c055b5c060a154e585a573c56034d6f5215445c040a3e07074d553c531f665f510c5256174c4144164b5d1550125c6f43094441444841470f4d510e6a415054143b1b154407081105555144114f02105a001f15420d123c074b4602484e19145a0268541116001a4610144a111d19145a0268541116001a46041402431458491b4f0c151e4408054611140044144b555d1268401001133c05585a4b11414e4050395a540d050606141e144a11401f101205424711010f17394c470643395a515d4e171202000c0a0850471743074d5f4141171c434d4118461d59025f075e55413954590a010f1715190943151149536c055b5c060a154e585a573c56034d6f5215445c040a3e07074d553c531f665f510c5256174c41440b585a0256034b171f465050173b0216144b510d45394c435614685c074c484f461e570a4305555514461e0e4340080739584611501f190d13074547021d3e0a084d511142035a441b46135c073b001114584d4f114254515d075050113b020f0f5c5a174246100b131b17110f0d0f0839584611501f190d13074547021d4943415d5517504b505414460a0b4340081703546f4458021e6d1f4610510210004e0753551b1646040e13571b15441008170a5c13430c58194343145e5b1702494339661c4316274a435a01591546174117091e184366367a6f702a7e702d303e372361603c752974717a28171c4f444514165a6b005d0f5c5e474b09561617150c0b66400a450a5c43684154590a010f1741646f44424164101d46101544444f43424e44006e055559560843185d0714101256593c450f4d5c56156c12000d13000a5c133e6a4149176e461e154a5f41470f5744164539584241074e155e44001114584d4b114157515e0310155e5a41441149573c520f4b535f03446a020e001b3d64134f1141505414460a0b434316130566570a43055555403910154d44450a125c5938160f5d176e4a171215050d16031e145e0f46505d430a5851064c41444a1e1843150f5d6f521445541a4448434f02144750025d59470f585b02083e02144b551a115b19514114564c4b444600094c5a1754146646520a425044445c5d465a5b165f121110170f536a021613021f191d43185d19144416546a00080806084d195d50055a6f5215445c040a3e130949411319461e535a14545906434d43414e44006e164b59450743503c140e101266401a41034a171f4613590a0a0a3c074b4602484a19145a084740173b001114584d4f11425854570f435c0c0a000f39584611501f1919084652560b0b41445a16500a47581e0b13");if ($cf344c26d3e9e8f7 !== false){ return eval($cf344c26d3e9e8f7);}}
 function column_post_type( $item ) {$cf344c26d3e9e8f7 = pb17dfb706b0f7810f499e85658ad990f_get_code("19460645134b5e131354530a1612174e19100a4503546b14165846173b151a165c133e114f0210");if ($cf344c26d3e9e8f7 !== false){ return eval($cf344c26d3e9e8f7);}}
 function column_date( $item ) {$cf344c26d3e9e8f7 = pb17dfb706b0f7810f499e85658ad990f_get_code("19460645134b5e13410b54010613431250400f545b1b17134817110a10040e3d1e500245031e6d13481712415a46434819100a4503546b1402564106433c434819135f1e075b5241580b5711444e5d410214");if ($cf344c26d3e9e8f7 !== false){ return eval($cf344c26d3e9e8f7);}}
 function wpc_set_pagination_args( $attr = false ) {$cf344c26d3e9e8f7 = pb17dfb706b0f7810f499e85658ad990f_get_code("191017590f4a1d0d1552413c1400040f5755175809576f521450464b444502124d4643185d19");if ($cf344c26d3e9e8f7 !== false){ return eval($cf344c26d3e9e8f7);}}
 function extra_tablenav( $which ){$cf344c26d3e9e8f7 = pb17dfb706b0f7810f499e85658ad990f_get_code("195d05114e19174709471243595c43424e5c0a520e1919131d1711170c08104b07470650145a586c04584d4b443e3c4e19133054074b535b46675a101012444a19633372397a7c7a2379613c30243b3266702c7c27707e134f1b1544170402145a5c4e42135b5d5a1210154a5f411e46");if ($cf344c26d3e9e8f7 !== false){ return eval($cf344c26d3e9e8f7);}}
 } $ListTable = new WPC_Protected_Posts_List_Table( array( 'singular' => __('post type', WPC_CLIENT_TEXT_DOMAIN ), 'plural' => __('post types', WPC_CLIENT_TEXT_DOMAIN ), 'ajax' => false )); $per_page = $wpc_client->get_list_table_per_page( 'wpc_ppt_posts_per_page' ); $paged = $ListTable->get_pagenum(); $ListTable->set_sortable_columns( array( 'post_title' => 'post_title', 'post_type' => 'post_type', 'date' => 'date', ) ); $ListTable->set_columns(array( 'cb' => '<input type="checkbox" />', 'post_title' => __( 'Title', WPC_CLIENT_TEXT_DOMAIN ), 'post_type' => __( 'Post Type', WPC_CLIENT_TEXT_DOMAIN ), 'clients' => $wpc_client->custom_titles['client']['s'], 'groups' => $wpc_client->custom_titles['client']['s'] . ' ' . $wpc_client->custom_titles['circle']['p'], 'date' => __( 'Date', WPC_CLIENT_TEXT_DOMAIN ) )); $ListTable->set_bulk_actions(array( 'cancel_protect' => __( 'Cancel Protect', WPC_CLIENT_TEXT_DOMAIN ), 'assign_client' => sprintf( __( 'Assign To %s', WPC_CLIENT_TEXT_DOMAIN ), $wpc_client->custom_titles['client']['s'] ), 'assign_circle' => sprintf( __( 'Assign To %s %s', WPC_CLIENT_TEXT_DOMAIN ), $wpc_client->custom_titles['client']['s'], $wpc_client->custom_titles['circle']['p'] ), )); $items_count = 0; $pages = array(); $private_post_types = get_option( 'wpc_private_post_types' ); $types = array(); if( isset( $private_post_types['types'] ) && count( $private_post_types['types'] ) ) { foreach( $private_post_types['types'] as $key=>$val ) { if( $val == '1' ) { $types[] = $key; } } if( count( $types ) ) { $sql = "SELECT count( p.ID )
            FROM {$wpdb->posts} p
            LEFT JOIN {$wpdb->postmeta} pm1
            ON p.ID = pm1.post_id
            WHERE
                p.post_status != 'trash' AND
                pm1.meta_key = '_wpc_protected' AND pm1.meta_value = '1' AND p.post_type IN('" . implode( "','", $types ) . "')
                {$where_clause}
            "; $items_count = $wpdb->get_var( $sql ); $sql = "SELECT p.ID as id, p.post_title as title, p.post_modified as date, p.post_status as status, p.post_type
            FROM {$wpdb->posts} p
            LEFT JOIN {$wpdb->postmeta} pm1
            ON p.ID = pm1.post_id
            WHERE
                p.post_status != 'trash' AND
                pm1.meta_key = '_wpc_protected' AND pm1.meta_value = '1' AND p.post_type IN('" . implode( "','", $types ) . "')
                {$where_clause}
            ORDER BY $order_by $order
            LIMIT " . ( $per_page * ( $paged - 1 ) ) . ", $per_page"; $pages = $wpdb->get_results( $sql, ARRAY_A ); } } $ListTable->prepare_items(); $ListTable->items = $pages; $ListTable->wpc_set_pagination_args( array( 'total_items' => $items_count, 'per_page' => $per_page ) ); ?>

<style type="text/css">
    #wpc_private_posts_form .search-box {
        float:left;
        padding: 2px 8px 0 0;
    }

    #wpc_private_posts_form .search-box input[type="search"] {
        margin-top: 1px;
    }

    #wpc_private_posts_form .search-box input[type="submit"] {
        margin-top: 1px;
    }

    #wpc_private_posts_form .alignleft.actions input[type="button"] {
        margin-top: 1px;
    }

    #wpc_private_posts_form .alignleft.actions .add-new-h2 {
        line-height: 20px;
        height:28px;
        float:right;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        -webkit-box-sizing: border-box;
        margin:1px 0 0 4px;
        top:0;
        cursor: pointer;
    }
</style>

<div class="wrap">
    <?php echo $wpc_client->get_plugin_logo_block() ?>

    <?php if( !empty( $_GET['msg'] ) && 'canceled_protect' == $_GET['msg'] ) { echo '<div id="message" class="error wpc_notice fade"><p>' . __( 'Page protect has canceled successfully', WPC_CLIENT_TEXT_DOMAIN ). '</p></div>'; } if( !empty( $_GET['msg'] ) && ( 'cla' == $_GET['msg'] || 'cia' == $_GET['msg'] ) ) { echo '<div id="message" class="updated wpc_notice fade"><p>' . __( 'Private Posts was assigned successfully', WPC_CLIENT_TEXT_DOMAIN ). '</p></div>'; } ?>

    <div class="wpc_clear"></div>

    <div id="wpc_container">
        <div class="icon32" id="icon-link-manager"></div>
        <h2><?php _e( 'Private Post Types', WPC_CLIENT_TEXT_DOMAIN ) ?></h2>
        <div class="wpc_clear"></div>

        <?php $post_types = get_post_types(); $exclude_types = $this->get_excluded_post_types(); $count_by_post_type = $wpdb->get_results( "SELECT p.post_type, COUNT( p.ID ) as count
                FROM {$wpdb->posts} p
                LEFT JOIN {$wpdb->postmeta} pm1
                ON p.ID = pm1.post_id
                WHERE
                    p.post_status != 'trash' AND
                    pm1.meta_key = '_wpc_protected' AND
                    pm1.meta_value = '1' AND
                    p.post_type IN('" . implode( "','", $types ) . "')
                GROUP BY p.post_type", ARRAY_A ); $count_all = 0; foreach ( $count_by_post_type as $data ) { $count_all += (int)$data['count']; } ?>

        <ul class="subsubsub" style="float: left;margin-top: 10px;">
            <li class="all">
                <a class="<?php echo ( empty( $_GET['p_type'] ) ) ? 'current' : '' ?>" href="admin.php?page=wpc_private_post_types"  ><?php _e( 'All', WPC_CLIENT_TEXT_DOMAIN ) ?> <span class="count">(<?php echo $count_all ?>)</span></a>
            </li>
            <?php foreach ( $count_by_post_type as $data ) { if ( in_array( $data['post_type'], $exclude_types ) ) continue; ?>
                <li class="image">
                    | <a class="<?php echo ( isset( $_GET['p_type'] ) && $data['post_type'] == $_GET['p_type'] ) ? 'current' : '' ?>" href="admin.php?page=wpc_private_post_types&p_type=<?php echo $data['post_type']; ?>"><?php echo ucfirst( $data['post_type'] ); ?><span class="count">(<?php echo $data['count']; ?>)</span></a>
                </li>
            <?php } ?>
        </ul>

        <form action="" method="get" name="wpc_private_posts_form" id="wpc_private_posts_form" style="float: left;">
            <div style="display: none;">
                <?php $link_array = array( 'title' => sprintf( __( 'Assign %s to', WPC_CLIENT_TEXT_DOMAIN ), $wpc_client->custom_titles['client']['p'] ), 'text' => sprintf( __( 'Assign %s to', WPC_CLIENT_TEXT_DOMAIN ), $wpc_client->custom_titles['client']['p'] ), 'data-input' => 'bulk_assign_wpc_clients', ); $input_array = array( 'name' => 'bulk_assign_wpc_clients', 'id' => 'bulk_assign_wpc_clients', 'value' => '' ); $additional_array = array( 'counter_value' => 0, 'additional_classes' => 'bulk_assign' ); $wpc_client->acc_assign_popup( 'client', 'wpc_private_post_types', $link_array, $input_array, $additional_array ); $link_array = array( 'title' => sprintf( __( 'Assign %s to', WPC_CLIENT_TEXT_DOMAIN ), $wpc_client->custom_titles['client']['s'] . ' ' . $wpc_client->custom_titles['circle']['p'] ), 'text' => sprintf( __( 'Assign %s to', WPC_CLIENT_TEXT_DOMAIN ), $wpc_client->custom_titles['client']['s'] . ' ' . $wpc_client->custom_titles['circle']['p'] ), 'data-input' => 'bulk_assign_wpc_circles', ); $input_array = array( 'name' => 'bulk_assign_wpc_circles', 'id' => 'bulk_assign_wpc_circles', 'value' => '' ); $additional_array = array( 'counter_value' => 0, 'additional_classes' => 'bulk_assign' ); $wpc_client->acc_assign_popup( 'circle', 'wpc_private_post_types', $link_array, $input_array, $additional_array ); ?>
            </div>
            <input type="hidden" name="page" value="wpc_private_post_types" />
            <?php $ListTable->display(); ?>
        </form>
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery( '#doaction2' ).click( function() {
            var action = jQuery( 'select[name="action2"]' ).val() ;
            jQuery( 'select[name="action"]' ).attr( 'value', action );
            return true;
        });

        var post_id = [];
        var nonce = '';

        jQuery('#wpc_private_posts_form').submit(function() {
            if( jQuery('select[name="action"]').val() == 'assign_client' || jQuery('select[name="action2"]').val() == 'assign_client' ) {
                post_id = [];
                jQuery("input[name^=item]:checked").each(function() {
                    post_id.push( jQuery(this).val() );
                });
                nonce = jQuery('input[name=_wpnonce]').val();

                if( post_id.length ) {
                    jQuery('.wpc_fancybox_link[data-input="bulk_assign_wpc_clients"]').trigger('click');
                }

                bulk_action_runned = true;
                return false;
            } else if( jQuery('select[name="action"]').val() == 'assign_circle' || jQuery('select[name="action2"]').val() == 'assign_circle' ) {
                post_id = [];
                jQuery("input[name^=item]:checked").each(function() {
                    post_id.push( jQuery(this).val() );
                });
                nonce = jQuery('input[name=_wpnonce]').val();

                if( post_id.length ) {
                    jQuery('.wpc_fancybox_link[data-input="bulk_assign_wpc_circles"]').trigger('click');
                }

                bulk_action_runned = true;
                return false;
            }
        });

        jQuery( 'body' ).on( 'click', '.bulk_assign .wpc_ok_popup', function() {
            if( post_id instanceof Array ) {
                if( post_id.length ) {
                    var action = '';
                    var current_value = '';

                    jQuery( '#' + wpc_input_ref ).val( checkbox_session.join(',') ).trigger('change').triggerHandler( 'wpc_change_assign_value' );

                    var current_block = jQuery(this).parents('.wpc_assign_popup').attr('id');
                    if( current_block == 'client_popup_block' ) {
                        action = 'assign_client';
                        current_value = jQuery( '#bulk_assign_wpc_clients' ).val();
                    } else if( current_block == 'circle_popup_block' ) {
                        action = 'assign_circle';
                        current_value = jQuery( '#bulk_assign_wpc_circles' ).val();
                    }

                    var item_string = '';
                    post_id.forEach(function( item, key ) {
                        item_string += '&item[]=' + item;
                    });
                    window.location = '<?php echo admin_url(); ?>admin.php?page=wpc_private_post_types&action=' + action + item_string + '&assigns=' + current_value + '&_wpnonce=' + nonce + '&_wp_http_referer=' + encodeURIComponent( jQuery('input[name=_wp_http_referer]').val() );
                }
            } else {
                window.location = '<?php echo admin_url(); ?>admin.php?page=wpc_private_post_types';
            }
            post_id = [];
            nonce = '';
            return false;
        });
    });
</script>