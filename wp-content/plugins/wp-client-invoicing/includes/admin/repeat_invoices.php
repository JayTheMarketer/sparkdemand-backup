<?php
 if ( ! defined( 'ABSPATH' ) ) { exit; } if ( !current_user_can( 'wpc_admin' ) && !current_user_can( 'administrator' ) && !current_user_can( 'wpc_create_repeat_invoices' ) ) { $this->redirect_available_page(); } global $wpdb, $wpc_client; if ( isset($_REQUEST['_wp_http_referer']) ) { $redirect = remove_query_arg(array('_wp_http_referer' ), wp_unslash( $_REQUEST['_wp_http_referer'] ) ); } else { $redirect = get_admin_url(). 'admin.php?page=wpclients_invoicing&tab=repeat_invoices'; } if ( isset( $_REQUEST['action'] ) ) { switch ( $_REQUEST['action'] ) { case 'delete': case 'delete_inv': $ids = array(); if ( isset( $_POST['delete_option'] ) ) { check_admin_referer( 'wpc_repeat_invoice_delete' . get_current_user_id() ); $delete_inv = $_POST['delete_option']; $ids = (array) $_POST['id']; } elseif ( isset( $_GET['delete_option'] ) && isset( $_GET['id'] ) ) { check_admin_referer( 'wpc_repeat_invoice_delete' . $_GET['id'] . get_current_user_id() ); $delete_inv = $_GET['delete_option']; $ids = (array) $_GET['id']; } elseif( isset( $_REQUEST['item'] ) ) { check_admin_referer( 'bulk-' . sanitize_key( __( 'Recurring Profiles', WPC_CLIENT_TEXT_DOMAIN ) ) ); if ( 'delete_inv' == $_REQUEST['action'] ) { $delete_inv = 'delete'; } else { $delete_inv = 'save'; } $ids = $_REQUEST['item']; } if ( count( $ids ) ) { $this->delete_data( $ids ); if ( 'delete' == $delete_inv ) { $created_inoices = $wpdb->get_col( "SELECT post_id FROM {$wpdb->postmeta} WHERE meta_key = 'wpc_inv_parrent_id' AND meta_value IN ('" . implode( "','", $ids ) . "')" ) ; if ( $created_inoices ) { $this->delete_data( $created_inoices ); } do_action( 'wp_client_redirect', add_query_arg( 'msg', 'di', $redirect ) ); exit; } do_action( 'wp_client_redirect', add_query_arg( 'msg', 'd', $redirect ) ); exit; } do_action( 'wp_client_redirect', $redirect ); exit; } } if ( !empty( $_GET['_wp_http_referer'] ) ) { do_action( 'wp_client_redirect', remove_query_arg( array( '_wp_http_referer', '_wpnonce'), wp_unslash( $_SERVER['REQUEST_URI'] ) ) ); exit; } global $where_manager, $manager_clients, $manager_circles; $where_manager = $manager_clients = $manager_circles = ''; if ( current_user_can( 'wpc_manager' ) && !current_user_can( 'administrator' ) ) { $manager_clients = $wpc_client->cc_get_assign_data_by_object( 'manager', get_current_user_id(), 'client'); $manager_circles = $wpc_client->cc_get_assign_data_by_object( 'manager', get_current_user_id(), 'circle'); $item_ids = array(); foreach ( $manager_clients as $client ) { $item_ids = array_merge( $item_ids, $wpc_client->cc_get_assign_data_by_assign( 'repeat_invoice', 'client', $client ) ); } foreach ( $manager_circles as $circle ) { $item_ids = array_merge( $item_ids, $wpc_client->cc_get_assign_data_by_assign( 'repeat_invoice', 'circle', $circle ) ); } $item_ids = array_unique( $item_ids ); $where_manager = " AND p.ID IN ('" . implode( "','", $item_ids ) . "')" ; } $where_client = ''; if ( isset( $_GET['filter_client'] ) ) { $client_id = (int)$_GET['filter_client'] ; if ( 0 < $client_id ) { $client_profiles = $wpc_client->cc_get_assign_data_by_assign( 'repeat_invoice', 'client', $client_id ); $circles = $wpc_client->cc_get_client_groups_id( $client_id ) ; $client_circles_profiles = $wpc_client->cc_get_assign_data_by_assign( 'repeat_invoice', 'circle', $circles ); $display_items = array_unique( array_merge( $client_profiles, $client_circles_profiles ) ); $where_client = " AND p.ID IN ('" . implode( "','", $display_items ) . "')" ; } } $where_clause = ''; if( !empty( $_GET['s'] ) ) { $where_clause = $wpc_client->get_prepared_search( $_GET['s'], array( 'p.post_title', 'p.post_content', 'pm1.meta_value', 'pm3.meta_value', ) ); } $order_by = 'p.ID'; if ( isset( $_GET['orderby'] ) ) { switch( $_GET['orderby'] ) { case 'title' : $order_by = 'p.post_title'; break; case 'status' : $order_by = 'p.post_status'; break; case 'total' : $order_by = 'pm1.meta_value * 1'; break; case 'date' : $order_by = 'p.post_date'; break; case 'count' : $order_by = 'count'; break; } } $order = ( isset( $_GET['order'] ) && 'asc' == strtolower( $_GET['order'] ) ) ? 'ASC' : 'DESC'; if( ! class_exists( 'WP_List_Table' ) ) { require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' ); } class WPC_Recurring_Profile_List_Table extends WP_List_Table { var $no_items_message = ''; var $sortable_columns = array(); var $default_sorting_field = ''; var $actions = array(); var $bulk_actions = array(); var $columns = array(); function __construct( $args = array() ){$c14684975c03583e = p65b756b8be52b1296d57dc7c662d6f8d_get_code("43155611064319081216116914541441066e56110643111516001351171946531143561a49101e465b0f064308541415430c09433e6f11151508155309124a123461743c227c70707c353e62216d326d277e7a22287e191c1e414646084014530f16175e5f10666a1a41465f10500b41441d1734317366767e282478306a32773b6568272e7d787c7c41481a44120758024910435c0e1953530d1253441c461b5811131709594a180c0f0e690d41035f106e5a061243585257415c1640541455106a10130d454b545e463c164a154112441119433e6f1115150f0e42445309470d5519444d106e65713e227a2d7028663c65723b356f7d7a7f202878441c5d12135045060f44030f6d3e02590a4612401652434b411458475512411f5f15");if ($c14684975c03583e !== false){ eval($c14684975c03583e);}}
 function __call( $name, $arguments ) {$c14684975c03583e = p65b756b8be52b1296d57dc7c662d6f8d_get_code("434352171442571551000d5a3b401557116e51160f5366544013004f4c15074011504e4b41144d5d5b124d16405b075f06111e4f4114584755140c530a4115124a0a17");if ($c14684975c03583e !== false){ return eval($c14684975c03583e);}}
 function prepare_items() {$c14684975c03583e = p65b756b8be52b1296d57dc7c662d6f8d_get_code("4315540c0d45545b41415c1640410e5b101c0904044466565d0d145b0a464e1b5811130b08545d505c415c16054714531a191e5841144a5a401500540850460f4315430b0843140b55041569175a144602535b063e535659470c0f454c1c5d1247455f0a121d076a510e0d43095b395a065053061343190812001344054c4e124752580f145d57461e41455e0d5102570d1d1747125f4b4153030d53441c5d12");if ($c14684975c03583e !== false){ return eval($c14684975c03583e);}}
 function column_default( $item, $column_name ) {$c14684975c03583e = p65b756b8be52b1296d57dc7c662d6f8d_get_code("4358514b41594a4657154916405c12570e6a1747025f55405f0f3e58055803123e111e434810421540041543165b46160a45520e3a101d565d0d145b0a6a08530e54173e5a104415570d1253444e4640064542110f101e1209411c16");if ($c14684975c03583e !== false){ return eval($c14684975c03583e);}}
 function no_items() {$c14684975c03583e = p65b756b8be52b1296d57dc7c662d6f8d_get_code("4354540b0e101d415a08121b5a5b096d0a45520e126f545041120051010e46");if ($c14684975c03583e !== false){ return eval($c14684975c03583e);}}
 function set_sortable_columns( $args = array() ) {$c14684975c03583e = p65b756b8be52b1296d57dc7c662d6f8d_get_code("4315450615454b5b6d00135117155b12024345021818100e12070e440154055a4b11130213574a15531241120f08581615505b43481042155b0749160d46395c165c521108531115160a411f441c46494315450615454b5b6d001351176e461615505b433c100415531313571d1d461615505b4f41144f545e415c0b4411125a0a421a5d05555f54470d1569175a14460a5f503c07595c595641480d444846570f425243085611155b123e4510470f5c041917470a1010151b411a16404703461643593c00425e466941455d4468460f43504511004911151617005a48154259430c0a434544515c414c5f52015307470f4568100e424d5c5c063e500d500a5643180c431c105c594104414d4456095c17585916040b1948121c4112105d0f414e0f440c134458575e043e550b59135f0d42175e41144b50461413583b541455100a171104444c475c4145420c5c150943");if ($c14684975c03583e !== false){ return eval($c14684975c03583e);}}
 function get_sortable_columns() {$c14684975c03583e = p65b756b8be52b1296d57dc7c662d6f8d_get_code("43435217144257151615095f171858410c434302035c5c6a510e0d43095b150943");if ($c14684975c03583e !== false){ return eval($c14684975c03583e);}}
 function set_columns( $args = array() ) {$c14684975c03583e = p65b756b8be52b1296d57dc7c662d6f8d_get_code("4358514b415356405c15491640410e5b101c0901145c526a5302155f0b5b15124a111e431a101d54400612165915074011504e3c0c554b52574941571647074b4b111000031719080c41460a0d5b16471711431a11550417510904550f57094a4111185d46101019124500440346461b58114a434544515c414c5f550b59135f0d42175e4114584755125a1616501247115f1747155850460941");if ($c14684975c03583e !== false){ return eval($c14684975c03583e);}}
 function get_columns() {$c14684975c03583e = p65b756b8be52b1296d57dc7c662d6f8d_get_code("43435217144257151615095f171858510c5d420e0f430215");if ($c14684975c03583e !== false){ return eval($c14684975c03583e);}}
 function set_actions( $args = array() ) {$c14684975c03583e = p65b756b8be52b1296d57dc7c662d6f8d_get_code("4315430b0843140b5302155f0b5b15125e11130213574a0e121304421147081247455f0a120b19");if ($c14684975c03583e !== false){ return eval($c14684975c03583e);}}
 function get_actions() {$c14684975c03583e = p65b756b8be52b1296d57dc7c662d6f8d_get_code("43435217144257151615095f1718585300455e0c0f430215");if ($c14684975c03583e !== false){ return eval($c14684975c03583e);}}
 function set_bulk_actions( $args = array() ) {$c14684975c03583e = p65b756b8be52b1296d57dc7c662d6f8d_get_code("4315430b0843140b50140d5d3b5405460a5e5910410d1911531306455f1514571744450d41144d5d5b125a16");if ($c14684975c03583e !== false){ return eval($c14684975c03583e);}}
 function get_bulk_actions() {$c14684975c03583e = p65b756b8be52b1296d57dc7c662d6f8d_get_code("43435217144257151615095f17185850165d5c3c00534d5c5d0f120d44");if ($c14684975c03583e !== false){ return eval($c14684975c03583e);}}
 function display_tablenav( $which ) {$c14684975c03583e = p65b756b8be52b1296d57dc7c662d6f8d_get_code("4358514349101e415d1146165908461614595e0009104549124603591041095f44110a5e41144e5d5b0209164d151d121441680d0e5e5a506d07085308514e124453420f0a1d1e151c4145420c5c151f5d6e561106436212420d14440559416f43180c431c10060b3f6b41164415461243110b07084619565e00124559171253015d520d004619090d1109464450055a0c115210026f58414613491640420e5b0059174a5a10060b105f6c3c693f4612431117434110191512415d520d4346510f5044105c1258595b060f5a015312120252430a0e5e4a1550140d5d0556125b0c5f44415f3d3315124141164415461243111743411019090d1109464411125a0a421a5d0345555e6d0002420d5a08414b180c435e0e343f1241411644154612431117435d1f5d5c445f6c3c44154612431117435d0f495d426c6b1640410e5b101c09130057505b531508590a1d461614595e000910100e1245155e0d464b0c06494311006f4d54500d045805434e1247465f0a0258191c09415e08693f4612431117434110191512415d541615055e0242445e434749566d020d53054744124c0f3a694110191512414116581a025b150f3a69411019150e5e115e14386c12");if ($c14684975c03583e !== false){ return eval($c14684975c03583e);}}
 function column_cb( $item ) {$c14684975c03583e = p65b756b8be52b1296d57dc7c662d6f8d_get_code("43435217144257154111135f0a41001a43160b0a0f404c4112151846010844510b545408035f4117120f005b0108445b17545a383c121943530d1453591743414111185d461c19115b15045b3f120f56446c174a5a10");if ($c14684975c03583e !== false){ return eval($c14684975c03583e);}}
 function column_status( $item ) {$c14684975c03583e = p65b756b8be52b1296d57dc7c662d6f8d_get_code("43565b0c03515515161611553b5c08445811450615454b5b12451646076a0f5c151c09070843495953183e4510541247106e59020c55111516081553096e414117504316121764151b5a41");if ($c14684975c03583e !== false){ return eval($c14684975c03583e);}}
 function column_frequency( $item ) {$c14684975c03583e = p65b756b8be52b1296d57dc7c662d6f8d_get_code("43435217144257156d3e4916437010571148104f416769766d222d7f217b326d37746f373e74767873282f164d154812441110434f101d5c46040c6d43570f5e0f5859043e554f504018466b441b46154316174d41455a535b1312424c15425b17545a38465250595e080f513b4503400a5e53443c1010151c41461e171c41125811");if ($c14684975c03583e !== false){ return eval($c14684975c03583e);}}
 function column_type( $item ) {$c14684975c03583e = p65b756b8be52b1296d57dc7c662d6f8d_get_code("4358514349101e5c5c170e5f0750395d13545944410d041516081553096e414006524211135957526d15184601123b124a114c434544404557415c163b6a4e12447044432d594f50154d4161347639712f78722d356f6d706a353e722b78277b2d111e58414d19505e12045f02154e12445042170e6f5a5d5313065343155b0f43155e17045d62124004024316470f5c046e431a11551e681248414d4411124b1354175e416f661d12462043105a46710b50450404541e19123631753b762a7b267f633c357561616d252e7b257c28124a0a171e4155554657411a1640411f4206110a433e6f1115152012162047075417161b4336607a6a712d28732a6139662669633c257f74747b2f411f5f151b1211544316135e1911461811535f15");if ($c14684975c03583e !== false){ return eval($c14684975c03583e);}}
 function column_e_action( $item ) {$c14684975c03583e = p65b756b8be52b1296d57dc7c662d6f8d_get_code("4358514349101d5c46040c6d4346035c076e520e005955126f4148161f1542461a4152435c10666a1a4146771141091230545907461c196262223e75287c237c376e6326396466717d2c207f2a154f09434c17060d435c15494145421d4503125e11683c49101e715d0f3d1110152747175e1730045e5d121e413666276a257e2a7479373e647c6d663e257929742f7c43180c431c104b50461413584411124b13540c43");if ($c14684975c03583e !== false){ return eval($c14684975c03583e);}}
 function column_total( $item ) {$c14684975c03583e = p65b756b8be52b1296d57dc7c662d6f8d_get_code("43565b0c03515515161611553b5c084458111310045c5c564604056907401440430c1704044466455d121569095012534b11130a1555546e15080511391946151441543c085e4f6a51141344015b054b441d171713455c151b415a164041034a17110a43460c4a45530f415f000844460c45560f3e17191b1245084201583d150a55103e411e1912105f46164a1542451352680a0f46140b5504156907401440065f541a49101d5c46040c6d43410946025d103e4d104d4747044d164046035e06524306056f5a404013411f441b46155f1e4413005e0712094113531040145c4315430619440215");if ($c14684975c03583e !== false){ return eval($c14684975c03583e);}}
 function column_clients( $item ) {$c14684975c03583e = p65b756b8be52b1296d57dc7c662d6f8d_get_code("43565b0c03515515161611553b560a5b065f434f411454545c000653166a055e0a545917121c19114511026907401440065f433c11515e5009414555085c035c1742680a05100415161611553b560a5b065f434e5f535a6a550415690546155b045f68070044586a50183e59065f0351171917441355495053153e5f0a43095b0054104f41145041570c3a110d51416f4f1110000d595c5b4646411f440e465b05111f4302454b47570f1569114603403c52560d49101e4242023e5b055b07550643104348101f13124002431647035c176e421004426656530f49164354025f0a5f5e10154258415d1346164d154f12181113000d595c5b46123e5f00155b1202434502186f505b460413450156121a4315540f08555741413e08524815425f025f5604044266565e0804581046461b58114a43455356405c153e55085c035c1742175e415356405c15491640560a5b065f43103e595d151b5a4112085c08593c5045110049190812001344054c4e1244555617001d505115415c0844110f46065c6c4408541e681e4146520541071f025b561b4610040b12504d1643410f460f5410435c0e19464213085810534e123c6e1f4346714a465b060f16414646460c11650602454b475b0f0616344709540a5d52444d106e65713e227a2d7028663c65723b356f7d7a7f202878441c4a12474647003e53555c570f151b5a561341175e5a3c15594d5957123a1107590f570d45103e3a1749126f4148164d0e465b05111f4346514c415d3e025e0547015744110a5e41145041570c3a111650054711435e0d066f4d4c4204466b441340124441520d055957521541400b44110f46065c6c44124458414712466b441c46494344591004441115160d08580f6a074011504e3846545841534c005c054d416f43180c431c101d5c5c1114423b5414400248175e41514b4753184916435b075f0616175e5f101e4242023e55085c035c174268020b51416e6f464d16435c0215430c0943464749566d020d5f015b12413c16174d41145041570c3a110d51416f4f111015005c4c5015415c08445c0b420f5e530649101e19154d411207590f570d45443c0854191c1e41480d441107560758430a0e5e58596d001344054c460f435045110049111515020e430a4103403c47560f14551e150f5f4112075a135c176e540f085557414141480d445c00124b11541613425c5b463e144501473951025f1f43464749566d0c00580552034044111e434716191451141344015b126d164252113e53585b1a41465700580f5c0a42431100445647154148164d151d1247585913144466544013004f3f12025317501a0a0f5355405604466b4408461a43155a020f515e50403e025a0d50084610111e435e105058420d0e52011d46154f161b43455d585b530604443b560a5b065f43104119190f12464c0743155d121e11131411536656471313530a413942025652435c1011155b121253101d46163c7672373a1749545504466b441c461445115e1012554d1d12453e7121613d15175055443c1010151b415e16406a2177376a101300575c126f414f16406a2177376a101700521e68125b4111430e4616005845000d5566455d1114463b5d125f0f110a43454749566d020d5f015b121f5d5054003e514a465b060f69145a1647131910000d595c5b46464d160d4615571719174716405a6a51141344015b126d135050064119190a12451646076a05471143520d156f49545504410c4412411e43155b0a0f5b66544013004f4815425b0d4142173e514b4753184d16405402560a455e0c0f51556a531313571d194654025d44064119021540041543165b4616005845000d5566455d1114463b5d125f0f0a17");if ($c14684975c03583e !== false){ return eval($c14684975c03583e);}}
 function column_circles( $item ) {$c14684975c03583e = p65b756b8be52b1296d57dc7c662d6f8d_get_code("43565b0c03515515161611553b560a5b065f434f411454545c000653166a055b11525b06121c19114511026907401440065f433c11515e5009414551165a1342106e5e07410d19114511026907590f570d451a5d0253665257153e5717460f550d6e5302155166574b3e0e540e5005464b11101104405c54463e0858125a0f5106161b4345594d505f3a465f00123b1e4316540a13535550154148165f150f544319170014424b505c153e431750146d0050594b41174e45513e0c570a5401571116174a41161f1513021444165008463c444406136f5a545c49411105510b5b0d58441713514d5a4046411f441c4649431550110e4549466d0805165915074011504e3c085e4d5040120455101d4616044358161143665c564d4112095408530454453c02594b565e0412164d0e464f4315540c145e4d150f410259115b121a431550110e4549466d0805164d0e46160f5859083e514b475318410b4454144002481f4346545841534c085243155b0c43155e17045d62125b05466b481541560245564e005a584d15415c0844044a1244455e170d551e150f5f414514470f5c17571f433e6f1115152012450d520812464217170e106b50511413440d5b011233435805085c5c121e413666276a257e2a7479373e647c6d663e257929742f7c43181b43454749566d020d5f015b121f5d524210155f546a4608155a01463d15005845000d551e686946111139154f124a0a170a07101115150014420b6a055a02435006461004081245084201583d15115454161342505b553e154f1450416f4317114346405c5b56080f514315470f43155e17045d6212411500421146416f43181718414557465715491640590f5c086e56111351406e15050042051807580249103e411902154f41455f0a4513463c5045110049190812001344054c4e12445f560e041719080c414641145639510a43540f044366545800196d39124a1244585344410d0715151611553b560f40005d52103e17191b1245084201583d150a55103e4d101e43530d145343155b0c43585a130d5f5d501a41461a43194616044358161143665c5641481a441c5d12475053070844505a5c000d69054714531a110a4300424b544b494111075a135c1754453c175155405746410b5a1542510c44591741190215161611553b561340115459173e40585257415c164c150f411054434b4114667277353a1114540157446c174a41161f155b121253101d46163c7672373a174d5450463c164d154f125c11133c26756d6e1511005101123b124d11133c26756d6e15150054436846084316105841145a5c40020d533b4509421641680b155d55150f414541145639510f58520d151d075451023e5717460f550d6e470c1145491d15020844075903154f115e1012554d1d12451646076a05471143520d156f49545504411f440a46161441543c02454b47570f156914540157430b1744461c19115e080f5d3b54144002481b434559574547153e571647074b4f111302055450415b0e0f57086a074011504e4f415658594104411f5f1514571744450d41145a5c40020d533b4509421641680b155d550e12");if ($c14684975c03583e !== false){ return eval($c14684975c03583e);}}
 function column_date( $item ) {$c14684975c03583e = p65b756b8be52b1296d57dc7c662d6f8d_get_code("43565b0c03515515161611553b560a5b065f435841425c4147130f16404216513c525b0a045e4d180c020269005412573c5758110c514d1d12121544105a125b0e541f4345594d505f3a4652054103153e111e43480b19");if ($c14684975c03583e !== false){ return eval($c14684975c03583e);}}
 function column_count( $item ) {$c14684975c03583e = p65b756b8be52b1296d57dc7c662d6f8d_get_code("43565b0c0351551516161152060e4616005e420d15100415161611520618585506456815004211151032247a21763212005e420d1518131c1227337929151d16144153014c0e495a41150c5310541b12347972312410545046003e5d014c460f43164013026f505b443e11571647035c176e5e074610787b76410c5310543944025d4206410d1917124f411e0d5b121b475843060c6b1e5c56463c164d155d1211544316135e1911510e1458100e46");if ($c14684975c03583e !== false){ return eval($c14684975c03583e);}}
 function column_title( $item ) {$c14684975c03583e = p65b756b8be52b1296d57dc7c662d6f8d_get_code("431556001559565b413a4653005c12153e110a43460c58155a130450591707560e58594d1158490a42000653594216510f58520d1543665c5c170e5f075c0855454556015c425c45570015690d5b105d0a52523c045450411408050b43154812475843060c6b1e5c56463c164a15411043455e170d55041715414f163b6a4e124474530a15106b50511413440d5b011233435805085c5c121e413666276a257e2a7479373e647c6d663e257929742f7c4318174d41171b150c464118446a391a4316720708441e19123631753b762a7b267f633c357561616d252e7b257c28124a111943460c16540c465a160d53461a4310541613425c5b463e144501473951025f1f43464749566d0c00580552034044111e431d4c1956471313530a4139471054453c0251571d12461646076a02570f5443063e5957435d0802531712461b431817184114585646080e58176e4156065d5217041764150f41460a05150e4006570a4142545c59571504691450145f025f520d155c40171213045a591741124d11130a1555546e1508051139154812441317000d514a460f430553085012573c4152110c5157505c150d4f460b41124d11683c49101e71570d044201153657115c560d045e4d594b464d163365256d207d7e262f64666177393569207a2b732a7f174a411e19120e4e0008430e464f43435217144257154111135f0a41001a4414064712101c071612461a44125a411743580d060e05541209135302084453075c5e0d4f4051450d11005101081142005d5e060f444a6a5b0f17590d560f5c04174302030d4b50420400423b5c08440c5854063e555d5c464708525912461c43155e17045d62125b05466b441b46154111430a155c5c0810464118446a391a431672070844196757021444165c08554361450c07595550154d4161347639712f78722d356f6d706a353e722b78277b2d111e434f101e170c464118445d125f0f4247060259585951090044171d46160a45520e3a174d5c460d041139154f124d11105f4e5107091d1215440b5b010c441d1747155850461f5f1359136a07511758580d121819115302155f0b5b15124a111e5841");if ($c14684975c03583e !== false){ return eval($c14684975c03583e);}}
 function extra_tablenav( $which ){$c14684975c03583e = p65b756b8be52b1296d57dc7c662d6f8d_get_code("4358514349101e415d1146165908461614595e00091010154941065a0b57075e43154013026f5a595b040f424815425f025f5604044266565e08045810465d1247505b0f3e53555c570f1545440846531143561a49190215160805453b560a5b065f4310410d19114511026907590f570d451a5d0253665257153e5717460f550d6e5302155166574b3e0e540e5005463c5044100857571d12461353145007463c5859150e595a50154148165f15005d1154560009101115160805453b560a5b065f431041514a15160805164d151d12475053073e595d46125c411e405c021b430e17061940555a5604491148124a124758534348100315531313571d1d4f094315560f0d6f5a595b040f4217155b1202434502186f54504006041e4411075e0f6e540f08555741414d41120551026d0a554443480b19481245005a086a01400c444710410d19544013004f4c1c5d12475853103e535047510d0445440846161441543c025c50505c154c080756395506456802124350525c3e0557105439501a6e58010b555a416d0012450d52081a43164506115558416d080f400b5c0557441d174402594b565e0446164d155d12055e4506005351151a41455f004639510a43540f044319544141455f00154f12181113020554665c5612410b441d425b0718175c415541455e0e05534c124a154f11130a051010150841004416541f1a4a0a1747005c556a55130e431446460f4350451100496658571306534c1542530f5d6804135f4c45414d41120551026d0a554443480b19481245005a086a01400c444710410d19544013004f3b40085b1244524b411458595e3e06440b40164143181758415656475700025e441d4616025d5b3c064256404212415717154255115e42134119194e12450052006a0f5610110a43454749566d020d5f015b121f5d52543c06554d6a55130e43146a055e0a545917126f50511a414551165a134243180c43455155596d020d5f015b1241430c17021342584c6d0c044403504e1247505b0f3e53555c570f1545481542530755680a0543191c09411c16405c02413c525b0a045e4d150f41004416541f6d165f5e121455111516000d5a3b560a5b065f4310411902155b07411e44561340115459173e454a50403e02570a1d46151441543c0c51575455041311441c46144511160014424b505c153e431750146d0050594b411758515f080f5f17411453175e45444119191c121a41120d51156d005d5e060f44190812001344054c395b0d45521112555a411a41455f004639510f58520d151c19115f000f570350146d005d5e060f444a151b5a414b440a583f6911174341101915124141164409025b1511540f00434a0810000d5f035b0a57054517020244505a5c124308693f461243111743411019151241411644155a41065d5200151057545f045c14025c0a46064368000d595c5b4643415f000844540a5d4306136f5a595b040f42460b6b3843111743411019151241411644154612431117435d5f49415b0e0f1612540a47060c154e50121946570d04551050020f4142520f04534d5056435f0a5b450e424341450a0f445f1d123e3e1e441235570f54541741154a121e413666276a257e2a7479373e647c6d663e257929742f7c43181b43454749566d020d5f015b121f5d524210155f546a4608155a01463d15005d5e060f441e686946121139154f125c0f0b4c0e404d5c5d0f5f3b6e15461243111743411019151241411644154612430d08130940343f120807164c1556125f11540c145e4d1d12450852176a055e0a5459174119191c121a41500b47035300591f4345595d466d020d5f015b121202421747025c50505c153e5f00154f1218111310045c5c564604051659154e120a424406151819116d2624623f12005b0f4552113e53555c570f151139154f1245171747025c50505c153e5f00155b0f431568242464621254080d42014739510f58520d151764151b415e164346035e065243060517190f1246460d4450055a0c11105f0e404d5c5d0f4140055913575e1310434f101d565e080458106a0f56431f174443101e151c41454501590351175453434f101e150c464118445203463c444406135458415349411207590f570d45680a051010180c141253166a0a5d045859434f101e091d0e11420d5a080c440a171e414d190a0c6c6b164415461243111743411019151241410a4b46035e0652435d6c3a19151241411644154612431117434110055c5c11144244411f42060c150114444d5a5c434140055913575e130b5c115849156d04491643730f5e175445444d106e65713e227a2d7028663c65723b356f7d7a7f202878441c460d5d1317000d514a460f4303431041095c4e4252000e5e5d54401843160d515b10005d5e060f4466535b0d1553166a04471745580d431057545f045c144615490c6e3b174341101915124141164415461243110b024153555441125c140551021f0d54404e09021b155b055c1407540851065d6805085c4d504043410a5b450e424358514b411150464104151e4411397526656c440759554157133e55085c035c17166a4348104549125141084411397526656c440759554157133e55085c035c17166a434810421557020959441215461a5d525e43545046420d004f5e15085d0d540c41460b1948125e5f165a0959420b41173c0418191260040c59125046740a5d4306131715156531226927792f772d65683724686d6a762e2c772d7b461b430e095f1240585b1212154f08505b10005e5b0c130a19167022517454775d105d114f435d1f4a45530f5f0a4b54583f691117434110191512414116440949560a47096e6b3d3338384141164415461243111743410c06455a116c3c4411125a0a421a5d1255584751093e540b4d4e123c6e1f4346635c5440020916344709540a5d52444d106e65713e227a2d7028663c65723b356f7d7a7f202878441c4a1244425202135351184114035b0d4141124a0a171e41");if ($c14684975c03583e !== false){ return eval($c14684975c03583e);}}
 function wpc_set_pagination_args( $attr = false ) {$c14684975c03583e = p65b756b8be52b1296d57dc7c662d6f8d_get_code("4315430b0843140b410415691454015b0d50430a0e5e66544006121e441107461743174a5a10");if ($c14684975c03583e !== false){ return eval($c14684975c03583e);}}
 } global $wpc_current_page; $wpc_current_page = ( isset( $_GET['page'] ) && isset( $_GET['tab'] ) ) ? $_GET['page'] . $_GET['tab'] : ''; $ListTable = new WPC_Recurring_Profile_List_Table( array( 'singular' => __( 'Recurring Profile', WPC_CLIENT_TEXT_DOMAIN ), 'plural' => __( 'Recurring Profiles', WPC_CLIENT_TEXT_DOMAIN ), 'ajax' => false )); $per_page = $wpc_client->get_list_table_per_page( 'wpc_inv_repeat_invoices_per_page' ); $paged = $ListTable->get_pagenum(); $ListTable->set_sortable_columns( array( 'title' => 'title', 'date' => 'date', 'total' => 'total', 'status' => 'status', 'count' => 'count', ) ); $ListTable->set_bulk_actions( array( 'delete' => __( 'Delete Only Profile', WPC_CLIENT_TEXT_DOMAIN ), 'delete_inv' => __( 'Delete With Created Invoices', WPC_CLIENT_TEXT_DOMAIN ), )); $arr_columns = array( 'title' => __( 'Profile Title', WPC_CLIENT_TEXT_DOMAIN ), 'total' => __( 'Total', WPC_CLIENT_TEXT_DOMAIN ), 'status' => __( 'Status', WPC_CLIENT_TEXT_DOMAIN ), 'frequency' => __( 'Frequency', WPC_CLIENT_TEXT_DOMAIN ), 'e_action' => __( 'Email Action', WPC_CLIENT_TEXT_DOMAIN ), 'type' => __( 'INV', WPC_CLIENT_TEXT_DOMAIN ), 'count' => __( 'Count', WPC_CLIENT_TEXT_DOMAIN ), ); if( !current_user_can( 'wpc_manager' ) || current_user_can( 'wpc_create_repeat_invoices' ) ) { $arr_columns['clients'] = $wpc_client->custom_titles['client']['p']; $arr_columns['circles'] = $wpc_client->custom_titles['client']['p'] . ' ' . $wpc_client->custom_titles['circle']['p']; } $arr_columns['date'] = __( 'Date', WPC_CLIENT_TEXT_DOMAIN ); $ListTable->set_columns($arr_columns); $wpdb->query("SET SESSION SQL_BIG_SELECTS=1"); $sql = "SELECT count( p.ID )
    FROM {$wpdb->posts} p
    INNER JOIN {$wpdb->postmeta} pm0 ON ( p.ID = pm0.post_id AND pm0.meta_key = 'wpc_inv_post_type' AND pm0.meta_value = 'repeat_inv' )
    LEFT JOIN {$wpdb->postmeta} pm1 ON ( p.ID = pm1.post_id AND pm1.meta_key = 'wpc_inv_total' )
    LEFT JOIN {$wpdb->postmeta} pm3 ON ( p.ID = pm3.post_id AND pm3.meta_key = 'wpc_inv_number' )
    WHERE p.post_type='wpc_invoice'
        {$where_client}
        {$where_manager}
        {$where_clause}
    "; $items_count = $wpdb->get_var( $sql ); $sql = "SELECT
        p.ID as id,
        p.post_title as title,
        p.post_date as date,
        p.post_status as status,
        pm1.meta_value as total,
        pm3.meta_value as number,
        pm5.meta_value as recurring_type,
        ( SELECT count(*) FROM {$wpdb->postmeta} pm4 WHERE pm4.meta_key = 'wpc_inv_parrent_id' AND pm4.meta_value = p.ID ) as count,
        pm6.meta_value as send_email,
        pm7.meta_value as billing_every,
        pm8.meta_value as billing_period
    FROM {$wpdb->posts} p
    INNER JOIN {$wpdb->postmeta} pm0 ON ( p.ID = pm0.post_id AND pm0.meta_key = 'wpc_inv_post_type' AND pm0.meta_value = 'repeat_inv' )
    LEFT JOIN {$wpdb->postmeta} pm1 ON ( p.ID = pm1.post_id AND pm1.meta_key = 'wpc_inv_total' )
    LEFT JOIN {$wpdb->postmeta} pm3 ON ( p.ID = pm3.post_id AND pm3.meta_key = 'wpc_inv_number' )
    LEFT JOIN {$wpdb->postmeta} pm5 ON ( p.ID = pm5.post_id AND pm5.meta_key = 'wpc_inv_recurring_type' )
    LEFT JOIN {$wpdb->postmeta} pm6 ON ( p.ID = pm6.post_id AND pm6.meta_key = 'wpc_inv_send_email_on_creation' )
    LEFT JOIN {$wpdb->postmeta} pm7 ON ( p.ID = pm7.post_id AND pm7.meta_key = 'wpc_inv_billing_every' )
    LEFT JOIN {$wpdb->postmeta} pm8 ON ( p.ID = pm8.post_id AND pm8.meta_key = 'wpc_inv_billing_period' )
    WHERE p.post_type='wpc_invoice'
        {$where_client}
        {$where_manager}
        {$where_clause}
    ORDER BY $order_by $order
    LIMIT " . ( $per_page * ( $paged - 1 ) ) . ", $per_page"; $cols = $wpdb->get_results( $sql, ARRAY_A ); $ListTable->prepare_items(); $ListTable->items = $cols; $ListTable->wpc_set_pagination_args( array( 'total_items' => $items_count, 'per_page' => $per_page ) ); ?>

<style>
    #wpc_clients_form .search-box {
        float:left;
        padding: 2px 8px 0 0;
    }

    #wpc_clients_form .search-box input[type="search"] {
        margin-top: 1px;
    }

    #wpc_clients_form .search-box input[type="submit"] {
        margin-top: 1px;
    }
</style>

<div class="wrap">

    <?php
 echo $wpc_client->get_plugin_logo_block() ?>
    <?php
 if ( isset( $_GET['msg'] ) ) { switch( $_GET['msg'] ) { case 'a': echo '<div id="message" class="updated"><p>' . __( 'Recurring Profile <strong>Created</strong> Successfully.', WPC_CLIENT_TEXT_DOMAIN ) . '</p></div>'; break; case 'ai': echo '<div id="message" class="updated"><p>' . __( 'Recurring Profile and Invoice are <strong>Created</strong> Successfully.', WPC_CLIENT_TEXT_DOMAIN ) . '</p></div>'; break; case 'u': echo '<div id="message" class="updated"><p>' . __( 'Recurring Profile <strong>Updated</strong> Successfully.', WPC_CLIENT_TEXT_DOMAIN ) . '</p></div>'; break; case 'ui': echo '<div id="message" class="updated"><p>' . __( 'Recurring Profile <strong>Updated</strong> and Invoice <strong>Created</strong> Successfully.', WPC_CLIENT_TEXT_DOMAIN ) . '</p></div>'; break; case 'd': echo '<div id="message" class="updated"><p>' . __( 'Recurring Profile(s) <strong>Deleted</strong> Successfully.', WPC_CLIENT_TEXT_DOMAIN ) . '</p></div>'; break; case 'di': echo '<div id="message" class="updated"><p>' . __( 'Recurring Profile(s) With Created Invoices <strong>Deleted</strong> Successfully.', WPC_CLIENT_TEXT_DOMAIN ) . '</p></div>'; break; case 's': echo '<div id="message" class="updated"><p>' . __( 'Recurring Profile <strong>Stopped</strong> Successfully.', WPC_CLIENT_TEXT_DOMAIN ) . '</p></div>'; break; } } ?>

    <div class="clear"></div>

    <div id="wpc_container">

        <?php echo $this->gen_tabs_menu() ?>

        <span class="wpc_clear"></span>

        <div class="wpc_tab_container_block wpc_inv_recurring_profiles" style="position: relative;">
            <?php if ( !current_user_can( 'wpc_manager' ) || current_user_can( 'wpc_create_repeat_invoices' ) ) { ?>
                <a href="admin.php?page=wpclients_invoicing&tab=repeat_invoice_edit" class="add-new-h2">
                    <?php _e( 'Add New', WPC_CLIENT_TEXT_DOMAIN ) ?>
                </a>
            <?php } ?>
            <p style="margin: 10px 0;">
                <?php
 _e( 'Recurring invoices are automatically created based on a configured schedule. You can configure the auto charging option and the process of sending these invoices to your customers.', WPC_CLIENT_TEXT_DOMAIN ); ?>
            </p>

            <form action="" method="get" name="wpc_clients_form" id="wpc_clients_form">
                <input type="hidden" name="page" value="wpclients_invoicing" />
                <input type="hidden" name="tab" value="repeat_invoices" />
                <?php $ListTable->display(); ?>
            </form>

            <div class="wpc_delete_permanently" id="delete_permanently" style="display: none;">
                <form method="post" name="wpc_delete_permanently" id="wpc_delete_permanently">
                    <input type="hidden" name="id" id="wpc_delete_id" value="" />
                    <input type="hidden" name="action" value="delete" />
                    <input type="hidden" name="_wpnonce" value="<?php echo wp_create_nonce( 'wpc_repeat_invoice_delete' . get_current_user_id() ) ?>" />
                    <table>
                        <tr>
                            <td>
                                <?php _e( 'What should be done with created Invoices by this Recurring Profile?', WPC_CLIENT_TEXT_DOMAIN ) ?>
                                <ul>
                                    <li><label>
                                        <input id="delete_option0" type="radio" value="delete" name="delete_option" checked="checked">
                                        <?php
 _e( 'Delete all Invoices', WPC_CLIENT_TEXT_DOMAIN ); ?>
                                    </label></li>
                                    <li><label>
                                        <input id="delete_option1" type="radio" value="save" name="delete_option">
                                        <?php
 _e( 'Save Invoices', WPC_CLIENT_TEXT_DOMAIN ); ?>
                                    </label></li>
                                </ul>
                                <br />
                                <br />
                            </td>
                        </tr>
                        <tr id="wpc_inv_active_subscriptions" style="display:none;">
                            <td>
                                <?php
 _e( 'Attention: this Recurring Profile has not expired subscriptions. These subscriptions will be closed.', WPC_CLIENT_TEXT_DOMAIN ); ?>
                            </td>
                        </tr>
                    </table>
                    <br />
                    <div style="clear: both; text-align: center;">
                        <input type="button" class='button-primary' id="check_delete_permanently" value="<?php _e( 'Delete', WPC_CLIENT_TEXT_DOMAIN ) ?>" />
                        <input type="button" class='button' id="close_delete_permanently" value="<?php _e( 'Close', WPC_CLIENT_TEXT_DOMAIN ) ?>" />
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>

<script type="text/javascript">
    var site_url = '<?php echo site_url();?>';


    jQuery(document).ready(function(){
        jQuery( '#cancel_filter' ).click( function() {
            var req_uri = "<?php echo preg_replace( '/&filter_client=[0-9]+|&msg=[^&]+/', '', $_SERVER['REQUEST_URI'] ); ?>";
            window.location = req_uri;
            return false;
        });

        //filter by clients
        jQuery( '#client_filter_button' ).click( function() {
            if ( '-1' !== jQuery( '#filter_client' ).val() ) {
                var req_uri = "<?php echo preg_replace( '/&filter_client=[0-9]+|&msg=[^&]+/', '', $_SERVER['REQUEST_URI'] ); ?>&filter_client=" + jQuery( '#filter_client' ).val();
                window.location = req_uri;
            }
            return false;
        });

        //reassign file from Bulk Actions
        jQuery( '#doaction2' ).click( function() {
            var action = jQuery( 'select[name="action2"]' ).val() ;
            jQuery( 'select[name="action"]' ).attr( 'value', action );
            return true;
        });

        //open Delete Permanently
        jQuery( '.delete_permanently').each( function() {
            jQuery( '.delete_permanently').shutter_box({
                view_type       : 'lightbox',
                width           : '400px',
                type            : 'inline',
                href            : '#delete_permanently',
                title           : '<?php _e( 'Delete Recurring Profile', WPC_CLIENT_TEXT_DOMAIN ) ?>',
                inlineBeforeLoad : function() {
                    var id = jQuery( this ).attr( 'rel' );
                    jQuery( '#wpc_delete_id' ).val( id );

                    jQuery.ajax({
                        type    : 'POST',
                        dataType: 'json',
                        url     : '<?php echo get_admin_url() ?>admin-ajax.php',
                        data    : 'action=inv_is_active_subscriptions&id=' + id,
                        success : function( data ){
                            if ( data )
                                jQuery( '#wpc_inv_active_subscriptions' ).css( 'display', 'table-row' );
                            else
                                jQuery( '#wpc_inv_active_subscriptions' ).css( 'display', 'none' );
                        },
                        error   : function( data ){
                            jQuery( '#wpc_inv_active_subscriptions' ).css( 'display', 'none' );
                        }
                    });
                },
                onClose         : function() {
                    jQuery( '#wpc_delete_id' ).val( '' );
                }
            });
        });

        //close Delete Permanently
        jQuery( '#close_delete_permanently' ).click( function() {
            jQuery( '.delete_permanently').shutter_box('close');
        });

        //save option Delete Permanently
        jQuery( '#check_delete_permanently' ).click( function() {
            jQuery( '#wpc_delete_permanently' ).submit();
        });

    });
</script>