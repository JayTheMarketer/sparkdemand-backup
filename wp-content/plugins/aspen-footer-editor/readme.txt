=== Plugin Name ===
Contributors: Aspengrovestudios.com
Donate link: http://Aspengrovestudios.com/
Tags: aspen footer editor
Requires at least: 3.0.1
Tested up to: 4.5
Stable tag: 4.5
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

An easy way to add or edit Footer Credit Text on Divi & Extra sites.

== Description ==

Please visit this link: http://aspengrovestudios.com/docs/divi-footer-editor-readme.pdf

== Installation ==

1. Upload the plugin files to the `/wp-content/plugins/` directory, or install the plugin through the WordPress plugins screen directly.
2. Activate the plugin through the 'Plugins' screen in WordPress
3. Use the Aspen Footer Editor screen from tab or click on Settings link to edit your footer

== Changelog ==

= 2.1 =
* Added quick link so the Aspen Footer Editor is accessible directly from the plugins dashboard. Simply click on Settings from the plugin dashboard to edit your footer.

= 2.1.1 =
* Minor bug fixes.