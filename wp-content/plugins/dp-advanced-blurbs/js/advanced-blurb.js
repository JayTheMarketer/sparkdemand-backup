// JavaScript Document

jQuery(document).ready(function () {

    jQuery('.dp-pb-onload').each(function (i) {
        if (jQuery(this).find('.fa').hasClass('db_pb_animation_left')) {
            jQuery(this).find('.fa').addClass('fadeInRight animated');
        }
        if (jQuery(this).find('.fa').hasClass('db_pb_animation_right')) {
            jQuery(this).find('.fa').addClass('fadeInLeft animated');
        }
        if (jQuery(this).find('.fa').hasClass('db_pb_animation_top')) {
            jQuery(this).find('.fa').addClass('fadeInDown animated');
        }
        if (jQuery(this).find('.fa').hasClass('db_pb_animation_bottom')) {
            jQuery(this).find('.fa').addClass('fadeInUp animated');
        }
        if (jQuery(this).find('.fa').hasClass('db_pb_animation_bounce')) {
            jQuery(this).find('.fa').addClass('bounce animated');
        }
        if (jQuery(this).find('.fa').hasClass('db_pb_animation_flash')) {
            jQuery(this).find('.fa').addClass('flash animated');
        }
        if (jQuery(this).find('.fa').hasClass('db_pb_animation_pulse')) {
            jQuery(this).find('.fa').addClass('pulse animated');
        }
        if (jQuery(this).find('.fa').hasClass('db_pb_animation_shake')) {
            jQuery(this).find('.fa').addClass('shake animated');
        }
        if (jQuery(this).find('.fa').hasClass('db_pb_animation_fa-spin')) {
            jQuery(this).find('.fa').addClass('fa-spin').delay(3000).queue(function () {
                jQuery(this).removeClass("fa-spin").dequeue();
            });
        }
    });



    jQuery('.dp-pb-hover').each(function (i) {
        if (jQuery(this).find('.fa').hasClass('db_pb_animation_left')) {
              jQuery(this).on("mouseover", function () {
                jQuery(this).find('.fa').addClass('fadeInRight animated');
            });
            jQuery(this).mouseleave(function () {
                jQuery(this).find('.fa').addClass('fadeInRight animated');
            });
            
        }
        if (jQuery(this).find('.fa').hasClass('db_pb_animation_right')) {
              jQuery(this).on("mouseover", function () {
                jQuery(this).find('.fa').addClass('fadeInLeft animated');
            });
            jQuery(this).mouseleave(function () {
                jQuery(this).find('.fa').addClass('fadeInLeft animated');
            });
            
        }
        if (jQuery(this).find('.fa').hasClass('db_pb_animation_top')) {
              jQuery(this).on("mouseover", function () {
                jQuery(this).find('.fa').addClass('fadeInDown animated');
            });
            jQuery(this).mouseleave(function () {
                jQuery(this).find('.fa').addClass('fadeInDown animated');
            });
            
        }
        if (jQuery(this).find('.fa').hasClass('db_pb_animation_bottom')) {
              jQuery(this).on("mouseover", function () {
                jQuery(this).find('.fa').addClass('fadeInUp animated');
            });
            jQuery(this).mouseleave(function () {
                jQuery(this).find('.fa').addClass('fadeInUp animated');
            });
            
        }
        if (jQuery(this).find('.fa').hasClass('db_pb_animation_bounce')) {
            jQuery(this).on("mouseover", function () {
                jQuery(this).find('.fa').addClass('bounce animated');
            });
            jQuery(this).mouseleave(function () {
                jQuery(this).find('.fa').removeClass('bounce animated');
            });
        }
        if (jQuery(this).find('.fa').hasClass('db_pb_animation_flash')) {
            jQuery(this).mouseover(function () {
                jQuery(this).find('.fa').addClass('flash animated');
            });
            jQuery(this).mouseleave(function () {
                jQuery(this).find('.fa').removeClass('flash animated');
            });
        }
        if (jQuery(this).find('.fa').hasClass('db_pb_animation_pulse')) {
            jQuery(this).mouseover(function () {
                jQuery(this).find('.fa').addClass('pulse animated');
            });
            jQuery(this).mouseleave(function () {
                jQuery(this).find('.fa').removeClass('pulse animated');
            });
        }
        if (jQuery(this).find('.fa').hasClass('db_pb_animation_shake')) {
            jQuery(this).mouseover(function () {
                jQuery(this).find('.fa').addClass('shake animated');
            });
            jQuery(this).mouseleave(function () {
                jQuery(this).find('.fa').removeClass('shake animated');
            });
        }
        if (jQuery(this).find('.fa').hasClass('db_pb_animation_fa-spin')) {
            jQuery(this).mouseover(function () {
                jQuery(this).find('.fa').addClass('fa-spin animated');
            });
            jQuery(this).mouseleave(function () {
                jQuery(this).find('.fa').removeClass('fa-spin animated');
            });
        }
    });

    jQuery('.dp-pb-infinite').each(function (i) {        
        if (jQuery(this).find('.fa').hasClass('db_pb_animation_left')) {
            jQuery(this).find('.fa').addClass('fadeInRight infinite');
        }
        if (jQuery(this).find('.fa').hasClass('db_pb_animation_right')) {
            jQuery(this).find('.fa').addClass('fadeInLeft infinite');
        }
        if (jQuery(this).find('.fa').hasClass('db_pb_animation_top')) {
            jQuery(this).find('.fa').addClass('fadeInDown infinite');
        }
        if (jQuery(this).find('.fa').hasClass('db_pb_animation_bottom')) {
            jQuery(this).find('.fa').addClass('fadeInUp infinite');
        }
        if (jQuery(this).find('.fa').hasClass('db_pb_animation_bounce')) {
            jQuery(this).find('.fa').addClass('bounce infinite');
        }
        if (jQuery(this).find('.fa').hasClass('db_pb_animation_flash')) {
            jQuery(this).find('.fa').addClass('flash infinite');
        }
        if (jQuery(this).find('.fa').hasClass('db_pb_animation_pulse')) {
            jQuery(this).find('.fa').addClass('pulse infinite');
        }
        if (jQuery(this).find('.fa').hasClass('db_pb_animation_shake')) {
            jQuery(this).find('.fa').addClass('shake infinite');
        }
        if (jQuery(this).find('.fa').hasClass('db_pb_animation_fa-spin')) {
            jQuery(this).find('.fa').addClass('fa-spin');
        }
    });


});