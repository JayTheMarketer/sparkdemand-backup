<?php

if (!function_exists('et_pb_ab_get_font_icon_list')) :

    function et_pb_ab_get_font_icon_list() {
        $icons = awesome_icons();
        $output = '';
        $output .= '<ul id="basic" class="et_font_icon" >';
        for ($i = 0; $i < count($icons); $i++) {
            $output .= '<li id="' . $icons[$i] . '"><i class="fa ' . $icons[$i] . ' fa-1x"></i></li>';
        }
        $output .= '</ul>';

        return $output;
    }

endif;

function awesome_icons() {
    $icons = array();

    $icons[] = 'fa-adjust';
    $icons[] = 'fa-anchor';
    $icons[] = 'fa-archive';
    $icons[] = 'fa-area-chart';
    $icons[] = 'fa-arrows';
    $icons[] = 'fa-arrows-h';
    $icons[] = 'fa-arrows-v';
    $icons[] = 'fa-asterisk';
    $icons[] = 'fa-at';
    $icons[] = 'fa-automobile';
    $icons[] = 'fa-balance-scale';
    $icons[] = 'fa-ban';
    $icons[] = 'fa-bank';
    $icons[] = 'fa-bar-chart';
    $icons[] = 'fa-bar-chart-o';
    $icons[] = 'fa-barcode';
    $icons[] = 'fa-bars';
    $icons[] = 'fa-battery-0';
    $icons[] = 'fa-battery-1';
    $icons[] = 'fa-battery-2';
    $icons[] = 'fa-battery-3';
    $icons[] = 'fa-battery-4';
    $icons[] = 'fa-battery-empty';
    $icons[] = 'fa-battery-full';
    $icons[] = 'fa-battery-half';
    $icons[] = 'fa-battery-quarter';
    $icons[] = 'fa-battery-three-quarters';
    $icons[] = 'fa-bed';
    $icons[] = 'fa-beer';
    $icons[] = 'fa-bell';
    $icons[] = 'fa-bell-o';
    $icons[] = 'fa-bell-slash';
    $icons[] = 'fa-bell-slash-o';
    $icons[] = 'fa-bicycle';
    $icons[] = 'fa-binoculars';
    $icons[] = 'fa-birthday-cake';
    $icons[] = 'fa-bluetooth';
    $icons[] = 'fa-bluetooth-b';
    $icons[] = 'fa-bolt';
    $icons[] = 'fa-bomb';
    $icons[] = 'fa-book';
    $icons[] = 'fa-bookmark';
    $icons[] = 'fa-bookmark-o';
    $icons[] = 'fa-briefcase';
    $icons[] = 'fa-bug';
    $icons[] = 'fa-building';
    $icons[] = 'fa-building-o';
    $icons[] = 'fa-bullhorn';
    $icons[] = 'fa-bullseye';
    $icons[] = 'fa-bus';
    $icons[] = 'fa-cab';
    $icons[] = 'fa-calculator';
    $icons[] = 'fa-calendar';
    $icons[] = 'fa-calendar-check-o';
    $icons[] = 'fa-calendar-minus-o';
    $icons[] = 'fa-calendar-o';
    $icons[] = 'fa-calendar-plus-o';
    $icons[] = 'fa-calendar-times-o';
    $icons[] = 'fa-camera';
    $icons[] = 'fa-camera-retro';
    $icons[] = 'fa-car';
    $icons[] = 'fa-caret-square-o-down';
    $icons[] = 'fa-caret-square-o-left';
    $icons[] = 'fa-caret-square-o-right';
    $icons[] = 'fa-caret-square-o-up';
    $icons[] = 'fa-cart-arrow-down';
    $icons[] = 'fa-cart-plus';
    $icons[] = 'fa-cc';
    $icons[] = 'fa-certificate';
    $icons[] = 'fa-check';
    $icons[] = 'fa-check-circle';
    $icons[] = 'fa-check-circle-o';
    $icons[] = 'fa-check-square';
    $icons[] = 'fa-check-square-o';
    $icons[] = 'fa-child';
    $icons[] = 'fa-circle';
    $icons[] = 'fa-circle-o';
    $icons[] = 'fa-circle-o-notch';
    $icons[] = 'fa-circle-thin';
    $icons[] = 'fa-clock-o';
    $icons[] = 'fa-clone';
    $icons[] = 'fa-close';
    $icons[] = 'fa-cloud';
    $icons[] = 'fa-cloud-download';
    $icons[] = 'fa-cloud-upload';
    $icons[] = 'fa-code';
    $icons[] = 'fa-code-fork';
    $icons[] = 'fa-coffee';
    $icons[] = 'fa-cog';
    $icons[] = 'fa-cogs';
    $icons[] = 'fa-comment';
    $icons[] = 'fa-comment-o';
    $icons[] = 'fa-commenting';
    $icons[] = 'fa-commenting-o';
    $icons[] = 'fa-comments';
    $icons[] = 'fa-comments-o';
    $icons[] = 'fa-compass';
    $icons[] = 'fa-copyright';
    $icons[] = 'fa-creative-commons';
    $icons[] = 'fa-credit-card';
    $icons[] = 'fa-credit-card-alt';
    $icons[] = 'fa-crop';
    $icons[] = 'fa-crosshairs';
    $icons[] = 'fa-cube';
    $icons[] = 'fa-cubes';
    $icons[] = 'fa-cutlery';
    $icons[] = 'fa-dashboard';
    $icons[] = 'fa-database';
    $icons[] = 'fa-desktop';
    $icons[] = 'fa-diamond';
    $icons[] = 'fa-dot-circle-o';
    $icons[] = 'fa-download';
    $icons[] = 'fa-edit';
    $icons[] = 'fa-ellipsis-h';
    $icons[] = 'fa-ellipsis-v';
    $icons[] = 'fa-envelope';
    $icons[] = 'fa-envelope-o';
    $icons[] = 'fa-envelope-square';
    $icons[] = 'fa-eraser';
    $icons[] = 'fa-exchange';
    $icons[] = 'fa-exclamation';
    $icons[] = 'fa-exclamation-circle';
    $icons[] = 'fa-exclamation-triangle';
    $icons[] = 'fa-external-link';
    $icons[] = 'fa-external-link-square';
    $icons[] = 'fa-eye';
    $icons[] = 'fa-eye-slash';
    $icons[] = 'fa-eyedropper';
    $icons[] = 'fa-fax';
    $icons[] = 'fa-feed';
    $icons[] = 'fa-female';
    $icons[] = 'fa-fighter-jet';
    $icons[] = 'fa-file-archive-o';
    $icons[] = 'fa-file-audio-o';
    $icons[] = 'fa-file-code-o';
    $icons[] = 'fa-file-excel-o';
    $icons[] = 'fa-file-image-o';
    $icons[] = 'fa-file-movie-o';
    $icons[] = 'fa-file-pdf-o';
    $icons[] = 'fa-file-photo-o';
    $icons[] = 'fa-file-picture-o';
    $icons[] = 'fa-file-powerpoint-o';
    $icons[] = 'fa-file-sound-o';
    $icons[] = 'fa-file-video-o';
    $icons[] = 'fa-file-word-o';
    $icons[] = 'fa-file-zip-o';
    $icons[] = 'fa-film';
    $icons[] = 'fa-filter';
    $icons[] = 'fa-fire';
    $icons[] = 'fa-fire-extinguisher';
    $icons[] = 'fa-flag';
    $icons[] = 'fa-flag-checkered';
    $icons[] = 'fa-flag-o';
    $icons[] = 'fa-flash';
    $icons[] = 'fa-flask';
    $icons[] = 'fa-folder';
    $icons[] = 'fa-folder-o';
    $icons[] = 'fa-folder-open';
    $icons[] = 'fa-folder-open-o';
    $icons[] = 'fa-frown-o';
    $icons[] = 'fa-futbol-o';
    $icons[] = 'fa-gamepad';
    $icons[] = 'fa-gavel';
    $icons[] = 'fa-gear';
    $icons[] = 'fa-gears';
    $icons[] = 'fa-gift';
    $icons[] = 'fa-glass';
    $icons[] = 'fa-globe';
    $icons[] = 'fa-graduation-cap';
    $icons[] = 'fa-group';
    $icons[] = 'fa-hand-grab-o';
    $icons[] = 'fa-hand-lizard-o';
    $icons[] = 'fa-hand-paper-o';
    $icons[] = 'fa-hand-peace-o';
    $icons[] = 'fa-hand-pointer-o';
    $icons[] = 'fa-hand-rock-o';
    $icons[] = 'fa-hand-scissors-o';
    $icons[] = 'fa-hand-spock-o';
    $icons[] = 'fa-hand-stop-o';
    $icons[] = 'fa-hashtag';
    $icons[] = 'fa-hdd-o';
    $icons[] = 'fa-headphones';
    $icons[] = 'fa-heart';
    $icons[] = 'fa-heart-o';
    $icons[] = 'fa-heartbeat';
    $icons[] = 'fa-history';
    $icons[] = 'fa-home';
    $icons[] = 'fa-hotel';
    $icons[] = 'fa-hourglass';
    $icons[] = 'fa-hourglass-1';
    $icons[] = 'fa-hourglass-2';
    $icons[] = 'fa-hourglass-3';
    $icons[] = 'fa-hourglass-end';
    $icons[] = 'fa-hourglass-half';
    $icons[] = 'fa-hourglass-o';
    $icons[] = 'fa-hourglass-start';
    $icons[] = 'fa-i-cursor';
    $icons[] = 'fa-image';
    $icons[] = 'fa-inbox';
    $icons[] = 'fa-industry';
    $icons[] = 'fa-info';
    $icons[] = 'fa-info-circle';
    $icons[] = 'fa-institution';
    $icons[] = 'fa-key';
    $icons[] = 'fa-keyboard-o';
    $icons[] = 'fa-language';
    $icons[] = 'fa-laptop';
    $icons[] = 'fa-leaf';
    $icons[] = 'fa-legal';
    $icons[] = 'fa-lemon-o';
    $icons[] = 'fa-level-down';
    $icons[] = 'fa-level-up';
    $icons[] = 'fa-life-bouy';
    $icons[] = 'fa-life-buoy';
    $icons[] = 'fa-life-ring';
    $icons[] = 'fa-life-saver';
    $icons[] = 'fa-lightbulb-o';
    $icons[] = 'fa-line-chart';
    $icons[] = 'fa-location-arrow';
    $icons[] = 'fa-lock';
    $icons[] = 'fa-magic';
    $icons[] = 'fa-magnet';
    $icons[] = 'fa-mail-forward';
    $icons[] = 'fa-mail-reply';
    $icons[] = 'fa-mail-reply-all';
    $icons[] = 'fa-male';
    $icons[] = 'fa-map';
    $icons[] = 'fa-map-marker';
    $icons[] = 'fa-map-o';
    $icons[] = 'fa-map-pin';
    $icons[] = 'fa-map-signs';
    $icons[] = 'fa-meh-o';
    $icons[] = 'fa-microphone';
    $icons[] = 'fa-microphone-slash';
    $icons[] = 'fa-minus';
    $icons[] = 'fa-minus-circle';
    $icons[] = 'fa-minus-square';
    $icons[] = 'fa-minus-square-o';
    $icons[] = 'fa-mobile';
    $icons[] = 'fa-mobile-phone';
    $icons[] = 'fa-money';
    $icons[] = 'fa-moon-o';
    $icons[] = 'fa-mortar-board';
    $icons[] = 'fa-motorcycle';
    $icons[] = 'fa-mouse-pointer';
    $icons[] = 'fa-music';
    $icons[] = 'fa-navicon';
    $icons[] = 'fa-newspaper-o';
    $icons[] = 'fa-object-group';
    $icons[] = 'fa-object-ungroup';
    $icons[] = 'fa-paint-brush';
    $icons[] = 'fa-paper-plane';
    $icons[] = 'fa-paper-plane-o';
    $icons[] = 'fa-paw';
    $icons[] = 'fa-pencil';
    $icons[] = 'fa-pencil-square';
    $icons[] = 'fa-pencil-square-o';
    $icons[] = 'fa-percent';
    $icons[] = 'fa-phone';
    $icons[] = 'fa-phone-square';
    $icons[] = 'fa-photo';
    $icons[] = 'fa-picture-o';
    $icons[] = 'fa-pie-chart';
    $icons[] = 'fa-plane';
    $icons[] = 'fa-plug';
    $icons[] = 'fa-plus';
    $icons[] = 'fa-plus-circle';
    $icons[] = 'fa-plus-square';
    $icons[] = 'fa-plus-square-o';
    $icons[] = 'fa-power-off';
    $icons[] = 'fa-print';
    $icons[] = 'fa-puzzle-piece';
    $icons[] = 'fa-qrcode';
    $icons[] = 'fa-question';
    $icons[] = 'fa-question-circle';
    $icons[] = 'fa-quote-left';
    $icons[] = 'fa-quote-right';
    $icons[] = 'fa-random';
    $icons[] = 'fa-recycle';
    $icons[] = 'fa-refresh';
    $icons[] = 'fa-registered';
    $icons[] = 'fa-remove';
    $icons[] = 'fa-reorder';
    $icons[] = 'fa-reply';
    $icons[] = 'fa-reply-all';
    $icons[] = 'fa-retweet';
    $icons[] = 'fa-road';
    $icons[] = 'fa-rocket';
    $icons[] = 'fa-rss';
    $icons[] = 'fa-rss-square';
    $icons[] = 'fa-search';
    $icons[] = 'fa-search-minus';
    $icons[] = 'fa-search-plus';
    $icons[] = 'fa-send';
    $icons[] = 'fa-send-o';
    $icons[] = 'fa-server';
    $icons[] = 'fa-share';
    $icons[] = 'fa-share-alt';
    $icons[] = 'fa-share-alt-square';
    $icons[] = 'fa-share-square';
    $icons[] = 'fa-share-square-o';
    $icons[] = 'fa-shield';
    $icons[] = 'fa-ship';
    $icons[] = 'fa-shopping-bag';
    $icons[] = 'fa-shopping-basket';
    $icons[] = 'fa-shopping-cart';
    $icons[] = 'fa-sign-in';
    $icons[] = 'fa-sign-out';
    $icons[] = 'fa-signal';
    $icons[] = 'fa-sitemap';
    $icons[] = 'fa-sliders';
    $icons[] = 'fa-smile-o';
    $icons[] = 'fa-soccer-ball-o';
    $icons[] = 'fa-sort';
    $icons[] = 'fa-sort-alpha-asc';
    $icons[] = 'fa-sort-alpha-desc';
    $icons[] = 'fa-sort-amount-asc';
    $icons[] = 'fa-sort-amount-desc';
    $icons[] = 'fa-sort-asc';
    $icons[] = 'fa-sort-desc';
    $icons[] = 'fa-sort-down';
    $icons[] = 'fa-sort-numeric-asc';
    $icons[] = 'fa-sort-numeric-desc';
    $icons[] = 'fa-sort-up';
    $icons[] = 'fa-space-shuttle';
    $icons[] = 'fa-spinner';
    $icons[] = 'fa-spoon';
    $icons[] = 'fa-square';
    $icons[] = 'fa-square-o';
    $icons[] = 'fa-star';
    $icons[] = 'fa-star-half';
    $icons[] = 'fa-star-half-empty';
    $icons[] = 'fa-star-half-full';
    $icons[] = 'fa-star-half-o';
    $icons[] = 'fa-star-o';
    $icons[] = 'fa-sticky-note';
    $icons[] = 'fa-sticky-note-o';
    $icons[] = 'fa-street-view';
    $icons[] = 'fa-suitcase';
    $icons[] = 'fa-sun-o';
    $icons[] = 'fa-support';
    $icons[] = 'fa-tablet';
    $icons[] = 'fa-tachometer';
    $icons[] = 'fa-tag';
    $icons[] = 'fa-tags';
    $icons[] = 'fa-tasks';
    $icons[] = 'fa-taxi';
    $icons[] = 'fa-television';
    $icons[] = 'fa-terminal';
    $icons[] = 'fa-thumb-tack';
    $icons[] = 'fa-thumbs-down';
    $icons[] = 'fa-thumbs-o-down';
    $icons[] = 'fa-thumbs-o-up';
    $icons[] = 'fa-thumbs-up';
    $icons[] = 'fa-ticket';
    $icons[] = 'fa-times';
    $icons[] = 'fa-times-circle';
    $icons[] = 'fa-times-circle-o';
    $icons[] = 'fa-tint';
    $icons[] = 'fa-toggle-down';
    $icons[] = 'fa-toggle-left';
    $icons[] = 'fa-toggle-off';
    $icons[] = 'fa-toggle-on';
    $icons[] = 'fa-toggle-right';
    $icons[] = 'fa-toggle-up';
    $icons[] = 'fa-trademark';
    $icons[] = 'fa-trash';
    $icons[] = 'fa-trash-o';
    $icons[] = 'fa-tree';
    $icons[] = 'fa-trophy';
    $icons[] = 'fa-truck';
    $icons[] = 'fa-tty';
    $icons[] = 'fa-tv';
    $icons[] = 'fa-umbrella';
    $icons[] = 'fa-university';
    $icons[] = 'fa-unlock';
    $icons[] = 'fa-unlock-alt';
    $icons[] = 'fa-unsorted';
    $icons[] = 'fa-upload';
    $icons[] = 'fa-user';
    $icons[] = 'fa-user-plus';
    $icons[] = 'fa-user-secret';
    $icons[] = 'fa-user-times';
    $icons[] = 'fa-users';
    $icons[] = 'fa-video-camera';
    $icons[] = 'fa-volume-down';
    $icons[] = 'fa-volume-off';
    $icons[] = 'fa-volume-up';
    $icons[] = 'fa-warning';
    $icons[] = 'fa-wheelchair';
    $icons[] = 'fa-wifi';
    $icons[] = 'fa-wrench';
    $icons[] = 'fa-hand-grab-o';
    $icons[] = 'fa-hand-lizard-o';
    $icons[] = 'fa-hand-o-down';
    $icons[] = 'fa-hand-o-left';
    $icons[] = 'fa-hand-o-right';
    $icons[] = 'fa-hand-o-up';
    $icons[] = 'fa-hand-paper-o';
    $icons[] = 'fa-hand-peace-o';
    $icons[] = 'fa-hand-pointer-o';
    $icons[] = 'fa-hand-rock-o';
    $icons[] = 'fa-hand-scissors-o';
    $icons[] = 'fa-hand-spock-o';
    $icons[] = 'fa-hand-stop-o';
    $icons[] = 'fa-thumbs-down';
    $icons[] = 'fa-thumbs-o-down';
    $icons[] = 'fa-thumbs-o-up';
    $icons[] = 'fa-thumbs-up';
    $icons[] = 'fa-ambulance';
    $icons[] = 'fa-automobile';
    $icons[] = 'fa-bicycle';
    $icons[] = 'fa-bus';
    $icons[] = 'fa-cab';
    $icons[] = 'fa-car';
    $icons[] = 'fa-fighter-jet';
    $icons[] = 'fa-motorcycle';
    $icons[] = 'fa-plane';
    $icons[] = 'fa-rocket';
    $icons[] = 'fa-ship';
    $icons[] = 'fa-space-shuttle';
    $icons[] = 'fa-subway';
    $icons[] = 'fa-taxi';
    $icons[] = 'fa-train';
    $icons[] = 'fa-truck';
    $icons[] = 'fa-wheelchair';
    $icons[] = 'fa-genderless';
    $icons[] = 'fa-intersex';
    $icons[] = 'fa-mars';
    $icons[] = 'fa-mars-double';
    $icons[] = 'fa-mars-stroke';
    $icons[] = 'fa-mars-stroke-h';
    $icons[] = 'fa-mars-stroke-v';
    $icons[] = 'fa-mercury';
    $icons[] = 'fa-neuter';
    $icons[] = 'fa-transgender';
    $icons[] = 'fa-transgender-alt';
    $icons[] = 'fa-venus';
    $icons[] = 'fa-venus-double';
    $icons[] = 'fa-venus-mars';
    $icons[] = 'fa-file';
    $icons[] = 'fa-file-archive-o';
    $icons[] = 'fa-file-audio-o';
    $icons[] = 'fa-file-code-o';
    $icons[] = 'fa-file-excel-o';
    $icons[] = 'fa-file-image-o';
    $icons[] = 'fa-file-movie-o';
    $icons[] = 'fa-file-o';
    $icons[] = 'fa-file-pdf-o';
    $icons[] = 'fa-file-photo-o';
    $icons[] = 'fa-file-picture-o';
    $icons[] = 'fa-file-powerpoint-o';
    $icons[] = 'fa-file-sound-o';
    $icons[] = 'fa-file-text';
    $icons[] = 'fa-file-text-o';
    $icons[] = 'fa-file-video-o';
    $icons[] = 'fa-file-word-o';
    $icons[] = 'fa-file-zip-o';
    $icons[] = 'fa-check-square';
    $icons[] = 'fa-check-square-o';
    $icons[] = 'fa-circle';
    $icons[] = 'fa-circle-o';
    $icons[] = 'fa-dot-circle-o';
    $icons[] = 'fa-minus-square';
    $icons[] = 'fa-minus-square-o';
    $icons[] = 'fa-plus-square';
    $icons[] = 'fa-plus-square-o';
    $icons[] = 'fa-square';
    $icons[] = 'fa-square-o';
    $icons[] = 'fa-cc-amex';
    $icons[] = 'fa-cc-diners-club';
    $icons[] = 'fa-cc-discover';
    $icons[] = 'fa-cc-jcb';
    $icons[] = 'fa-cc-mastercard';
    $icons[] = 'fa-cc-paypal';
    $icons[] = 'fa-cc-stripe';
    $icons[] = 'fa-cc-visa';
    $icons[] = 'fa-credit-card';
    $icons[] = 'fa-google-wallet';
    $icons[] = 'fa-paypal';
    $icons[] = 'fa-area-chart';
    $icons[] = 'fa-bar-chart';
    $icons[] = 'fa-bar-chart-o';
    $icons[] = 'fa-line-chart';
    $icons[] = 'fa-pie-chart';
    $icons[] = 'fa-bitcoin';
    $icons[] = 'fa-btc';
    $icons[] = 'fa-cny';
    $icons[] = 'fa-dollar';
    $icons[] = 'fa-eur';
    $icons[] = 'fa-euro';
    $icons[] = 'fa-gbp';
    $icons[] = 'fa-gg';
    $icons[] = 'fa-gg-circle';
    $icons[] = 'fa-ils';
    $icons[] = 'fa-inr';
    $icons[] = 'fa-jpy';
    $icons[] = 'fa-krw';
    $icons[] = 'fa-money';
    $icons[] = 'fa-rmb';
    $icons[] = 'fa-rouble';
    $icons[] = 'fa-rub';
    $icons[] = 'fa-ruble';
    $icons[] = 'fa-rupee';
    $icons[] = 'fa-shekel';
    $icons[] = 'fa-sheqel';
    $icons[] = 'fa-try';
    $icons[] = 'fa-turkish-lira';
    $icons[] = 'fa-usd';
    $icons[] = 'fa-won';
    $icons[] = 'fa-yen';
    $icons[] = 'fa-align-center';
    $icons[] = 'fa-align-justify';
    $icons[] = 'fa-align-left';
    $icons[] = 'fa-align-right';
    $icons[] = 'fa-bold';
    $icons[] = 'fa-chain';
    $icons[] = 'fa-chain-broken';
    $icons[] = 'fa-clipboard';
    $icons[] = 'fa-columns';
    $icons[] = 'fa-copy';
    $icons[] = 'fa-cut';
    $icons[] = 'fa-dedent';
    $icons[] = 'fa-eraser';
    $icons[] = 'fa-file';
    $icons[] = 'fa-file-o';
    $icons[] = 'fa-file-text';
    $icons[] = 'fa-file-text-o';
    $icons[] = 'fa-files-o';
    $icons[] = 'fa-floppy-o';
    $icons[] = 'fa-font';
    $icons[] = 'fa-header';
    $icons[] = 'fa-indent';
    $icons[] = 'fa-italic';
    $icons[] = 'fa-link';
    $icons[] = 'fa-list';
    $icons[] = 'fa-list-alt';
    $icons[] = 'fa-list-ol';
    $icons[] = 'fa-list-ul';
    $icons[] = 'fa-outdent';
    $icons[] = 'fa-paperclip';
    $icons[] = 'fa-paragraph';
    $icons[] = 'fa-paste';
    $icons[] = 'fa-repeat';
    $icons[] = 'fa-rotate-left';
    $icons[] = 'fa-rotate-right';
    $icons[] = 'fa-save';
    $icons[] = 'fa-scissors';
    $icons[] = 'fa-strikethrough';
    $icons[] = 'fa-subscript';
    $icons[] = 'fa-superscript';
    $icons[] = 'fa-table';
    $icons[] = 'fa-text-height';
    $icons[] = 'fa-text-width';
    $icons[] = 'fa-th';
    $icons[] = 'fa-th-large';
    $icons[] = 'fa-th-list';
    $icons[] = 'fa-underline';
    $icons[] = 'fa-undo';
    $icons[] = 'fa-unlink';
    $icons[] = 'fa-angle-double-down';
    $icons[] = 'fa-angle-double-left';
    $icons[] = 'fa-angle-double-right';
    $icons[] = 'fa-angle-double-up';
    $icons[] = 'fa-angle-down';
    $icons[] = 'fa-angle-left';
    $icons[] = 'fa-angle-right';
    $icons[] = 'fa-angle-up';
    $icons[] = 'fa-arrow-circle-down';
    $icons[] = 'fa-arrow-circle-left';
    $icons[] = 'fa-arrow-circle-o-down';
    $icons[] = 'fa-arrow-circle-o-left';
    $icons[] = 'fa-arrow-circle-o-right';
    $icons[] = 'fa-arrow-circle-o-up';
    $icons[] = 'fa-arrow-circle-right';
    $icons[] = 'fa-arrow-circle-up';
    $icons[] = 'fa-arrow-down';
    $icons[] = 'fa-arrow-left';
    $icons[] = 'fa-arrow-right';
    $icons[] = 'fa-arrow-up';
    $icons[] = 'fa-arrows';
    $icons[] = 'fa-arrows-alt';
    $icons[] = 'fa-arrows-h';
    $icons[] = 'fa-arrows-v';
    $icons[] = 'fa-caret-down';
    $icons[] = 'fa-caret-left';
    $icons[] = 'fa-caret-right';
    $icons[] = 'fa-caret-square-o-down';
    $icons[] = 'fa-caret-square-o-left';
    $icons[] = 'fa-caret-square-o-right';
    $icons[] = 'fa-caret-square-o-up';
    $icons[] = 'fa-caret-up';
    $icons[] = 'fa-chevron-circle-down';
    $icons[] = 'fa-chevron-circle-left';
    $icons[] = 'fa-chevron-circle-right';
    $icons[] = 'fa-chevron-circle-up';
    $icons[] = 'fa-chevron-down';
    $icons[] = 'fa-chevron-left';
    $icons[] = 'fa-chevron-right';
    $icons[] = 'fa-chevron-up';
    $icons[] = 'fa-exchange';
    $icons[] = 'fa-hand-o-down';
    $icons[] = 'fa-hand-o-left';
    $icons[] = 'fa-hand-o-right';
    $icons[] = 'fa-hand-o-up';
    $icons[] = 'fa-long-arrow-down';
    $icons[] = 'fa-long-arrow-left';
    $icons[] = 'fa-long-arrow-right';
    $icons[] = 'fa-long-arrow-up';
    $icons[] = 'fa-toggle-down';
    $icons[] = 'fa-toggle-left';
    $icons[] = 'fa-toggle-right';
    $icons[] = 'fa-toggle-up';
    $icons[] = 'fa-arrows-alt';
    $icons[] = 'fa-backward';
    $icons[] = 'fa-compress';
    $icons[] = 'fa-eject';
    $icons[] = 'fa-expand';
    $icons[] = 'fa-fast-backward';
    $icons[] = 'fa-fast-forward';
    $icons[] = 'fa-forward';
    $icons[] = 'fa-pause';
    $icons[] = 'fa-pause-circle';
    $icons[] = 'fa-pause-circle-o';
    $icons[] = 'fa-play';
    $icons[] = 'fa-play-circle';
    $icons[] = 'fa-play-circle-o';
    $icons[] = 'fa-random';
    $icons[] = 'fa-step-backward';
    $icons[] = 'fa-step-forward';
    $icons[] = 'fa-stop';
    $icons[] = 'fa-stop-circle';
    $icons[] = 'fa-stop-circle-o';
    $icons[] = 'fa-youtube-play';
    $icons[] = 'fa-500px';
    $icons[] = 'fa-adn';
    $icons[] = 'fa-amazon';
    $icons[] = 'fa-android';
    $icons[] = 'fa-angellist';
    $icons[] = 'fa-apple';
    $icons[] = 'fa-behance';
    $icons[] = 'fa-behance-square';
    $icons[] = 'fa-bitbucket';
    $icons[] = 'fa-bitbucket-square';
    $icons[] = 'fa-bitcoin';
    $icons[] = 'fa-black-tie';
    $icons[] = 'fa-bluetooth';
    $icons[] = 'fa-btc';
    $icons[] = 'fa-buysellads';
    $icons[] = 'fa-cc-amex';
    $icons[] = 'fa-cc-diners-club';
    $icons[] = 'fa-cc-discover';
    $icons[] = 'fa-cc-jcb';
    $icons[] = 'fa-cc-mastercard';
    $icons[] = 'fa-cc-paypal';
    $icons[] = 'fa-cc-stripe';
    $icons[] = 'fa-cc-visa';
    $icons[] = 'fa-chrome';
    $icons[] = 'fa-codepen';
    $icons[] = 'fa-codiepie';
    $icons[] = 'fa-connectdevelop';
    $icons[] = 'fa-contao';
    $icons[] = 'fa-css3';
    $icons[] = 'fa-dashcube';
    $icons[] = 'fa-delicious';
    $icons[] = 'fa-deviantart';
    $icons[] = 'fa-digg';
    $icons[] = 'fa-dribbble';
    $icons[] = 'fa-dropbox';
    $icons[] = 'fa-drupal';
    $icons[] = 'fa-edge';
    $icons[] = 'fa-empire';
    $icons[] = 'fa-expeditedssl';
    $icons[] = 'fa-facebook';
    $icons[] = 'fa-facebook-f';
    $icons[] = 'fa-facebook-official';
    $icons[] = 'fa-facebook-square';
    $icons[] = 'fa-firefox';
    $icons[] = 'fa-flickr';
    $icons[] = 'fa-fonticons';
    $icons[] = 'fa-fort-awesome';
    $icons[] = 'fa-forumbee';
    $icons[] = 'fa-foursquare';
    $icons[] = 'fa-ge';
    $icons[] = 'fa-get-pocket';
    $icons[] = 'fa-gg';
    $icons[] = 'fa-gg-circle';
    $icons[] = 'fa-git';
    $icons[] = 'fa-git-square';
    $icons[] = 'fa-github';
    $icons[] = 'fa-github-alt';
    $icons[] = 'fa-github-square';
    $icons[] = 'fa-gittip';
    $icons[] = 'fa-google';
    $icons[] = 'fa-google-plus';
    $icons[] = 'fa-google-plus-square';
    $icons[] = 'fa-google-wallet';
    $icons[] = 'fa-gratipay';
    $icons[] = 'fa-hacker-news';
    $icons[] = 'fa-houzz';
    $icons[] = 'fa-html5';
    $icons[] = 'fa-instagram';
    $icons[] = 'fa-internet-explorer';
    $icons[] = 'fa-ioxhost';
    $icons[] = 'fa-joomla';
    $icons[] = 'fa-jsfiddle';
    $icons[] = 'fa-lastfm';
    $icons[] = 'fa-lastfm-square';
    $icons[] = 'fa-leanpub';
    $icons[] = 'fa-linkedin';
    $icons[] = 'fa-linkedin-square';
    $icons[] = 'fa-linux';
    $icons[] = 'fa-maxcdn';
    $icons[] = 'fa-meanpath';
    $icons[] = 'fa-medium';
    $icons[] = 'fa-mixcloud';
    $icons[] = 'fa-modx';
    $icons[] = 'fa-odnoklassniki';
    $icons[] = 'fa-odnoklassniki-square';
    $icons[] = 'fa-opencart';
    $icons[] = 'fa-openid';
    $icons[] = 'fa-opera';
    $icons[] = 'fa-optin-monster';
    $icons[] = 'fa-pagelines';
    $icons[] = 'fa-paypal';
    $icons[] = 'fa-pied-piper';
    $icons[] = 'fa-pied-piper-alt';
    $icons[] = 'fa-pinterest';
    $icons[] = 'fa-pinterest-p';
    $icons[] = 'fa-pinterest-square';
    $icons[] = 'fa-product-hunt';
    $icons[] = 'fa-qq';
    $icons[] = 'fa-ra';
    $icons[] = 'fa-rebel';
    $icons[] = 'fa-reddit';
    $icons[] = 'fa-reddit-alien';
    $icons[] = 'fa-reddit-square';
    $icons[] = 'fa-renren';
    $icons[] = 'fa-safari';
    $icons[] = 'fa-scribd';
    $icons[] = 'fa-sellsy';
    $icons[] = 'fa-share-alt';
    $icons[] = 'fa-share-alt-square';
    $icons[] = 'fa-shirtsinbulk';
    $icons[] = 'fa-simplybuilt';
    $icons[] = 'fa-skyatlas';
    $icons[] = 'fa-skype';
    $icons[] = 'fa-slack';
    $icons[] = 'fa-slideshare';
    $icons[] = 'fa-soundcloud';
    $icons[] = 'fa-spotify';
    $icons[] = 'fa-stack-exchange';
    $icons[] = 'fa-stack-overflow';
    $icons[] = 'fa-steam';
    $icons[] = 'fa-steam-square';
    $icons[] = 'fa-stumbleupon';
    $icons[] = 'fa-stumbleupon-circle';
    $icons[] = 'fa-tencent-weibo';
    $icons[] = 'fa-trello';
    $icons[] = 'fa-tripadvisor';
    $icons[] = 'fa-tumblr';
    $icons[] = 'fa-tumblr-square';
    $icons[] = 'fa-twitch';
    $icons[] = 'fa-twitter';
    $icons[] = 'fa-twitter-square';
    $icons[] = 'fa-usb';
    $icons[] = 'fa-viacoin';
    $icons[] = 'fa-vimeo';
    $icons[] = 'fa-vimeo-square';
    $icons[] = 'fa-vine';
    $icons[] = 'fa-vk';
    $icons[] = 'fa-wechat';
    $icons[] = 'fa-weibo';
    $icons[] = 'fa-weixin';
    $icons[] = 'fa-whatsapp';
    $icons[] = 'fa-wikipedia-w';
    $icons[] = 'fa-windows';
    $icons[] = 'fa-wordpress';
    $icons[] = 'fa-xing';
    $icons[] = 'fa-xing-square';
    $icons[] = 'fa-y-combinator';
    $icons[] = 'fa-y-combinator-square';
    $icons[] = 'fa-yahoo';
    $icons[] = 'fa-yc';
    $icons[] = 'fa-yc-square';
    $icons[] = 'fa-yelp';
    $icons[] = 'fa-youtube';
    $icons[] = 'fa-youtube-play';
    $icons[] = 'fa-youtube-square';
    $icons[] = 'fa-ambulance';
    $icons[] = 'fa-h-square';
    $icons[] = 'fa-heart';
    $icons[] = 'fa-heart-o';
    $icons[] = 'fa-heartbeat';
    $icons[] = 'fa-hospital-o';
    $icons[] = 'fa-medkit';
    $icons[] = 'fa-plus-square';
    $icons[] = 'fa-stethoscope';
    $icons[] = 'fa-user-md';
    $icons[] = 'fa-wheelchair';
    $icons[] = 'fa-assistive-listening-systems';
    $icons[] = 'fa-audio-description';
    $icons[] = 'fa-blind';
    $icons[] = 'fa-braille';
    $icons[] = 'fa-deaf';
    $icons[] = 'fa-envira';
    $icons[] = 'fa-first-order';
    $icons[] = 'fa-font-awesome';
    $icons[] = 'fa-gitlab';
    $icons[] = 'fa-glide-g';
    $icons[] = 'fa-glide';
    $icons[] = 'fa-google-plus-official';
    $icons[] = 'fa-instagram';
    $icons[] = 'fa-low-vision';
    $icons[] = 'fa-pied-piper';
    $icons[] = 'fa-question-circle-o';
    $icons[] = 'fa-sign-language';
    $icons[] = 'fa-snapchat-ghost';
    $icons[] = 'fa-snapchat-square';
    $icons[] = 'fa-snapchat';
    $icons[] = 'fa-themeisle';
    $icons[] = 'fa-universal-access';
    $icons[] = 'fa-viadeo-square';
    $icons[] = 'fa-viadeo';
    $icons[] = 'fa-volume-control-phone';
    $icons[] = 'fa-wheelchair-alt';
    $icons[] = 'fa-wpbeginner';
    $icons[] = 'fa-wpforms';
    $icons[] = 'fa-yoast';
    $icons[] = 'fa-address-book-o';
    $icons[] = 'fa-address-book';
    $icons[] = 'fa-address-card-o';
    $icons[] = 'fa-address-card';
    $icons[] = 'fa-bandcamp';
    $icons[] = 'fa-bath';
    $icons[] = 'fa-eercast';
    $icons[] = 'fa-envelope-open-o';
    $icons[] = 'fa-envelope-open';
    $icons[] = 'fa-etsy';
    $icons[] = 'fa-free-code-camp';
    $icons[] = 'fa-grav';
    $icons[] = 'fa-handshake-o';
    $icons[] = 'fa-id-badge';
    $icons[] = 'fa-id-card-o';
    $icons[] = 'fa-id-card';
    $icons[] = 'fa-imdb';
    $icons[] = 'fa-linode';
    $icons[] = 'fa-meetup';
    $icons[] = 'fa-microchip';
    $icons[] = 'fa-podcast';
    $icons[] = 'fa-quora';
    $icons[] = 'fa-ravelry';
    $icons[] = 'fa-shower';
    $icons[] = 'fa-snowflake-o';
    $icons[] = 'fa-superpowers';
    $icons[] = 'fa-telegram';
    $icons[] = 'fa-thermometer-empty';
    $icons[] = 'fa-thermometer-full';
    $icons[] = 'fa-thermometer-half';
    $icons[] = 'fa-thermometer-quarter';
    $icons[] = 'fa-thermometer-three-quarters';
    $icons[] = 'fa-thermometer';
    $icons[] = 'fa-user-circle-o';
    $icons[] = 'fa-user-circle';
    $icons[] = 'fa-user-o';
    $icons[] = 'fa-window-close-o';
    $icons[] = 'fa-window-close';
    $icons[] = 'fa-window-maximize';
    $icons[] = 'fa-window-minimize';
    $icons[] = 'fa-window-restore';
    $icons[] = 'fa-wpexplorer';

    return $icons;
}
