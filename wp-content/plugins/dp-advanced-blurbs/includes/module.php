<?php

if (class_exists('ET_Builder_Module')) {//IF Divi Theme activated

    class ET_Builder_Module_Advanced_Blurb extends ET_Builder_Module {

        function init() {
            $this->name = __('DP Advanced Blurb', 'et_builder');
            $this->slug = 'et_pb_advanced_blurb';
            $this->fb_support = true;
            $this->main_css_element = '%%order_class%%.et_pb_blurb';

            $this->whitelisted_fields = array(
                'title',
                'content_new',
                'url',
                'url_new_window',
                'use_icon',
                'font_icon',
                'icon_color',
                'use_circle',
                'circle_color',
                'use_circle_border',
                'circle_border_color',
                'icon_placement',
                'use_icon_font_size',
                'icon_font_size',
                'icon_font_size_tablet',
                'icon_font_size_phone',
                'image',
                'alt',
                'max_width',
                'max_width_tablet',
                'max_width_phone',
                'animation',
                'trigger',
                'background_layout',
                'text_orientation',
                'admin_label',
                'module_id',
                'module_class',
            );

            $et_accent_color = et_builder_accent_color();

            $this->fields_defaults = array(
                'url_new_window' => array('off'),
                'use_icon' => array('off'),
                'font_icon' => array('%%777%%', 'add_default_setting'),
                'icon_color' => array($et_accent_color, 'add_default_setting'),
                'use_circle' => array('off'),
                'circle_color' => array($et_accent_color, 'only_default_setting'),
                'use_circle_border' => array('off'),
                'circle_border_color' => array($et_accent_color, 'only_default_setting'),
                'icon_placement' => array('top'),
                'animation' => array('off'),
                'background_layout' => array('light'),
                'text_orientation' => array('center'),
                'use_icon_font_size' => array('off'),
                'trigger' => array('onload'),
            );

            $this->options_toggles = array(
                'general' => array(
                    'toggles' => array(
                        'main_content' => esc_html__('Text', 'et_builder'),
                        'link' => esc_html__('Link', 'et_builder'),
                        'image' => esc_html__('Image & Icon', 'et_builder'),
                    ),
                ),
                'advanced' => array(
                    'toggles' => array(
                        'icon_settings' => esc_html__('Image & Icon', 'et_builder'),
                        'text' => array(
                            'title' => esc_html__('Text', 'et_builder'),
                            'priority' => 49,
                        ),
                        'width' => array(
                            'title' => esc_html__('Sizing', 'et_builder'),
                            'priority' => 65,
                        ),
                    ),
                ),
                'custom_css' => array(
                    'toggles' => array(
                        'animation' => array(
                            'title' => esc_html__('Animation', 'et_builder'),
                            'priority' => 90,
                        ),
                    ),
                ),
            );

            $this->advanced_options = array(
                'fonts' => array(
                    'header' => array(
                        'label' => __('Header', 'et_builder'),
                        'css' => array(
                            'main' => "{$this->main_css_element} h4, {$this->main_css_element} h4 a",
                        ),
                    ),
                    'body' => array(
                        'label' => __('Body', 'et_builder'),
                        'css' => array(
                            'line_height' => "{$this->main_css_element} p",
                        ),
                    ),
                ),
                'background' => array(
                    'settings' => array(
                        'color' => 'alpha',
                    ),
                ),
                'border' => array(),
                'custom_margin_padding' => array(
                    'css' => array(
                        'important' => 'all',
                    ),
                ),
            );
            $this->custom_css_options = array(
                'blurb_image' => array(
                    'label' => __('Blurb Image', 'et_builder'),
                    'selector' => '.dp_pb_main_blurb_image',
                ),
                'blurb_title' => array(
                    'label' => __('Blurb Title', 'et_builder'),
                    'selector' => 'h4',
                ),
                'blurb_content' => array(
                    'label' => __('Blurb Content', 'et_builder'),
                    'selector' => '.dp_pb_blurb_content',
                ),
            );
        }

        function get_fields() {
            $et_accent_color = et_builder_accent_color();

            $image_icon_placement = array(
                'top' => __('Top', 'et_builder'),
            );

            if (!is_rtl()) {
                $image_icon_placement['left'] = __('Left', 'et_builder');
            } else {
                $image_icon_placement['right'] = __('Right', 'et_builder');
            }

            $fields = array(
                'title' => array(
                    'label' => __('Title', 'et_builder'),
                    'type' => 'text',
                    'option_category' => 'basic_option',
                    'description' => __('The title of your blurb will appear in bold below your blurb image.', 'et_builder'),
                    'tab_slug' => 'general',
                    'toggle_slug' => 'main_content',
                ),
                'content_new' => array(
                    'label' => __('Content', 'et_builder'),
                    'type' => 'tiny_mce',
                    'option_category' => 'basic_option',
                    'description' => __('Input the main text content for your module here.', 'et_builder'),
                    'tab_slug' => 'general',
                    'toggle_slug' => 'main_content',
                ),
                'url' => array(
                    'label' => __('Url', 'et_builder'),
                    'type' => 'text',
                    'option_category' => 'basic_option',
                    'description' => __('If you would like to make your blurb a link, input your destination URL here.', 'et_builder'),
                    'tab_slug' => 'general',
                    'toggle_slug' => 'link',
                ),
                'url_new_window' => array(
                    'label' => __('Url Opens', 'et_builder'),
                    'type' => 'select',
                    'option_category' => 'configuration',
                    'options' => array(
                        'off' => __('In The Same Window', 'et_builder'),
                        'on' => __('In The New Tab', 'et_builder'),
                    ),
                    'description' => __('Here you can choose whether or not your link opens in a new window', 'et_builder'),
                    'tab_slug' => 'general',
                    'toggle_slug' => 'link',
                ),
                'use_icon' => array(
                    'label' => __('Use Icon', 'et_builder'),
                    'type' => 'yes_no_button',
                    'option_category' => 'basic_option',
                    'options' => array(
                        'off' => __('No', 'et_builder'),
                        'on' => __('Yes', 'et_builder'),
                    ),
                    'affects' => array(
                        '#et_pb_font_icon',
                        '#et_pb_use_circle',
                        '#et_pb_icon_color',
                        '#et_pb_image',
                        '#et_pb_alt',
                    ),
                    'description' => __('Here you can choose whether icon set below should be used.', 'et_builder'),
                    'tab_slug' => 'general',
                    'toggle_slug' => 'image',
                ),
                'font_icon' => array(
                    'label' => __('Icon', 'et_builder'),
                    'type' => 'text',
                    'option_category' => 'basic_option',
                    'class' => array('et-pb-font-icon'),
                    'renderer' => 'et_pb_ab_get_font_icon_list',
                    'renderer_with_field' => true,
                    'description' => __('Choose an icon to display with your blurb.', 'et_builder'),
                    'depends_default' => true,
                    'tab_slug' => 'general',
                    'toggle_slug' => 'image',
                ),
                'icon_color' => array(
                    'label' => __('Icon Color', 'et_builder'),
                    'type' => 'color-alpha',
                    'description' => __('Here you can define a custom color for your icon.', 'et_builder'),
                    'depends_default' => true,
                    'tab_slug' => 'advanced',
                    'toggle_slug' => 'icon_settings',
                ),
                'use_circle' => array(
                    'label' => __('Circle Icon', 'et_builder'),
                    'type' => 'yes_no_button',
                    'option_category' => 'configuration',
                    'options' => array(
                        'off' => __('No', 'et_builder'),
                        'on' => __('Yes', 'et_builder'),
                    ),
                    'affects' => array(
                        '#et_pb_use_circle_border',
                        '#et_pb_circle_color',
                    ),
                    'description' => __('Here you can choose whether icon set above should display within a circle.', 'et_builder'),
                    'depends_default' => true,
                    'tab_slug' => 'advanced',
                    'toggle_slug' => 'icon_settings',
                ),
                'circle_color' => array(
                    'label' => __('Circle Color', 'et_builder'),
                    'type' => 'color',
                    'description' => __('Here you can define a custom color for the icon circle.', 'et_builder'),
                    'depends_default' => true,
                    'tab_slug' => 'advanced',
                    'toggle_slug' => 'icon_settings',
                ),
                'use_circle_border' => array(
                    'label' => __('Show Circle Border', 'et_builder'),
                    'type' => 'yes_no_button',
                    'option_category' => 'layout',
                    'options' => array(
                        'off' => __('No', 'et_builder'),
                        'on' => __('Yes', 'et_builder'),
                    ),
                    'affects' => array(
                        '#et_pb_circle_border_color',
                    ),
                    'description' => __('Here you can choose whether if the icon circle border should display.', 'et_builder'),
                    'depends_default' => true,
                    'tab_slug' => 'advanced',
                    'toggle_slug' => 'icon_settings',
                ),
                'circle_border_color' => array(
                    'label' => __('Circle Border Color', 'et_builder'),
                    'type' => 'color',
                    'description' => __('Here you can define a custom color for the icon circle border.', 'et_builder'),
                    'depends_default' => true,
                    'tab_slug' => 'advanced',
                    'toggle_slug' => 'icon_settings',
                ),
                'icon_placement' => array(
                    'label' => __('Image/Icon Placement', 'et_builder'),
                    'type' => 'select',
                    'option_category' => 'layout',
                    'options' => $image_icon_placement,
                    'description' => __('Here you can choose where to place the icon.', 'et_builder'),
                    'tab_slug' => 'advanced',
                    'toggle_slug' => 'icon_settings',
                ),
                'use_icon_font_size' => array(
                    'label' => __('Use Icon Font Size', 'et_builder'),
                    'type' => 'yes_no_button',
                    'option_category' => 'font_option',
                    'options' => array(
                        'off' => __('No', 'et_builder'),
                        'on' => __('Yes', 'et_builder'),
                    ),
                    'affects' => array(
                        '#et_pb_icon_font_size',
                    ),
                    'tab_slug' => 'advanced',
                    'toggle_slug' => 'icon_settings',
                ),
                'icon_font_size' => array(
                    'label' => __('Icon Font Size', 'et_builder'),
                    'type' => 'range',
                    'option_category' => 'font_option',
                    'tab_slug' => 'advanced',
                    'toggle_slug' => 'icon_settings',
                    'default' => '96px',
                    'range_settings' => array(
                        'min' => '1',
                        'max' => '120',
                        'step' => '1',
                    ),
                    'mobile_options' => true,
                    'depends_default' => true,
                ),
                'icon_font_size_tablet' => array(
                    'type' => 'skip',
                    'tab_slug' => 'advanced',
                    'toggle_slug' => 'icon_settings',
                ),
                'icon_font_size_phone' => array(
                    'type' => 'skip',
                    'tab_slug' => 'advanced',
                    'toggle_slug' => 'icon_settings',
                ),
                'max_width' => array(
                    'label' => __('Image Max Width', 'et_builder'),
                    'type' => 'text',
                    'option_category' => 'layout',
                    'tab_slug' => 'advanced',
                    'toggle_slug' => 'width',
                    'mobile_options' => true,
                    'validate_unit' => true,
                ),
                'max_width_tablet' => array(
                    'type' => 'skip',
                    'tab_slug' => 'advanced',
                    'toggle_slug' => 'width',
                ),
                'max_width_phone' => array(
                    'type' => 'skip',
                    'tab_slug' => 'advanced',
                    'toggle_slug' => 'width',
                ),
                'animation' => array(
                    'label' => __('Image/Icon Animation', 'et_builder'),
                    'type' => 'select',
                    'option_category' => 'configuration',
                    'options' => array(
                        'off' => __('No Animation', 'et_builder'),
                        'bounce' => __('Bounce', 'et_builder'),
                        'flash' => __('Flash', 'et_builder'),
                        'pulse' => __('Pulse', 'et_builder'),
                        'shake' => __('Shake', 'et_builder'),
                        'fa-spin' => __('Spin/Rotate', 'et_builder'),
                        'top' => __('Top To Bottom', 'et_builder'),
                        'left' => __('Left To Right', 'et_builder'),
                        'right' => __('Right To Left', 'et_builder'),
                        'bottom' => __('Bottom To Top', 'et_builder'),
                    ),
                    'description' => __('This controls the animation style of loading icon.', 'et_builder'),
                    'tab_slug' => 'custom_css',
                    'toggle_slug' => 'animation',
                ),
                'trigger' => array(
                    'label' => __('Trigger', 'et_builder'),
                    'type' => 'select',
                    'option_category' => 'configuration',
                    'options' => array(
                        'onload' => __('On Load', 'et_builder'),
                        'hover' => __('On Hover', 'et_builder'),
                        'infinite' => __('Infinite', 'et_builder'),
                    ),
                    'description' => __('This controls the icon effect on page load, on hover, or continuous.', 'et_builder'),
                    'tab_slug' => 'custom_css',
                    'toggle_slug' => 'animation',
                ),
                'background_layout' => array(
                    'label' => __('Text Color', 'et_builder'),
                    'type' => 'select',
                    'option_category' => 'color_option',
                    'options' => array(
                        'light' => __('Dark', 'et_builder'),
                        'dark' => __('Light', 'et_builder'),
                    ),
                    'description' => __('Here you can choose whether your text should be light or dark. If you are working with a dark background, then your text should be light. If your background is light, then your text should be set to dark.', 'et_builder'),
                    'tab_slug' => 'advanced',
                    'toggle_slug' => 'text',
                ),
                'text_orientation' => array(
                    'label' => __('Text Orientation', 'et_builder'),
                    'type' => 'select',
                    'option_category' => 'layout',
                    'options' => et_builder_get_text_orientation_options(),
                    'description' => __('This will control how your blurb text is aligned.', 'et_builder'),
                    'tab_slug' => 'advanced',
                    'toggle_slug' => 'text',
                ),
                'disabled_on' => array(
                    'label' => __('Disable on', 'et_builder'),
                    'type' => 'multiple_checkboxes',
                    'options' => array(
                        'phone' => __('Phone', 'et_builder'),
                        'tablet' => __('Tablet', 'et_builder'),
                        'desktop' => __('Desktop', 'et_builder'),
                    ),
                    'additional_att' => 'disable_on',
                    'option_category' => 'configuration',
                    'description' => __('This will disable the module on selected devices', 'et_builder'),
                ),
                'admin_label' => array(
                    'label' => __('Admin Label', 'et_builder'),
                    'type' => 'text',
                    'description' => __('This will change the label of the module in the builder for easy identification.', 'et_builder'),
                    'tab_slug' => 'general',
                    'toggle_slug' => 'admin_label',
                ),
                'module_id' => array(
                    'label' => __('CSS ID', 'et_builder'),
                    'type' => 'text',
                    'option_category' => 'configuration',
                    'tab_slug' => 'custom_css',
                    'toggle_slug' => 'classes',
                    'option_class' => 'et_pb_custom_css_regular',
                ),
                'module_class' => array(
                    'label' => __('CSS Class', 'et_builder'),
                    'type' => 'text',
                    'option_category' => 'configuration',
                    'tab_slug' => 'custom_css',
                    'toggle_slug' => 'classes',
                    'option_class' => 'et_pb_custom_css_regular',
                ),
            );
            return $fields;
        }

        function shortcode_callback($atts, $content = null, $function_name) {
            $module_id = $this->shortcode_atts['module_id'];
            $module_class = $this->shortcode_atts['module_class'];
            $title = $this->shortcode_atts['title'];
            $url = $this->shortcode_atts['url'];
            $url_new_window = $this->shortcode_atts['url_new_window'];
            $background_layout = $this->shortcode_atts['background_layout'];
            $text_orientation = $this->shortcode_atts['text_orientation'];
            $animation = $this->shortcode_atts['animation'];
            $trigger = $this->shortcode_atts['trigger'];
            $icon_placement = $this->shortcode_atts['icon_placement'];
            $font_icon = $this->shortcode_atts['font_icon'];
            $use_icon = $this->shortcode_atts['use_icon'];
            $use_circle = $this->shortcode_atts['use_circle'];
            $use_circle_border = $this->shortcode_atts['use_circle_border'];
            $icon_color = $this->shortcode_atts['icon_color'];
            $circle_color = $this->shortcode_atts['circle_color'];
            $circle_border_color = $this->shortcode_atts['circle_border_color'];
            $max_width = $this->shortcode_atts['max_width'];
            $max_width_tablet = $this->shortcode_atts['max_width_tablet'];
            $max_width_phone = $this->shortcode_atts['max_width_phone'];
            $use_icon_font_size = $this->shortcode_atts['use_icon_font_size'];
            $icon_font_size = $this->shortcode_atts['icon_font_size'];
            $icon_font_size_tablet = $this->shortcode_atts['icon_font_size_tablet'];
            if ($icon_font_size_tablet == '')
                $icon_font_size_tablet = $icon_font_size;
            $icon_font_size_phone = $this->shortcode_atts['icon_font_size_phone'];
            if ($icon_font_size_phone == '')
                $icon_font_size_phone = $icon_font_size;


            $module_class = ET_Builder_Element::add_module_order_class($module_class, $function_name);

            if ('off' !== $use_icon_font_size) {

                if ('' == $icon_font_size) {
                    $icon_font_size = '96px';
                }

                if ($use_circle_border == 'on' || $use_circle == 'on') {
                    $font_size_values = array(
                        'desktop' => intval($icon_font_size) / 2,
                        'tablet' => intval($icon_font_size_tablet) / 2,
                        'phone' => intval($icon_font_size_phone) / 2,
                    );
                    et_pb_generate_responsive_css($font_size_values, '%%order_class%% .dp-pb-icon.dp-pb-icon-circle', 'font-size', $function_name);
                } else {
                    $font_size_values = array(
                        'desktop' => $icon_font_size,
                        'tablet' => $icon_font_size_tablet,
                        'phone' => $icon_font_size_phone,
                    );
                    et_pb_generate_responsive_css($font_size_values, '%%order_class%% .dp-pb-icon', 'font-size', $function_name);
                }
            }

            if ('' !== $max_width_tablet || '' !== $max_width_phone || '' !== $max_width) {
                $max_width_values = array(
                    'desktop' => $max_width,
                    'tablet' => $max_width_tablet,
                    'phone' => $max_width_phone,
                );

                et_pb_generate_responsive_css($max_width_values, '%%order_class%% .dp_pb_main_blurb_image img', 'max-width', $function_name);
            }

            if (is_rtl() && 'left' === $text_orientation) {
                $text_orientation = 'right';
            }

            if (is_rtl() && 'left' === $icon_placement) {
                $icon_placement = 'right';
            }

            if ('' !== $title && '' !== $url) {
                $title = sprintf('<a href="%1$s"%3$s>%2$s</a>', esc_url($url), esc_html($title), ( 'on' === $url_new_window ? ' target="_blank"' : '')
                );
            }

            if ('' !== $title) {
                $title = "<h4>{$title}</h4>";
            }

            $image = "";

            if ('on' === $use_icon) {

                $icon_style = sprintf('color: %1$s;', esc_attr($icon_color));

                if ('on' === $use_circle) {
                    $icon_style .= sprintf(' background-color: %1$s;', esc_attr($circle_color));

                    if ('on' === $use_circle_border) {
                        $icon_style .= sprintf(' border-color: %1$s;', esc_attr($circle_border_color));
                    }
                }

                $icons = awesome_icons();

                $image = sprintf('<div id="db_icon_container"><i class="fa ' . $icons[str_replace('%', '', $font_icon)] . ' dp-pb-icon %2$s%3$s%4$s" style="%5$s"></i></div>', esc_attr(et_pb_process_font_icon($font_icon)), esc_attr("db_pb_animation_{$animation}"), ( 'on' === $use_circle ? ' dp-pb-icon-circle' : ''), ( 'on' === $use_circle && 'on' === $use_circle_border ? ' et-pb-icon-circle-border' : ''), $icon_style
                );
            }

            $image = sprintf(
                    '<div class="dp_pb_main_blurb_image">%1$s</div>', ( '' !== $url ? sprintf(
                            '<a href="%1$s"%3$s>%2$s</a>', esc_url($url), $image, ( 'on' === $url_new_window ? ' target="_blank"' : '')
                    ) : $image
                    )
            );

            $video_background = $this->video_background();
            $parallax_image_background = $this->get_parallax_image_background();

            $class = " et_pb_module et_pb_bg_layout_{$background_layout} et_pb_text_align_{$text_orientation}";

            $output = sprintf(
                    '<div%5$s class="dp-pb-' . $trigger . ' et_pb_blurb%4$s%6$s%7$s%8$s%10$s">
				%11$s
				%9$s
				<div class="dp_pb_blurb_content">
					%2$s
					<div class="dp_pb_blurb_container">
						%3$s
						%1$s
					</div>
				</div> <!-- .et_pb_blurb_content -->
			</div> <!-- .et_pb_blurb -->', $this->shortcode_content, $image, $title, esc_attr($class), ( '' !== $module_id ? sprintf(' id="%1$s"', esc_attr($module_id)) : ''), ( '' !== $module_class ? sprintf(' %1$s', esc_attr($module_class)) : ''), sprintf(' dp_pb_blurb_position_%1$s', esc_attr($icon_placement)), '' !== $video_background ? ' et_pb_section_video et_pb_preload' : '', $video_background, '' !== $parallax_image_background ? ' et_pb_section_parallax' : '', $parallax_image_background
            );

            return $output;
        }

    }

    new ET_Builder_Module_Advanced_Blurb;
}//IF Divi Theme activated
?>