<?php
/**
 * Plugin Name: DP Advanced Blurbs
 * Plugin URI: http://www.diviplugins.com/advanced-blurbs-plugin/
 * Description: Creates an advanced blurb module that replaces the default Divi icons with the Font Awesome collection of icons and adds new animations.
 * Version: 1.1.4
 * Author: DiviPlugins
 * Author URI: http://www.diviplugins.com
 * */
// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}

define('DPAB_PLUGIN_NAME', 'dp_ab');
define('DPAB_PLUGIN_VERSION', '1.1.4');
define('DPAB_PLUGIN_DIR', WP_PLUGIN_DIR . '/' . basename(dirname(__FILE__)));
define('DPAB_STORE_URL', 'https://diviplugins.com/');
define('DPAB_ITEM_NAME', 'Advanced Blurbs');
define('DPAB_ITEM_ID', '4547');
define('DPAB_PLUGIN_LICENSE_PAGE', 'dp_ab_license');

if (!class_exists('DP_AB_Plugin_Updater')) {
    require_once DPAB_PLUGIN_DIR . '/dp-ab-plugin-updater.php';
}

if (get_option('dp_ab_license_status') !== 'valid') {
    add_action('admin_notices', 'dp_ab_admin_notice_activation');
}

add_action('et_builder_ready', 'dp_ab_get_the_modules');

function dp_ab_get_the_modules() {
    require 'includes/functions.php';

    if (dp_ab_get_divi_version() >= 3045) {
        require 'includes/module.php';
    } else {
        require 'includes/module_45.php';
    }
}

// REGISTER ADMIN STYLES
function register_advanced_blurb_admin_styles() {
    wp_register_style('advanced-blurb-css', plugin_dir_url(__FILE__) . 'admin/css/style.css', false, '1.0.0');
    wp_enqueue_style('advanced-blurb-css');

    wp_register_style('advanced-blurb-icons', plugin_dir_url(__FILE__) . 'css/font-awesome.css', false, '1.0.1');
    wp_enqueue_style('advanced-blurb-icons');

    wp_register_style('advanced-blurb-animation', plugin_dir_url(__FILE__) . 'css/animate.css', false, '1.0.0');
    wp_enqueue_style('advanced-blurb-animation');
}

add_action('admin_enqueue_scripts', 'register_advanced_blurb_admin_styles');

// REGISTER STYLES
function register_advanced_blurb_module_styles() {
    wp_register_style('advanced-blurb-css', plugin_dir_url(__FILE__) . 'css/style.css', false, '1.0.0');
    wp_enqueue_style('advanced-blurb-css');

    wp_register_style('advanced-blurb-icons', plugin_dir_url(__FILE__) . 'css/font-awesome.css', false, '1.0.1');
    wp_enqueue_style('advanced-blurb-icons');

    wp_register_style('advanced-blurb-animation', plugin_dir_url(__FILE__) . 'css/animate.css', false, '1.0.0');
    wp_enqueue_style('advanced-blurb-animation');

    wp_enqueue_script('advanced-blurb-js', plugin_dir_url(__FILE__) . 'js/advanced-blurb.js', array('jquery'));
}

add_action('wp_enqueue_scripts', 'register_advanced_blurb_module_styles');

if (!function_exists('dp_ab_get_divi_version')) {

    function dp_ab_get_divi_version() {
        $my_theme = wp_get_theme('Divi');
        return str_replace('.', '', $my_theme->get('Version'));
    }

}
/*
 * Activation Functions
 */

function dp_ab_plugin_updater() {

    // retrieve our license key from the DB
    $license_key = trim(get_option('dp_ab_license_key'));

    // setup the updater
    $dp_ab_updater = new dp_ab_Plugin_Updater(DPAB_STORE_URL, __FILE__, array(
        'version' => DPAB_PLUGIN_VERSION, // current version number
        'license' => $license_key, // license key (used get_option above to retrieve from DB)
        'item_id' => DPAB_ITEM_ID, // id of this plugin
        'item_name' => DPAB_ITEM_NAME, // name of this plugin
        'author' => 'Divi Plugins', // author of this plugin
        'beta' => false
            )
    );
}

add_action('admin_init', 'dp_ab_plugin_updater', 0);


/* * **********************************
 * the code below is just a standard
 * options page. Substitute with
 * your own.
 * *********************************** */

function dp_ab_license_menu() {
    add_plugins_page('Plugin License', 'AB License', 'manage_options', DPAB_PLUGIN_LICENSE_PAGE, 'dp_ab_license_page');
}

add_action('admin_menu', 'dp_ab_license_menu');

function dp_ab_license_page() {
    $license = get_option('dp_ab_license_key');
    $status = get_option('dp_ab_license_status');
    ?>
    <div class="wrap">
        <h2><?php _e('Advanced Blurbs License Options'); ?></h2>
        <form method="post" action="options.php">

            <?php settings_fields('dp_ab_license'); ?>

            <table class="form-table">
                <tbody>
                    <tr valign="top">
                        <th scope="row" valign="top">
                            <?php _e('License Key', DPAB_PLUGIN_NAME); ?>
                        </th>
                        <td>
                            <input id="dp_ab_license_key" name="dp_ab_license_key" type="text" class="regular-text" value="<?php esc_attr_e($license); ?>" />
                            <label class="description" for="dp_ab_license_key"><?php _e('Enter your license key', DPAB_PLUGIN_NAME); ?></label>
                        </td>
                    </tr>
                    <?php if (false !== $license) { ?>
                        <tr valign="top">
                            <th scope="row" valign="top">
                                <?php _e('Activate License', DPAB_PLUGIN_NAME); ?>
                            </th>
                            <td>
                                <?php if ($status !== false && $status == 'valid') { ?>
                                    <span style="color:green; font-weight: bold; font-size: 18px; vertical-align: bottom;"><?php _e('Active', DPAB_PLUGIN_NAME); ?></span>
                                    <?php wp_nonce_field('dp_ab_license_nonce', 'dp_ab_license_nonce'); ?>
                                    <input type="submit" class="button-secondary" name="dp_ab_license_deactivate" value="<?php _e('Deactivate License', DPAB_PLUGIN_NAME); ?>"/>
                                    <?php
                                } else {
                                    wp_nonce_field('dp_ab_license_nonce', 'dp_ab_license_nonce');
                                    ?>
                                    <input type="submit" class="button-secondary" name="dp_ab_license_activate" value="<?php _e('Activate License', DPAB_PLUGIN_NAME); ?>"/>
                                <?php } ?>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <?php submit_button(); ?>

        </form>
    </div>
    <?php
}

function dp_ab_register_option() {
    // creates our settings in the options table
    register_setting('dp_ab_license', 'dp_ab_license_key', 'dp_ab_sanitize_license');
}

add_action('admin_init', 'dp_ab_register_option');

function dp_ab_sanitize_license($new) {
    $old = get_option('dp_ab_license_key');
    if ($old && $old != $new) {
        delete_option('dp_ab_license_status'); // new license has been entered, so must reactivate
    }
    return $new;
}

/* * **********************************
 * this illustrates how to activate
 * a license key
 * *********************************** */

function dp_ab_activate_license() {

    // listen for our activate button to be clicked
    if (isset($_POST['dp_ab_license_activate'])) {

        // run a quick security check
        if (!check_admin_referer('dp_ab_license_nonce', 'dp_ab_license_nonce')) {
            return; // get out if we didn't click the Activate button
        }

        // retrieve the license from the database
        $license = trim(get_option('dp_ab_license_key'));

        // data to send in our API request
        $api_params = array(
            'edd_action' => 'activate_license',
            'license' => $license,
            'item_name' => urlencode(DPAB_ITEM_NAME), // the name of our product in EDD
            'url' => home_url()
        );

        // Call the custom API.
        $response = wp_remote_post(DPAB_STORE_URL, array('timeout' => 15, 'sslverify' => false, 'body' => $api_params));

        // make sure the response came back okay
        if (is_wp_error($response) || 200 !== wp_remote_retrieve_response_code($response)) {

            if (is_wp_error($response)) {
                $message = $response->get_error_message();
            } else {
                $message = __('An error occurred, please try again.');
            }
        } else {
            $license_data = json_decode(wp_remote_retrieve_body($response));
            if (false === $license_data->success) {
                switch ($license_data->error) {
                    case 'expired' :
                        $message = sprintf(
                                __('Your license key expired on %s.', DPAB_PLUGIN_NAME), date_i18n(get_option('date_format'), strtotime($license_data->expires, current_time('timestamp')))
                        );
                        break;
                    case 'revoked' :
                        $message = __('Your license key has been disabled.', DPAB_PLUGIN_NAME);
                        break;
                    case 'missing' :
                        $message = __('Invalid license.', DPAB_PLUGIN_NAME);
                        break;
                    case 'invalid' :
                    case 'site_inactive' :
                        $message = __('Your license is not active for this URL.', DPAB_PLUGIN_NAME);
                        break;
                    case 'item_name_mismatch' :
                        $message = sprintf(__('This appears to be an invalid license key for %s.', DPAB_PLUGIN_NAME), DPAB_ITEM_NAME);
                        break;
                    case 'no_activations_left':
                        $message = __('Your license key has reached its activation limit.', DPAB_PLUGIN_NAME);
                        break;
                    default :
                        $message = __('An error occurred, please try again.', DPAB_PLUGIN_NAME);
                        break;
                }
            }
        }

        // Check if anything passed on a message constituting a failure
        if (!empty($message)) {
            $base_url = admin_url('plugins.php?page=' . DPAB_PLUGIN_LICENSE_PAGE);
            $redirect = add_query_arg(array('sl_activation' => 'false', 'message' => urlencode($message)), $base_url);

            wp_redirect($redirect);
            exit();
        }

        // $license_data->license will be either "valid" or "invalid"

        update_option('dp_ab_license_status', $license_data->license);
        wp_redirect(admin_url('plugins.php?page=' . DPAB_PLUGIN_LICENSE_PAGE));
        exit();
    }
}

add_action('admin_init', 'dp_ab_activate_license');


/* * *********************************************
 * Illustrates how to deactivate a license key.
 * This will decrease the site count
 * ********************************************* */

function dp_ab_deactivate_license() {

    // listen for our activate button to be clicked
    if (isset($_POST['dp_ab_license_deactivate'])) {

        // run a quick security check
        if (!check_admin_referer('dp_ab_license_nonce', 'dp_ab_license_nonce')) {
            return; // get out if we didn't click the Activate button
        }

        // retrieve the license from the database
        $license = trim(get_option('dp_ab_license_key'));

        // data to send in our API request
        $api_params = array(
            'edd_action' => 'deactivate_license',
            'license' => $license,
            'item_name' => urlencode(DPAB_ITEM_NAME), // the name of our product in EDD
            'url' => home_url()
        );

        // Call the custom API.
        $response = wp_remote_post(DPAB_STORE_URL, array('timeout' => 15, 'sslverify' => false, 'body' => $api_params));

        // make sure the response came back okay
        if (is_wp_error($response) || 200 !== wp_remote_retrieve_response_code($response)) {

            if (is_wp_error($response)) {
                $message = $response->get_error_message();
            } else {
                $message = __('An error occurred, please try again.');
            }

            $base_url = admin_url('plugins.php?page=' . DPAB_PLUGIN_LICENSE_PAGE);
            $redirect = add_query_arg(array('sl_activation' => 'false', 'message' => urlencode($message)), $base_url);

            wp_redirect($redirect);
            exit();
        }

        // decode the license data
        $license_data = json_decode(wp_remote_retrieve_body($response));

        // $license_data->license will be either "deactivated" or "failed"
        if ($license_data->license == 'deactivated') {
            delete_option('dp_ab_license_status');
        }

        wp_redirect(admin_url('plugins.php?page=' . DPAB_PLUGIN_LICENSE_PAGE));
        exit();
    }
}

add_action('admin_init', 'dp_ab_deactivate_license');

/**
 * This is a means of catching errors from the activation method above and displaying it to the customer
 */
function dp_ab_admin_notices() {
    if (isset($_GET['sl_activation']) && !empty($_GET['message']) && $_GET['page'] === 'dp_ab_license') {
        switch ($_GET['sl_activation']) {
            case 'false':
                $message = urldecode($_GET['message']);
                ?>
                <div class="notice notice-error is-dismissible">
                    <p><?php echo $message; ?></p>
                </div>
                <?php
                break;
            case 'true':
            default:
                // Developers can put a custom success message here for when activation is successful if they way.
                ?>
                <?php
                break;
        }
    }
}

add_action('admin_notices', 'dp_ab_admin_notices');

function dp_ab_admin_notice_activation() {
    ?>
    <div class="notice notice-info is-dismissible">
        <p><?php _e('Please activate your Advanced Blurbs license.', DPAB_PLUGIN_NAME); ?> <a href="plugins.php?page=<?php echo DPAB_PLUGIN_LICENSE_PAGE; ?>"><?php _e('AB Activation Page', DPAB_PLUGIN_NAME); ?></a></p>
    </div>
    <?php
}
?>
