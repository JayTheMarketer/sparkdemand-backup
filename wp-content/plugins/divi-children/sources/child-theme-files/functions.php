<?php
/**
 * Functions - Child theme custom functions
 */


/*************************************************************************************************************************
****************************** CAUTION: do not remove or edit anything within this section ******************************/

/**
 * Enqueue styles for child theme.
 * Do not remove this, or your child theme will not work unless you include a @import rule in your child stylesheet.
 */
function dce_enqueue_styles() {
    wp_enqueue_style( 'divi-parent-styles', get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-theme-styles', get_stylesheet_directory_uri() .'/style.css', array( 'divi-parent-styles' ) );
}
add_action( 'wp_enqueue_scripts', 'dce_enqueue_styles' );

/**
 * Makes the Divi Children Engine available for the child theme.
 * Do not remove this, or you will lose all the customization capabilities created by Divi Children Engine.
 */
require_once('divi-children-engine/divi_children_engine.php');

/***********************************************************************************************************************/


/*- You can include any custom code for your child theme below this line -*/


?>