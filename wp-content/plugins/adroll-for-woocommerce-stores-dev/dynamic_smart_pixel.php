<script type="text/javascript">
adroll_adv_id = "<?php echo $adv_id ?>";
adroll_pix_id = "<?php echo $pix_id ?>";

<?php
global $woocommerce, $wp, $product;

$is_woocommerce_3 = version_compare($woocommerce->version, "3.0", ">=");

$adroll_query_vars = $wp->query_vars;
if (isset($adroll_query_vars["order-received"])) {
    $order_id = $adroll_query_vars["order-received"];
    $order = wc_get_order($order_id);
}
?>

<?php if (isset($order)): ?>
    adroll_conversion_value = "<?php echo $order->get_total(); ?>";
    adroll_currency = "<?php echo method_exists($order, 'get_currency') ? $order->get_currency() : $order->get_order_currency(); ?>";
    adroll_email = "<?php echo $is_woocommerce_3 ? $order->get_billing_email() : $order->billing_email; ?>";
    adroll_custom_data = {
        "ORDER_ID": "<?php echo $is_woocommerce_3 ? $order->get_id() : $order->id; ?>",
        "USER_ID": "<?php echo $is_woocommerce_3 ? $order->get_customer_id() : $order->customer_user; ?>"
    };
    adroll_checkout_ids = [
    <?php
        echo implode(', ', array_map(function($item) {
            return '"' . $item['product_id'] . '"';
        }, $order->get_items()));
    ?>
    ];
<?php endif; ?>

<?php if (isset($product)): ?>
    adroll_product_id = "<?php echo $product->get_id(); ?>";
<?php endif; ?>

var oldonload = window.onload;
window.onload = function(){
    __adroll_loaded=true;
    var scr = document.createElement("script");
    var host = (("https:" == document.location.protocol) ? "https://s.adroll.com" : "http://a.adroll.com");
    scr.setAttribute('async', 'true');
    scr.type = "text/javascript";
    scr.src = host + "/j/roundtrip.js";
    ((document.getElementsByTagName('head') || [null])[0] ||
    document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
    if(oldonload){oldonload()}
}();
</script>
