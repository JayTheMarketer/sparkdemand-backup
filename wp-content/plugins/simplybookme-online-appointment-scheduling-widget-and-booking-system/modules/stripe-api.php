<?php
/**
 * User: vetal
 * Date: 14.01.16
 * Time: 12:06
 */



include_once SIMPLYBOOK_DIR.'/class/stripe.php';

\Stripe\Stripe::setApiKey($simplybook_cfg['stripe_api_secret_key']);

//print_r($simplybook_cfg);
$method = sanitize_text_field($_REQUEST['method']); //sanitized
$params = ( isset( $_REQUEST['params'] ) && ! empty( $_REQUEST['params'] ) ) ?  (array)$_REQUEST['params'] : array();

$params = array_map_recursive( 'esc_attr', $params ); //sanitized

try {
	// Use Stripe's library to make requests...

	switch($method){
		case 'charge':

			$res_token = $params[0];
			if(!is_array($params[1])){
				break;
			}
			//$bookings = array_map ('intval', $params[1]);
			list($buyType, $bookings) = sb_get_api_type($params[1]);
			$cart_res = (array)simplybook_get_api('getBookingCart', $params[1]);

			$charge = \Stripe\Charge::create(array(
				"amount" => $cart_res["amount"]*100, // amount in cents, again
				"currency" => $cart_res["currency"],
				"source" => $res_token,
				"capture" => true,
				"description" => "From: " .$_SESSION['book_params'][4]['name']. ". " . $buyType . " IDs: ".implode(',',$bookings)
			));

			if($simplybook_cfg['stripe_log']){
				simplybook_save_log('stripe_'.date('d.m.y H:i:s'), json_encode(array($params, $cart_res, $charge)) );
			}

			echo json_encode(simplybook_get_api('confirmBookingCart', $cart_res, 'Stripe', $simplybook_cfg['api_secret_key'], $buyType));
			break;
	}



} catch(\Stripe\Error\Card $e) {
	// Since it's a decline, \Stripe\Error\Card will be caught
	$body = $e->getJsonBody();
	$err  = $body['error'];
/*
	print('Status is:' . $e->getHttpStatus() . "\n");
	print('Type is:' . $err['type'] . "\n");
	print('Code is:' . $err['code'] . "\n");
	// param is '' in this case
	print('Param is:' . $err['param'] . "\n");
	print('Message is:' . $err['message'] . "\n");*/

	echo '{"error":{"code":-9999,"message":"Status is: '. $e->getHttpStatus(). ' Message is:' . $err['message'] . ' ","data":null},"id":"1"}';
} catch (\Stripe\Error\RateLimit $e) {
	// Too many requests made to the API too quickly
	echo '{"error":{"code":-9999,"message":"Too many requests made to the API too quickly.","data":null},"id":"1"}';
} catch (\Stripe\Error\InvalidRequest $e) {
	// Invalid parameters were supplied to Stripe's API
	echo '{"error":{"code":-9999,"message":"Invalid parameters were supplied to Stripe\'s API","data":null},"id":"1"}';
} catch (\Stripe\Error\Authentication $e) {
	// Authentication with Stripe's API failed
	// (maybe you changed API keys recently)
	echo '{"error":{"code":-9999,"message":"Authentication with Stripe\'s API failed (incorrect API keys )","data":null},"id":"1"}';
} catch (\Stripe\Error\ApiConnection $e) {
	// Network communication with Stripe failed
	echo '{"error":{"code":-9999,"message":"Network communication with Stripe failed.","data":null},"id":"1"}';
} catch (\Stripe\Error\Base $e) {
	// Display a very generic error to the user, and maybe send
	// yourself an email
	echo '{"error":{"code":-9999,"message":"Unknown error. The payment is not passed.","data":null},"id":"1"}';
}
catch (Exception $e) {
	//var_dump($e, $e->getJsonBody());
	// Something else happened, completely unrelated to Stripe
	echo '{"error":{"code":-9999,"message":"Something else happened, completely unrelated to Stripe","data":null},"id":"1"}';
}



?>