<?php


function capi_sb_apiError(){
	$dns = dns_get_record("user-api.simplybook.me", DNS_A);
	$pageDocument = @file_get_contents('https://user-api.simplybook.me');
	$pageDocument2 = @curl_load('https://user-api.simplybook.me');
	$errMess = "Seems the method cannot be found on your server. Possible reasons may include: PHP option 'allow_url_fopen' is not enabled, PHP CURL is not enabled. Please ask your hoster for support regarding absence of required methods. ";

	if ( !isset($dns[0]) || strlen($dns[0]['ip']) < 3) {
		$err = ' We find error: DNS not worked';
	} else
	if( !ini_get('allow_url_fopen') ) {
		$err = ' We find error: PHP option \'allow_url_fopen\' is disabled ';
	} else
	if( !_is_curl_installed() ) {
		$err = ' We find error: PHP CURL is disabled ';
	} else
	if( $pageDocument === false ) {
		$err = ' We find error: PHP function \'file_get_contents\' not worked ';
	} else
	if( strlen($pageDocument2) < 10 ) {
		$err = ' We find error: PHP CURL not worked';
	} else {
		$err = '';
	}
	throw new Exception('{"error":{"code":-32601,"message":"' . $errMess . ' ' . $err . ' ","data":null},"id":"1"}');
}

function capi_sb_getAllList($client, $method, $params){
	$buffmethods = array();
	$buffparams = array();

	$list_arr = array(
		"getLocationsList"  =>  array(true),
		"getCategoriesList" =>  array(),
		"getEventList"  =>  array(),
		"getUnitList"   =>  array(),
		"getCompanyParam"   =>  array('max_group_bookings')
	);

	$pl_list = array('any_unit','location',	'event_category', 'group_booking');
	$pl_status = $client->__call( 'getPluginStatuses', array($pl_list) );

	if(!$pl_status->location){
		unset($list_arr['getLocationsList']);
	}

	if(!$pl_status->event_category){
		unset($list_arr['getCategoriesList']);
	}

	if(!$pl_status->group_booking){
		unset($list_arr['getCompanyParam']);
	}

	//print_r($list_arr);
	$res = array();

	foreach($list_arr as $curr_method=>$curr_params){
		$key = md5(serialize($curr_params));
		$res[$curr_method] = get_transient( 'simplybook_'.$curr_method );

		if ( !isset($res[$curr_method][$key]) ) {
			array_push($buffmethods, $curr_method);
			array_push($buffparams, $curr_params);
			//$res[$curr_method] =  $client->__call( $curr_method, $curr_params ) ;
			//set_transient( 'simplybook_'.$curr_method, array($key => $res[$curr_method]), 24 * HOUR_IN_SECONDS );
		} else {
			$res[$curr_method] = $res[$curr_method][$key];
		}
	}

	if(count($buffmethods)>0){
		$newres = $client->__call( $buffmethods, $buffparams );

		foreach($newres as $curr_method=>$curr_res){
			$res[$curr_method] =  $curr_res;
			$key = md5(serialize($list_arr[$curr_method]));
			set_transient( 'simplybook_'.$curr_method, array($key => $res[$curr_method]), 24 * HOUR_IN_SECONDS );
		}
	}

	return $res;
}


function capi_sb_getClientTimezoneOffset($client, $method, $params){
	$company_timezone = get_transient( 'simplybook_company_timezone');

	if ( !$company_timezone ) {
		$company_timezone = $client->__call('getCompanyParam',array('company_timezone'));
		set_transient( 'simplybook_company_timezone', $company_timezone, 24 * HOUR_IN_SECONDS );
	}

	$company_dtz = new DateTimeZone($company_timezone);
	$company_dt = new DateTime("now", $company_dtz);

	$client_offset = (int)$params[0]; //in minutes
	$company_offset = $company_dtz->getOffset($company_dt)/60; //in minutes

	return array(
		'company_timezone' => $company_timezone,
		'company_offset' => $company_offset,
		'client_offset' => $client_offset,
		'offset' => $company_offset - $client_offset
	);
}

function capi_sb_getBookingCart($client, $method, $params, $simplybook_cfg){
	list($type, $ids) = sb_get_api_type($params[0]);

	switch ($type){
		case 'membership':
			$data =  (array)capi_sb_makeMembershipPayment($client, 'makeMembershipPayment', array($ids[0]), $simplybook_cfg);
			$data = array_change_keys($data, // hack to replace membership payment keys to booking cart keys
				array(
					'payed_amount' => 'amount',
					'payment_hash' => 'cart_hash',
					'id' => 'cart_id'
				));
			$data['amount'] = floatval($data['amount']);

			return $data;
			break;

		default:
			return $client->__call($method, array($ids));
			break;
	}
}

function capi_sb_confirmBookingCart($client, $method, $params, $simplybook_cfg){
	list($cart_info, $payment_system, $secret_key, $type) = $params;

	if(!$cart_info || !$payment_system || !$secret_key){
		return false;
	}

	if(!$type){
		$type = 'booking';
	}

	$cart_info = array_change_keys($cart_info, // hack to replace membership payment keys to booking cart keys
		array(
			'payed_amount' => 'amount',
			'payment_hash' => 'cart_hash',
			'id' => 'cart_id'
		)
	);

	$sign = md5($cart_info['cart_id'] .  $cart_info['cart_hash']. $secret_key);

	switch ($type){
		case 'membership':
			return $client->__call('confirmMembershipPayment', array( $cart_info['cart_id'], $payment_system, $sign) );
			break;

		default:
			return $client->__call('confirmBookingCart', array( $cart_info['cart_id'], $payment_system, $sign) );
			break;
	}
}


function capi_sb_confirmDelayPayment($client, $method, $params, $simplybook_cfg){
	$allow_delay = $client->__call('getCompanyParam',array('allow_delay_payment'));

	if($allow_delay){
		//$bookings = array_map ('intval', $params[0]);
		list($type, $ids) = sb_get_api_type($params[0]);

		switch ($type){
			case 'membership':
				return capi_sb_membership_confirmDelayPayment($client, $ids, $simplybook_cfg);
				break;

			default:
				return capi_sb_booking_confirmDelayPayment($client, $ids, $simplybook_cfg);
				break;
		}
	} else{
		return false;
	}
}

function capi_sb_booking_confirmDelayPayment($client, $ids, $simplybook_cfg){
	$cart_res = (array)$client->__call('getBookingCart', array($ids));
	$sign = md5($cart_res['cart_id'] .  $cart_res['cart_hash']. $simplybook_cfg['api_secret_key']);
	return $client->__call('confirmBookingCart', array( $cart_res['cart_id'], 'delay', $sign) );
}

function capi_sb_membership_confirmDelayPayment($client, $ids, $simplybook_cfg){
	$payment_info = (array)capi_sb_makeMembershipPayment($client, 'makeMembershipPayment', array($ids[0]), $simplybook_cfg);
	$sign = md5($payment_info['id'] .  $payment_info['payment_hash']. $simplybook_cfg['api_secret_key']);
	return $client->__call('confirmMembershipPayment', array( $payment_info['id'], 'delay', $sign) );
}

function capi_sb_cancelBooking($client, $method, $params, $simplybook_cfg){
	$booking_id = (int)$params[0];
	$booking_hash = $params[1];

	if($booking_id && $booking_hash){
		$sign = md5($booking_id . $booking_hash . $simplybook_cfg['api_secret_key']);
		return $client->__call('cancelBooking',array($booking_id, $sign));

	} else {
		$in_book = false;
		$bookings = json_decode($_SESSION['book'])->bookings;

		foreach($bookings as $el){
			if($booking_id == (int)$el->id){
				$in_book = true;
				$sign = md5($booking_id . $el->hash . $simplybook_cfg['api_secret_key']);
				return $client->__call('cancelBooking',array($booking_id, $sign));
			}
		}

		if(!$in_book){
			return false;
		}
	}
}

function capi_sb_saveToSession($method, $params){
	if(!session_id()) {
		session_start();
	}
	$sessionId = session_id();
	foreach($params as $param){
		foreach($param as $key=>$val){
			if(isset($key) && is_string($key) && isset($val)){
				set_transient( $sessionId . '_' . $key, $val, 7*24 * HOUR_IN_SECONDS );
			}
		}
	}
	return true;
}

function capi_sb_getFromSession($method, $params){
	if(!session_id()) {
		session_start();
	}
	$sessionId = session_id();
	$result = array();

	foreach($params as $key){
		if(isset($key) && is_string($key)){
			$result[$key] = get_transient($sessionId . '_' . $key);
		}
	}
	return $result;
}

function capi_sb_book($client, $method, $params, $simplybook_cfg){
	$clientData = &$params[4];
	$clientInfo = capi_sb_getFromSession('getFromSession', array('user_data'));

	if(isset($clientData['client_hash'])){
		$clientData['client_sign'] = capi_sb_get_client_sign($simplybook_cfg);
		unset( $clientData['client_hash'] );
	}

	$result = $client->__call( $method, $params );

	if( $clientInfo && $clientInfo['user_data'] && $clientInfo['user_data']['id']){
		$clientData = array_merge($clientData, $clientInfo['user_data']);
	}

	$_SESSION['book'] = json_encode($result);
	$_SESSION['book_params'] = $params;

	return $result;
}

function capi_sb_modifyClientInfo($client, $method, $params, $simplybook_cfg){
	$clientInfo = capi_sb_get_client_info();

	$result = $client->__call( $method, array(
		$clientInfo['id'], $params[0], capi_sb_get_client_sign($simplybook_cfg)
	));

	capi_sb_saveToSession('saveToSession', array(
		array('user_data' => (array)$result)
	));

	return $result;
}

function capi_sb_getClientBookings($client, $method, $params, $simplybook_cfg){
	$clientInfo = capi_sb_get_client_info();

	$result = $client->__call( $method, array(
		$clientInfo['id'], capi_sb_get_client_sign($simplybook_cfg), $params[0]
	));

	return $result;
}

function capi_sb_getClientMembershipList($client, $method, $params, $simplybook_cfg){
	$clientInfo = capi_sb_get_client_info();

	$result = $client->__call( $method, array(
		$params[0], $clientInfo['id'], capi_sb_get_client_sign($simplybook_cfg)
	));

	return $result;
}

function capi_sb_getMembershipPaymentHistory($client, $method, $params, $simplybook_cfg){
	$clientInfo = capi_sb_get_client_info();

	$result = $client->__call( $method, array(
		$clientInfo['id'], capi_sb_get_client_sign($simplybook_cfg)
	));

	return $result;
}

function capi_sb_makeMembershipPayment($client, $method, $params, $simplybook_cfg){
	$clientInfo = capi_sb_get_client_info();

	$result = $client->__call( $method, array(
		$params[0], $clientInfo['id'], capi_sb_get_client_sign($simplybook_cfg)
	));

	return $result;
}

function capi_sb_membershipCheckClientAccess($client, $method, $params, $simplybook_cfg){
	$clientInfo = capi_sb_get_client_info();
	array_splice( $params, 1, 0, array($clientInfo['id'], capi_sb_get_client_sign($simplybook_cfg)) );
	$result = $client->__call( $method, $params);
	return $result;
}

function capi_sb_getPluginStatuses($client, $method, $params, $simplybook_cfg){
	$result =  $client->__call( $method, $params );

	$last_result = get_transient('simplybook_plugins_status');

	if(isset($last_result) && $last_result != json_encode($result) ){ //if plugin status changed - clear cache
		simplybook_clear_cache();
	}

	set_transient( 'simplybook_plugins_status', json_encode($result), 7*24 * HOUR_IN_SECONDS );

	return $result;
}

function capi_sb_default($client, $method, $params, $simplybook_cfg){
	$result =  $client->__call( $method, $params );
	return $result;
}

//////////////////////
/// Additional functions
/////////////////////
///
function capi_sb_get_client_sign($simplybook_cfg){
	$clientInfo = capi_sb_get_client_info();
	return md5($clientInfo['id'] . $clientInfo['client_hash'] . $simplybook_cfg['api_secret_key']);
}

function capi_sb_get_client_info(){
	$clientInfo = capi_sb_getFromSession('getFromSession', array('user_data'));

	if( !$clientInfo || !$clientInfo['user_data'] || !$clientInfo['user_data']['id']){
		return array();
	}
	return $clientInfo['user_data'];
}

function array_change_keys( $array, $transform_list ) {
	if(!is_array($transform_list) || !is_array($array)){
		return $array;
	}

	$keys = array_keys( $array );
	$transform_keys = array_keys($transform_list);

	for($i=0; $i<count($transform_keys); $i++) {
		$old_key = $transform_keys[$i];
		if ( ! array_key_exists( $old_key, $array ) ) {
			continue;
		}
		$keys[ array_search( $old_key, $keys ) ] = $transform_list[$old_key];
	}

	return array_combine( $keys, $array );
}



?>