<?php
/**
 * User: vetal
 * Date: 28.12.15
 * Time: 10:49
 */

include_once SIMPLYBOOK_DIR."/class/json-rpc-client.php";
include_once SIMPLYBOOK_DIR."/class/api-client.php";
include_once SIMPLYBOOK_DIR."/modules/custom-function-api.php";


try {
	$acc_protocols = stream_get_wrappers();

	$protocol = 'http://';

	if(in_array('https', $acc_protocols)){
		$protocol = 'https://';
	} elseif(!in_array('https', $acc_protocols) && !in_array('http', $acc_protocols)){
		throw new Exception('{"error":{"code":-99999,"message":"http and https protocols not supported","data":null},"id":"1"}');
	}

	$client = new ApiClient($protocol . $simplybook_cfg['url'] . '/', $simplybook_cfg['login'], $simplybook_cfg['api_key'] );

	if ( false === ( $available_methods = get_transient( 'simplybook_available_methods' ) ) ) {
		$arrContextOptions=array(
			"ssl"=>array(
				"verify_peer"=>false, //ONLY FOR TEST API
				"verify_peer_name"=>false, //ONLY FOR TEST API
			),
		);
		$available_methods = json_decode( file_get_contents( $protocol . $simplybook_cfg['url'], false, stream_context_create($arrContextOptions) ) );
		$available_methods = array_keys( (array) $available_methods->services );
		set_transient( 'simplybook_available_methods', $available_methods, 24 * HOUR_IN_SECONDS );
	}

	$method = sanitize_text_field($_REQUEST['method']); //sanitized
	$params = ( isset( $_REQUEST['params'] ) && ! empty( $_REQUEST['params'] ) ) ?  (array)$_REQUEST['params'] : array();
	$params = array_map_recursive( 'esc_attr', $params ); //sanitized

	if (in_array($method, $simplybook_cfg['cached_methods'])/* && false*/) {  //if cached method
		$key = md5( serialize( $params ) );
		$cache_data = get_transient( 'simplybook_' . $method );

		if ( ! isset( $cache_data[ $key ] ) ) {
			$cache_data[ $key ] = $client->__call( $method, $params );
			set_transient( 'simplybook_' . $method, $cache_data, 24 * HOUR_IN_SECONDS );
		}
		echo json_encode( $cache_data[ $key ] );

	} else {

		$api_result = null;

		switch($method) {
			case 'getAllList':  // getAllList
				$api_result = capi_sb_getAllList($client, $method, $params);
				break;

			case 'getClientTimezoneOffset': //getClientTimezoneOffset
				$api_result = capi_sb_getClientTimezoneOffset($client, $method, $params);
				break;

			case 'getBookingCart':
				$api_result = capi_sb_getBookingCart($client, $method, $params, $simplybook_cfg);
				break;

			case 'confirmBookingCart':
				$api_result = capi_sb_confirmBookingCart($client, $method, $params, $simplybook_cfg);
				break;

			case 'confirmDelayPayment': //confirmDelayPayment
				$api_result = capi_sb_confirmDelayPayment($client, $method, $params, $simplybook_cfg);
				break;

			case 'cancelBooking': //confirmDelayPayment
				$api_result = capi_sb_cancelBooking($client, $method, $params, $simplybook_cfg);
				break;

			case 'saveToSession':
				$api_result = capi_sb_saveToSession($method, $params);
				break;

			case 'getFromSession':
				$api_result = capi_sb_getFromSession($method, $params);
				break;

			case 'book':
				$api_result = capi_sb_book($client, $method, $params, $simplybook_cfg);
				break;

			case 'modifyClientInfo':
				$api_result = capi_sb_modifyClientInfo($client, $method, $params, $simplybook_cfg);
				break;

			case 'getClientBookings':
				$api_result = capi_sb_getClientBookings($client, $method, $params, $simplybook_cfg);
				break;

			case 'getClientMembershipList':
				$api_result = capi_sb_getClientMembershipList($client, $method, $params, $simplybook_cfg);
				break;

			case 'getMembershipPaymentHistory':
				$api_result = capi_sb_getMembershipPaymentHistory($client, $method, $params, $simplybook_cfg);
				break;

			case 'makeMembershipPayment':
				$api_result = capi_sb_makeMembershipPayment($client, $method, $params, $simplybook_cfg);
				break;

			case 'membershipCheckClientAccess':
				$api_result = capi_sb_membershipCheckClientAccess($client, $method, $params, $simplybook_cfg);
				break;

			case 'getPluginStatuses':
				$api_result = capi_sb_getPluginStatuses($client, $method, $params, $simplybook_cfg);
				break;

			default:
				if ( in_array( $method, $available_methods ) ) {  //non cached methods
					$api_result = capi_sb_default($client, $method, $params, $simplybook_cfg);
				} else {  //Method not found
					capi_sb_apiError();
				}
				break;
		}

		echo json_encode($api_result);
	}



} catch (Exception $e) {
	delete_transient( 'simplybook_token' );
	simplybook_clear_cache();

	echo $e->getMessage();
}

?>