<?php
/**
 * User: vetal
 * Date: 24.12.15
 * Time: 18:14
 */

try {
	if ( isset( $_POST['submit'] ) ) {  //if form submitted
		unset( $_POST['submit'] );

		foreach ( $_POST as $key => $val ) {

			if ( isset( $_POST[ $key ] ) ) { //if key exist
				switch($key){
					case 'custom-url':
						if (isset($val) && strlen($val)>2){
							if(filter_var('http://'.$val, FILTER_VALIDATE_URL) === FALSE) {
								throw new Exception( sb_get_text( 'Not a valid API URL' ) );
							}
							$_POST['url'] = $val;
							$key = 'url';
						}
						unset($_POST['custom-url']);
						break;

					case 'url':
						if (filter_var('http://'.$val, FILTER_VALIDATE_URL) === FALSE) {
							throw new Exception(sb_get_text('Not a valid API URL'));
						}
						break;

					case 'login':
						$val = trim($val);
						if(strlen($val)<2 || strlen($val)>128){
							throw new Exception(sb_get_text('Not a valid company login'));
						}
						break;

					case 'api_key':
					case 'api_secret_key':
						$val = trim($val);
						if(strlen($val)<10 || strlen($val)>128){
							throw new Exception(sb_get_text('Not a valid API key or API secret key'));
						}

				}
				if($key == 'custom_css'){
					$simplybook_cfg[ $key ] = implode( "\n", array_map( 'sanitize_text_field', explode( "\n", $val ) ) );
				} else {
					$simplybook_cfg[ $key ] = sanitize_text_field( $val );
				}
			}
		}
		update_option( $simplybook_domain, $simplybook_cfg );
		simplybook_clear_cache();

		echo '<div id="message" class="updated">
		        <p><strong>'.sb_get_text("Settings saved").'</strong></p>
		    </div>';
	}
} catch (Exception $e) {

	echo '<div id="message" class="error">
		        <p><strong>'.sb_get_text($e->getMessage()).'</strong></p>
		    </div>';
}

if(isset($_REQUEST['clear_cache'])){
	simplybook_clear_cache();
}


$theme_list = simplybook_get_theme_list();
$theme_options = '';
foreach($theme_list as $theme){
	$theme_options .= "<option value='{$theme['name']}' ".($theme['name']==$simplybook_cfg['template']?'selected':'').">{$theme['name']}</option>";
}

///FORM

$apiUrls = array(
	'user-api.simplybook.me',
	'user-api.simplybook.it'
);

$isApiChanged = true;

if(in_array($simplybook_cfg['url'], $apiUrls)){
	$isApiChanged = false;
}

//////DEFAULT VALUES
$defSpbValues = array(
	'loader_mode' => 'global',
	'custom_css' => '',
	'stripe_status' => false,
	'stripe_log' => false,
	'stripe_api_key' => '',
	'stripe_api_secret_key' => '',
	'stripe_description' => '',

	'paypal_status' => false,
	'paypal_log' => false,
	'paypal_account' => '',

	'paymentwall_status' => false,
	'paymentwall_log' => false,
	'paymentwall_api_key' => '',
	'paymentwall_api_secret_key' => '',

	'adyen_status' => false,
	'adyen_log' => false,
	'adyen_mode' => 'live',
	'adyen_skin_code' => '',
	'adyen_merchant_account' => '',
	'adyen_hmac_key' => '',
);

foreach ( $defSpbValues as $key => $value){
	if(!isset($simplybook_cfg[$key])){
		$simplybook_cfg[$key] = $value;
	}
}
/////


$result = "
	<div class='card'>
	<h2>".sb_get_text("Plugin settings")."</h2>

    <form method='POST' action='{$_SERVER['PHP_SELF']}?page={$simplybook_domain}' >
        <table class=\"form-table\">
            <tr>
                <th scope=\"row\"><label>".sb_get_text("API url")."</label></th>
                <td>
                    <select name=\"url\" class=\"regular-text\" >
";

					for($i=0; $i<count($apiUrls); $i++){
						$result .= "<option value='{$apiUrls[$i]}'  " . ($apiUrls[$i] == $simplybook_cfg['url']?'selected="selected"':'') . " >{$apiUrls[$i]}</option>";
					}

$result .= "                        
                    </select>
                    <input type=\"checkbox\" class=\"use-custom-url\"  " . ($isApiChanged?'checked="checked"':'') . "/> ".sb_get_text("Custom API url") ."
                    
                   	<input name=\"custom-url\" value=\"{$simplybook_cfg['url']}\" class=\"regular-text\" disabled=\"disabled\" style=\"display:none\" />
                    <p class=\"description\" id=\"tagline-description\">".sb_get_text("Please use default url user-api.simplybook.me without changing it! No modifications needed to default api url, this field is only for simplybook plugin developers ")." </p>
                </td>
            </tr>            

            <tr>
                <th scope=\"row\"><label>".sb_get_text("Company login")."</label></th>
                <td>
                    <input name=\"login\" value=\"{$simplybook_cfg['login']}\" class=\"regular-text\" />
                    <p class=\"description\" id=\"tagline-description\">".sb_get_text("SimplybBook company login")." </p>
                </td>
            </tr>

            <tr>
                <th scope=\"row\"><label>".sb_get_text("API public key")."</label></th>
                <td>
                    <input name=\"api_key\" value=\"{$simplybook_cfg['api_key']}\" class=\"regular-text\" />
                    <p class=\"description\" id=\"tagline-description\">".sb_get_text("SimplybBook public key from API plugin")." </p>
                </td>
            </tr>

            <tr>
                <th scope=\"row\"><label>".sb_get_text("API secret key")."</label></th>
                <td>
                    <input name=\"api_secret_key\" value=\"{$simplybook_cfg['api_secret_key']}\" class=\"regular-text\" />
                    <p class=\"description\" id=\"tagline-description\">".sb_get_text("SimplybBook secret key from API plugin")." </p>
                </td>
            </tr>

            <tr>
                <th scope=\"row\"><label>".sb_get_text("Plugin themes:")."</label></th>
                <td>
                    <select name=\"template\" class=\"regular-text\">
						{$theme_options}
                    </select>
                    <p class=\"description\" id=\"tagline-description\">".sb_get_text("Plugin themes from 'wordpress_dir/wp-content/plugins/simplybook.me/themes' ")." </p>
                </td>
            </tr>

        </table>
        <p class=\"submit\">
            <input type=\"submit\" name=\"submit\" id=\"submit\" class=\"button button-primary\" value=\"".sb_get_text("Save changes")."\">
        </p>
    </form>
    <p>".sb_get_text("The plug-in cache stores data for 24 hours. If you made changes to the simplybook admin panel (eg added a new service) then clear cache")."</p>
	<a href='{$_SERVER['PHP_SELF']}?page={$simplybook_domain}&updated=true&clear_cache'><span class=\"button button - secondary\" >".sb_get_text("Clear cache")."</span></a>
    </div>
    ";


$result .= "
	<div class='card'>
	<h2>".sb_get_text("View settings")."</h2>

    <form method='POST' action='{$_SERVER['PHP_SELF']}?page={$simplybook_domain}' >
        <table class=\"form-table\">
            <tr>
                <th scope=\"row\"><label>".sb_get_text("Preloader mode").":</label></th>
                <td>
                    <select name=\"loader_mode\" class=\"regular-text\">
						<option value='global' ".($simplybook_cfg['loader_mode']=='global'?'selected':'')." >".sb_get_text("Global")."</option>
						<option value='local' ".($simplybook_cfg['loader_mode']=='local'?'selected':'')." >".sb_get_text("Local")."</option>
						<option value='local_simple' ".($simplybook_cfg['loader_mode']=='local_simple'?'selected':'')." >".sb_get_text("Local simple")."</option>
                    </select>
                    <p class=\"description\" id=\"tagline-description\">".sb_get_text("Would you like to switch preloader mode?")." </p>
                </td>
            </tr>

        </table>
        <p class=\"submit\">
            <input type=\"submit\" name=\"submit\" id=\"submit\" class=\"button button-primary\" value=\"".sb_get_text("Save changes")."\">
        </p>
    </form>
	</div>
    ";

$result .= "
	<div class='card'>
	<h2>".sb_get_text("Custom CSS")."</h2>

	<p>".sb_get_text(" If plugin is displayed incorrectly with your theme, you can create your own CSS styles and insert them in this field.")."</p>

    <form method='POST' action='{$_SERVER['PHP_SELF']}?page={$simplybook_domain}' >
        <table class=\"form-table\">
            <tr>
                <td>
                    <textarea name=\"custom_css\" class=\"large-text code\" style=\"min-height:200px; white-space: normal;\">{$simplybook_cfg['custom_css']}</textarea>
                    <p class=\"description\" id=\"tagline-description\">".sb_get_text("Please note, there is no need to insert reset CSS because they are already applied.")." </p>
                </td>
            </tr>


        </table>
        <p class=\"submit\">
            <input type=\"submit\" name=\"submit\" id=\"submit\" class=\"button button-primary\" value=\"".sb_get_text("Save changes")."\">
        </p>
    </form>
	</div>
    ";


$result .= "
<div class='card' >

	<h2>".sb_get_text("Stripe setting")."</h2>

	<p>".sb_get_text("You can get all your keys from your <a href=\"https://dashboard.stripe.com/account/apikeys\" target=\"_blank\">account page</a>, or learn more about <a href=\"https://stripe.com/docs/tutorials/dashboard#livemode-and-testing\" target=\"_blank\">live mode and testing</a>.")."</p>

    <form method='POST' action='{$_SERVER['PHP_SELF']}?page={$simplybook_domain}' >
        <table class=\"form-table\">
            <tr>
                <th scope=\"row\"><label>".sb_get_text("Status:")."</label></th>
                <td>
                    <select name=\"stripe_status\" class=\"regular-text\">
                    	<option value='0'>".sb_get_text("Disabled")."</option>
						<option value='1' ".($simplybook_cfg['stripe_status']?'selected':'')." >".sb_get_text("Enabled")."</option>
                    </select>
                    <p class=\"description\" id=\"tagline-description\">".sb_get_text("Would you like to switch on this payment system?")." </p>
                </td>
            </tr>
            <tr>
                <th scope=\"row\"><label>".sb_get_text("Save payment logs:")."</label></th>
                <td>
                    <select name=\"stripe_log\" class=\"regular-text\">
                    	<option value='0'>".sb_get_text("Disabled")."</option>
						<option value='1' ".($simplybook_cfg['stripe_log']?'selected':'')." >".sb_get_text("Enabled")."</option>
                    </select>
                    <p class=\"description\" id=\"tagline-description\">".sb_get_text("Logs are saved in ~/wp-content/{plugin_dir}/logs/")." </p>
                </td>
            </tr>
            <tr>
                <th scope=\"row\"><label>".sb_get_text("API url")."</label></th>
                <td>
                    <input name=\"stripe_url\" value=\"{$simplybook_cfg['stripe_url']}\" class=\"regular-text\" />
                    <p class=\"description\" id=\"tagline-description\">".sb_get_text("SimplybBook API url (without 'http://') (Default: 'api.stripe.com')")." </p>
                </td>
            </tr>

            <tr>
                <th scope=\"row\"><label>".sb_get_text("API public key")."</label></th>
                <td>
                    <input name=\"stripe_api_key\" value=\"{$simplybook_cfg['stripe_api_key']}\" class=\"regular-text\" />
                    <p class=\"description\" id=\"tagline-description\">".sb_get_text("Stripe public key")." </p>
                </td>
            </tr>

            <tr>
                <th scope=\"row\"><label>".sb_get_text("API secret key")."</label></th>
                <td>
                    <input name=\"stripe_api_secret_key\" value=\"{$simplybook_cfg['stripe_api_secret_key']}\" class=\"regular-text\" />
                    <p class=\"description\" id=\"tagline-description\">".sb_get_text("Stripe secret key")." </p>
                </td>
            </tr>

            <tr>
                <th scope=\"row\"><label>".sb_get_text("Stripe payment name")."</label></th>
                <td>
                    <input name=\"stripe_name\" value=\"{$simplybook_cfg['stripe_name']}\" class=\"regular-text\" />
                    <p class=\"description\" id=\"tagline-description\">".sb_get_text("stripe payment title")." </p>
                </td>
            </tr>

            <tr>
                <th scope=\"row\"><label>".sb_get_text("Stripe payment description")."</label></th>
                <td>
                    <input name=\"stripe_description\" value=\"{$simplybook_cfg['stripe_description']}\" class=\"regular-text\" />
                    <p class=\"description\" id=\"tagline-description\">".sb_get_text("stripe payment desctiption")." </p>
                </td>
            </tr>
        </table>
        <p class=\"submit\">
            <input type=\"submit\" name=\"submit\" id=\"submit\" class=\"button button-primary\" value=\"".sb_get_text("Save changes")."\">
        </p>
    </form>

</div>
";



$result .= "
<div class='card' >

	<h2>".sb_get_text("PayPal setting")." <i style='font-size:14px; color:red; margin-left: 10px;'>".sb_get_text("NEW!")."</i></h2>


    <form method='POST' action='{$_SERVER['PHP_SELF']}?page={$simplybook_domain}' >
        <table class=\"form-table\">
            <tr>
                <th scope=\"row\"><label>".sb_get_text("Status:")."</label></th>
                <td>
                    <select name=\"paypal_status\" class=\"regular-text\">
                    	<option value='0'>".sb_get_text("Disabled")."</option>
						<option value='1' ".($simplybook_cfg['paypal_status']?'selected':'')." >".sb_get_text("Enabled")."</option>
                    </select>
                    <p class=\"description\" id=\"tagline-description\">".sb_get_text("Would you like to switch on this payment system?")." </p>
                </td>
            </tr>

            <tr>
                <th scope=\"row\"><label>".sb_get_text("Save payment logs:")."</label></th>
                <td>
                    <select name=\"paypal_log\" class=\"regular-text\">
                    	<option value='0'>".sb_get_text("Disabled")."</option>
						<option value='1' ".($simplybook_cfg['paypal_log']?'selected':'')." >".sb_get_text("Enabled")."</option>
                    </select>
                    <p class=\"description\" id=\"tagline-description\">".sb_get_text("Logs are saved in ~/wp-content/{plugin_dir}/logs/")." </p>
                </td>
            </tr>

            <tr>
                <th scope=\"row\"><label>".sb_get_text("Paypal server:")."</label></th>
                <td>
                    <select name=\"paypal_url\" class=\"regular-text\">
                    	<option value='www.paypal.com'>".sb_get_text("www.paypal.com")."</option>
						<option value='www.sandbox.paypal.com' ".($simplybook_cfg['paypal_url'] == 'www.sandbox.paypal.com'?'selected':'')." >".sb_get_text("www.sandbox.paypal.com (Sandbox only for test accs)")."</option>
                    </select>
                    <p class=\"description\" id=\"tagline-description\">".sb_get_text("Please use Sandbox for test accounts only. Sandbox doesn't work with regular PayPal accounts.")." </p>
                </td>
            </tr>

            <tr>
                <th scope=\"row\"><label>".sb_get_text("Paypal business email:")."</label></th>
                <td>
                    <input name=\"paypal_account\" value=\"{$simplybook_cfg['paypal_account']}\" class=\"regular-text\" />
                    <p class=\"description\" id=\"tagline-description\">".sb_get_text("paypal account")." </p>
                </td>
            </tr>

        </table>
        <p class=\"submit\">
            <input type=\"submit\" name=\"submit\" id=\"submit\" class=\"button button-primary\" value=\"".sb_get_text("Save changes")."\">
        </p>
    </form>

</div>
";





$result .= "
<div class='card' >

	<h2>".sb_get_text("Paymentwall setting")." <i style='font-size:14px; color:red; margin-left: 10px;'>".sb_get_text("NEW!")."</i></h2>

	<p>".sb_get_text("You can get all your keys from your <a href=\"https://api.paymentwall.com/developers/applications\" target=\"_blank\">account page</a>")."</p>

    <form method='POST' action='{$_SERVER['PHP_SELF']}?page={$simplybook_domain}' >
        <table class=\"form-table\">
            <tr>
                <th scope=\"row\"><label>".sb_get_text("Status:")."</label></th>
                <td>
                    <select name=\"paymentwall_status\" class=\"regular-text\">
                    	<option value='0'>".sb_get_text("Disabled")."</option>
						<option value='1' ".($simplybook_cfg['paymentwall_status']==1?'selected':'')." >".sb_get_text("Enabled Brick")."</option>
						<option value='2' ".($simplybook_cfg['paymentwall_status']==2?'selected':'')." >".sb_get_text("Enabled Digital Goods")."</option>
                    </select>
                    <p class=\"description\" id=\"tagline-description\">".sb_get_text("Would you like to switch on this payment system?")." </p>
                </td>
            </tr>
            <tr>
                <th scope=\"row\"><label>".sb_get_text("Save payment logs:")."</label></th>
                <td>
                    <select name=\"paymentwall_log\" class=\"regular-text\">
                    	<option value='0'>".sb_get_text("Disabled")."</option>
						<option value='1' ".($simplybook_cfg['paymentwall_log']?'selected':'')." >".sb_get_text("Enabled")."</option>
                    </select>
                    <p class=\"description\" id=\"tagline-description\">".sb_get_text("Logs are saved in ~/wp-content/{plugin_dir}/logs/")." </p>
                </td>
            </tr>

            <tr>
                <th scope=\"row\"><label>".sb_get_text("API public key")."</label></th>
                <td>
                    <input name=\"paymentwall_api_key\" value=\"{$simplybook_cfg['paymentwall_api_key']}\" class=\"regular-text\" />
                    <p class=\"description\" id=\"tagline-description\">".sb_get_text("Paymentwall public key")." </p>
                </td>
            </tr>

            <tr>
                <th scope=\"row\"><label>".sb_get_text("API secret key")."</label></th>
                <td>
                    <input name=\"paymentwall_api_secret_key\" value=\"{$simplybook_cfg['paymentwall_api_secret_key']}\" class=\"regular-text\" />
                    <p class=\"description\" id=\"tagline-description\">".sb_get_text("Paymentwall secret key")." </p>
                </td>
            </tr>

            <tr>
                <th scope=\"row\"><label>".sb_get_text("Paymentwall PING url")."</label></th>
                <td>
                    <input value=\"".(admin_url('admin-ajax.php')."?action=sb_api&method=paymentwall_ping")."\" class=\"regular-text\" readonly />
                    <p class=\"description\" id=\"tagline-description\">".sb_get_text("Please insert this link into the field \"Pingback URL\" in the settings of your project")." </p>
                </td>
            </tr>

            <tr>
                <th scope=\"row\"><label>".sb_get_text("Pingback signature version")."</label></th>
                <td>
                    <input value=\"2\" class=\"regular-text\" readonly />
                    <p class=\"description\" id=\"tagline-description\">".sb_get_text("Please select this version into the field \"Pingback signature version\" in the settings of your project")." </p>
                </td>
            </tr>


        </table>
        <p class=\"submit\">
            <input type=\"submit\" name=\"submit\" id=\"submit\" class=\"button button-primary\" value=\"".sb_get_text("Save changes")."\">
        </p>
    </form>

</div>
";




$result .= "
<div class='card' >

	<h2>".sb_get_text("Adyen setting")." <i style='font-size:14px; color:red; margin-left: 10px;'>".sb_get_text("NEW!")."</i></h2>

	<p>".sb_get_text("You can get all your keys from your account page <a href=\"https://ca-live.adyen.com/ca/ca/skin/skins.shtml\" target=\"_blank\">skin configuration</a>")."</p>

    <form method='POST' action='{$_SERVER['PHP_SELF']}?page={$simplybook_domain}' >
        <table class=\"form-table\">
            <tr>
                <th scope=\"row\"><label>".sb_get_text("Status:")."</label></th>
                <td>
                    <select name=\"adyen_status\" class=\"regular-text\">
                    	<option value='0'>".sb_get_text("Disabled")."</option>
						<option value='1' ".($simplybook_cfg['adyen_status']?'selected':'')." >".sb_get_text("Enabled")."</option>
                    </select>
                    <p class=\"description\" id=\"tagline-description\">".sb_get_text("Would you like to switch on this payment system?")." </p>
                </td>
            </tr>
            <tr>
                <th scope=\"row\"><label>".sb_get_text("Save payment logs:")."</label></th>
                <td>
                    <select name=\"adyen_log\" class=\"regular-text\">
                    	<option value='0'>".sb_get_text("Disabled")."</option>
						<option value='1' ".($simplybook_cfg['adyen_log']?'selected':'')." >".sb_get_text("Enabled")."</option>
                    </select>
                    <p class=\"description\" id=\"tagline-description\">".sb_get_text("Logs are saved in ~/wp-content/{plugin_dir}/logs/")." </p>
                </td>
            </tr>

            <tr>
                <th scope=\"row\"><label>".sb_get_text("Adyen mode:")."</label></th>
                <td>
                    <select name=\"adyen_mode\" class=\"regular-text\">
                    	<option value='live'>".sb_get_text("Live mode")."</option>
						<option value='test' ".($simplybook_cfg['adyen_mode'] == 'test'?'selected':'')." >".sb_get_text("Test mode")."</option>
                    </select>
                    <p class=\"description\" id=\"tagline-description\">".sb_get_text("Use test mode with test keys only. Active cards don't work in test mode and purchase won't be proceeded")." </p>
                </td>
            </tr>

            <tr>
                <th scope=\"row\"><label>".sb_get_text("Skin code")."</label></th>
                <td>
                    <input name=\"adyen_skin_code\" value=\"{$simplybook_cfg['adyen_skin_code']}\" class=\"regular-text\" />
                    <p class=\"description\" id=\"tagline-description\">".sb_get_text("the skin to be used")." </p>
                </td>
            </tr>

            <tr>
                <th scope=\"row\"><label>".sb_get_text("Merchant account")."</label></th>
                <td>
                    <input name=\"adyen_merchant_account\" value=\"{$simplybook_cfg['adyen_merchant_account']}\" class=\"regular-text\" />
                    <p class=\"description\" id=\"tagline-description\">".sb_get_text("merchant Account e.g. TestCompanyCOM")." </p>
                </td>
            </tr>

            <tr>
                <th scope=\"row\"><label>".sb_get_text("Hmac key")."</label></th>
                <td>
                    <input name=\"adyen_hmac_key\" value=\"{$simplybook_cfg['adyen_hmac_key']}\" class=\"regular-text\" />
                    <p class=\"description\" id=\"tagline-description\">".sb_get_text("the shared HMAC key. Example: D21EB2ASD44BA234C8A0...")." </p>
                </td>
            </tr>

            <tr>
                <th scope=\"row\"><label>".sb_get_text("Result URLs")."</label></th>
                <td>
                    <input value=\"".(admin_url('admin-ajax.php')."?action=sb_api&method=adyen_result")."\" class=\"regular-text\" readonly />
                    <p class=\"description\" id=\"tagline-description\">".sb_get_text("Please insert this link into the field \"Result URLs\" in the settings of your aplication (skin tab)")." </p>
                </td>
            </tr>

        </table>
        <p class=\"submit\">
            <input type=\"submit\" name=\"submit\" id=\"submit\" class=\"button button-primary\" value=\"".sb_get_text("Save changes")."\">
        </p>
    </form>

</div>
";




$result .= "
<script>
  window.onload = function() {
      
      	var showCustomUrlSB = function(isChecked) {
    	  	if(isChecked){
    	  	    jQuery(':input[name=\"url\"]').prop('disabled', true);
    	  	    jQuery(':input[name=\"custom-url\"]').prop('disabled', false).show();
    	  	} else{
    	  	    jQuery(':input[name=\"url\"]').prop('disabled', false);
    	  	    jQuery(':input[name=\"custom-url\"]').prop('disabled', true).hide();
    	  	}
      	};
      
    	jQuery(document).on('click', '.use-custom-url', function() {
    	    var isChecked = jQuery(this).prop('checked');
			showCustomUrlSB(isChecked);
    	});
";

if($isApiChanged){
	$result .= "showCustomUrlSB(true);";
}

$result .= "
    	
  };
</script>
";



echo $result;

?>