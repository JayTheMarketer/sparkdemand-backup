<?php
/**
 * User: vetal
 * Date: 27.01.16
 * Time: 15:46
 */

//print_r($simplybook_cfg);
$method = sanitize_text_field($_REQUEST['method']); //sanitized
$params = ( isset( $_REQUEST['params'] ) && ! empty( $_REQUEST['params'] ) ) ?  (array)$_REQUEST['params'] : array();

$params = array_map_recursive( 'esc_attr', $params ); //sanitized

try{

	switch($method){
		case 'returnPage':
			include_once SIMPLYBOOK_DIR.'/modules/paypal/return.html';
			break;

		case 'notify':
			$paypal_valid = validate_paypal_response();

			if($simplybook_cfg['paypal_log']){
				simplybook_save_log('paypal_'.date('d.m.y H:i:s'), json_encode($_POST));
			}

			if($paypal_valid){
				$descr = (string)$_POST['item_name1'];
				$ids = explode(":",$descr);
				$ids = trim($ids[count($ids)-1]);
				$hash = md5($ids);

				set_transient( 'simplybook_paypal_'.$hash, true, 24 * HOUR_IN_SECONDS );//use WP storage, API not have "check booking payment status" function

				$bookings = explode(',', $ids);

				$cart_res = (array)simplybook_get_api('getBookingCart', $bookings);
				list($buyType, $bookings) = sb_get_api_type($bookings);

				//$sign = md5($cart_res['cart_id'] .  $cart_res['cart_hash']. $simplybook_cfg['api_secret_key']);
				echo $res = json_encode(simplybook_get_api('confirmBookingCart', $cart_res, 'paypal', $simplybook_cfg['api_secret_key'], $buyType));
			}
			break;

		case 'check':
			$bookings = $params[0];
			if(!is_array($bookings)){
				$bookings = array($bookings);
			}

			$ids = trim(implode(',', $bookings));
			$hash = md5($ids);

			$paypal_status = get_transient( 'simplybook_paypal_'.$hash );
			if ( $paypal_status ) {
				echo json_encode(true);
			} else {
				sleep(7); // Sleep and repeat
				$paypal_status = get_transient( 'simplybook_paypal_'.$hash );
				if ( $paypal_status ) {
					echo json_encode( true );
				}else{
					echo json_encode(false);
				}
			}
			break;
	}

} catch (Exception $e) {
	echo '{"error":{"code":-9999,"message":"'.$e->getJsonBody().'","data":null},"id":"1"}';
}


function validate_paypal_response(){
	global $simplybook_cfg;

	$req = 'cmd=_notify-validate';
	foreach ($_POST as $key => $value) {
		$value = urlencode($value);
		$req .= "&$key=$value";
	}

	$header = "POST /cgi-bin/webscr HTTP/1.0\r\n";
	$header .= "Host: " . $simplybook_cfg['paypal_url'] . "\r\n";
	$header .= "Content-Type: application/x-www-form-urlencoded\r\n";
	$header .= "Content-Length: " . strlen($req) . "\r\n\r\n";
	$fp = fsockopen ('ssl://' . $simplybook_cfg['paypal_url'], 443, $errno, $errstr, 30);
	$result = false;
	if (!$fp) {
		throw new Exception(sb_get_text('Payment system error! Can not connect to PayPal server.'));
	} else {
		fputs($fp, $header . $req);
		while (!feof($fp)) {
			$res = strtoupper(fgets($fp, 1024));
			//Util::save_trace($res, 'paypal_req' . time() . '.txt');
			if ($res == 'VERIFIED') {
				//$this->_setResponse($_POST);
				$result = true;
			} elseif ($res == 'INVALID') {
				//throw new Exception(sb_get_text('ePaymentSystemInvalidResponse'));
				$result = false;
			}
		}
	}
	fclose ($fp);
	return $result;
}
