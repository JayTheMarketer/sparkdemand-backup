<?php
/**
 * User: vetal
 * Date: 02.02.16
 * Time: 12:42
 */

$method = sanitize_text_field($_REQUEST['method']); //sanitized
$request = ( isset( $_REQUEST ) && ! empty( $_REQUEST ) ) ?  (array)$_REQUEST : array();
$request = array_map_recursive( 'esc_attr', $request ); //sanitized
$params = $request['params'];

// The character escape function
$escapeval = function($val) {
	return str_replace(':','\\:',str_replace('\\','\\\\',$val));
};

try{

	switch($method){

		case 'createWidgetParams':
			$cart_res = (array)simplybook_get_api('getBookingCart', $params[0]);

			list($buyType, $bookingsCleared) = sb_get_api_type($params[0]);

			$bookings = $params[0];
			if(!is_array($bookings)){
				$bookings = array($bookings);
			}

			$adyen_params = array(
				"merchantReference" => "From: " .$_SESSION['book_params'][4]['name']. ". " . $buyType . " IDs: ".implode(',',$bookings),
				"merchantAccount"   =>  $simplybook_cfg['adyen_merchant_account'],
				"currencyCode"      => $cart_res["currency"],
				"paymentAmount"     => $cart_res["amount"]*100,
				"sessionValidity"   => str_replace('+00:00', 'Z', gmdate('c', time()+15*60)),
				"shipBeforeDate"    => gmdate('Y-m-d'),
				/*"shopperLocale"     => "en_GB",*/
				"skinCode"          => $simplybook_cfg['adyen_skin_code'],
				"brandCode"         => "",
				"shopperEmail"      => $_SESSION['book_params'][4]['email'],
				"shopperReference"  => $_SESSION['book_params'][4]['name']
			);



			// Sort the array by key using SORT_STRING order
			ksort($adyen_params, SORT_STRING);

			// Generate the signing data string
			$signData = implode(":",array_map($escapeval,array_merge(array_keys($adyen_params), array_values($adyen_params))));

			// base64-encode the binary result of the HMAC computation
			$merchantSig = base64_encode(hash_hmac('sha256',$signData,pack("H*" , $simplybook_cfg['adyen_hmac_key']),true));
			$adyen_params["merchantSig"] = $merchantSig;

			echo json_encode( $adyen_params );

			break;

		case 'result':
			//https://test.adyen.com/hpp/result.shtml?authResult=AUTHORISED&merchantReference=SKINTEST-1454492728186&merchantSig=KbEXBeXfG6P5vtptWEZFgu5EE9jp%2BxIYCHq4RTlba6E%3D&paymentMethod=visa&pspReference=8514544928172600&shopperLocale=en_GB&skinCode=n70xzuGk
			//http://wp.vetal.notando.com.ua/wp-admin/admin-ajax.php?action=sb_api&method=adyen_check&authResult=AUTHORISED&merchantReference=SKINTEST-1454493257431&merchantSig=NbOg45y%2BlVWQtI2NXxaQKe8QuHqz8uv7i8Vplh%2BCXfw%3D&paymentMethod=visa&pspReference=8514544932971104&shopperLocale=en_GB&skinCode=n70xzuGk
			// Get and store all the URL parameters
			$adyen_params = adyen_fix($_SERVER['QUERY_STRING']);
			// Retrieve the merchantSig from the URL parameters
			$res_merchantSig = $adyen_params['merchantSig'];
			// Remove the merchantSig field for the signature calculation
			unset($adyen_params['merchantSig']);
			unset($adyen_params['action']);
			unset($adyen_params['method']);

			// Sort the array by key using SORT_STRING order
			ksort($adyen_params, SORT_STRING);
			// Generate the signing data string
			$signData = implode(":",array_map($escapeval,array_merge(array_keys($adyen_params),
				array_values($adyen_params))));
			// base64-encode the binary result of the HMAC computation
			$merchantSig = base64_encode(hash_hmac('sha256',$signData,pack("H*" , $simplybook_cfg['adyen_hmac_key']),true));
			// Compare the calculated signature with the signature from the URL parameters

			if ($merchantSig === $res_merchantSig) {

				//print "Correct merchant signature";
				if($adyen_params['authResult'] == 'AUTHORISED'){
					$ids = explode(":",$adyen_params['merchantReference']);
					$ids = trim($ids[count($ids)-1]);
					$hash = md5($ids);

					set_transient( 'simplybook_adyen_'.$hash, 1, 24 * HOUR_IN_SECONDS );//use WP storage, API not have "check booking payment status" function

					$bookings = explode(',', $ids);
					$cart_res = (array)simplybook_get_api('getBookingCart', $bookings);

					$sign = md5($cart_res['cart_id'] .  $cart_res['cart_hash']. $simplybook_cfg['api_secret_key']);
					//$res = json_encode(simplybook_get_api('confirmBookingCart', $cart_res['cart_id'], 'adyen', $sign));
					list($buyType, $bookings) = sb_get_api_type($bookings);
					$res = json_encode(simplybook_get_api('confirmBookingCart', $cart_res, 'adyen', $simplybook_cfg['api_secret_key'], $buyType));
					//echo $res;
				} elseif($adyen_params['authResult'] == 'PENDING'){
					set_transient( 'simplybook_adyen_'.$hash, 2, 24 * HOUR_IN_SECONDS );//use WP storage, API not have "check booking payment status" function
				}

				print_r($adyen_params);
			} else {
				//print "Incorrect merchant signature";
			}

			if($simplybook_cfg['adyen_log']){
				simplybook_save_log('adyen_'.date('d.m.y H:i:s'), json_encode( array($adyen_params, 'sig correct : '.($merchantSig === $res_merchantSig?'true':'false'), 'Bookings confirm:'.$res ) ) );
			}

			include_once SIMPLYBOOK_DIR.'/modules/adyen/return.html';
			break;

		case 'check':
			$bookings = $params[0];
			if(!is_array($bookings)){
				$bookings = array($bookings);
			}

			$ids = trim(implode(',', $bookings));
			$hash = md5($ids);

			$adyen_status = get_transient( 'simplybook_adyen_'.$hash );

			if ( $adyen_status == 1 ) { // if AUTHORISED

				echo json_encode(array(true, 1));

			} elseif ( $adyen_status == 2 ) { //if PENDING

				sleep(7); // Sleep and repeat
				$adyen_status = get_transient( 'simplybook_adyen_'.$hash );
				if ( $adyen_status ==1 ) {
					echo json_encode( array(true, 1) ); //AUTORIZED
				}else{
					echo json_encode( array(true, 2) ); //PENDING
				}

			} else {
				echo json_encode( array(false, 0) ); // CANCELED
			}
			break;
	}


} catch (Exception $e) {
	echo '{"error":{"code":-9999,"message":"'.$e->getJsonBody().'","data":null},"id":"1"}';
}


// Function to preserve the original special character
function adyen_fix($source) {
	$source = preg_replace_callback(
		'/(^|(?<=&))[^=[&]+/',
		function($key) {
			return bin2hex(urldecode($key[0]));
		},
		$source
	);
	parse_str($source, $post);
	return array_combine(array_map('hex2bin', array_keys($post)), $post);
}

