<?php
/**
 * User: vetal
 * Date: 29.01.16
 * Time: 12:06
 */

$method = sanitize_text_field($_REQUEST['method']); //sanitized
$request = ( isset( $_REQUEST ) && ! empty( $_REQUEST ) ) ?  (array)$_REQUEST : array();
$request = array_map_recursive( 'esc_attr', $request ); //sanitized
$params = $request['params'];

try{

	include_once SIMPLYBOOK_DIR.'/class/paymentwall/paymentwall.php';

	switch($method){
		case 'checkBrick':
			Paymentwall_Config::getInstance()->set(array(
				'private_key' => $simplybook_cfg['paymentwall_api_secret_key']
			));

			$bookings = explode(',', $request['bookings']);
			$cart_res = (array)simplybook_get_api('getBookingCart', $bookings);

			list($buyType, $bookingsCleared) = sb_get_api_type($bookings);

			$cardInfo = array(
				'email' => $request['email'],
				'amount' => $cart_res["amount"],
				'currency' => $cart_res["currency"],
				'token' => $request['brick_token'],
				'fingerprint' => $request['brick_fingerprint'],
				'description' => $buyType . ' IDs:'.$request['bookings'],
				'evaluation' => '1'
			);

			$charge = new Paymentwall_Charge();
			$charge->create($cardInfo);
			$response = $charge->getPublicData();

			if($simplybook_cfg['paymentwall_log']){
				unset($cardInfo['token']); //NOT SAFE, token can be reused
				simplybook_save_log('paymentwall_'.date('d.m.y H:i:s'), json_encode(array($cardInfo,$response)));
			}

			if ($charge->isSuccessful()) {
				if ($charge->isCaptured()) {

					//$sign = md5($cart_res['cart_id'] .  $cart_res['cart_hash']. $simplybook_cfg['api_secret_key']);
					//$res = json_encode(simplybook_get_api('confirmBookingCart', $cart_res['cart_id'], 'paymentwall', $sign));

					$res = json_encode(simplybook_get_api('confirmBookingCart', $cart_res, 'paymentwall', $simplybook_cfg['api_secret_key'], $buyType));
					//echo $res;
					echo '{"success":1}';
					break;

				} elseif ($charge->isUnderReview()) {
					// decide on risk charge
					echo '{"success":2}';
					break;
				}
			} else {
				$errors = json_decode($response, true);
			}
			echo $response;
			break;

		case 'ping':

			$param_string = $request;
			ksort($param_string);// sort parameters by key
			unset($param_string['sig']); //remove sig
			$param_string['method'] = 'paymentwall_ping';
			$param_string = str_replace('&', '', urldecode(http_build_query($param_string)) );

			$signature = md5($param_string . trim($simplybook_cfg['paymentwall_api_secret_key']) );

			if($signature == $request['sig']){

				$bookings = explode(',', $request['goodsid']);
				$cart_res = (array)simplybook_get_api('getBookingCart', $bookings);

				//$sign = md5($cart_res['cart_id'] .  $cart_res['cart_hash']. $simplybook_cfg['api_secret_key']);
				//$res = json_encode(simplybook_get_api('confirmBookingCart', $cart_res['cart_id'], 'paymentwall', $sign));

				list($buyType, $bookings) = sb_get_api_type($bookings);
				$res = json_encode(simplybook_get_api('confirmBookingCart', $cart_res, 'paymentwall', $simplybook_cfg['api_secret_key'], $buyType));
				//echo $res;

				$hash = md5($request['goodsid']);
				set_transient( 'simplybook_paymentwall_'.$hash, true, 24 * HOUR_IN_SECONDS );//use WP storage, API not have "check booking payment status" function

				//echo json_encode(true);
			} else {
				//echo json_encode(false);
			}

			if($simplybook_cfg['paymentwall_log']){
				simplybook_save_log( 'paymentwall_ping_'.date('d.m.y H:i:s'), json_encode( array($request, "Sign valid : " .($signature == $request['sig']?'true':'false')  ) ) );
			}

			echo "OK";

			break;


		case 'check':
			$bookings = $params[0];
			if(!is_array($bookings)){
				$bookings = array($bookings);
			}
			$ids = trim(implode(',', $bookings));
			$hash = md5($ids);

			$paypal_status = get_transient( 'simplybook_paymentwall_'.$hash );
			if ( $paypal_status ) {
				echo json_encode(true);
			} else {
				sleep(7); // Sleep and repeat
				$paypal_status = get_transient( 'simplybook_paymentwall_'.$hash );
				if ( $paypal_status ) {
					echo json_encode( true );
				}else{
					echo json_encode(false);
				}
			}
			break;


		case 'createWidgetLink':

			$url = "https://wallapi.com/api/subscription?";
			$cart_res = (array)simplybook_get_api('getBookingCart', $params[0]);

			list($buyType, $bookingsCleared) = sb_get_api_type($params[0]);

			$bookings = $params[0];
			if(!is_array($bookings)){
				$bookings = array($bookings);
			}

			$str_params = array(
				"ag_external_id" => trim(implode(',', $bookings)),
				"ag_name" => $buyType . ' IDs:'.trim(implode(',', $bookings)),
				"ag_type" => "fixed",
				"amount" => $cart_res["amount"],
				"currencyCode" => $cart_res["currency"],
				"key" => $simplybook_cfg['paymentwall_api_key'],
				"sign_version" => "2",
				"uid" => $_SESSION['book_params'][4]['name'],
				"widget" => "p3"
			);

			$signature = md5( str_replace("&", "", urldecode(http_build_query($str_params))) . trim($simplybook_cfg['paymentwall_api_secret_key']) );
			$url .= http_build_query($str_params) . '&sign=' . $signature;

			echo json_encode($url);

			break;

	}


} catch (Exception $e) {
	echo '{"error":{"code":-9999,"message":"'.$e->getJsonBody().'","data":null},"id":"1"}';
}
