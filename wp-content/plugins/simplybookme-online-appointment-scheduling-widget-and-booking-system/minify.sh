#!/bin/sh
basedir="$PWD"

cd "$basedir"/js && rm init.min.js && python -m jsmin \
\
../libs/bootstrap/js/bootstrap.min.js \
../libs/handlebars/handlebars-v4.0.5.js \
../libs/underscore/underscore-min.js \
\
common/locale.js \
common/CommonOptions.js \
common/Preloader.js \
common/Date.js \
common/Handlebars.js \
common/CommonModel.js \
common/CommonPluginModel.js \
common/CommonPayments.js \
common/CommonView.js \
\
validators/ValidatorAbstract.js \
validators/ValidatorEmail.js \
validators/ValidatorPhone.js \
validators/ValidatorRequired.js \
validators/ValidatorPassword.js \
validators/ValidatorLength.js \
\
model/api-client.js \
model/api-model.js \
model/client-login.model.js \
model/membership.model.js \
\
view/flexible-timeline.js \
view/client-login.view.js  \
view/membership.view.js \
view/paymentwall.js \
view/paypal.js \
view/adyen.js \
view/view.js \
\
controller.js \
> init.min.js
cd "$basedir"/themes/modern/js && rm booking.view.min.js && python -m jsmin booking.view.js > booking.view.min.js
