<?php
/**
 * User: vetal
 * Date: 25.12.15
 * Time: 13:22
 */


switch($do){
	case 'show':
		$theme_url = SIMPLYBOOK_URL.'/themes/'.$simplybook_cfg['template'];

		ob_start();
		include_once SIMPLYBOOK_DIR.'/themes/'.$simplybook_cfg['template'].'/template.php';
		$content = ob_get_contents();
		ob_end_clean();
		break;

	case 'admin_page':
		include_once SIMPLYBOOK_DIR.'/modules/admin-page.php';
		break;

	case 'api':
		include SIMPLYBOOK_DIR.'/modules/api.php';
		break;

	case 'stripe_api':
		include SIMPLYBOOK_DIR.'/modules/stripe-api.php';
		break;

	case 'paypal_api':
		include SIMPLYBOOK_DIR.'/modules/paypal-api.php';
		break;

	case 'paymentwall_api':
		include SIMPLYBOOK_DIR.'/modules/paymentwall-api.php';
		break;

	case 'adyen_api':
		include SIMPLYBOOK_DIR.'/modules/adyen-api.php';
		break;
}


?>