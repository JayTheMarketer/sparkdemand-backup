


var apiClient = function(api_url, onError, preloader){
    this.init(api_url, onError, preloader);
};



jQuery.extend(apiClient.prototype, CommonOptions.prototype, {

    options: {
        preloader : null,
    },

    preloader : null,

    init : function(apiUrl, onError, preloader){
        this.apiUrl = apiUrl;
        this.onError = onError;
        this.preloader = preloader;
    },

    getData : function(params){
        if(!this.preloader){
            if(this.hasOption('preloader')){
                this.preloader = this.getOption('preloader');
            } else {
                this.preloader = new sbPreloader();
            }
        }

        params = [].slice.call(params); //arguments obj to array

        var callback = null;

        if(typeof params[params.length-1] === 'function'){
            callback = params.pop();
        }

        var method = params.shift();

        if(method == 'getCompanyParam'){
            if(callback==null){
                return simplybook_params[params[0]];
            } else {
                callback(simplybook_params[params[0]]);
            }
        } else
        if(method == 'getCompanyParams'){
            var res_params = {};
            for(var id in params[0]){
                var param = params[0][id];
                res_params[param] = simplybook_params[param];
            }

            if(callback==null){
                return res_params;
            } else {
                callback(res_params);
            }
        } else {
            return this._downloadData(method, params, callback);
        }

    },


    _downloadData : function(method, params, callback){

        //console.log(method, params, callback, this.apiUrl);
        var instance = this;
        var res = null;
        this.preloader.show();

        jQuery.ajax({
            url: this.apiUrl,
            type: 'post',
            async: callback==null?false:true,
            data: {
                method: method,
                params: this.__validate(params)
            },
            dataType: 'json',
            cache: false,
            success: function(data) {
                instance.preloader.hide();
                //console.log(data);
                if(data && typeof data['error'] !== 'undefined'){
                    instance.onError(data['error']);
                    res = false;
                } else
                if(callback==null){
                    res = data;
                } else {
                    callback(data);
                }
            }
        });

        return res;

    },

    __validate : function(obj){
        if(obj) {
            for (var keyName in obj) {
                if (obj.hasOwnProperty(keyName) && _.isObject(obj[keyName]) && !_.isNull(obj[keyName]) && Object.keys(obj[keyName]).length == 0) {
                    obj[keyName] = [""];
                }
            }
        }
        return obj;
    }


});

