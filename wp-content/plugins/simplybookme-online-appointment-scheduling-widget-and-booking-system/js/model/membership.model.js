/**
 * Created by vetal on 29.03.17.
 */

var MembershipPluginModel = function (model, options) {
    this._construct(model, options);
};

jQuery.extend(MembershipPluginModel.prototype, CommonPluginModel.prototype,{
    globalModel : null,
    loc : null,
    cache : null,

    options : {
        onError : function (error) {
            console.log(error);
        }
    },

    loggedCallback : null,
    globalModelErrorHandler: null,

    _construct : function( model, opts ) {
        var instance = this;
        this.setOptions(opts);
        this.loc = new sbLocale(simplybook_translation);
        this.cache = [];
        this.globalModel = model;
    },

    getUserData : function (callback) {
        this.globalModel.getClientInfo(jQuery.proxy(function (res) {
            callback(res);
        }, this));
    },

    getMembershipList : function (callback, options) {
        var inst = this;
        if(this.hasOption('membership_list')){
            callback(this.getOption('membership_list'));
            return;
        }

        var cachedData = this.getTransient('membership_list');
        if(cachedData){
            callback(cachedData);
            return;
        }

        this._enableInterceptionError(options?options.onError:null);
        this.globalModel.request('getMembershipList', function (result) {
            inst._disableInterceptionError();
            inst.setTransient('membership_list', result, 15*60); //cache to 15 min
            inst.setOption('membership_list', result);
            if(callback){
                callback(result);
            }
        });
    },

    getMembership : function (membershipId, callback, options) {
        this.getMembershipList(jQuery.extend(function (membershipList) {
            var isFinded = false;
            _.each(membershipList, function (membership) {
                 if(!isFinded && parseInt(membership.id) === parseInt(membershipId)){
                     callback(membership);
                     isFinded = true
                 }
            });
            if(!isFinded){
                callback(null);
            }
        }, this), options);
    },

    getClientMembershipList  : function (callback, options) {
        var inst = this;
        var cachedData = this.getTransient('client_membership_list');

        if(cachedData){
            callback(cachedData);
            return;
        }

        this._enableInterceptionError(options?options.onError:null);
        this.globalModel.request('getClientMembershipList', function (result) {
            inst._disableInterceptionError();
            inst.setTransient('client_membership_list', result, 5*60); //cache to 5 min
            if(callback){
                callback(result);
            }
        });
    },

    getPaymentHistory  : function (callback, options) {
        var inst = this;
        var cachedData = this.getTransient('client_membership_payment_history');

        if(cachedData){
            callback(cachedData);
            return;
        }

        this._enableInterceptionError(options?options.onError:null);
        this.globalModel.request('getMembershipPaymentHistory', function (result) {
            inst._disableInterceptionError();
            inst.setTransient('client_membership_payment_history', result, 5*60); //cache to 5 min
            if(callback){
                callback(result);
            }
        });
    },


    makeMembershipPayment  : function (membershipId, callback, options) {
        var inst = this;

        this._enableInterceptionError(options?options.onError:null);
        this.globalModel.request('makeMembershipPayment', membershipId, function (result) {
            inst._disableInterceptionError();
            inst.deleteTransient('client_membership_list');
            inst.deleteTransient('client_membership_payment_history');
            if(callback){
                callback(result);
            }
        });
    }

});