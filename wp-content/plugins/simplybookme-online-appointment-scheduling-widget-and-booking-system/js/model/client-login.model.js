/**
 * Created by vetal on 29.03.17.
 */

var ClientLoginPluginModel = function (model, options) {
    this._construct(model, options);
};

jQuery.extend(ClientLoginPluginModel.prototype, CommonPluginModel.prototype,{
    globalModel : null,
    loc : null,
    cache : null,

    options : {
        registerForm: {
            validators : {
                name : [new ValidatorLength({required:true})],
                email : [new ValidatorEmail({required:true})],
                phone : [new ValidatorPhone({required:true})],
                password : [new ValidatorPassword({required:true})]
            }
        },

        editProfileForm: {
            validators : {
                name : [new ValidatorLength({required:true})],
                email : [new ValidatorEmail({required:true})],
                phone : [new ValidatorPhone({required:true})],
                password : [new ValidatorPassword({required:false})]
            }
        },

        autorizationForm: {
            validators : {
                email : [new ValidatorEmail({required:true})],
                password : [new ValidatorPassword({required:true})]
            }
        },

        remindPasswordForm: {
            validators : {
                email : [new ValidatorEmail({required:true})]
            }
        },
        onError : function (error) {
            console.log(error);
        }
    },

    loggedCallback : null,
    userData : null,
    globalModelErrorHandler: null,

    _construct : function( model, opts ) {
        var instance = this;
        this.setOptions(opts);
        this.loc = new sbLocale(simplybook_translation);
        this.cache = [];
        this.globalModel = model;

    },

    register : function (data, callback, options) {
        this._enableInterceptionError(options?options.onError:null);
        this.globalModel.request('signUpClient', data, jQuery.proxy(function(result) {
            this._disableInterceptionError();
            if(result && result.id){
                this.userData = result;
                this.setTransient('user_data', result, 24*60); //cache
                this.globalModel.request('saveToSession', {
                    user_data : result
                });
                callback(result);
            } else {
                //  error
                this.getOption('onError')(this.loc.gettext('api_error'));
            }
        }, this));
    },

    auth : function (email, password, callback, options) {
        this._enableInterceptionError(options?options.onError:null);
        this.globalModel.request('getClientInfoByLoginPassword', email, password, jQuery.proxy(function(result) {
            this._disableInterceptionError();
            if(result && result.id){
                if(parseInt(result.is_blocked)){
                    this.getOption('onError')(this.loc.gettext('api_error'));
                }
                this.userData = result;
                this.setTransient('user_data', result, 24*60); //cache
                callback(result);

                this.globalModel.request('saveToSession', {
                    user_data : result
                });
            } else {
                //  error
                this.getOption('onError')(this.loc.gettext('api_error'));
            }
        }, this));
    },

    unauth : function (callback) {
        this.userData = null;
        this.setTransient('user_data', null, 24*60); //cache
        this.globalModel.request('saveToSession', {
            user_data : null
        }, jQuery.proxy(function() {
            callback();
        }, this));
    },

    remind : function (email, callback, options) {
        this._enableInterceptionError(options?options.onError:null);
        this.globalModel.request('remindClientPassword', email, jQuery.proxy(function (res) {
            this._disableInterceptionError();
            if(callback){
                callback(res);
            }
        }, this));
    },

    editProfile : function (data, callback, options) {
        this._enableInterceptionError(options?options.onError:null);

        for(var key in data){
            if(typeof data[key] == 'string' && data[key].length == 0){
                delete data[key];
            }
        }

        this.globalModel.request('modifyClientInfo', data, jQuery.proxy(function (res) {
            this._disableInterceptionError();
            if(callback){
                callback(res);
            }
        }, this));
    },

    fetchUserData : function (callback) {
        this.globalModel.getClientInfo(jQuery.proxy(function (res) {
            this.userData = res;
            callback(res);
        }, this));
    },

    getClientBookings : function (callback, options) {
        this._enableInterceptionError(options?options.onError:null);
        this.globalModel.request('getClientBookings', jQuery.proxy(function (res) {
            this._disableInterceptionError();
            if(callback){
                callback(res);
            }
        }, this));
    },

    isLogged : function (callback) {
        if(this.userData && !parseInt(this.userData.is_blocked) ) {
            callback(this.userData);
        } else {
            this.fetchUserData(callback);
        }
    }

});