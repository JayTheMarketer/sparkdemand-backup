/**
 * Created by vetal on 01.12.15.
 */


var simplybookApiModel = function (options) {
    //this.__construct(options);

    this._init(options);
};


jQuery.extend(simplybookApiModel.prototype, CommonModel.prototype,{

    options: {
        'url' : simplybook_api_url,
    },

    tmp: {},
    selectedData: {},

    _init : function(options){
        var instance = this;
        this.setOptions(options);
        this.deleteTransient('user_data'); //remove user data

        this.apiClient = new apiClient(this.options.url, function(res){
            if( _.isUndefined(instance.options.on_error) ){
                instance._onError(res);
            }else {
                instance.options.on_error(res);
            }
        }, this.getOption('preloader'));
    },

    _onError : function(error){
        console.log(error);
    },

    getApiClient : function () {
        return this.apiClient;
    },

    setLoginData : function(login,apiKey){
        this.options.api_key = apiKey;
        this.options.login = login;
    },

    
    request : function(){
        return this.apiClient.getData(arguments);
    },

    downloadUnitList : function(callback){
        var instance = this;

        this.request('getUnitList',function(res){
            instance.tmp['unit_list'] = res;
            callback(res, Object.keys(res).length);
        });
    },

    getUnitList : function(){
        var res = this.addAnyUnitPlugin(this.tmp['unit_list'], this.tmp['anyunit']);
        res = this.filterUnitsByLocation(res, this.tmp['loc_list'], this.selectedData['location_id']);
        res = this.filterUnitsByService(res, this.tmp['event_list'], this.selectedData['service_id']);
        return res;
    },

    downloadEventList : function(callback){
        var instance = this;

        this.request('getEventList',function(res){
            instance.tmp['event_list'] = res;
            callback(res, res, Object.keys(res).length);
        });
    },

    getEventList : function(){
        var res = this.filterServicesByCategory(this.tmp['event_list'], this.tmp['cat_list'], this.selectedData['category_id'] );
        res = this.filterServicesByLocation(res, this.tmp['loc_list'], this.selectedData['location_id']);
        return res;
    },

    downloadCategoriesList : function(callback){
        var instance = this;

        this.request('getCategoriesList',function(res){
            instance.tmp['cat_list'] = res;
            callback(res, Object.keys(res).length);
        });

    },

    getCategoriesList : function(){
        return this.tmp['cat_list'];
    },

    downloadLocationsList : function(param, callback){
        var instance = this;

        this.request('getLocationsList',param, function(res){
            instance.tmp['loc_list'] = res;
            callback(res, Object.keys(res).length);
        });
    },

    getLocationsList: function(){
        return this.tmp['loc_list'];
    },


    getAllList : function(callback){
        var instance = this;
        var pluginStatuses = instance.tmp['plugin_statuses'];

        if(!_.isUndefined(instance.tmp['all_list'])){
            callback(instance.tmp['all_list']);
            return;
        }

        var resFunc = function () {
            instance.request('getAllList', function(res){
                if(!pluginStatuses || pluginStatuses.location) {
                    instance.tmp['loc_list'] = res['getLocationsList'];
                }
                if(!pluginStatuses || pluginStatuses.event_category){
                    instance.tmp['cat_list'] = res['getCategoriesList'];
                }

                if(!pluginStatuses || pluginStatuses.group_booking) {  //get max group booking
                    instance.options['max_group_bookings'] = parseInt(res['getCompanyParam']);
                } else {
                    instance.options['max_group_bookings'] = false;
                }

                instance.tmp['event_list'] = res['getEventList'];
                instance.tmp['unit_list'] = res['getUnitList'];


                instance.tmp['all_list'] = {
                    loc_list : instance.tmp['loc_list']?instance.tmp['loc_list']:[],
                    cat_list : instance.tmp['cat_list']?instance.tmp['cat_list']:[],
                    event_list : instance.tmp['event_list']?instance.tmp['event_list']:[],
                    unit_list : instance.tmp['unit_list']?instance.tmp['unit_list']:[],
                    max_group_bookings : instance.hasOption('max_group_bookings')?instance.getOption('max_group_bookings'):null,
                };

                callback(instance.tmp['all_list']);
            });
        };

        if(_.isUndefined(pluginStatuses) ){ //if isset plugin status
            this.getPluginsStatus(function () {
                resFunc();
            });
        } else {
            resFunc();
        }
    },


    getAnyUnitData : function(callback){
        var instance = this;

        this.request('getAnyUnitData',function(res){
            instance.tmp['anyunit'] = res;
            callback(res);
        });
    },

    getPluginsStatus : function(callback){
        var instance = this;

        if(!_.isUndefined(this.tmp['plugin_statuses'])){
            callback(this.tmp['plugin_statuses']);
            return;
        }

        var plugins_list = [
            'any_unit',
            'location',
            'event_category',
            'event_field',
            'group_booking',
            'multiple_booking',
            'promo',
            'approve_booking',
            'user_license',
            'cancelation_policy',
            'client_login',
            'membership'
        ];

        this.request('getPluginStatuses', plugins_list, function(res){
            instance.tmp['plugin_statuses'] = res;
            callback(res);
        });
    },

    getMaxUnitQty : function(unitData){
        var maxQty = 1;
        for(var unit_id in unitData){
            if(maxQty<parseInt(unitData[unit_id].qty)){
                maxQty = parseInt(unitData[unit_id].qty);
            }
        }
        return maxQty;
    },

    addAnyUnitPlugin : function( unitData, anyUnitData ){
        if(anyUnitData != null){ // if any unit plugin active
            var unit_obj = jQuery.extend(anyUnitData,{
                id: '-1',
                is_active: "1",
                is_visible: "1"
            });

            jQuery.extend(unitData, {
                '-1' : unit_obj
            });

            if(anyUnitData['hide_other_units']){
                unitData = {
                    '-1' : unit_obj
                };
            }
        }

        return unitData;
    },

    getServiceDuration : function(serviceId, unitId){
        if(_.isUndefined(serviceId)){
            serviceId = this.getServiceId();
        }

        var duration = 0;

        if(this.tmp['event_list'] && this.tmp['event_list'][serviceId] && this.tmp['event_list'][serviceId]['duration']){
            duration = this.tmp['event_list'][serviceId]['duration'];
        }

        if(!_.isUndefined(unitId) && this.tmp['event_list'] &&
            this.tmp['event_list'][serviceId] && this.tmp['event_list'][serviceId]['unit_map'] &&
            this.tmp['event_list'][serviceId]['unit_map'][unitId] && this.tmp['event_list'][serviceId]['unit_map'][unitId] != null)
        {
            duration = this.tmp['event_list'][serviceId]['unit_map'][unitId];
        }

        return parseInt(duration);
    },

    filterUnitsByLocation : function(unitData, locationData, selectedLocationId){

        if(!_.isUndefined(locationData) && selectedLocationId){   /// Get avialable units in current location
            var unitsInLoc = locationData[selectedLocationId]['units'];
            unitsInLoc.push('-1'); //any unit plugin
        }

        if(!_.isUndefined(unitsInLoc)) {  //clear not avialable by location
            var newRes = {};
            for (var i = 0; i< unitsInLoc.length; i++){
                for (var unit in unitData) {
                    if( parseInt(unitData[unit]['id']) ==  parseInt(unitsInLoc[i]) ){
                        newRes[unit] = unitData[unit];
                    }
                }
            }
            unitData = newRes;
        }

        return unitData;
    },

    filterUnitsByService : function(unitData, serviceData, selectedServiceId){

        if(!_.isUndefined(serviceData) && selectedServiceId){

            if( _.isObject(serviceData[selectedServiceId]['unit_map'])){
                var unitsInService = Object.keys(serviceData[selectedServiceId]['unit_map']);
            } else {
                var unitsInService = Object.keys(unitData);
            }
            unitsInService.push('-1'); //any plugin unit

            var newRes = {};
            for (var i = 0; i< unitsInService.length; i++){
                for (var unit in unitData) {
                    if( parseInt(unitData[unit]['id']) ==  parseInt(unitsInService[i]) ){
                        newRes[unit] = unitData[unit];
                    }
                }
            }
            unitData = newRes;
        }

        return unitData;
    },

    filterServicesByCategory : function(serviceData, categoryData, selectedCategoryId){

        if(!_.isUndefined(categoryData) && selectedCategoryId){   /// Get avialable services in current category
            var cat = selectedCategoryId;
            var servicesInCat = categoryData[cat]['events'];
        }

        if(!_.isUndefined(servicesInCat)) {  //clear not avialable by services
            var newRes = {};
            for (var i = 0; i< servicesInCat.length; i++){
                for (var service in serviceData) {
                    if( parseInt(serviceData[service]['id']) ==  parseInt( parseInt(servicesInCat[i])) ){
                        newRes[service] = serviceData[service];
                    }
                }
            }
            serviceData = newRes;
        }

        return serviceData;
    },

    filterServicesByLocation : function(serviceData, locationData, selectedLocationId){

        if(!_.isUndefined(locationData) && selectedLocationId){   /// Get avialable units in current location
            var unitsInLoc = locationData[selectedLocationId]['units'];
        } else {
            return serviceData;
        }

        for(var servId in serviceData){
            if(_.isObject(serviceData[servId]['unit_map'])){

                var unitsInService = Object.keys(serviceData[servId]['unit_map']);

                var intersect = jQuery(unitsInService).filter(unitsInLoc);

                if(intersect.length == 0){
                    delete serviceData[servId];
                }
            }
        }

        return serviceData;
    },

    getClientInfo : function (callback) {
        var cachedData = this.getTransient('user_data');

        if(cachedData){
            callback(cachedData);
            return;
        }

        this.request('getFromSession', 'user_data', jQuery.proxy(function (res) {
            this.setTransient('user_data', res['user_data'], 24*60); //cache
            callback(res['user_data']);
        }, this));
    },

    membershipCheckClientAccess : function (eventId, callback) {
        var cachedData = this.getTransient('user_data');
        if(!cachedData){
            return;
        }
        this.request('membershipCheckClientAccess', eventId, jQuery.proxy(function (res) {
            callback(res);
        }, this));
    },

    setLocationId : function(id){
        this.selectedData['location_id'] = parseInt(id);
    },

    getLocationId : function(){
        return this.selectedData['location_id'];
    },

    setCategoryId : function(id){
        this.selectedData['category_id'] = parseInt(id);
    },

    getCategoryId : function(){
        return this.selectedData['category_id'];
    },

    setServiceId : function(id){
        this.selectedData['service_id'] = parseInt(id);
    },

    getServiceId : function(){
        return this.selectedData['service_id'];
    },

    setProviderId : function(id){
        this.selectedData['provider_id'] = parseInt(id);
    },

    getProviderId : function(){
        return this.selectedData['provider_id'];
    },

    setGroupBooking : function(count){
        this.selectedData['group_booking'] = parseInt(count);
    },

    getGroupBooking : function(){
        return _.isUndefined(this.selectedData['group_booking'])? 1 : this.selectedData['group_booking'];
    },

    getMaxGroupBooking : function(){
        return  this.options['max_group_bookings'];
    }


});