/**
 * Created by vetal on 27.01.16.
 */


var AdyenPayments = function(container){
    this.init(container);
};


jQuery.extend(AdyenPayments.prototype, CommonOptions.prototype, {

    formObj : null,
    popupWindow : null,
    blurObj : null,
    onClosedCallback : null,

    options : {
        adyen_form : '.adyen_form'
    },

    init : function(container){
        var instance = this;

        if(_.isObject(container) && container.length){
            this.formObj = container.find(this.getOption('adyen_form'));
        } else {
            this.formObj = jQuery(this.getOption('adyen_form'));
        }

        this.formObj.submit(function() {
            instance.popupWindow =  window.open('', 'adyen_formpopup', 'width=1000,height=600,resizeable,scrollbars');
            this.target = 'adyen_formpopup';

            var loop = setInterval(function() {
                if(instance.popupWindow.closed) {
                    clearInterval(loop);
                    instance.popupClosed();
                }
            }, 1000);

            instance.addBlur();
        });
    },

    popupClosed : function(){
        this.removeBlur();

        if(typeof this.onClosedCallback == 'function'){
            this.onClosedCallback();
        }
    },

    onPopupClosed : function(callback){
        this.onClosedCallback = callback;
    },

    addBlur : function(){
        this.blurObj = jQuery('<div class="adyen_inprocess"></div>');
        jQuery('BODY').append(this.blurObj);
    },

    removeBlur : function(){
        this.blurObj.remove();
    },

    setFormData : function(paramObj){
        this.formObj.attr('action', 'https://' + (simplybook_adyen_mode=='test'?'test':'live') + '.adyen.com/hpp/select.shtml');

        for(var pName in paramObj){
            var value = paramObj[pName];
            this.formObj.append('<input type="hidden" name="' + pName + '" value="' + value + '" />');
        }
    }



});


