/**
 * Created by vetal on 29.01.16.
 */


var PaymentwallPayments = function(){
    this.init();
};


jQuery.extend(PaymentwallPayments.prototype, CommonOptions.prototype, {

    options : {
        brickform : 'paymentwall-payment-form-container'
    },

    popupWindow : null,
    brickWindow : null,
    blurObj : null,
    onClosedCallback : null,

    init : function(){


    },

    onPopupClosed : function(callback){
        this.onClosedCallback = callback;
    },

    addBlur : function(){
        this.blurObj = jQuery('<div class="paymentwall_inprocess"></div>');
        jQuery('BODY').append(this.blurObj);
    },

    removeBlur : function(){
        this.blurObj.remove();
    },

    setBrickData : function(name, serviceName, amount, currency, bookings, button_name){
        this.brickWindow = new Brick({
            public_key: simplybook_paymentwall_key,
            amount: amount,
            currency: currency,
            container: this.getOption('brickform'),
            action: simplybook_api_url + '&method=paymentwall_checkBrick&bookings=' + bookings.join(","),
            form: {
                merchant: name,
                product: serviceName, //bookings ids
                pay_button: button_name?button_name:'Pay',
                zip: true
            }
        });

    },

    showWidget : function(url){
        var instance = this;
        this.popupWindow =  window.open(url, 'paymentwall_formpopup', 'width=1000,height=600,resizeable,scrollbars');

        var loop = setInterval(function() {
            if(instance.popupWindow.closed) {
                clearInterval(loop);
                instance.onClosedCallback();
                instance.removeBlur();
            }
        }, 1000);

        this.addBlur();
    },

    showBrick : function(){
        var instance = this;

        this.brickWindow.showPaymentForm(
            function(data) {
                if(typeof instance.onClosedCallback == 'function'){
                    instance.onClosedCallback(true,data);
                    jQuery('#paymentwall-brick-payment-t-container').hide('slow').detach(); //hack, global block
                }
                //instance.removeBlur();
            },
            function(errors) {
                if(typeof instance.onClosedCallback == 'function'){
                    instance.onClosedCallback(false,errors);
                }
                //instance.removeBlur();
            }
        );

        //this.addBlur();
    }


});



/*

 jQuery( document ).ready(function() {
 var paypalp = new PaymentwallPayments();
     paypalp.setData("Vetal", "Service 1", 10, "USD", [111,112],'Pay');
     paypalp.show();

 });*/
