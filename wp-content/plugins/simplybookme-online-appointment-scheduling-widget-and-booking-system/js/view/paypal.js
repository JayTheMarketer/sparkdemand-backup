/**
 * Created by vetal on 27.01.16.
 */


var PayPalPayments = function(container){
    this.init(container);
};


jQuery.extend(PayPalPayments.prototype, CommonOptions.prototype, {

    options : {
        paypal_form : '.paypal_form'
    },

    formObj : null,
    popupWindow : null,
    blurObj : null,
    onClosedCallback : null,

    init : function(container){
        var instance = this;

        if(_.isObject(container) && container.length){
            this.formObj = container.find(this.getOption('paypal_form'));
        } else {
            this.formObj = jQuery(this.getOption('paypal_form'));
        }

        this.formObj.submit(function() {
            instance.popupWindow =  window.open('', 'paypal_formpopup', 'width=1000,height=600,resizeable,scrollbars');
            this.target = 'paypal_formpopup';

            var loop = setInterval(function() {
                if(instance.popupWindow.closed) {
                    clearInterval(loop);
                    instance.popupClosed();
                }
            }, 1000);

            instance.addBlur();
        });
    },

    popupClosed : function(){
        this.removeBlur();

        if(typeof this.onClosedCallback == 'function'){
            this.onClosedCallback();
        }
    },

    onPopupClosed : function(callback){
        this.onClosedCallback = callback;
    },

    addBlur : function(){
        this.blurObj = jQuery('<div class="paypal_inprocess"></div>');
        jQuery('BODY').append(this.blurObj);
    },

    removeBlur : function(){
        this.blurObj.remove();
    },

    setFormData : function(){
        this.formObj.attr('action', 'https://' + simplybook_paypal_url + '/cgi-bin/webscr');
        //this.formObj.find('input[name="first_name"]').val(name);
        //this.formObj.find('input[name="item_name_1"]').val(serviceName);
        //this.formObj.find('input[name="amount_1"]').val(amount);
        //this.formObj.find('input[name="currency_code"]').val(currency);
        this.formObj.find('input[name="business"]').val(simplybook_paypal_account);

        this.formObj.find('input[name="shopping_url"]').val(this.getHostname(simplybook_api_url) );

        this.formObj.find('input[name="return"]').val(simplybook_api_url + '&method=paypal_returnPage');
        this.formObj.find('input[name="cancel_return"]').val(simplybook_api_url + '&method=paypal_returnPage');
        this.formObj.find('input[name="notify_url"]').val(simplybook_api_url + '&method=paypal_notify');
    },

    getHostname : function(url) {
        var m = url.match(/^http:\/\/[^/]+/);
        return m ? m[0] : null;
    }

});

/*
jQuery( document ).ready(function() {
    var paypalp = new PayPalPayments();
    paypalp.setFormData("Vetal", "Service 1", 10, "USD");

});*/
