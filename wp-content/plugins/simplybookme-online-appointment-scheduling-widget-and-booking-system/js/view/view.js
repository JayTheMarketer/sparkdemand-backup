

var SimplybookView = function ( options) {
    this._construct( options);
};


jQuery.extend(SimplybookView.prototype, CommonView.prototype, {
    model: null,
    loc: null,
    cache: null,
    options: {
        container : '#sb-plugin-container',
        preloaderClass: '.simplybook_preloader',
        viewContainer : '.sb-view-window-node',
        viewList : {
            bookings : '#simplybook_view',
            clientLogin : '#client-login-plugin-c',
            clientLoginHistory : '#client-login-bookings-c',
            membership : '#membership-plugin-c'
        }
    },
    pluginStatuses : {},

    membershipPlugin : null,
    clientLoginPlugin : null,
    node : null,
    childViewsNode : null,

    preloader : null,

    _construct: function (opts) {
        var instance = this;
        this.setOptions(opts);

        this.node = jQuery(this.getOption('container'));
        this.loc = this.getOption('loc');
        this.handlebars = this.getOption('handlebars');
        this.model = this.getOption('model');
        this.cache = [];

        this.preloader = new sbPreloader({
            preloaderClass : this.getOption('preloaderClass'),
            preloaderMode : _.isUndefined(window['simplybook_loader_mode'])?'global':simplybook_loader_mode,
        });

        this.model.setOptions({
            preloader: this.preloader,
        });

        this.model.getApiClient().setOptions({
            preloader: this.preloader,
        });

        instance.model.getPluginsStatus( function(pluginStatuses) {
            instance.pluginStatuses = pluginStatuses;
            instance.handlebars.registerPluginStatusHelper();
            instance.handlebars.registerDateTimeHelper();

            instance.initViews();
            instance.initEvents();
        });
    },

    _initCustomPlugins : function () {
        this.membershipPlugin = new MembershipPlugin({
            form : this.getOption('viewList').membership,
            plugins : this.pluginStatuses,
            model : this.model,
            loc : this.loc,
            handlebars : this.handlebars
        });

        this.clientLoginPlugin = new ClientLoginPlugin({
            form : this.getOption('viewList').clientLogin,
            model : this.model,
            plugins : this.pluginStatuses,
            membership : this.membershipPlugin,
            loc : this.loc,
            handlebars : this.handlebars
        });
    },

    initViews : function () {
        this._initCustomPlugins();

        this.view = new SimplybookBookingView(this.getOption('viewList').bookings, {
            handlebars : this.handlebars,
            model : this.model,
            loc : this.loc,
            membershipPlugin : this.membershipPlugin,
            clientLoginPlugin : this.clientLoginPlugin
        });

    },

    initEvents : function () {
        var inst = this;

        //initViewsParams
        var viewsNames = _.keys(this.getOption('viewList'));
        var viewListNodes = {};
        _.each(viewsNames, function (key, val) {
            viewListNodes[key] = inst.node.find(inst.getOption('viewList')[key]);
            viewListNodes[key].addClass(inst.getOption('viewContainer').replace('.', ''));
        });
        this.childViewsNode = jQuery(this.getOption('viewContainer'));
        this.setOption('viewListNode', viewListNodes);

        //init triggers
        this.on('viewSwitch', function (param) {
            inst.viewSwitch(param);
        });
    },

    viewSwitch : function (view) {
        var viewNodes = this.getOption('viewListNode');
        var viewArrs = _.keys(viewNodes);

        if(!_.isUndefined(view) && _.contains(viewArrs, view)){
            this.childViewsNode.hide();
            viewNodes[view].show();
        }
    },

});