/**
 * Created by vetal on 16.02.16.
 */


var FlexibleTimeline = function (model, options) {
    this.init(model, options);
};


jQuery.extend(FlexibleTimeline.prototype, CommonOptions.prototype, {
    model : null,
    options : {
        timelineClassId : null,
        timeframe : null,
        eventId : null,
        performerId : null,
        duration : 0,
        serviceCount : 1,
        errorCallback: null,
        companyTimezone : null
    },
    currentTimeframes : null,
    apiRequsetFunction : null,
    cache : null,

    init : function( model, opts ){
        var instance = this;
        this.loc = new sbLocale(simplybook_translation);
        this.setOptions(opts);

        this.cache = [];
        this.model = model;
        this.getTimeFrame();
    },

    showError : function(mess){
        if( typeof this.options.errorCallback == 'function' ){
            this.options.errorCallback(mess);
        }
    },

    setCompanyTimezone : function(timezone){
        this.options.companyTimezone = timezone;
    },

    getTimeFrame : function(callback){
        var instance = this;

        if(this.options['timeframe'] == null){
            this.request('getTimeframe', function(timeframe){
                instance.options['timeframe'] = parseInt(timeframe);
                if(callback){
                    callback(timeframe);
                }
            });
        } else if(callback){
            callback(instance.options['timeframe']);
        }
    },

    request : function(){
        this.model.apiClient.getData(arguments);
    },

    setData : function(performerId, eventId, count){
        var instance = this;

        this.options.performerId = performerId;
        this.options.eventId = eventId;
        this.options.serviceCount = count;
        this.options.duration = this.model.getServiceDuration( this.options.eventId, this.options.performerId );

        var currMouseTime = null;
        var container = jQuery(instance.options.timelineClassId);

        container.off("mouseenter").on("mouseenter", 'span.time', function(){
            var type = jQuery(this).attr('data-type');
            if(type != 'free'){
                return;
            }

            var currTime = jQuery(this).attr('data-time');

            if(currMouseTime != currTime){
                currMouseTime = currTime;

                var usedTimeframes = instance._getUsedTimeframes(currTime);

                for(var id in usedTimeframes){
                    jQuery("span.time[data-time='" + usedTimeframes[id] + "']", container).addClass('selected');
                }
            }

        });

        container.off("mouseleave").on("mouseleave", 'span.time', function(){
            var currTime = jQuery(this).attr('data-time');
            jQuery('span.time', container).removeClass('selected');
        });
    },

    checkTimeframe : function(date, time, callback){
        var instance = this;

        var usedTimeframes = this._getUsedTimeframes(time);

        this.isReservedExist(date, function(reserved){

            //Check if selected timeframes already reserved by othe users or Google calendar
            //console.log("Check if selected timeframes already reserved...");
            //console.log("Selected timeframes:", usedTimeframes);

            var reservedTimes = reserved[date][0]['events'];//jQuery.merge(reserved[date][0]['events'], reserved[date][0]['dd']); //reserved + reserved by Google calendar
            //console.log("Reserved timeframes:", reservedTimes);

            for(var id in usedTimeframes){
                var currTimeframe = usedTimeframes[id];

                for(var jd in reservedTimes){
                    var currReserved = reservedTimes[jd];

                    var a = [instance.HMTosec(currTimeframe), instance.HMTosec(currTimeframe)+instance.options.timeframe];
                    var b = [instance.HMTosec(currReserved.from), instance.HMTosec(currReserved.to)];

                    if( instance._isIntervalsOverlap( a, b ) ){
                        //console.log("Error: timeframe ",currTimeframe, "reserved by ", currReserved);
                        callback(false); // Error: Already reserved
                        return;
                    }
                }
            }
            //console.log("Ok! Selected timeframes not reserved :)");

            //check if is work times
            //console.log("Check if is work times...");

            instance.request('calculateEndTime', date + ' ' + time, instance.options.eventId, instance.options.performerId, function(res) {
                if(res == false || res == 'false'){
                    //console.log("Error: is not worked time :(");
                    callback(false); //Error: not worked time
                    return;
                } else {
                    //console.log("Ok! Current day checked. ");
                    var endDate = instance.formatDate( Date.createFromMysql(res) );

                    if(endDate != date){
                        //console.log("Hmm..Current date is " + date + ", but service end date is " + endDate );
                        //TODO
                        callback(true);

                    } else {
                        //console.log("All good!");
                        callback(true);
                    }
                }
            });
        });

    },

    isTimeframeExist : function(date, callback){
        var instance = this;

        //console.log(this.currentTimeframes);

        if(this.currentTimeframes && this.currentTimeframes[date]){
            callback(this.currentTimeframes);
        } else {
            this.getTimeframeMap(date, date, callback, function (res) {
                callback(res);
            });
        }
    },

    isReservedExist : function(date, callback){
        var instance = this;

        if(!this.cache['reservedTime']){
            this.cache['reservedTime'] = [];
        }

        if(this.cache['reservedTime'][date]){
            callback(this.cache['reservedTime']);
        } else {
            this.request('getReservedTime', date, date, this.options.eventId, this.options.performerId, this.options.serviceCount, function (res) {
                for(var id in res){
                    if(!instance.cache['reservedTime'][id]){
                        instance.cache['reservedTime'][id] = jQuery.extend({}, res[id]);
                    }
                }
                callback(instance.cache['reservedTime']);
            });
        }
    },

    _getUsedTimeframes : function(HMtime){
        var workSecFrom = this.HMTosec(HMtime);
        var workSecTo = workSecFrom + this.options.duration*60;
        var usedTimeframes = [this.secToHM(workSecFrom,true)];

        for(var i = workSecFrom + this.options.timeframe*60; i <= workSecTo  && (i + this.options.timeframe*60) <= workSecTo; i+= this.options.timeframe*60){
            usedTimeframes.push(this.secToHM(i,true));
        }
        return usedTimeframes;
    },

    getTimeframeMap : function(dateFrom, dateTo, callback){
        var instance = this;

        this.request('getReservedTime', this.formatDate(dateFrom), this.formatDate(dateTo), this.options.eventId, this.options.performerId, this.options.serviceCount, function(reserved){
            instance.request('getWorkDaysInfo', instance.formatDate(dateFrom), instance.formatDate(dateTo), instance.options.performerId, instance.options.eventId, function(workDayInfo) {
                instance.cache['reservedTime'] = reserved;
                 instance.cache['workDayInfo'] = workDayInfo;

                 var dateFromD = new Date(dateFrom);
                 var dateToD = new Date(dateTo);

                 var currentDate = new Date( dateFromD.getTime() );
                 var timeframes = [];

                 if(!instance.currentTimeframes){
                     instance.currentTimeframes = [];
                 }

                 while(currentDate <= dateToD){
                     var nowDate = instance.formatDate(currentDate);
                     var currReserved = reserved[nowDate];
                     var currDayInfo = workDayInfo[nowDate];
                     timeframes[nowDate] = [];


                     var workSecFrom = Math.min.apply(Math, jQuery.map( currDayInfo, function(val) { return instance.HMTosec(val.from); }) );
                     var workSecTo = Math.max.apply(Math, jQuery.map( currDayInfo, function(val) { return instance.HMTosec(val.to); }) );


                     //Create all time slots
                     for(var i = workSecFrom; i <= workSecTo  && (i + instance.options.timeframe*60) <= workSecTo; i+= instance.options.timeframe*60){
                         var currFrame = instance.secToHM(i);

                         timeframes[nowDate].push({
                             from: currFrame,
                             to: instance.secToHM( i + instance.options.timeframe*60),
                             fromSec: i,
                             toSec: i + instance.options.timeframe*60,
                             type: 'free'
                         });
                     }

                     //filter slots reserved by Google calendar
                     //instance._filterTimeframesByIntervalArray(timeframes[nowDate], currReserved[0]['dd'], 'reserved_google');

                     //filter slots reserved by other users
                     instance._filterTimeframesByIntervalArray(timeframes[nowDate], currReserved[0]['events'], 'reserved');

                     //filter not worked time
                     instance._filterTimeframesByIntervalArray(timeframes[nowDate], currReserved[1]['events'], 'not_worked');

                     currentDate = currentDate.setDate(currentDate.getDate() + 1); //Add one day

                     instance.currentTimeframes[nowDate] = jQuery.extend({}, timeframes[nowDate]);
                 }

                 //instance.currentTimeframes = timeframes;
                 callback(timeframes);
             });
         });
    },


    _filterTimeframesByIntervalArray : function(timeframeIntervals, intervals, newType){
        var instance = this;

        for(var tid in timeframeIntervals){
            var currTimeframeInterval = timeframeIntervals[tid];

            for(var eid in intervals){
                var currEventInterval = intervals[eid];

                var a = [currTimeframeInterval.fromSec, currTimeframeInterval.toSec];
                var b = [instance.HMTosec(currEventInterval.from), instance.HMTosec(currEventInterval.to)];

                if( instance._isIntervalsOverlap( a, b ) ){
                    currTimeframeInterval.type = newType;
                    break;
                }
            }
        }
        return timeframeIntervals;
    },

    _isIntervalsOverlap : function(a, b){
        var a_start = a[0], a_end = a[1], b_start = b[0], b_end = b[1];
        if( a_start == b_start && a_end == b_end) return true; //a == b
        if( a_start < b_start && a_end < b_end && a_end == b_start) return false; // b starts in a end
        if( a_start > b_start && a_end > b_end && a_start == b_end) return false; // a starts in b end
        if (a_start <= b_start && b_start <= a_end) return true; // b starts in a
        if (a_start <= b_end   && b_end   <= a_end) return true; // b ends in a
        if (b_start <  a_start && a_end   <  b_end) return true; // a in b
        return false;
    },

    showDayTimeline : function(date){
        var instance = this;
        if(this.options.timelineClassId != null){
            this.model.request('getCompanyParam', 'time_format', function(time_format) {

                var viewObj = jQuery(instance.options.timelineClassId);
                viewObj.addClass('flexible-timeline time-format-'+time_format).empty();

                var viewTable = jQuery('<table class="timeline-grid with-titles"></table>').appendTo(viewObj);

                instance.getTimeframeMap(date, date, function (timeframes) {
                    var timeframe = timeframes[instance.formatDate(date)];
                    //console.log(timeframe);
                    var viewTr = null;
                    var currentHour = '';

                    for (var id in timeframe) {
                        var currFrame = timeframe[id];
                        var frameHour = instance.secToHM(parseInt(currFrame.fromSec / 3600) * 3600);
                        frameHour = Handlebars.helpers.formatDateTime('2010-10-10 '+ frameHour, 'time' );  //without seconds (H:i)

                        if (frameHour != currentHour) {
                            currentHour = frameHour;
                            viewTr = jQuery('<tr class="hour-row"></tr>').appendTo(viewTable);
                            viewTr.append('<th class="hour-title"><div class="dot"><span>&#x1f550;</span></div><div class="info"><span class="group-time">' + currentHour + '</span></div></th>');
                            viewTr.append('<td class="time-items"></td>');
                        }

                        var hourPlusDate = Handlebars.helpers.formatDateTime('2010-10-10 '+ currFrame.from, 'plus_day' )
                        var plusDay = (hourPlusDate!=0?'<i class="date_plus_day">'+(hourPlusDate>0?"+":"")+hourPlusDate+' '+instance.loc.gettext('day')+'</i>':'');
                        var frameFrom = Handlebars.helpers.formatDateTime('2010-10-10 '+ currFrame.from, 'time' )  //without seconds (H:i)

                        viewTr.find('.time-items').append('<div class="time-item time-select-div ' + currFrame.type + '"><span class="time" data-date="' + instance.formatDate(date) + '" data-time="' + currFrame.from + ':00"  data-type="' + currFrame.type + '" >' + frameFrom + plusDay + '</span></div>');
                    }
                });
            });
        }
    },

    formatDate : function (date) {
        var year = date.getFullYear();
        var month = ("0" + (date.getMonth() + 1)).slice(-2);
        var day = ("0" + date.getDate()).slice(-2);

        return year + '-' + month + '-' + day;
    },

    secToHM : function(seconds, returnSeconds){
        var hours = ("0" + parseInt( seconds / 3600 ) ).slice(-2);
        seconds = seconds % 3600;
        var minutes = ("0" + parseInt( seconds / 60 ) ).slice(-2);
        seconds = ("0" + seconds % 60 ).slice(-2);
        if(returnSeconds){
            return hours + ':' + minutes + ':' + seconds;
        } else {
            return hours + ':' + minutes;
        }
    },

    HMTosec : function(HMString){
        var p = HMString.split(':');
        var s = (+p[0]) * 60 * 60 + (+p[1]) * 60;
        if(p[2]){
            s += (+p[2]);
        }
        return s;
    }

});