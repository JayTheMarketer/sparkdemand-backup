
var MembershipPlugin = function (options) {
    this._construct(options);
    return this;
};

jQuery.extend(MembershipPlugin.prototype, CommonView.prototype, {
    globalModel : null,
    loc : null,
    cache : null,
    options : {
        form : '',
        menu : {
            history : ".sb-membership-menu-history",
            plans : ".sb-membership-menu-plans",
            purchased : ".sb-membership-menu-purchased"
        },
        content : '#sb-membership-content',
        buyBtn : '.sb-membership-action .active-container',
        paymentMethodBtn : '.methods .method_status',
        paymentResult : '#sb-membership-payment-result',
        buyMembershipModal : '#sb-membership-buy-c',
        buyMembershipBtn : '#sb_buy_membership_btn',
        plansFilter: '#sb-membership-filter',
        plansMemItem: '.sb-membership-item'
    },

    loggedCallback : function () {},
    changedCallback : function () {},

    _construct : function( opts ) {
        var instance = this;

        this.setOptions(opts);
        this.loc = this.getOption('loc');
        this.handlebars = this.getOption('handlebars');
        this.cache = [];
        this.globalModel = this.getOption('model');
        this.model = new MembershipPluginModel(this.globalModel, {});
        this.model.setOption('onError', jQuery.proxy(this.onError,this));
    },

    show : function () {
        this.init();
        this.trigger('viewSwitch', 'membership');
    },

    init : function (handler) {
        if(!this.options.plugins['membership']){
            return;
        }

        this.handlebars.initHandlebars({}, 'membership-plugin-t', this.getOption('form'));
        this.initDom();
        this.initEvents();
    },

    initDom : function () {
        this.formNode = jQuery(this.getOption('form'));
        this.membershipModalObj = jQuery(this.getOption('buyMembershipModal'));

        this.showPlans();  //show plans by default
    },

    initEvents : function () {
        var inst = this;
        var membershipMenu = this.getOption('menu');

        this.formNode.on('click', membershipMenu.plans , function () {
            inst.showPlans();
            return false;
        });

        this.formNode.on('click', membershipMenu.purchased , function () {
            inst.showPurchased();
            return false;
        });

        this.formNode.on('click', membershipMenu.history , function () {
            inst.showPaymentHistory();
            return false;
        });

        this.formNode.on('click', this.getOption('buyBtn'), function () {
            var membershipId = parseInt(jQuery(this).data('id'));
            if(membershipId){
                inst.buyMembership(membershipId);
            }
        });

        this.formNode.on('click', this.getOption('paymentMethodBtn'), function () {
            var paymentMethod = jQuery(this).data('payment');
            if(paymentMethod){
                inst.payMembership(paymentMethod);
            }
        });

        this.membershipModalObj.on('click', this.getOption('buyMembershipBtn'), function () {
            var serviceId = jQuery(this).data('id');
            inst.membershipModalObj.modal('hide'); //bootstrap modal
            inst.trigger('viewSwitch', 'membership');
            inst.showPlans(function () {
                inst.filterMembershipPlans(serviceId);
            });

        });

        this.formNode.on('click', this.getOption('plansFilter') + ' a', function () {
            var serviceId = jQuery(this).data('service');
            inst.filterMembershipPlans(serviceId);
            return false;
        });
    },

    _showError : function (error, notHide) {
        this.onError(error, notHide, jQuery(this.remindPasswordForm.find('.plugin_info .error') ));
    },

    showPlans : function (callback) {
        var inst = this;
        this.globalModel.getAllList(function (itemsList) {
            inst.model.getClientMembershipList (function (clientMemberships) {
                inst.model.getMembershipList(jQuery.proxy(function (memberships) {
                    this.handlebars.initHandlebars(jQuery.extend({
                        memberships : memberships,
                        clientMemberships : clientMemberships,
                        clientMembershipIds : _.map(clientMemberships, function (val, key) {
                            return val.id;
                        })
                    },itemsList), 'membership-plugin-plans-t', this.getOption('content'));

                    if(callback){
                        callback();
                    }
                }, inst), {
                    onError: jQuery.proxy(inst._showError, inst)
                });
            }, {
                onError: jQuery.proxy(inst._showError, inst)
            });
        });
    },

    filterMembershipPlans : function (serviceId) {
        var itemsNode = this.formNode.find(this.getOption('plansMemItem'));

        if(serviceId){
            itemsNode.hide();
            this.model.getMembershipList(function (memberships) {
                _.each(memberships, function (membership) {
                    if(!membership.services){
                        return;
                    }
                    var services = _.map(membership.services, function (val) {
                        return parseInt(val);
                    });

                    if(_.isArray(services) && services.indexOf(serviceId) >= 0){
                        itemsNode.filter(function() {
                            return parseInt(jQuery(this).data("id")) === parseInt(membership.id)
                        }).show();
                    }
                });
            });
        } else {
            itemsNode.show();
        }
    },


    showPurchased : function () {
        this.model.getClientMembershipList (jQuery.proxy(function (memberships) {
            this.handlebars.initHandlebars({memberships : memberships}, 'membership-plugin-purchased-t', this.getOption('content'));
        }, this), {
            onError: jQuery.proxy(this._showError, this)
        });
    },

    showPaymentHistory : function () {
        this.model.getPaymentHistory(jQuery.proxy(function (payments) {
            this.handlebars.initHandlebars({payments : payments}, 'membership-plugin-history-t', this.getOption('content'));
        }, this), {
            onError: jQuery.proxy(this._showError, this)
        });
    },

    buyMembership : function (membershipId) {
        var inst = this;
        this.cache['lastMembershipId'] = membershipId;

        this.model.getMembership(membershipId, jQuery.proxy(function (membership) {
            inst.model.getClientMembershipList (jQuery.proxy(function (clientMemberships) {
                clientMemberships = _.map(clientMemberships, function (val, key) {
                    return parseInt(val.id);
                });

                var price = membership.first_price;
                if(clientMemberships.indexOf(parseInt(membership)) >= 0){
                    price = membership.recurring_price;
                }

                if(price > 0){ //membership paid
                    var processorsStatus = {
                        stripe : parseInt(simplybook_stripe_status),
                        paypal : parseInt(simplybook_paypal_status),
                        adyen : parseInt(simplybook_adyen_status),
                        paymentwall: parseInt(simplybook_paymentwall_status)==2,
                        paymentwall_brick: parseInt(simplybook_paymentwall_status)==1,
                    };

                    this.handlebars.initHandlebars({/*payment: paymentData*/id : membershipId, status : processorsStatus}, 'membership-plugin-payments-t', this.getOption('content'));
                } else { //membership free
                    this.model.makeMembershipPayment(membershipId, jQuery.proxy(function (res) {
                        this.handlebars.initHandlebars(jQuery.extend({
                            result : (res && res.status=='paid')?1:0,
                            name : membership.name,
                            membership: true
                        }, res), 'payment-result-t', inst.getOption('content'));
                    }, inst), {
                        onError: jQuery.proxy(inst._showError, inst)
                    });
                }
            }, inst), {
                onError: jQuery.proxy(inst._showError, inst)
            });
        }, this), {
            onError: jQuery.proxy(this._showError, this)
        });
    },

    payMembership : function (paymentMethod) {
        //var paymentData = this.cache['lastPaymentData'];
        var membershipId = this.cache['lastMembershipId'];
        this.paymentsView = new CommonPayments();
        this.paymentsView.setOptions({
            loc : this.loc,
            model : this.globalModel,
            handlebars : this.handlebars
        });
        this.paymentsView.initPayments();

        if( _.isFunction(this['_payMembershiWith' + paymentMethod]) ){
            this['_payMembershiWith' + paymentMethod].call(this, 'm' + membershipId/*, paymentData*/);
        } else {
            console.error('This payment processor not supported');
        }
    },

    showBuyMessage : function (eventId) {
        if(!this.membershipModalObj){
            this.init();
        }

        this.handlebars.initHandlebars({
            eventId : eventId
        }, 'membership-plugin-buy-membership-t', this.membershipModalObj);

        this.membershipModalObj.modal(); //bootstrap modal
    },

    _payMembershiWithdelay : function (membershipId) {
        var inst = this;

        this.paymentsView.payDelayPayment(membershipId, function (res) {
            inst.handlebars.initHandlebars({result : res?1:0}, 'payment-result-t', inst.getOption('content'));
        });
    },

    _payMembershiWithstripe : function (membershipId) {
        var inst = this;

        this.paymentsView.initStripePayment(membershipId, function () {
            inst.paymentsView.payStripePayment(function (res) {
                inst.handlebars.initHandlebars({result : res?1:0}, 'payment-result-t', inst.getOption('content'));
            });
        });
    },

    _payMembershiWithpaypal : function (membershipId) {
        var inst = this;

        this.model.getUserData(function (clientData) {
            inst.paymentsView.initPaypalPayment(membershipId, clientData, inst.getOption('content'), function () {
                inst.paymentsView.payPaypalPayment(function (res) {
                    inst.handlebars.initHandlebars({result : res?1:2}, 'payment-result-t', inst.getOption('content'));
                });
            });
        });
    },

    _payMembershiWithadyen : function (membershipId) {
        var inst = this;

        this.model.getUserData(function (clientData) {
            inst.paymentsView.initAdyenPayment(membershipId, clientData, inst.getOption('content'), function () {
                inst.paymentsView.payAdyenPayment(function (status, statusId) {
                    var status = (status && statusId>0)?statusId:0;
                    inst.handlebars.initHandlebars({result : status}, 'payment-result-t', inst.getOption('content'));
                });
            });
        });
    },

    _payMembershiWithpaymentwall : function (membershipId) {
        var inst = this;

        this.paymentsView.initPaymentwallPayment(membershipId, function () {
        });

        this.paymentsView.payPaymentwallPayment(function (res) {
            inst.handlebars.initHandlebars({result : res?1:2}, 'payment-result-t', inst.getOption('content'));
        });
    },

    _payMembershiWithpaymentwall_brick : function (membershipId) {
        var inst = this;

        this.model.getUserData(function (clientData) {
            inst.paymentsView.initPaymentwallBrickPayment(membershipId, clientData, inst.getOption('content'), function () {
                inst.paymentsView.payPaymentwallBrickPayment(function (res, data) {
                    var status = (res && data['success'])?data['success']:0;
                    inst.handlebars.initHandlebars({result : status}, 'payment-result-t', inst.getOption('content'));
                });
            });
        });
    }


});