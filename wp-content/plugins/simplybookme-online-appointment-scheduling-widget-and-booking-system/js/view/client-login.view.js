
var ClientLoginPlugin = function ( options) {
    this._construct( options);
};


jQuery.extend(ClientLoginPlugin.prototype, CommonView.prototype, {
    globalModel : null,
    loc : null,
    cache : null,
    options : {
        form : '', //#client-login
        bookingSteps : '#simplybook_view',
        registerForm: {
            'selector' : '#sb_sign_up_form',
            'submit' : '#sb_sign_up_btn',
            'data' : {
                name : '#sb_sign_up_name',
                email : '#sb_sign_up_email',
                phone : '#sb_sign_up_phone',
                password : '#sb_sign_up_password'
            }
        },
        autorizationForm: {
            'selector' : '#sb_sign_in_form',
            'submit' : '#sb_sign_in_btn',
            'data' : {
                email : '#sb_sign_in_email',
                password : '#sb_sign_in_password'
            }
        },

        remindPasswordForm: {
            'selector' : '#sb-remind-password-c',
            'submit' : '#sb_remind_btn',
            'data' : {
                email : '#sb_remind_email',
            }
        },

        editProfileForm: {
            'selector' : '#sb-edit-profile-c',
            'submit' : '#sb_edit_profile_btn',
            'data' : {
                name : '#sb_edit_profile_name',
                email : '#sb_edit_profile_email',
                phone : '#sb_edit_profile_phone',
                password : '#sb_edit_profile_password'
            }
        },

        userProfile : {
            selector : '#sb-account-panel-c',
            menu: {
                home : '#sb-home-booking',
                edit : '#sb-profile-edit',
                history : '#sb-profile-history',
                membership : '#sb-profile-membership',
                logout : '#sb-profile-logout'
            }
        },

        bookingsHistory: {
            selector : '#client-login-bookings-c',
            actions : {
                cancelBooking : '#sb-bookings-cancel-booking',
                back : '#sb-bookings-back'
            }
        }

    },

    loggedCallback : function () {},
    changedCallback : function () {},

    _construct : function( opts ) {
        var instance = this;
        this.setOptions(opts);

        this.loc = this.getOption('loc');
        this.handlebars = this.getOption('handlebars');
        this.cache = [];
        this.globalModel = this.getOption('model');
        this.model = new ClientLoginPluginModel(this.globalModel, {});
        this.model.setOption('onError', jQuery.proxy(this.onError,this));
    },

    init : function (handler) {
        this.onLogged(handler);

        if(!this.options.plugins['client_login']){
            this._logged(null);
            return;
        }

        this.handlebars.initHandlebars({}, 'client-login-plugin-t', this.getOption('form'));
        this.handlebars.initHandlebars({}, 'client-login-plugin-remind-password-t', this.getOption('remindPasswordForm').selector);

        this.initDom();
        this.initEvents();

        //if plugin activated - hide booking node and show authorization node
        this._onChangeCheckStatus();

    },

    _onChangeCheckStatus : function () {
        this.trigger('viewSwitch', 'clientLogin');
        this.model.isLogged(jQuery.proxy(function (res) {
            if(res){
                this.trigger('viewSwitch', 'bookings');
                this._logged(res);
            }
        }, this));
    },

    initDom : function () {
        this.formNode = jQuery(this.getOption('form'));
        this.bookingNode = jQuery(this.getOption('bookingSteps'));
        this.registerNewClientForm = jQuery(this.getOption('registerForm').selector);
        this.autorizationForm = jQuery(this.getOption('autorizationForm').selector);
        this.remindPasswordForm = jQuery(this.getOption('remindPasswordForm').selector);
        this.editProfileForm = jQuery(this.getOption('editProfileForm').selector);
        this.userProfileNode = jQuery(this.getOption('userProfile').selector);
        this.bookingHistoryNode = jQuery(this.getOption('bookingsHistory').selector);
    },

    initEvents : function () {
        var inst = this;

        this.registerNewClientForm.on('click', this.getOption('registerForm').submit, jQuery.proxy( function () {
            this._getFormData('registerForm', jQuery.proxy( function (res) {
                this.register(res);
            }, this));
        }, this));

        this.autorizationForm.on('click', this.getOption('autorizationForm').submit, jQuery.proxy( function () {
            this._getFormData('autorizationForm', jQuery.proxy( function (res) {
                this.auth(res);
            }, this));
        }, this));

        this.remindPasswordForm.on('click', this.getOption('remindPasswordForm').submit, jQuery.proxy( function () {
            this._getFormData('remindPasswordForm', jQuery.proxy( function (res) {
                this.remind(res.email);
            }, this));
        }, this));

        this.editProfileForm.on('click', this.getOption('editProfileForm').submit, jQuery.proxy( function () {
            this._getFormData('editProfileForm', jQuery.proxy( function (res) {
                this.editProfile(res);
            }, this));
        }, this));

        this.userProfileNode.on('mouseenter', jQuery.proxy( function (res) {
            this.showUserProfile();
        }, this));

        this.userProfileNode.on('mouseleave', jQuery.proxy( function (res) {
            this.hideUserProfile();
        }, this));


        //User profile
        var userProfileMenu = this.getOption('userProfile').menu;

        this.userProfileNode.on('click', userProfileMenu.logout ,jQuery.proxy( function (res) {
            this.unauth(jQuery.proxy(function () {
                this._onChangeCheckStatus();
            }, this));
        }, this));

        this.userProfileNode.on('click', userProfileMenu.edit ,jQuery.proxy( function (res) {
            this.editProfileForm.modal(); //bootstrap modal
        }, this));

        this.userProfileNode.on('click', userProfileMenu.history ,jQuery.proxy( function (res) {
            this.showBookingsHistory();
        }, this));

        this.userProfileNode.on('click', userProfileMenu.membership ,jQuery.proxy( function (res) {
            this.showMembership();
        }, this));

        this.userProfileNode.on('click', userProfileMenu.home ,jQuery.proxy( function (res) {
            this.showBookingProcess();
        }, this));


        //Bookings history

        var userHistoryActions = this.getOption('bookingsHistory').actions;

        this.bookingHistoryNode.on('click', userHistoryActions.cancelBooking , function (res) {
            var bookingHash = jQuery(this).data('hash');
            var bookingId = jQuery(this).data('id');
            inst.cancelBooking(bookingId, bookingHash);
        });

        this.bookingHistoryNode.on('click', userHistoryActions.back ,jQuery.proxy( function (res) {
            this.trigger('viewSwitch', 'bookings');
        }, this));

    },

    initUserProfile : function () {
        this.isLogged( jQuery.proxy(function (userData) {
            this.handlebars.initHandlebars(userData, 'client-login-plugin-panel-t', this.getOption('userProfile').selector);
            this.handlebars.initHandlebars(userData, 'client-login-plugin-edit-profile-t', this.getOption('editProfileForm').selector);
            //this.userProfileNode.find('.user_name').text(userData.name);
            this.userProfileNode.show();
        }, this));
    },

    showUserProfile : function () {
        this.getUserData(jQuery.proxy( function (userData) {
            //this._fillForm('editProfileForm', userData);
            this.userProfileNode.find('.profile').show();
        }, this));

    },

    hideUserProfile : function () {
        this.userProfileNode.find('.profile').hide();
    },

    fetchUserData : function (callback) {
        this.model.fetchUserData(function () {
            if(callback){
                callback();
            }
        });
    },

    getUserData : function (callback) {
        var oldLoggedHandler = this.loggedCallback;
        if(!this.options.plugins['client_login']){
            callback(null);
            return;
        }
        this.model.isLogged(jQuery.proxy(function (res) {
            if(res){
                callback(res)
            } else {
                this.trigger('viewSwitch', 'clientLogin');
                this.onLogged(jQuery.proxy(function (data) {
                    this.trigger('viewSwitch', 'bookings');
                    callback(data);
                    this.onLogged(oldLoggedHandler);
                }, this));
            }
        }, this));
    },

    register : function (data) {
        this.model.register(data, jQuery.proxy(function (res) {
            this._logged(res);
        }, this));
    },

    auth : function (data, callback) {
        this.model.auth(data['email'], data['password'], jQuery.proxy(function (res) {
            this._logged(res);
            if(callback){
                callback();
            }
        }, this));
    },

    unauth : function (callback) {
        this.model.unauth( jQuery.proxy(function () {
            this._unlogged();
            if(callback){
                callback();
            }
        }, this));
    },

    remind : function (email, callback) {
        this.model.remind( email, jQuery.proxy(function () {
            this.onMessage(this.loc.gettext('remind_email_sent'), null, jQuery(this.remindPasswordForm.find('.remind_info .mess')) );
            if(callback){
                callback();
            }
        }, this), {
            onError: jQuery.proxy(function (error, notHide) {
                this.onError(error, notHide, jQuery(this.remindPasswordForm.find('.remind_info .error') ));
            }, this)
        });
    },

    editProfile : function (data, callback) {
        this.model.editProfile(data, jQuery.proxy(function () {
            this.onMessage(this.loc.gettext('edit_profile_succ'), null, jQuery(this.editProfileForm.find('.edit_profile_info .mess')) );

            this.fetchUserData(jQuery.proxy(function () {
                this.initUserProfile();
                if(callback){
                    callback();
                }
            }, this));

        }, this), {
            onError: jQuery.proxy(function (error, notHide) {
                this.onError(error, notHide, jQuery(this.editProfileForm.find('.edit_profile_info .error') ));
            }, this)
        });
    },

    showBookingsHistory : function () {
        var inst = this;
        this.globalModel.getAllList(function (itemsList) {
            inst.model.getClientBookings(jQuery.proxy(function (bookings) {

                _.each(bookings, function (val, key) {
                    bookings[key].is_cancellable = parseInt(bookings[key].is_cancellable);
                });

                this.handlebars.initHandlebars(jQuery.extend({
                    bookings : _.isArray(bookings)?bookings.reverse():[]
                }, itemsList), 'client-login-booking-history-t', this.getOption('bookingsHistory').selector);

                this.trigger('viewSwitch', 'clientLoginHistory');

            }, inst), {
                onError: jQuery.proxy(function (error, notHide) {
                    this.onError(error, notHide, jQuery(this.editProfileForm.find('.edit_profile_info .error') ));
                }, inst)
            });
        });
    },

    cancelBooking : function (id, hash, callback) {
        var inst = this;
        if(id && hash){
            var isConfirmed = confirm(this.loc.gettext("Are you sure you want to cancel this booking?"));

            if(isConfirmed){
                this.globalModel.request('cancelBooking',id, hash, function(res){
                    inst.showBookingsHistory();
                });
            }
        }
    },

    showMembership : function () {
        var membership = this.getOption('membership');
        membership.show();
    },


    showBookingProcess : function () {
        this.trigger('viewSwitch', 'bookings');
    },

    onLogged : function (handler) {
        if(handler){
            this.loggedCallback = handler;
        }
    },

    onChange : function (handler) {
        if(handler){
            this.changedCallback = handler;
        }
    },

    _logged : function (data) {
        this.trigger('viewSwitch', 'bookings');
        if(data){
            this.initUserProfile();
        }
        this.loggedCallback(data);
        this.changedCallback(data);
    },

    _unlogged : function () {
        this.handlebars.removeBlock( this.getOption('userProfile').selector );
        this.changedCallback(null);
    },


    isLogged : function (callback) {
        this.model.isLogged(callback);
    }
});