

var SimplybookController = function (options) {
    this._construct(options);
};


jQuery.extend(SimplybookController.prototype, CommonOptions.prototype, {

    options : {

    },

    _construct : function (opts) {
        this.setOptions(opts);

        this.loc = new sbLocale(simplybook_translation);
        this.model = new simplybookApiModel({});

        this.handlebars = new HandlebarsInit({
            loc : this.loc,
            model : this.model
        });

        this.view = new SimplybookView({
            loc : this.loc,
            model : this.model,
            handlebars : this.handlebars
        });
    }
});



jQuery( document ).ready(function() {
    var sbcontroller = new SimplybookController({});
});