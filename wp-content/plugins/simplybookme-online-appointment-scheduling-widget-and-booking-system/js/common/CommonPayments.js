

var CommonPayments = function (model, options) {
};


jQuery.extend(CommonPayments.prototype, CommonOptions.prototype, {

    handlebars : null,

    stripeHandler : null,
    paypalHandler : null,
    paymentwallBrickHandler : null,
    paymentwallHandler : null,
    adyenHandler : null,

    initPayments : function () {
        this.handlebars = this.getOption('handlebars');
        this.loc = this.getOption('loc');
        this.model = this.getOption('model');
    },

    payDelayPayment: function (bookingIds, callback) {
        var instance = this;

        this.model.request('confirmDelayPayment',bookingIds, function(res){
            instance.model.deleteTransient('client_membership_payment_history');
            callback(res);
        });
    },

    initStripePayment : function (bookingIds, callback) {
        var instance = this;

        this.model.request("getBookingCart", bookingIds, function(cart){
            instance.setOption('stipe_booking_cart', cart);
            instance.setOption('stipe_booking_ids', bookingIds);

            // Close Checkout on page navigation
            jQuery(window).on('popstate', function() {
                instance.stripeHandler.close();
            });

            if(callback){
                callback(cart);
            }

        });
    },

    payStripePayment: function (callback) {
        var instance = this;
        var cart = this.getOption('stipe_booking_cart');
        var bookingIds = this.getOption('stipe_booking_ids');

        if(!_.isArray(bookingIds)){
            bookingIds = [bookingIds];
        }

        this.stripeHandler = StripeCheckout.configure({
            key: simplybook_stripe_key,
            locale: 'auto',
            token: function(token) {
                instance.model.request('stripe_charge', token.id , bookingIds, function(res){
                    instance.model.deleteTransient('client_membership_payment_history');
                    callback(res);
                });
            }
        });

        // Open Checkout with further options
        this.stripeHandler.open({
            name: simplybook_stripe_name,
            description: simplybook_stripe_description + '. Booking IDs: ' + bookingIds.join(","),
            amount: cart['amount']*100,
            currency: cart['currency']
        });
    },


    initPaypalPayment : function (bookingIds, clientData, container, callback) {
        var instance = this;

        this.model.request("getBookingCart", bookingIds, function(cart){
            if(!_.isArray(bookingIds)){
                bookingIds = [bookingIds];
            }

            instance.setOption('paypal_booking_cart', cart);
            instance.setOption('paypal_booking_ids', bookingIds);
            instance.setOption('paypal_client_data', clientData);

            if(!_.isObject(container)){
                container = jQuery(container);
            }

            instance.handlebars.initHandlebars({
                cart : cart,
                bookingIds: bookingIds.join(","),
                clientData: clientData,
            }, 'paypal-payment-t', container);

            instance.paypalHandler = new PayPalPayments(container);
            //instance.paypalHandler.setFormData(clientData['name'], 'Booking IDs: ' + bookingIds.join(","), cart['amount'], cart['currency']);
            instance.paypalHandler.setFormData();

            if(callback){
                callback(cart);
            }
        });
    },
    
    payPaypalPayment : function (callback) {
        var instance = this;
        var bookingIds = this.getOption('paypal_booking_ids');

        this.paypalHandler.onPopupClosed(function(){
            instance.model.request('paypal_check', bookingIds, function(res){
                instance.model.deleteTransient('client_membership_payment_history');
                callback(res);
            });
        });
    },


    initPaymentwallBrickPayment : function (bookingIds, clientData, container, callback) {
        var instance = this;

        this.model.request("getBookingCart", bookingIds, function(cart){
            if(!_.isArray(bookingIds)){
                bookingIds = [bookingIds];
            }

            instance.setOption('paymentwall_booking_cart', cart);
            instance.setOption('paymentwall_booking_ids', bookingIds);
            instance.setOption('paymentwall_client_data', clientData);

            if(!_.isObject(container)){
                container = jQuery(container);
            }

            instance.handlebars.initHandlebars({
                cart : cart,
                bookingIds: bookingIds.join(","),
                clientData: clientData,
            }, 'paymentwall-brick-payment-t'/*, container*/);

            instance.paymentwallBrickHandler = new PaymentwallPayments();
            instance.paymentwallBrickHandler.setOption('brickform', 'pw-form-' + cart.cart_id);
            instance.paymentwallBrickHandler.setBrickData(clientData['name'], bookingIds.join(","), cart['amount'], cart['currency'], bookingIds, instance.loc.gettext('Pay') );

            if(callback){
                callback(cart);
            }
        });
    },

    payPaymentwallBrickPayment : function (callback) {
        var instance = this;

        this.paymentwallBrickHandler.onPopupClosed(function(res, data){
            instance.model.deleteTransient('client_membership_payment_history');
            callback(res, data);
        });

        this.paymentwallBrickHandler.showBrick();
    },

    initPaymentwallPayment : function (bookingIds, callback) {
        var instance = this;

        this.setOption('paymentwall2_booking_ids', bookingIds);

        this.paymentwallHandler = new PaymentwallPayments();

        this.model.request("paymentwall_createWidgetLink", bookingIds, function(url){
            instance.paymentwallHandler.showWidget(url);
        });

        if(callback){
            callback();
        }
    },

    payPaymentwallPayment : function (callback) {
        var instance = this;

        var bookingIds = this.getOption('paymentwall2_booking_ids');

        this.paymentwallHandler.onPopupClosed(function(){
            instance.model.request('paymentwall_check', bookingIds, function(res){
               // instance.paymentwallHandler.hide();
                instance.model.deleteTransient('client_membership_payment_history');
                callback(res);
            });
        });
    },



    initAdyenPayment : function (bookingIds, clientData, container, callback) {
        var instance = this;

        this.model.request("getBookingCart", bookingIds, function(cart){
            if(!_.isArray(bookingIds)){
                bookingIds = [bookingIds];
            }

            instance.setOption('adyen_booking_cart', cart);
            instance.setOption('adyen_booking_ids', bookingIds);
            instance.setOption('adyen_client_data', clientData);

            if(!_.isObject(container)){
                container = jQuery(container);
            }

            instance.handlebars.initHandlebars({
                cart : cart,
                bookingIds: bookingIds.join(","),
                clientData: clientData,
            }, 'adyen-payment-t', container);

            instance.adyenHandler = new AdyenPayments();

            instance.model.request("adyen_createWidgetParams", bookingIds, function(paramsObj){
                //instance.adyenHandler.showWidget(url);
                instance.adyenHandler.setFormData(paramsObj);
            });

            if(callback){
                callback(cart);
            }
        });
    },

    payAdyenPayment : function (callback) {
        var instance = this;
        var bookingIds = this.getOption('adyen_booking_ids');

        this.adyenHandler.onPopupClosed(function(){
            instance.model.request('adyen_check', bookingIds, function(res){
                var status = res[0],
                    statusId = res[1];
                instance.model.deleteTransient('client_membership_payment_history');
                callback(status, statusId);
            });
        });
    },



});