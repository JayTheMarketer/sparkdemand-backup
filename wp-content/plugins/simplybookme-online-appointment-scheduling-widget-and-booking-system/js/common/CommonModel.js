

var CommonModel = function (model, options) {
};


jQuery.extend(CommonModel.prototype, CommonOptions.prototype, {

    getTransient : function (key) {
        var pObj = localStorage.getItem(key);
        //console.log(key, pObj);
        if(pObj){
            pObj = JSON.parse(pObj);
            var currTime = parseInt( (new Date).getTime() /1000 );
            if(pObj.time >= currTime){
                return pObj.data;
            } else {
                this.deleteTransient(key);
            }
        }
        return null;
    },


    setTransient : function (key, data, ttl) { //ttl in sec
        if(!ttl){
            ttl = 24*3600*3; //1 year;
        }
        var pObj = {
            time : parseInt( (new Date).getTime() /1000 ) + parseInt(ttl),
            data : data
        };
        localStorage.setItem(key, JSON.stringify(pObj));
    },

    deleteTransient : function (key) {
        localStorage.removeItem(key);
    },

    clearTransient : function (key, fromStart) {
        if(!key){
            localStorage.clear();
        } else {
            for (var lkey in localStorage){
                var pos = lkey.indexOf(key);

                if(fromStart && pos == 0){
                    this.deleteTransient(lkey);
                } else
                if(!fromStart && pos != -1){
                    this.deleteTransient(lkey);
                }
            }
        }
    }

});