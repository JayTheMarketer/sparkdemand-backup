jQuery.extend(Date.prototype, {

    monthsLong: ['January','February','March','April','May','June','July','August','September','October','November','December'],
    monthsShort: ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
    weekDaysLong: ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'],
    weekDayShort: ['Su','Mo','Tu','We','Th','Fr','Sa'],

    /**
     * Return date in mysql format
     *
     * @return String
     */
    getMysqlDate: function () {
        var month = this.getMonth() + 1;
        month = month > 9 ? month : '0' + month;

        var day = this.getDate();
        day = day > 9 ? day : '0' + day;

        var year = this.getFullYear();

        var result = year + '-' + month + '-' + day;

        return result;
    },

    /**
     * Return time in mysql format
     *
     * @return String
     */
    getMysqlTime: function () {
        var hours = this.getHours();
        hours = hours > 9 ? hours : '0' + hours;

        var mins = this.getMinutes();
        mins = mins > 9 ? mins : '0' + mins;

        var seconds = this.getSeconds();
        seconds = seconds > 9 ? seconds : '0' + seconds;

        return hours + ':' + mins + ':' + seconds;
    },

    /**
     * Return week days array
     * @return days array
     */

    getWeekDaysList: function (mondayIsFirst, shortNames) {
        if(_.isUndefined(shortNames)){
            shortNames = false;
        }
        this.loc = new sbLocale(simplybook_translation);
        var days = [];
        for (var i = 0; i < this.weekDaysLong.length; i++) {
            days.push(this.loc.gettext(shortNames?this.weekDaysLong[i]:this.weekDayShort[i]));
        }
        if (mondayIsFirst) {
            days.push(days.shift());
        }
        return days;
    },


    getMonthsList: function (shortNames) {
        if(_.isUndefined(shortNames)){
            shortNames = false;
        }
        this.loc = new sbLocale(simplybook_translation);
        var months = [];
        for (var i = 0; i < this.monthsLong.length; i++) {
            months.push(this.loc.gettext(shortNames?this.monthsShort[i]:this.monthsLong[i]));
        }
        return months;
    },

    /**
     * Return datetime in mysql format
     *
     * @return String
     */
    getMysqlDateTime: function () {
        var date = this.getMysqlDate();
        var time = this.getMysqlTime();

        return date + ' ' + time;
    },


    /**
     * Return number days in a specified month
     * @returns {number}
     */

    getMonthDays: function(){
        var d = new Date(this.getFullYear(), this.getMonth() + 1, 0);
        return d.getDate();
    },

    getFormattedDateTime : function () {
        return this.getFormattedDate() + ' ' + this.getFormattedTime();
    },

    getFormattedDate : function () {
        var format = (!_.isUndefined(simplybook_params) && !_.isUndefined(simplybook_params['date_format']))?simplybook_params['date_format']:'d-m-Y';
        return this.formatDate(format);
    },

    getFormattedTime : function (withSeconds) {
        var format = (!_.isUndefined(simplybook_params) && !_.isUndefined(simplybook_params['time_format']))?simplybook_params['time_format']:'24hr';
        if(_.isUndefined(withSeconds)){
            withSeconds = false;
        }

        if(format == '24hr'){
            return this.formatDate('H:i' + (withSeconds?':s':''));
        }
        return this.formatDate('h:i' + (withSeconds?':s':'') + ' A');
    },


    /**
     * Fromat date by format
     *
     * @param String format
     * @return String
     */
    formatDate: function (format) {
        this.loc = new sbLocale(simplybook_translation);
        var formatArr = format.split('');
        var result = [];
        for (var i = 0; i < format.length; i++) {
            var str = format[i];
            switch (format[i]) {
                case 'd':
                    str = this.getDate() > 9 ? this.getDate() : '0' + this.getDate();
                    break;
                case 'm':
                    var month = this.getMonth() + 1;
                    str = month > 9 ? month : '0' + month;
                    break;
                case 'Y':
                    str = this.getFullYear();
                    break;
                case 'H':
                    str = this.getHours();
                    break;
                case 'h':
                    var hours = this.getHours();
                    hours = hours % 12;
                    hours = hours ? hours : 12; // the hour '0' should be '12'
                    str = hours;
                    break;
                case 'A':
                    hours = this.getHours();
                    str = hours >= 12 ? 'pm' : 'am';
                    break;
                case 'i':
                    var minutes = this.getMinutes();
                    str = minutes < 10 ? '0'+minutes : minutes;
                    break;
                case 's':
                    str = this.getSeconds();
                    break;
                case 'y':
                    str = new String(this.getFullYear()).substr(2,2);
                    break;
                case 'M':
                    str = this.loc.gettext(this.monthsShort[this.getMonth()]);
                    break;
                case 'F':
                    str = this.loc.gettext(this.monthsLong[this.getMonth()]);
                    break;
                case 'l':
                    str = this.loc.gettext(this.weekDaysLong[this.getDay()]);
                    break;
            }

            result.push(str);
        }
        return result.join('');
    }

});

/**
 * Parse date/datetime string in mysql format
 *
 * @static
 * @param String date
 * @return Date
 */
Date.parseMysqlDate = function (mysqlString){
    var t, result = null;

    if( typeof mysqlString === 'string' ) {
        t = mysqlString.split(/[- :]/);

        result = new Date(t[0] + "/" + t[1] + "/" + t[2]);

        if(!_.isUndefined(t[3]) && !_.isNull(t[3])){
            result.setHours(t[3]);
        }
        if(!_.isUndefined(t[4]) && !_.isNull(t[4])){
            result.setMinutes(t[4]);
        }
        if(!_.isUndefined(t[5]) && !_.isNull(t[5])){
            result.setSeconds(t[4]);
        }
        //console.log(t[0], t[1] - 1, t[2], t[3] || 0, t[4] || 0, t[5] || 0);
        //result = new Date(t[0], t[1] - 1, t[2], t[3] || 0, t[4] || 0, t[5] || 0);
    }
    return result;
}


Date.createFromMysql = function (mysql_string) {
    return  Date.parseMysqlDate(mysql_string);
};

Date.formatHour = function(format, date, timezone_offset) {
    var day_original = date.getDate();
    //console.log(date, new Date( date.getTime() - timezone_offset*60*1000 ), timezone_offset);
    date = new Date( date.getTime() - timezone_offset*60*1000 );

    var hours = date.getHours();
    var minutes = date.getMinutes();
    var suffix = '';
    var day = 0;

    if(date.getDate() != day_original){
        if(timezone_offset>0){
            day--;
        } else{
            day++;
        }
    }

    if(format != '24hr'){
        suffix = (hours >= 12)? 'pm' : 'am';
        hours = ((hours + 11) % 12 + 1);
    }

    return Array(hours, minutes, suffix, day, date);
};