

var sbPreloader = function(options){
    this._init(options);
};


jQuery.extend(sbPreloader.prototype, CommonOptions.prototype, {

    mods : ['global', 'local', 'local_simple'],

    options : {
        preloaderMode : 'global',
        preloaderClass : '',
    },

    loaderObj : null,

    _init : function(options){
        this.setOptions(options);

        if(this.hasOption('preloaderClass') ){
            this.loaderObj = jQuery( this.getOption('preloaderClass') );
        } else {
            this.loaderObj = jQuery('<span class="simplybook_preloader"></span>').appendTo(jQuery('BODY'));
        }

        this._setLoaderMode();
    },

    _setLoaderMode : function () {
        var mod = this.getOption('preloaderMode');

        switch (mod){
            case 'local' :
                this.loaderObj.addClass('local');
                break;
            case 'local_simple' :
                this.loaderObj.addClass('local_simple');
                break;

            default :
                this.loaderObj.addClass('global');
                break;
        }
    },

    show : function () {
        this.loaderObj.show();
    },
    
    hide : function () {
        //console.log('hide');
        this.loaderObj.hide();
    }
});
