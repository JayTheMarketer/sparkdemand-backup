

var CommonView = function (model, options) {
};


jQuery.extend(CommonView.prototype, CommonOptions.prototype, {

    _getFormData : function (formName, callback) {
        var res = {};
        this.selectedFormNode = jQuery(this.getOption(formName).selector);
        jQuery.each(this.getOption(formName).data, jQuery.proxy( function (key, val) {
            res[key] = this.selectedFormNode.find(val).val();
        }, this));

        this.model.validateForm(formName, res, jQuery.proxy(function (errors) {
            var errorsLength = Object.keys(errors).length;
            this.selectedFormNode.find('.help-block').empty();

            if(!errorsLength){
                callback(res);
            } else{ //show error
                jQuery.each(errors, jQuery.proxy( function (nodeName, nodeErrors) {
                    this.selectedFormNode.find( this.getOption(formName).data[nodeName] ).next('.help-block').text(nodeErrors[0]);
                }, this));
            }
        }, this));
    },

    _fillForm : function (formName, data) {
        this.selectedFormNode = jQuery(this.getOption(formName).selector);

        jQuery.each(this.getOption(formName).data, jQuery.proxy( function (key, val) {
            if(data[key] !== undefined){
                this.selectedFormNode.find(val).val(data[key]);
            }
        }, this));
    },

    onError : function (error, notHide, customNode) {
        var infoBlock = this.formNode.find('.login_info, .plugin_info').find('.error');
        if(customNode){
            infoBlock = customNode;
        }
        var mess = this.loc.gettext('api_error');

        if(error && error.message){
            mess = error.message;
        }

        infoBlock.show('slow').html(mess);
        if(!notHide) {
            setTimeout(function () {
                infoBlock.hide('slow').empty();
            }, 7000);
        }
    },

    onMessage : function (message, notHide, customNode) {
        var infoBlock = this.formNode.find('.login_info, .plugin_info').find('.mess');
        if(customNode){
            infoBlock = customNode;
        }
        var mess = this.loc.gettext('api_error');

        infoBlock.show('slow').html(message);
        if(!notHide) {
            setTimeout(function () {
                infoBlock.hide('slow').empty();
            }, 7000);
        }
    },

    trigger : function (param, data) {
        jQuery(document).trigger('sb_plugin_mess', [param, data]);
    },
    
    on : function (param, callback) {
        jQuery(document).on('sb_plugin_mess', jQuery.proxy(function (event, reqTrigger, reqData) {
            if(reqTrigger == param){
                callback(reqData);
            }
        }));
    },




});