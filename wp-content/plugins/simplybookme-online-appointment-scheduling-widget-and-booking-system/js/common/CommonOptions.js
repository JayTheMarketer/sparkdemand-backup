

var CommonOptions = function (model, options) {
};


jQuery.extend(CommonOptions.prototype, {

    getOption : function (paramKey, defaultValue) {
        return this.getParam(paramKey, defaultValue);
    },

    setOption : function (paramKey, value) {
        return this.setParam(paramKey, value);
    },

    hasOption : function (paramKey) {
        return this.hasParam(paramKey);
    },

    setOptions: function (options) {
        this.__initNewOptions();
        this.options = jQuery.extend({}, this.options, options);
    },

    getParam: function (paramKey, defaultValue) {
        this.__initNewOptions();
        if (jQuery.type(this.options[paramKey]) === 'undefined') {
            return defaultValue;
        }
        return this.options[paramKey];
    },

    hasParam : function (paramKey) {
        return jQuery.type(this.options[paramKey]) !== 'undefined';
    },

    setParam: function (paramKey, value) {
        this.__initNewOptions();
        this.options[paramKey] = value;
    },

    __initNewOptions : function () {
        if( this.options === undefined){
            this.options = {};
        }
    },

});