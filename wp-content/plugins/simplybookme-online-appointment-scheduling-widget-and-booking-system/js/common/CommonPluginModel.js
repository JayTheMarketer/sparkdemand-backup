

var CommonPluginModel = function (model, options) {
};


jQuery.extend(CommonPluginModel.prototype, CommonModel.prototype, {

    _enableInterceptionError : function (customCallback) {
        this.globalModelErrorHandler = this.globalModel.getOption('on_error');
        this.globalModel.setOption('on_error', jQuery.proxy(function (a,b,c) {
            if(customCallback){
                customCallback(a,b,c);
            } else {
                var errorFunc = this.getOption('onError');
                errorFunc(a,b,c);
            }
        }, this));
    },

    _disableInterceptionError : function (customCallback) {
        this.globalModel.setOption('on_error',  this.globalModelErrorHandler);
    },

    validateForm : function (formName, data, callback) {
        var formValidators = this.getOption(formName).validators;
        if(!formValidators || !data){
            callback(true);
        }
        var errors = {};
        var dataLength = Object.keys(data).length;
        var dataCounter = 0;

        var resultFunc = function () {
            callback(errors);
        };

        jQuery.each(data, function (key, val) {
            if(!formValidators[key]){
                return;
            }
            jQuery.each(formValidators[key], function (i, currValidator) {
                currValidator.validate(val, function (isValid) {
                    if(!isValid){
                        errors[key] = currValidator.getErrors();
                    }
                    if(!i){
                        dataCounter++;
                    }
                    if(dataCounter == dataLength){
                        resultFunc();
                    }
                });
            });
        });
    },

});