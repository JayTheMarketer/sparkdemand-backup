

var HandlebarsInit = function (options) {
    this._init( options);
    return this;
};


jQuery.extend(HandlebarsInit.prototype, CommonOptions.prototype, {
    options : {
        imageUrl : 'http://simplybook.me/'
    },
    loc : null,

    _init : function (options) {
        this.setOptions(options);
        this.loc = this.getOption('loc');
        this.model = this.getOption('model');

        if( !_.isUndefined(simplybook_url) ) {
            this.setOption('imageUrl', simplybook_url);
        }

        this.registerHelpers();
    },

    initHandlebars : function (data, template, container) {
        var source = jQuery("#" + template).html();
        var templateRes = Handlebars.compile(source);

        if(!container){
            container = jQuery( '<div id="' + template + '-container"></div>' );
            container.insertBefore( jQuery("#" + template) );
        }

        jQuery(container).html(templateRes(data));
    },
    
    removeBlock : function (container) {
        jQuery(container).empty();
    },
    
    registerHelpers : function () {
        this.registerTranslateHelper();
        this.registerConditionHelper();
        //this.registerPluginStatusHelper();
    },


    registerTranslateHelper : function () {
        var instance = this;

        Handlebars.registerHelper('_t', function(text, options) {
            var result = instance.loc.gettext(text);
            _.each(options.hash, function (value, key) {
                result = result.replace('%' + key + '%', value);
            });
            result = instance.loc.gettext(result);
            return result;
        });

    },


    _conditionFunction : function(v1, rule, v2, options, instance) {
        switch (rule) {
            case '>':
                if (v1 > v2) {
                    return options.fn(instance);
                }
                break;
            case '<':
                if (v1 < v2) {
                    return options.fn(instance);
                }
                break;
            case '>=':
                if (v1 >= v2) {
                    return options.fn(instance);
                }
                break;
            case '<=':
                if (v1 <= v2) {
                    return options.fn(instance);
                }
                break;
            case '==':
                if (v1 == v2) {
                    return options.fn(instance);
                }
                break;
            case '!=':
                if (v1 != v2) {
                    return options.fn(instance);
                }
                break;
            case '===':
                if (v1 === v2) {
                    return options.fn(instance);
                }
                break;
        }
        return options.inverse(instance);
    },

    registerConditionHelper : function () {
        var instance = this;

        Handlebars.registerHelper('ifCond', function(v1, rule, v2, options) {
            return instance._conditionFunction(v1, rule, v2, options, this);
        });

        Handlebars.registerHelper('ifCondLength', function(v1, rule, v2, options) {
            if(!_.isObject(v1) && !_.isArray(v1)){ // if null or undefined
                v1 = null;
            }
            if(!_.isObject(v2) && !_.isArray(v2)){ // if null or undefined
                v2 = 0;
            }
            return instance._conditionFunction(v1?v1.length:0, rule, v2, options, this);
        });

        Handlebars.registerHelper('ifIn', function(elem, list, options) {
            if(!list){
                return false;
            }

            if(_.isArray(list)){
                if(list.indexOf(elem) > -1) {
                    return options.fn(this);
                }
            } else {
                if(_.isObject(list) && !_.isUndefined(list[elem])) {
                    return options.fn(this);
                }
            }

            return options.inverse(this);
        });

        Handlebars.registerHelper('var',function(name, value, context){
            this[name] = value;
        });

        Handlebars.registerHelper('for', function(from, to, incr, block) {
            if(!block && typeof incr == 'object'){
                block = incr;
                incr = 1;
            }
            var accum = '';
            for(var i = from; i <= to; i += incr) {
                if(block) {
                    accum += block.fn(i);
                }
            }
            return accum;
        });

        Handlebars.registerHelper('object', function(data, options) {
            return options.fn(data[options.hash.key]);
        });

        Handlebars.registerHelper('concat', function() {
            var outStr = '';
            for(var arg in arguments){
                if(!_.isObject(arguments[arg])){
                    outStr += arguments[arg];
                }
            }
            return outStr;
        });


        Handlebars.registerHelper('Config', function(param, options) { // NB options is obligatory standart param of handlebars
            if(!_.isUndefined(simplybook_params) && !_.isUndefined(simplybook_params[param])){
                return simplybook_params[param];
            }
            return undefined;
        });

        Handlebars.registerHelper('_toInt', function(param, options) { // NB options is obligatory standart param of handlebars
            return parseInt(param);
        });

        Handlebars.registerHelper('formatImagePath', function(path, options) {
            return instance.getOption('imageUrl') + path;
        });

        Handlebars.registerHelper('debug', function(options) {
            console.log('----- debug -----');
            _.each(this, function (value, key) {
                console.log(key, ' = ', value);
            });
        });
    },


    registerPluginStatusHelper: function () {

        this.model.getPluginsStatus( jQuery.proxy(function (pluginStatus) {

            Handlebars.registerHelper('ifPluginActive', function(pluginName, options) { // NB options is obligatory standart param of handlebars
                if(!_.isUndefined(pluginStatus[pluginName]) && pluginStatus[pluginName]){
                    return options.fn(this); // this syntax for all 'if'-like helpers
                } else {
                    return options.inverse(this);
                }
            });

        }, this));

    },

    registerDateTimeHelper : function (callback) {
        var instance = this;
        this._getClientTimeZone(jQuery.proxy(function (companyTimezone) {
            /**
             * Date time format
             *
             * @param mixed value
             * @param String format
             * @param Integer timeDiff - time difference in minutes
             * @return String
             */
            Handlebars.registerHelper('formatDateTime', function(value, format, timeDiff, options) {
                if(!value){
                    return;
                }
                if (_.isUndefined(timeDiff)) {
                    timeDiff = 0;
                    timeDiff = companyTimezone?-companyTimezone.offset:0;
                }

                if (_.isObject(timeDiff)) {
                    options = timeDiff;
                    timeDiff = companyTimezone?-companyTimezone.offset:0;
                }


                var val = null;

                if (_.isString(value)) {
                    val = Date.parseMysqlDate(value);
                } else
                if (_.isDate(value)) {
                    val = new Data();
                    val.setTime(value.getTime());
                } else {
                    val = new Date(value);
                }

                var originalDate = new Date(val.getTime());
                val.setTime(val.getTime() + parseInt(timeDiff) * 60 * 1000);
                var res = null;

                switch (format) {
                    case 'day':
                        res = val.getDate();
                        break;
                    case 'date':
                        res = val.getFormattedDate();
                        break;
                    case 'time':
                        res = val.getFormattedTime();
                        break;
                    case 'datetime':
                        res = val.getFormattedDateTime();
                        break;
                    case 'month':
                        res = val.getMonthsList()[val.getMonth()];
                        break;
                    case 'month_short':
                        res = val.getMonthsList(true)[val.getMonth()];
                        break;
                    case 'plus_day':
                        var formatData = Date.formatHour('24hr', originalDate, -timeDiff);
                        res = formatData[3];
                        break;
                }

                return res;
            });

            Handlebars.registerHelper('formatDuration', function(duration, options) {
                var type = 'time';
                if (options.hash && options.hash.type) {
                    type = options.hash.type;
                }

                switch (type) {
                    case 'year':
                    case 'month':
                    case 'week':
                    case 'day':
                        duration = parseInt(duration);
                        if (duration === 1) {
                            return duration + ' ' + instance.loc.gettext(type);
                        }
                        var lastDigit = Math.abs(duration % 10);
                        var preLastDigit = Math.abs(Math.round(duration / 10) % 10);
                        if (lastDigit === 1 && preLastDigit !== 1) {
                            return duration + ' ' + instance.loc.gettext('x1_' + type + 's');
                        }
                        if (lastDigit > 1 && lastDigit < 5 && preLastDigit !== 1) {
                            return duration + ' ' + instance.loc.gettext('2_3_4_' + type + 's');
                        }
                        return duration + ' ' + instance.loc.gettext(type + 's');
                    default:
                        var hours = Math.floor(duration / 60);
                        var minutes = duration - (hours * 60);

                        var result = '';
                        if (hours) {
                            result += hours + ' ' + instance.loc.gettext('hr.');
                        }
                        if (minutes) {
                            if (result.length) {
                                result += ' ';
                            }
                            result += minutes + ' ' + instance.loc.gettext('mins.');
                        }
                        return result;
                }
            });


            if(callback){
                callback();
            }
        },this));
    },


    _getClientTimeZone : function(callback){
        var instance = this;
        if( !this.hasOption('company_timezone')){
            var client_offset = new Date().getTimezoneOffset();
            this.model.request('getCompanyParam', 'show_booking_page_in_client_timezone', function(inClientTimezone){
                if(inClientTimezone){
                    instance.model.request('getClientTimezoneOffset', -client_offset, function(companyTimezone) {
                        instance.setOption('company_timezone' , companyTimezone);
                        callback(companyTimezone);
                    });
                } else {
                    instance.setOption('company_timezone' , false);
                    callback(false);
                }
            });

        } else {
            callback(this.getOption('company_timezone'));
        }
    }

});