/**
 * Created by vetal on 29.03.17.
 */

var ValidatorEmail = function (options) {
    this.__construct(options);
};

jQuery.extend(ValidatorEmail.prototype, ValidatorAbstract.prototype, {

    options: {
        'required': false
    },

    _validate: function (value, callback) {
        if (!value && this.getParam('required')) {
            this.addError('is_required');
        } else if (value) {
            var regexp = /\S+@\S+\.\S+/;
            if (!regexp.test(value)) {
                this.addError('invalid_characters');
            }
        }
        if (this.errors.length) {
            return callback(false);
        }
        return callback(true);
    }

});