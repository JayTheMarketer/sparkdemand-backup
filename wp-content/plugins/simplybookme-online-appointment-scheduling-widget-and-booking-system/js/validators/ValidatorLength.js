var ValidatorLength = function (options) {
    this.__construct(options);
};

jQuery.extend(ValidatorLength.prototype, ValidatorAbstract.prototype, {

    options: {
        'required': false,
        'min_length' : 6,
        'max_length' : 32
    },

    _validate: function (value, callback) {

        if(!value && this.getParam('required')){
            this.addError('is_required');
        } else if(value && value.length < this.getParam('min_length')){
            this.addError('min_length_error');
        } else if(value && value.length > this.getParam('max_length')){
            this.addError('max_length_error');
        }

        if (this.errors.length) {
            return callback(false);
        }
        return callback(true);

    }

});