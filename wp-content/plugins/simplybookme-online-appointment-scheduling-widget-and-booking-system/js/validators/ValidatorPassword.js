var ValidatorPassword = function (options) {
    this.__construct(options);
};

jQuery.extend(ValidatorPassword.prototype, ValidatorAbstract.prototype, {

    options: {
        'required': false
    },

    _validate: function (value, callback) {

        if(!value && this.getParam('required')){
            this.addError('is_required');
        } else if(value && value.length<6){
            this.addError('password_min_length_6');
        }

        if (this.errors.length) {
            return callback(false);
        }
        return callback(true);

    }

});