var ValidatorRequired = function (options) {
    this.__construct(options);
};

jQuery.extend(ValidatorRequired.prototype, ValidatorAbstract.prototype, {


    _validate: function (value, callback) {
        if (!value) {
            this.addError('is_required');
            return callback(false);
        }
        return callback(true);
    }

});