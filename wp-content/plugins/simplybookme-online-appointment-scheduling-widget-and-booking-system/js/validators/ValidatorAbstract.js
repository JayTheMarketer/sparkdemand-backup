/**
 * Created by vetal on 29.03.17.
 */

var ValidatorAbstract = function (options) {
    this.__construct(options);
};


jQuery.extend(ValidatorAbstract.prototype, CommonOptions.prototype, {
    errors: [],
    options: {},
    loc : null,

    __construct: function (options) {
        this.setOptions(options);
        this.loc = new sbLocale(simplybook_translation);
    },

    validate: function (value, callback) {
        this.errors = [];
        this._validate(value, callback);
    },

    getErrors: function () {
        return this.errors;
    },

    addError: function (err, skipFieldPrefix) {
/*        if (this.getParam('field') && !skipFieldPrefix) {
            err = this.getParam('field') + '_' + err;
        }*/
        this.errors.push(this.loc.gettext(err));
    }
});

