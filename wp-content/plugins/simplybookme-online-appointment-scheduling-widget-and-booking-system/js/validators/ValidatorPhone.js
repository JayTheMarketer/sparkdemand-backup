var ValidatorPhone = function (options) {
    this.__construct(options);
};

jQuery.extend(ValidatorPhone.prototype, ValidatorAbstract.prototype, {

    options: {
        'required': false
    },

    _validate: function (value, callback) {
        var phone = value.replace(this.getParam('prefix'), '');
        if (!phone && this.getParam('required')) {
            this.addError('is_required');
        } else if (phone) {
            var regexp = /^\+?[\d\s\-]{8,14}$/;
            if (!regexp.test(value)) {
                this.addError('invalid_characters');
            }
        }
        if (this.errors.length) {
            return callback(false);
        }
        return callback(true);
    }
});