/**
 * Created by vetal on 01.12.15.
 */

var SimplybookBookingView = function (currNode, options) {
    //this.__construct(options);
    this.model = {};

    this.defaultSteps = [
        stList.STEP_LOCATION,
        stList.STEP_CATEGORY,
        stList.STEP_SERVICE,
        stList.STEP_GROUP_BOOKING,
        stList.STEP_PROVIDER,
        stList.STEP_DATE_TIME,
        stList.STEP_FIELDS,
        stList.STEP_BOOK
    ];

    this._init(currNode, options);
};

var stList = {};
stList.STEP_LOCATION = 'location';
stList.STEP_CATEGORY = 'category';
stList.STEP_SERVICE = 'service';
stList.STEP_GROUP_BOOKING = 'group_booking';
stList.STEP_PROVIDER = 'provider';
stList.STEP_DATE_TIME = 'date_time';
stList.STEP_FIELDS = 'fields';
stList.STEP_BOOK = 'book';


jQuery.extend(SimplybookBookingView.prototype, CommonView.prototype, CommonPayments.prototype, {

    options: {
        image_url: '//simplybook.me/',
        selected:{},
    },
    handlebars : null,
    
    pluginStatuses : {},
    cache: {},
    bookings: {},
    steps : [],
    currentStepIndex: 1,
    flexibleTimeline : null,
    monthNames :  null,

    _initParams : function (currNode) {
        this.model = this.getOption('model');
        this.handlebars = this.getOption('handlebars');
        this.loc = this.getOption('loc');
        this.monthNames = new Date().getMonthsList();

        this.handlebars.initHandlebars({}, 'full-booking-t');

        this.ident = jQuery(currNode);
        this.steps = this.defaultSteps.slice();
        if(simplybook_url) {
            this.setOption('image_url', simplybook_url);
        }

        this.model.setOptions({
            on_error : jQuery.proxy(function(err){ //if error call onError function
                this.onError(err);
                return false;
            }, this)
        });

        this.model.getApiClient().setOptions({
            on_error : jQuery.proxy(function(err){ //if error call onError function
                this.onError(err);
                return false;
            }, this)
        });
    },
    
    _initCustomPlugins : function () {
        this.membershipPlugin = this.getOption('membershipPlugin');
        this.clientLoginPlugin = this.getOption('clientLoginPlugin');
    },

    _init : function(currNode, options){
        var inst = this;
        this.setOptions(options);
        this._initParams(currNode);
        this._initCustomPlugins();

        inst.model.getPluginsStatus( jQuery.proxy(function(pluginStatuses) {
            this.pluginStatuses = pluginStatuses;

            this.clientLoginPlugin.init(jQuery.proxy(function () {
                inst.model.getAllList( function(){
                    inst._getAnyUnitData(function(){
                        inst.options['max_group_bookings'] = inst.model.getMaxGroupBooking();
                        inst.initStepsData();

                        inst.model.request('getCompanyParam', 'user_public_timeline_type', function(timelineType){
                            if(timelineType != 'modern' && timelineType != 'modern_week'){
                                inst.flexibleTimeline = new FlexibleTimeline(inst.model, {
                                    timelineClassId : '#starttime'
                                });
                            }
                        });

                        jQuery(document).trigger('initBookingView', [inst.steps]);
                        inst.initDom(pluginStatuses);
                        inst.initEvents(pluginStatuses);
                        inst._showNextStep();
                    });
                });
            }, this));

        }, this));
    },

    initStepsData : function () {
        var instance = this;
        var res, resultKeys;

        if (!this.pluginStatuses.location) {
            this.removeStep(stList.STEP_LOCATION);
        } else {
            res =  this.model.getLocationsList();
            resultKeys = Object.keys(res);
            if(resultKeys.length <= 1) { //no locations
                if (resultKeys.length) { //if 1
                    this.setLocationId(res[resultKeys[0]].id);
                }
                this.removeStep(stList.STEP_LOCATION);
            }
        }

        if (!this.pluginStatuses.event_category) {
            this.removeStep(stList.STEP_CATEGORY);
        } else {
            res = this.model.getCategoriesList();
            resultKeys = Object.keys(res);

            if(resultKeys.length <= 1) { //no categories
                if (resultKeys.length) { //if 1
                    this.setCategoryId(res[resultKeys[0]].id);
                }
                this.removeStep(stList.STEP_CATEGORY);
            }
        }

        //Check service list (if one - hide step)
        res = this.model.getEventList();
        resultKeys = Object.keys(res);

        if (resultKeys.length == 1 && !this.pluginStatuses.membership) { //if 1
            this.setServiceId(res[resultKeys[0]].id);
            this.removeStep(stList.STEP_SERVICE);
        }

        //Check provider list (if one - hide step)
        res = this.model.getUnitList();
        resultKeys = Object.keys(res);

        if (resultKeys.length == 1) { //if 1
            this.setProviderId(res[resultKeys[0]].id);
            this.removeStep(stList.STEP_PROVIDER);
        }

        if (!this.pluginStatuses.group_booking) {
            this.removeStep(stList.STEP_GROUP_BOOKING);
        }
    },

    initEvents : function(pluginStatuses){
        var instance = this;
        
        this.ident.find('.location_block input').click(function() {
            instance.setLocationId(jQuery(this).attr('data-id'));
            instance._showNextStep(stList.STEP_LOCATION);
        });

        this.ident.find('.category_block input').click(function() {
            instance.setCategoryId(jQuery(this).attr('data-id'));
            instance._showNextStep(stList.STEP_CATEGORY);
        });

        this.ident.find('.service_block input').click(function() {
             instance.setServiceId(jQuery(this).attr('data-id'), function (hasAccess) {
                if(hasAccess){
                    instance._showNextStep(stList.STEP_SERVICE);
                }
            });
        });

        this.ident.find('.group_bookings_block select').change(function () {
            instance.model.setGroupBooking(jQuery(this).val());

            if(jQuery(this).val()>1){
                instance.ident.find('.multiple-book').hide();
            } else
            if(pluginStatuses.multiple_booking){
                instance.ident.find('.multiple-book').show();
            }
        });

        this.ident.find('.provider_block input').click(function() {
            instance.setProviderId(jQuery(this).attr('data-id'));
            instance._showNextStep(stList.STEP_PROVIDER);
        });

        this.ident.find('.btn-book').click(function(){
            instance._showNextStep(stList.STEP_DATE_TIME);
            return false;
        });

        this.ident.find('.methods').find('.stripe').click(function(){
            instance.showStripePayment();
        });

        this.ident.find('.methods').find('.paypal').click(function(){
            instance.showPaypalPayment();
        });

        this.ident.find('.methods').find('.adyen').click(function(){
            instance.showAdyenPayment();
        });

        this.ident.find('.methods').find('.paymentwall_brick').click(function(){
            instance.showPaymentwallBrickPayment();
        });

        this.ident.find('.methods').find('.paymentwall').click(function(){
            instance.showPaymentwallPayment();
        });

        this.ident.find('.methods').find('.delay').click(function(){
            instance.showDelayPayment();
        });

        this.ident.find('.booked_list').on("click", '.close', function(){
            var id = jQuery(this).attr('data-id');
            jQuery(this).parent().remove();
            instance.deleteFromBatch(id);
        });

        this.ident.find('.btn-back').click(function(){
            instance._showBackStep();
            if(instance.currentStepIndex <= 0){
                jQuery(this).hide();
            }
        });

        this.ident.on('click', '.user_license a', function(){
            jQuery('.user_license_popup').dialog({
                width: '70%',
                height: '500',
                modal: true,
                buttons: {
                    "Ok": function() {
                        jQuery( this ).dialog( "close" );
                    }
                }
            });
            return false;
        });

        this.ident.on('change', '.cancelation_policy input', function(){
            instance.model.request('getCompanyParam', 'cancelation_policy_text', function(cancelationPolicyText) {
                if(String(cancelationPolicyText).length<5){
                    cancelationPolicyText = instance.loc.gettext('No description.');
                }

                jQuery('.cancelation_policy_popup').html(cancelationPolicyText).show().dialog({
                    width: '70%',
                    height: '500',
                    modal: true,
                    buttons: {
                        "Ok": function() {
                            jQuery( this ).dialog( "close" );
                        }
                    }
                });
            });

            return false;
        });

        this.ident.on('click','#starttime.modern-timeline span',function(){
            jQuery('#starttime span').removeClass('selected');
            jQuery(this).addClass('selected');
            instance.setTime(jQuery(this).attr("data-time"));
        });

        this.ident.on('click','#starttime.flexible-timeline span',function(){
            var type = jQuery(this).attr('data-type');
            if(type != 'free'){
                return;
            }

            var selDate = jQuery(this).attr("data-date");
            var selTime = jQuery(this).attr("data-time");

            instance.flexibleTimeline.checkTimeframe(selDate, selTime, function(res){
                if(res){
                    instance.options['selected']['date'] = selDate;
                    instance.setTime(selTime);
                } else {
                    instance.showError("This date and time cannot be booked because they are already reserved. Please choose another time.");
                }
            });

        });

        this.clientLoginPlugin.onChange(jQuery.proxy(function(res){
            this._setAuthorizeData(res);
        }, this));

        this.clientLoginPlugin.getUserData(jQuery.proxy(function(res){
            this._setAuthorizeData(res);
        }, this));

        this.ident.on('click', '#sb_sign_out_btn', jQuery.proxy(function(res){
            this.clientLoginPlugin.unauth(jQuery.proxy(function(res){
                this.clientLoginPlugin.getUserData(function (res) {

                });
            }, this));
        }, this));

        if(pluginStatuses.multiple_booking){
            this.ident.find('.multiple-book').show().click(function(){
                instance.multipleBooking();
            });
        }

    },

    initDom: function (pluginStatuses) {
        var instance = this;

        if (pluginStatuses.location) {
            this.initLocations();
        }

        if (pluginStatuses.event_category) {
            this.initCategories();
        }

/*        if(pluginStatuses.group_booking){
            this.initGroupBooking();
        }*/

        this.initServices();
        this.initProviders();

    },

    /*
     steps_old: [
     'getPluginsStatus',
     'getAllList',
     'showAnyUnitData',
     'showLocations',
     'showCategories',
     'showServices',
     'showGroupBooking',
     'showProviders',
     'showDateTime',
     'showBookButton',
     'showField',
     'bookProcess',
     '_showBookResult'
     ],*/
    _showStep: function (step) {
        var instance = this;

        jQuery(document).trigger('setBookingViewStep', [step]);

        switch (step) {
            case stList.STEP_LOCATION:
                this.showLocations();
                break;
            case stList.STEP_CATEGORY:
                this.showCategories();
                break;
            case stList.STEP_SERVICE:
                this.showServices();
                break;
            case stList.STEP_GROUP_BOOKING:
                this.showGroupBooking();
                break;
            case stList.STEP_PROVIDER:
                this.showProviders();
                break;
            case stList.STEP_DATE_TIME:
                this.showDateTime();
                break;
            case stList.STEP_FIELDS:
                this.showField();
                break;
            case stList.STEP_BOOK:
                this.bookProcess(function(result){
                    instance._showBookResult(result);
                });
                break;
        }
    },

    _showNextStep: function (currentStep) {

        if(_.isUndefined(currentStep)){
            var index = -1;
        } else {
            var index = jQuery.inArray(currentStep, this.steps);
            this.ident.find('.btn-back').show();
        }

        if (index >= -1) {
            var nextStep = index + 1;
            this.currentStepIndex = nextStep;

            if (this.steps[nextStep]) {
                this._showStep(this.steps[nextStep]);
            }
        }
    },


    _showBackStep: function () {
        this.currentStepIndex--;

        var index = this.currentStepIndex;

        if (index >= 0) {
            if (this.steps[index]) {
                if( this.steps[index] == stList.STEP_FIELDS || this.steps[index] == stList.STEP_GROUP_BOOKING ){
                    index--;
                    this.currentStepIndex--;
                }

                this._showStep(this.steps[index]);
            }
        }
    },

    onError : function(error){
        console.log(error);
        this.showError(error.message);
    },

    showMess : function(mess, notHide){
        var infoBlock = this.ident.find('.booking_info').find('.mess');

        infoBlock.show('slow').html(mess);
        jQuery('body').scrollTop(0);

        if(!notHide){
            setTimeout(function(){
                infoBlock.hide().empty();
            }, 10000);
        }

    },

    showError : function(mess, notHide){
        var infoBlock = this.ident.find('.booking_info').find('.error');

        infoBlock.show('slow').html(mess);
        jQuery('body').scrollTop(0);

        if(!notHide) {
            setTimeout(function () {
                infoBlock.hide().empty();
            }, 10000);
        }
    },

    restoreStep : function(step){

        if(jQuery.inArray(step, this.steps) === false){
            for(var i = 0, j = 0; i<this.defaultSteps.length;i++, j++){
                if(this.defaultSteps[i] != this.steps[j]){
                    if(this.defaultSteps[i] == step){
                        this.steps.splice(i, 0, step);
                    }
                    i++;
                }
            }
        }
    },

    removeStep : function (step) {
        this.steps.splice( jQuery.inArray(step, this.steps), 1 );
    },

    _hideNextStepsBlocks: function(currentStep){

        var stepsAsoc = [
            '.location_block', //stList.STEP_LOCATION
            '.category_block', //stList.STEP_CATEGORY
            '.service_block, .group_bookings_block', //stList.STEP_SERVICE
            '', //stList.STEP_GROUP_BOOKING  - no hide, b. shows with service
            '.provider_block', //stList.STEP_PROVIDER
            '.date_block', // stList.STEP_DATE_TIME
            '.button_block', //  stList.STEP_FIELDS
            '' // stList.STEP_BOOK
        ];

        if(_.isUndefined(currentStep)){
            index = -1; // hide all steps
        } else {
            var index = this.defaultSteps.indexOf(currentStep);
        }

        for(var i = index+1; i<this.defaultSteps.length; i++){
            if(stepsAsoc[i] === '') continue;
            jQuery(stepsAsoc[i]).hide();
        }
    },


    orderObjectsByParam : function(objects, orderParam){
        var keys = Object.keys(objects);
        keys.sort(function(a, b){
            var positionA = parseInt( objects[a][orderParam] );
            var positionB = parseInt( objects[b][orderParam] );
            return positionA - positionB;
        });
        return keys;
    },

    initLocations : function(){
        var res = this.model.getLocationsList();
        var orderedKeys = this.orderObjectsByParam(res, 'position');

        var selector = this.ident.find('.location_block .content_block').empty();
        //selector.append('<option value="">' + this.loc.gettext("Select location...") + '</option>');

        //show options
        for(var i = 0; i<orderedKeys.length; i++){
            var loc = orderedKeys[i];
            res[loc].city = res[loc].city==null?'':res[loc].city;
            res[loc].address1 = res[loc].address1==null?'':res[loc].address1;
            res[loc].address2 = res[loc].address2==null?'':res[loc].address2;

            selector.append(
                '<div class="select_item" data-id="'+res[loc].id+'">' +
                    '<div class="image_item"'+
                        (res[loc].picture?'style="background-image: url('+this.options.image_url+res[loc].picture_path + ');" >':'>') +
                    '</div>' +
                    '<div class="title_item">'+
                        res[loc].title+
                    '</div>' +
                    '<div class="description_item"><span>'+
                        jQuery('<span>' + (res[loc].description?res[loc].description:this.loc.gettext("No description.")) + '</span>').text() +
                    '</span></div>' +
                    '<div class="add_info_item">' +
                        res[loc].city+', '+res[loc].address1 + ', ' + res[loc].address2 +
                    '</div>' +
                    '<input data-id="'+res[loc].id+'" type="button" value="' + this.loc.gettext("Select") + '">' +
                '</div>'
            );
        }

    },

    showLocations : function(){
        this.ident.find('.stage_2, .stage_3').hide();
        this.ident.find('.stage_1').show();
        this._hideNextStepsBlocks();
        this.ident.find('.location_block').show();
        //this.ident.find('.location_block select').val('');
        //this._hideNextStepsBlocks(stList.STEP_LOCATION);
    },

    setLocationId : function(id){
        if(parseInt(id)>0){
            this._hideNextStepsBlocks(stList.STEP_CATEGORY);
            this.model.setLocationId(id);

        }
    },

    initCategories : function(){
        var res = this.model.getCategoriesList();
        var orderedKeys = this.orderObjectsByParam(res, 'position');
        this.cache['categories'] = res;

        var selector = this.ident.find('.category_block .content_block').empty();

        //show options
        for(var i = 0; i<orderedKeys.length; i++){
            var cat = orderedKeys[i];
            selector.append(
                '<div class="select_item" data-id="'+res[cat].id+'">' +
                '<div class="image_item"'+
                (res[cat].picture?'style="background-image: url('+this.options.image_url+res[cat].picture_path + ');" >':'>') +
                '</div>' +
                '<div class="title_item">'+
                res[cat].name +
                '</div>' +
                '<div class="description_item"><span>'+
                    jQuery('<span>' + (res[cat].description?res[cat].description:this.loc.gettext("No description.")) + '</span>').text() +
                '</span></div>' +
                '<div class="add_info_item">' +

                '</div>' +
                '<input data-id="'+res[cat].id+'" type="button" value="' + this.loc.gettext("Select") + '">' +
                '</div>'
            );
        }

    },

    showCategories : function(){
        this.ident.find('.stage_2, .stage_3').hide();
        this.ident.find('.stage_1').show();
        this._hideNextStepsBlocks();
        this.ident.find('.category_block').show();
        //this.ident.find('.category_block select').val('');
        //this._hideNextStepsBlocks(stList.STEP_CATEGORY);
    },

    setCategoryId : function(id){
        if(parseInt(id)>0){
            this._hideNextStepsBlocks(stList.STEP_SERVICE);
            this.model.setCategoryId(id);
        }
    },

    initServices : function(){
        var res = this.model.getEventList();
        var orderedKeys = this.orderObjectsByParam(res, 'position');
        this.cache['services'] = res;

        var selector = this.ident.find('.service_block .content_block').empty();
        //selector.append('<option value="">' + this.loc.gettext("Select service...") + '</option>');
        //show options
        for(var i = 0; i<orderedKeys.length; i++){
            var service = orderedKeys[i];
            //selector.append('<option value="'+res[service].id+'">'+res[service].name+'</option>');
            selector.append(
                '<div class="select_item" data-id="'+res[service].id+'">' +
                '<div class="image_item"'+
                (res[service].picture?'style="background-image: url('+this.options.image_url+res[service].picture_path + ');" >':'>') +
                '</div>' +
                '<div class="title_item">'+
                res[service].name +
                '</div>' +
                '<div class="description_item"><span>'+
                    jQuery('<span>' + (res[service].description?res[service].description:this.loc.gettext("No description.")) + '</span>').text() +
                '</span></div>' +
                '<div class="add_info_item">' +
                    (parseInt(res[service].hide_duration)==0?this.loc.gettext('Duration: ') + res[service].duration + 'min. <br>':'') +
                    (parseFloat(res[service].price)>0.01?this.loc.gettext('Price: ') + parseFloat(res[service].price) + res[service].currency:'') +
                '</div>' +
                '<input data-id="'+res[service].id+'" type="button" value="' + this.loc.gettext("Select") + '">' +
                '</div>'
            );
        }


    },

    showServices : function(){
        this.ident.find('.stage_2, .stage_3').hide();
        this.ident.find('.stage_1').show();
        this._hideNextStepsBlocks();
        this.ident.find('.service_block').show();
        //this.ident.find('.service_block select').val('');
        //this._hideNextStepsBlocks(stList.STEP_SERVICE);

        var filtered = this.model.getEventList();

        var selector = this.ident.find('.service_block .content_block');
        //selector.find('option').hide().prop("disabled", true); //hide all options
        selector.find('.select_item').hide(); //hide all options

        for(var hid in filtered){
            var id = filtered[hid].id;
            selector.find(".select_item[data-id='"+id+"']").show();
        }
    },


    _setServiceIdIfHasAccess : function(id){
        if(parseInt(id)>0){
            this._hideNextStepsBlocks(stList.STEP_PROVIDER);
            this.model.setServiceId(id);

            if(!this.cache['services']){
                this.cache['services'] = this.model.getEventList();
            }

            if( parseInt(this.cache['services'][id]['is_recurring']) ){
                this.cache['pluginStatuses'] = jQuery.extend({}, this.pluginStatuses); //backup plugin status
                this.pluginStatuses['group_booking'] = false;
                this.pluginStatuses['multiple_booking'] = false;
            } else
            if(!!this.cache['pluginStatuses'] && !this.bookings['confirmed']){ // if back step and first booking than restore group bookin and multiple bookings
                this.pluginStatuses['group_booking'] = this.cache['pluginStatuses']['group_booking'];
                this.pluginStatuses['multiple_booking'] = this.cache['pluginStatuses']['multiple_booking'];
            }
        }
     },


    setServiceId : function(id, callback){
        var inst = this;

        if(!this.pluginStatuses.membership){
            this._setServiceIdIfHasAccess(id);
            if(callback){
                callback(true);
            }
            return;
        }

        this.model.membershipCheckClientAccess(id, jQuery.proxy(function (isHasAccess) {
            if(isHasAccess){
                this._setServiceIdIfHasAccess(id);
            } else {
                this.membershipPlugin.showBuyMessage(id);
            }
            if(callback){
                callback(isHasAccess);
            }
        }, this));
    },

    initGroupBooking : function(){
        var filtered = this.model.getUnitList();
        var groupBookings = Math.min(this.options['max_group_bookings'], this.model.getMaxUnitQty(filtered));

        if(parseInt(this.options['max_group_bookings']) === 0){
            groupBookings = this.model.getMaxUnitQty(filtered);
        }

        var selector = this.ident.find('.group_bookings_block select').empty();

        for (var i = 1; i <= groupBookings; i++) {
            selector.append('<option value="' + i + '">' + i + '</option>')
        }
    },

    showGroupBooking : function(){
        var instance = this;

        if(instance.pluginStatuses['group_booking']/* && !instance.pluginStatuses['multiple_booking']*/ ) {  //show group bookings selector (if no multiple booking)
            this.initGroupBooking();
        }

        instance._showNextStep(stList.STEP_GROUP_BOOKING);
    },

    initProviders : function(){
        var res = this.model.getUnitList();
        var orderedKeys = this.orderObjectsByParam(res, 'position');
        this.cache['providers'] = res;

        var selector = this.ident.find('.provider_block .content_block').empty();
        //selector.append('<option value="">' + this.loc.gettext("Select provider...") + '</option>');

        //show options
        for(var i = 0; i<orderedKeys.length; i++){
            var unit = orderedKeys[i];
            if(parseInt(res[unit].is_active) == 1 && parseInt(res[unit].is_visible) == 1 ){
                //selector.append('<option value="'+res[unit].id+'">'+res[unit].name+'</option>');

                selector.append(
                    '<div class="select_item" data-id="'+res[unit].id+'">' +
                    '<div class="image_item"'+
                    ((res[unit].picture || res[unit].image)?'style="background-image: url('+this.options.image_url+res[unit].picture_path + ');" >':'>') +
                    '</div>' +
                    '<div class="title_item">'+
                    res[unit].name +
                    '</div>' +
                    '<div class="description_item"><span>'+
                        jQuery('<span>' + (res[unit].description?res[unit].description:this.loc.gettext("No description.")) + '</span>').text() +
                    '</span></div>' +
                    '<div class="add_info_item">' +
                        (res[unit].email?this.loc.gettext('Email: ') + res[unit].email + '<br>':'') +
                        (res[unit].phone?this.loc.gettext('Phone: ') + res[unit].phone:'') +
                    '</div>' +
                    '<input data-id="'+res[unit].id+'" type="button" value="' + this.loc.gettext("Select") + '">' +
                    '</div>'
                );
            }
        }

    },

    showProviders : function(){

        this._hideNextStepsBlocks();

        if (this.pluginStatuses['group_booking'] && !this.options['batch_id'] ) {  //show group bookings selector (if no multiple booking)
            this.ident.find('.group_bookings_block').show();
            this.ident.find('.group_bookings_block select').val(1);
            this.model.setGroupBooking(1);
        } else {
            this.ident.find('.group_bookings_block').hide("slow");
        }

        this.ident.find('.provider_block').show();

        //this.ident.find('.provider_block select').val('');
        //this._hideNextStepsBlocks(stList.STEP_PROVIDER);

        var filtered = this.model.getUnitList();

        var selector = this.ident.find('.provider_block .content_block');
        //selector.find('option').hide().prop("disabled", true); //hide all options
        selector.find('.select_item').hide(); //hide all options

        for(var hid in filtered){
            var id = filtered[hid].id;
            //selector.find("option[value='"+id+"']").show().removeAttr( "disabled" );
            selector.find(".select_item[data-id='"+id+"']").show();
        }

    },

    setProviderId : function(id){
        id = parseInt(id);
        if(id == -1 || id>0){ //any unit or unit id
            this._hideNextStepsBlocks(stList.STEP_DATE_TIME); //if multiple booking, than hide html blocks after provider block
            this.model.setProviderId(id);
        }
    },

    _formatDate : function (date) {
        var year = date.getFullYear();
        var month = ("0" + (date.getMonth() + 1)).slice(-2);
        var day = ("0" + date.getDate()).slice(-2);

        return year + '-' + month + '-' + day;
    },

    _drawModernTimelineMatrix : function(matrix) {
        var instance = this;
        jQuery('#starttime').addClass('modern-timeline').empty();

        //String(matrix[i]).match(/\d{2}:\d{2}|[AMP]+/g).join(' ')
        
        if(matrix.length == 0){
            jQuery('#starttime').append( '<div style="color:red;">' + this.loc.gettext("Unfortunately, no more slots available for this day") + '</div>' );
        }


        for (var i = 0; i < matrix.length; i++) {
            //var date_time = String(matrix[i]).split(':');
            var hourPlusDate = Handlebars.helpers.formatDateTime('2010-10-10 '+ matrix[i], 'plus_day' );
            var plus_day = (hourPlusDate!=0?'<i class="date_plus_day">'+(hourPlusDate>0?"+":"")+hourPlusDate+' '+instance.loc.gettext('day')+'</i>':'');
            jQuery('#starttime').append('<span data-time="' + matrix[i] + '">' + Handlebars.helpers.formatDateTime('2010-10-10 '+ matrix[i], 'time' )  + plus_day + '</span>');  //without seconds (H:i)
        }



    },

    getPerformerId : function(){
        var performerId = this.model.getProviderId() == -1?null:this.model.getProviderId();

        if(performerId == null && _.isObject(this.cache['services'][this.model.getServiceId()]['unit_map']) ){ //if isset unit_map
            performerId = Object.keys(this.cache['services'][this.model.getServiceId()]['unit_map']); //performers ids array
        }

        return performerId;
    },

    showDateTime : function(){
        var instance = this;
        this.ident.find('.form_field').attr('novalidate', 'novalidate'); // Chrome hack :(
        this.ident.find('.stage_2, .stage_3').hide();
        this.ident.find('.stage_1').show();
        this._hideNextStepsBlocks();
        //instance._hideNextStepsBlocks(stList.STEP_DATE_TIME);
        this.ident.find('.date_block').show();


        this.model.request('getCompanyParam', 'user_public_timeline_type', function(timelineType){
            if(timelineType == 'modern' || timelineType == 'modern_week'){
                instance._showModernTimeline();
            } else {
                instance._showFlexibleTimeline();
            }

        });


    },


    _showFlexibleTimeline : function(){
        var instance = this;
        var count = 1; // How many slots book

        if(this.model.getGroupBooking()){
            count = this.model.getGroupBooking();
        }

        var serviceId = this.model.getServiceId();
        var performerId = this.getPerformerId();

        this.flexibleTimeline.setData(performerId, serviceId, count);

        this._showCalendar(function(date){
            instance.setFlexibleTimelineDate( date );
        });
    },

    _showModernTimeline : function(){
        var instance = this;
        this._showCalendar(function(date){
            instance.setModernTimelineDate(date);
        });
    },

    _showCalendar : function(callback){
        var instance = this;

        var performerId = instance.getPerformerId();
        var serviceId = instance.model.getServiceId();

        if( Array.isArray( performerId ) ){
            performerId = null;
        }

        var selectedData = performerId;
        if(serviceId){
            selectedData = {
                'unit_group_id' : performerId,
                'event_id' :  serviceId,
            };
        }

        instance.model.request('getFirstWorkingDay',performerId, function(firstWorkingDay){

            if(!firstWorkingDay){
                firstWorkingDay = new Date().toISOString().slice(0, 19).replace('T', ' ');
            }

            var firstWorkingDateArr = firstWorkingDay.split('-');
            instance.model.request('getWorkCalendar',firstWorkingDateArr[0], firstWorkingDateArr[1], selectedData, function(workCalendar){
                instance.model.request('getCompanyParam', 'monday_is_first_day', function(monday_is_first_day){

                    jQuery('#calendar').datepicker({
                        firstDay: monday_is_first_day, // if 1 Monday is first
                        monthNames: instance.monthNames,
                        dayNamesMin: (new Date).getWeekDaysList(false),
                        'onChangeMonthYear': function (year, month, inst) {
                            var serviceId = instance.model.getServiceId();
                            var selectedData = performerId;

                            if(serviceId){
                                selectedData = {
                                    'unit_group_id' : performerId,
                                    'event_id' :  serviceId,
                                };
                            }

                            instance.model.request('getWorkCalendar',year, month, selectedData, function(data){
                                workCalendar = data;
                                jQuery('#calendar').datepicker('refresh');
                            });

                        },
                        'beforeShowDay': function (date) {
                            var year = date.getFullYear();
                            var month = ("0" + (date.getMonth() + 1)).slice(-2);
                            var day = ("0" + date.getDate()).slice(-2);

                            var currentDate = new Date();
                            currentDate.setDate(currentDate.getDate()-1);

                            if(date<=currentDate){
                                return [false, "", ""];
                            }

                            var date = year + '-' + month + '-' + day;

                            if (!_.isUndefined(workCalendar[date])) {
                                if (parseInt(workCalendar[date].is_day_off) == 1) {
                                    return [false, "", ""];
                                }
                            }
                            return [true, "", ""];
                        }
                    });

                    jQuery('#calendar').datepicker('option', 'onSelect', function () {
                        //instance.setDate(jQuery(this).datepicker('getDate'));
                        callback(jQuery(this).datepicker('getDate'));
                    });

                    jQuery('#calendar').datepicker("setDate", Date.createFromMysql(firstWorkingDay) );
                    //instance.setDate(Date.createFromMysql(firstWorkingDay));
                    callback(Date.createFromMysql(firstWorkingDay));


                });
            });
        });
    },

    setModernTimelineDate : function(date){ //js Date
        var instance = this;
        var startDate = this._formatDate(date);
        this.options['selected']['date'] = startDate;

        var count = 1; // How many slots book

        if(this.model.getGroupBooking()){
            count = this.model.getGroupBooking();
        }

        var serviceId = this.model.getServiceId();
        var performerId = this.getPerformerId();

        var startMatrix = this.model.request('getStartTimeMatrix', startDate, startDate, serviceId, performerId, count, function(startMatrix){
            instance._drawModernTimelineMatrix(startMatrix[startDate]);
            instance._hideNextStepsBlocks(stList.STEP_DATE_TIME);
        });
    },

    setFlexibleTimelineDate : function(date){ //js Date
        var instance = this;
        var startDate = this._formatDate(date);
        this.options['selected']['date'] = startDate;

        //this.flexibleTimeline.setCompanyTimezone(instance.cache['company_timezone']);
        this.flexibleTimeline.showDayTimeline(date);

        instance._hideNextStepsBlocks(stList.STEP_DATE_TIME);
    },


    setTime : function(time){ //H:i:s
        this.options['selected']['time'] = time;

        var instance = this;
        var count = 1; // How many slots book

        if(instance.model.getGroupBooking()){
            count = instance.model.getGroupBooking();
        }

        var serviceId = instance.model.getServiceId();

        if(instance.model.getProviderId() == -1){
            var dateTime = instance.options['selected']['date'] + ' ' + instance.options['selected']['time'];

            instance.model.request('getAvailableUnits',serviceId, dateTime, count, function(res){

                if(res.length >= 1) {
                    instance.model.setProviderId( res[0] );
                    instance.showBookButton();
                }else {
                    instance.showError("No available units");
                    instance.showDateTime();
                }
            });

        } else {
            instance.showBookButton();
        }

    },

    showBookButton : function(){
        var instance = this;
        this.ident.find('.button_block').show();
        instance._showNextStep(stList.STEP_DATE_TIME);
    },

    showField : function(callback){
        var instance = this;

        this.ident.find('.stage_1').hide();
        this.ident.find('.stage_2').show();

        var item = this.ident.find('.detailed_item:first');
        var tpl = item.clone();
        var infoBlock = item.parent();
        infoBlock.empty();


        if(!_.isUndefined(this.model.getLocationId())){
            var currLocation = this.model.getLocationsList()[this.model.getLocationId()];
            var tplLocation = tpl.clone();

            tplLocation.find('.title_item').html( this.loc.gettext("Location:") );
            if(currLocation.picture){
                tplLocation.find('.image_item').find('img').show().attr('src',this.options.image_url+currLocation.picture_path);
            }
            tplLocation.find('.name_item').html(currLocation.title);
            tplLocation.find('.description_item').text(currLocation.city+', '+currLocation.address1+', '+currLocation.address2);

            infoBlock.append(tplLocation);
        }

        if(!_.isUndefined(this.model.getCategoryId())){
            var currCategory = this.model.getCategoriesList()[this.model.getCategoryId()];
            var tplCategory = tpl.clone();

            tplCategory.find('.title_item').html( this.loc.gettext("Category:") );
            if(currCategory.picture){
                tplCategory.find('.image_item').find('img').show().attr('src',this.options.image_url+currCategory.picture_path);
            }
            tplCategory.find('.name_item').html(currCategory.name);
            var descriptionText = jQuery('<span>' + (currCategory.description?currCategory.description:'') + '</span>').html();
            tplCategory.find('.description_item').html( this.clearText(descriptionText) );

            infoBlock.append(tplCategory);
        }

        if(!_.isUndefined(this.cache['services'])){
            var currService = this.cache['services'][this.model.getServiceId()];
            var tplService = tpl.clone();

            tplService.find('.title_item').html( this.loc.gettext("Service:") );
            if(currService.picture){
                tplService.find('.image_item').find('img').show().attr('src',this.options.image_url+currService.picture_path);
            }
            tplService.find('.name_item').html(currService.name);
            descriptionText = jQuery('<span>' + (currService.description?currService.description:'') + '</span>').html();
            tplService.find('.description_item').html( this.clearText(descriptionText) );

            var price = parseFloat(currService.price).toFixed(2);
            if (price >= 0) {
                tplService.find('.description_item').append('<div class="price_service">'+ this.loc.gettext("Price: ") + price + currService.currency +'</div>');
            }

            infoBlock.append(tplService);
        }

        if(!_.isUndefined(this.cache['providers'])){
            var cproviderId = (instance.cache['anyunit'] && instance.cache['anyunit'].hide_other_units)?-1:this.model.getProviderId();
            var currProvider = this.cache['providers'][cproviderId];

            var tplProvider = tpl.clone();

            tplProvider.find('.title_item').html( this.loc.gettext("Provider:") );
            if(currProvider['picture']) {
                tplProvider.find('.image_item').find('img').show().attr('src', this.options.image_url + currProvider.picture_path);
            }
            tplProvider.find('.name_item').html(currProvider.name);
            descriptionText = jQuery('<span>' + (currProvider.description?currProvider.description:'') + '</span>').html();
            tplProvider.find('.description_item').html( this.clearText(descriptionText) );
            infoBlock.append(tplProvider);
        }

        //date and time info
        var tplAdditional = tpl.clone();
        tplAdditional.find('.title_item').hide();
        tplAdditional.find('.image_item').hide();
        tplAdditional.find('.name_item').html(this.loc.gettext("Date and time:")).hide();
        var tplDescrItem = tplAdditional.find('.description_item');
        tplDescrItem.html('');


        this.model.request('getCompanyParam', 'time_format', function(time_format){
            instance.options['time_format'] = time_format;

            if(parseInt(currService['is_recurring'])){ //if recurring booking
                instance.model.request("getRecurringDatetimes", instance.model.getServiceId(), instance.model.getProviderId(), instance.options['selected']['date'], instance.options['selected']['time'], function(recData){

                    for(var id in recData['dates']){
                        instance._showDetailCalendarPage(recData['dates'][id], instance.options['selected']['time'], tplDescrItem, time_format);
                    }

                    for(var id in recData['error_dates']){
                        instance._showDetailCalendarPage(recData['error_dates'][id], instance.options['selected']['time'], tplDescrItem, time_format, true);
                    }
                });
            } else {
                instance._showDetailCalendarPage(instance.options['selected']['date'], instance.options['selected']['time'], tplAdditional.find('.description_item'), time_format);
            }
        });
        infoBlock.append(tplAdditional);

        if(this.options['batch_id']){  //if multiple booking
            this.showMultipleSavedBookings(function(){
                instance.showAdditionalField(function(){
                    instance._showNextStep(stList.STEP_FIELDS);
                });
            });
        } else {
            this.editBaseField(function(){
                instance.showAdditionalField(function(){
                    instance._showNextStep(stList.STEP_FIELDS);
                });
            });
        }

        this.showPromoField();
        this.showCancellationPolicy();
        this.showUserLicense();


        if(this.pluginStatuses['multiple_booking'] && this.model.getGroupBooking()<=1) {
            this.ident.find('.multiple-book').show();
        } else {
            this.ident.find('.multiple-book').hide();
        }
    },

    _showDetailCalendarPage : function(mysqlDate, mysqlTime, jQObject, time_format, notAvailable){

        jQObject.append(
            "<time datetime=\""+mysqlDate+"\" class=\"icon "+(notAvailable?'not-available':'')+"\" >"+
            "<em>"+ Handlebars.helpers.formatDateTime(mysqlDate +' '+ mysqlTime, 'month' )  +"</em>"+
            "<strong>&#x1f550; " + Handlebars.helpers.formatDateTime(mysqlDate +' '+ mysqlTime, 'time' )  + "</strong>"+
            "<span>" + Handlebars.helpers.formatDateTime(mysqlDate +' '+ mysqlTime, 'day' )  + "</span>"+
            "</time>"
        );
    },

    showUserLicense : function(){
        var instance = this;

        if(this.pluginStatuses['user_license']){
            this.ident.find('.user_license').show();


            if(!this.cache['user_license']){
                this.model.request('getUserLicenseText', function(user_license){
                    instance.cache['user_license'] = user_license;
                    instance.ident.find('.user_license_popup').html(user_license);
                });
            }
        }
    },


    showCancellationPolicy: function () {
        var instance = this;

        if(this.pluginStatuses['cancelation_policy']){
            this.model.request('getCompanyParam', 'cancelation_policy_label', function(cancelationPolicyLabel){
                var policyNode = instance.ident.find('.cancelation_policy');
                policyNode.show().empty();

                policyNode.append(
                    '<div class="checkbox">' +
                        '<label>' +
                            '<input class="additional_field" type="checkbox" name="cancelation_policy" required />' +
                            cancelationPolicyLabel +
                            '<span class="required_p">*</span>' +
                        '</label>' +
                    '</div>'
                );

                policyNode.show();
            });
        }
    },


    _setAuthorizeData : function (userData) {
        var autorizeBlock = this.ident.find('.stage_2 .authorize_block');
        if(userData){
            autorizeBlock.find('.user_name').text( (userData.name)?userData.name:'none' );
        } else {
            autorizeBlock.hide();
        }
    },

    editBaseField : function(callback){
        var instance = this;

        this.clientLoginPlugin.getUserData(jQuery.proxy(function (userData) {
            var form = this.ident.find('.form_field');
            form.removeAttr("novalidate");  // Chrome hack =(

            if(userData){
                form.find('.fields_base').empty();
                this.cache['clientData'] = {
                    client_id : userData.id,
                    client_sign : userData.client_hash,
                    client_hash : userData.client_hash
                };

                callback();
            } else {
                if(!this.cache['editBaseField']){ //only one download
                    this.model.request('getCompanyParams', ['require_fields', 'fixed_country_prefix', 'changable_prefix_by_client'], function(res){
                        instance.cache['editBaseField'] = true;

                        var country_prefix = res['fixed_country_prefix'];
                        var changable_prefix = res['changable_prefix_by_client'];
                        var require_fields = res['require_fields'];

                        require_fields = require_fields.split('_');

                        for(var it = 0; it < require_fields.length; it++){
                            instance.ident.find('input[name="'+require_fields[it]+'"]').prop('required',true);
                            instance.ident.find('.client_'+require_fields[it]).append('<span class="required_p">*</span>');
                        }

                        instance.model.request('getCountryPhoneCodes', function (res) {
                            var select = instance.ident.find('#countries_phone1');

                            for (var item in res) {
                                select.append('<option value="' + res[item].value + '" >' + res[item].label + '</option>');
                            }

                            select.val(country_prefix);

                            if(!parseInt(changable_prefix) && country_prefix){
                                select.attr('disabled', 'disabled');
                            }
                            callback();
                        });
                    });
                } else {
                    callback();
                }
            }

        }, this));

    },

    showAdditionalField : function(callback){
        var instance = this;

        var eventId = this.model.getServiceId();
        var fieldsBlock = this.ident.find('.fields .fields_adds');
        fieldsBlock.empty();

        this.model.request('getAdditionalFields',eventId, function(res){

            for(var i=0; i<res.length; i++){
                var el = res[i];
                if(!el){
                    continue;
                }
                el['default'] =  (_.isUndefined(el['default']) || _.isNull(el['default']))?'':jQuery.trim(el['default']);

                var isRequired = (el.is_null != "1")?'required':'';
                var isRequired2 = (el.is_null != "1")?'<span class="required_p">*</span>':'';

               // if(parseInt(el.on_main_page)){
                    switch(el.type){
                        case 'digits':
                            el.type = 'number';
                        case 'text':
                            var field = jQuery(' <h3>'+el.title+': '+isRequired2+'</h3> <input class="form-control additional_field" size="'+el.length+'" type="'+el.type+'" name="'+el.name+'" value="'+el['default']+'" '+isRequired+' />');
                            break;
                        case 'checkbox':
                            var field = jQuery(' <h3>'+el.title+': '+isRequired2+'</h3> <div class="checkbox"><label> <input class="additional_field" size="'+el.length+'" type="'+el.type+'" name="'+el.name+'" '+isRequired+' /> '+el['default']+'</label></div>');
                            break;
                        case 'textarea':
                            var field = jQuery(' <h3>'+el.title+': '+isRequired2+'</h3> <textarea class="form-control additional_field" size="'+el.length+'" name="'+el.name+'"  '+isRequired+' >'+el['default']+'</textarea>');
                            break;

                        case 'select':
                            var field = jQuery(' <div><h3>'+el.title+': '+isRequired2+'</h3> <select class="form-control additional_field" name="'+el.name+'" '+isRequired+' ></select></div>');
                            var select = field.find('.additional_field');
                            var items = String(el.values).split(",");

                            select.append('<option></option>');
                            for(var j=0; j<items.length; j++){
                                var option = jQuery('<option value="'+jQuery.trim(items[j])+'" >'+jQuery.trim(items[j])+'</option>');
                                select.append(option);
                            }
                            select.val(el['default']);

                            break;
                    }

                    fieldsBlock.append(field);
                    fieldsBlock.find('*:input').last().attr('data-title',el.title); //add title

            //    }
            }

            callback();

        });
    },


    showPromoField : function(){

        if(this.pluginStatuses.promo){ ///if plugin activated
            var promoBlock = this.ident.find('.fields .fields_promo').empty();
            promoBlock.append('<h3>'+ this.loc.gettext("Promotion code") +'</h3> <input name="promocode" class="form-control additional_field" type="text" />');
        }
    },

    showMultipleSavedBookings : function(callback){
        var instance = this;
        //hide base fields
        var baseFlds = this.ident.find('.fields_base');
        baseFlds.find('input').remove();
        baseFlds.hide();

        //show saved bookings block
        this.ident.find('.booked_list').show('slow');

        var item = this.ident.find('.booked_item:first');
        var tpl = item.clone();
        var parrBlock = item.parent();
        parrBlock.empty();

        //show items
        for(var ids in this.bookings['confirmed']){
            var bookedItem = this.bookings['confirmed'][ids];

            tpl.find('.name').html(this.cache['services'][bookedItem.event_id].name);
            tpl.find('.provider .tx').html(this.cache['providers'][bookedItem.unit_id].name);
            tpl.find('.time').html(bookedItem.start_date_time);

            tpl.find('.close').attr('data-id',bookedItem.id);

            parrBlock.append(tpl.clone());
        }

        callback();
    },

    multipleBooking : function(){
        var instance = this;

        if(!this._validateForm()){
            return;
        }

        var formData = this.getFormData();

        if(!this.options['batch_id']){
            this.options['batch_id'] = this.model.request("createBatch");
            this.bookings['reserved'] = [];
        }

        this.bookings['reserved'].push(formData);

        var result = this.model.request("book", formData.serviceId, formData.performerId, formData.startDate,
                    formData.startTime, formData.clientData, formData.additionalField, formData.count, this.options['batch_id'], function(res){
                        instance.bookings['confirmed'] =  res['bookings'];
                        instance._showNextStep(); // go to show location (func num 3)
                    });


    },

    deleteFromBatch : function(id){
        var instance = this;
        delete this.bookings['confirmed'][id];

        this.model.request('cancelBooking', id, function(res){
            instance.showMess(instance.loc.gettext('Booking was successfully deleted!'));
        });
    },

    getFormData : function(){
        var form = this.ident.find('.form_field');

        var clientData = this.cache['clientData'];
        var additionalField = {};

        if(!clientData) {
            clientData = {};
            form.find('.fields_base').find('*:input').each(function () {
                clientData[jQuery(this).attr('name')] = jQuery(this).val();
            });
            if(clientData['phone_code']) {
                clientData['phone'] = clientData['phone_code'] + clientData['phone'];
            }
            delete clientData['phone_code'];
            this.cache['clientData'] = clientData?clientData:'';
        }

        form.find('.fields_adds, .fields_promo').find('*:input').each(function () {
            if(jQuery(this).attr('type') == 'checkbox'){
                additionalField[jQuery(this).attr('name')] = jQuery(this).is(':checked')?'1':'0';
            } else {
                additionalField[jQuery(this).attr('name')] = jQuery(this).val();
            }
        });

        if (this.pluginStatuses.location && this.model.getLocationId()) {
            additionalField['location_id'] = this.model.getLocationId();
        }

        return {
            performerId: this.model.getProviderId(),
            serviceId: this.model.getServiceId(),
            startDate: this.options['selected']['date'],
            startTime: this.options['selected']['time'],
            count: this.model.getGroupBooking(),
            clientData: (this.options['batch_id'])?this.bookings['reserved'][0].clientData:clientData,
            additionalField: additionalField,
            batchId: (this.options['batch_id']) ? this.options['batch_id'] : null
        };

    },

    _validateForm : function(){

        var instance = this;
        var form = this.ident.find('.form_field');

        var formIsValid = true;

        var errorList = {
            0 : "Field '{0}' not checked",
            1 : "Field '{0}' is required.",
            2 : "Incorrect field '{0}' "
        };


        form.find('*:input').each(function () {

            var attr = jQuery(this).attr('data-title');
            var fieldName = (!_.isUndefined(attr) && attr !== false) ? attr : jQuery(this).attr('name');

            if( jQuery(this).prop('required') ){

                if(jQuery(this).attr('type') == 'checkbox'){
                    if(!jQuery(this).is(':checked')){
                        instance.showError(errorList[0].replace('{0}',fieldName));
                        formIsValid = false;
                    }
                } else
                if(jQuery(this).val().length == 0){
                    instance.showError(errorList[1].replace('{0}',fieldName));
                    formIsValid = false;
                } else
                if( jQuery(this).attr('name') == 'email' && !instance._isEmail(jQuery(this).val()) ){
                    instance.showError(errorList[2].replace('{0}',fieldName));
                    formIsValid = false;
                } else
                if(jQuery(this).attr('name') == 'phone' || jQuery(this).attr('type') == 'number'){
                    var isNumber =  /^\d+$/.test( jQuery(this).val() );

                    if(!isNumber){
                        instance.showError(errorList[2].replace('{0}',fieldName));
                        formIsValid = false;
                    }
                }
            } else {

                if( jQuery(this).attr('name') == 'email' && jQuery(this).val().length > 0 && !instance._isEmail(jQuery(this).val()) ){
                    instance.showError(errorList[2].replace('{0}',fieldName));
                    formIsValid = false;
                }
            }

            if(!formIsValid){
                return false; //break from each
            }
        });

        return formIsValid;

    },

    _isEmail : function(email) {
        var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    },

    bookProcess : function(callback){
        var instance = this;

        var form = this.ident.find('.form_field');

        form.unbind('submit').bind('submit', function(){
            if(instance._validateForm()){
                var formData = instance.getFormData();

                var result = instance.model.request("book", formData.serviceId, formData.performerId, formData.startDate,
                    formData.startTime, formData.clientData, formData.additionalField, formData.count, formData.batchId);

                if(result !== false) {

                    if (result['require_confirm']) {
                        instance.model.request('isPaymentRequired', formData.serviceId, function (res) {
                            result['is_payment'] = res;
                            instance.cache['book'] = result;
                            callback();
                        });
                    } else {
                        result['is_payment'] = false;
                        instance.cache['book'] = result;
                        callback();
                    }
                }
            }

            return false;
        });

    },

    _showBookResult : function(){
        var instance = this;

        var result = instance.cache['book'];

        this.ident.find('.stage_1, .stage_2').hide();
        this.ident.find('.stage_3').show();

        this.showDetailedBookingsBlock();

        if(result['is_payment']){
            instance.showPaymentMethods(result);
        } else {

            if(this.pluginStatuses.approve_booking){
                this.ident.find('.stage_3').find('.result').text(this.loc.gettext("Your data was successfully saved. You will receive confirmation when administrator confirms your booking"));
            } else {
                this.ident.find('.stage_3').find('.result').text(this.loc.gettext("You've successfully reserved the service. Please check your email to see the notification!"));
            }

        }

    },


    _getAnyUnitData : function(callback) {
        var instance = this;
        if (this.pluginStatuses.any_unit){
            this.model.getAnyUnitData(function (res) {
                instance.cache['anyunit'] = res;
                callback();
            });
        }else{
            callback();
        }
    },

    showPaymentMethods : function(){
        var instance = this;
        var methodsBlock = this.ident.find('.methods');
        methodsBlock.show();

        var methods_count = 0;

        this.model.request('getCompanyParam','allow_delay_payment', function(delay_method){
            if(parseInt(delay_method)){
                methodsBlock.find('.delay').show();
                methods_count++;
            }

            if(parseInt(simplybook_stripe_status)){
                methodsBlock.find('.stripe').show();
                methods_count++;
            }

            if(parseInt(simplybook_paypal_status)){
                methodsBlock.find('.paypal').show();
                methods_count++;
            }

            if(parseInt(simplybook_adyen_status)){
                methodsBlock.find('.adyen').show();
                methods_count++;
            }

            if(parseInt(simplybook_paymentwall_status)==1){
                methodsBlock.find('.paymentwall_brick').show();
                methods_count++;
            }

            if(parseInt(simplybook_paymentwall_status)==2){
                methodsBlock.find('.paymentwall').show();
                methods_count++;
            }

            if(!methods_count){
                instance.showError(instance.loc.gettext("No payment systems are available"), true);
            }
        });


    },

    showDetailedBookingsBlock : function(){
        var instance = this;
        var bookings = this.cache['book']['bookings'];

        jQuery(document).trigger('setBookingViewStep', ['result']);

        var tpl_item = this.ident.find('.result_booked_item');
        var tpl = tpl_item.clone();
        var infoBlock = tpl_item.parent();
        infoBlock.show().empty();

        jQuery.each(bookings, function( index, booking ) {
            var curr_tpl = tpl.clone();

            curr_tpl.find('.name').html(instance.cache['services'][booking.event_id].name);

            var cproviderId = (instance.cache['anyunit'] && instance.cache['anyunit'].hide_other_units)?-1:booking.unit_id;

            curr_tpl.find('.provider .tx').html(instance.cache['providers'][cproviderId].name);
            var date_time = booking.start_date_time.split(' ');
            instance._showDetailCalendarPage(date_time[0],date_time[1],curr_tpl.find('.time'), instance.options['time_format']);
            //tpl.find('.time').html(bookedItem.start_date_time);

            infoBlock.append(curr_tpl);
        });

    },

    showDelayPayment: function () {
        var instance = this;
        this.ident.find('.methods').hide();

        this.payDelayPayment(this._getBookingIds(), function () {
            instance.ident.find('.stage_3').find('.result').text(instance.loc.gettext("You've successfully reserved the service. Please check your email to see the notification!"));
        });
    },


    _getBookingIds : function () {
        var result = this.cache['book'];
        var bookingsIds = [];

        jQuery.each(result['bookings'], function( index, booking ) {
            bookingsIds.push(booking.id);
        });

        return bookingsIds;
    },


    showStripePayment : function(){
        var instance = this;

        this.ident.find('.methods').hide();
        var stipeBlock = this.ident.find('.stripe_payment');
        stipeBlock.show();

        this.initStripePayment(this._getBookingIds(), function (cart) {
            stipeBlock.find('button').text(instance.loc.gettext('Pay') + ' ' + cart['amount'] + cart['currency']);

            jQuery('#stripePayButton').off('click').on('click', function(e) {
                instance.payStripePayment(function (res) {

                    stipeBlock.hide();
                    instance.ident.find('.stage_3').find('.result').text(instance.loc.gettext("You've successfully reserved the service. Please check your email to see the notification!"));
                });
                e.preventDefault();
            });
        });
    },

    showPaypalPayment : function(){
        var instance = this;

        this.ident.find('.methods').hide();
        var paypalBlock = this.ident.find('.paypal_payment');
        paypalBlock.show();

        this.initPaypalPayment(this._getBookingIds(), this.cache['clientData'], jQuery('#paypal-payment-c'), function (cart) {
            paypalBlock.find('input[type="submit"]').val(instance.loc.gettext('Pay') + ' ' + cart['amount'] + cart['currency']);

            instance.payPaypalPayment(function (res) {
                paypalBlock.hide();
                if(res){
                    paypalBlock.hide();
                    instance.ident.find('.stage_3').find('.result').text(instance.loc.gettext("You've successfully reserved the service. Please check your email to see the notification!"));
                } else {
                    instance.ident.find('.stage_3').find('.result').text(instance.loc.gettext("We haven't received the payment from payment system yet. If you made a payment, please wait until it is processed. Booking confirmation will be sent to your email. Sorry for inconveniences."));
                }
            });
        });

    },


    showPaymentwallBrickPayment : function(){
        var instance = this;

        this.ident.find('.methods').hide();
        var paymentwallBlock = this.ident.find('.paymentwall_brick_payment');
        paymentwallBlock.show();

        this.initPaymentwallBrickPayment(this._getBookingIds(), this.cache['clientData'], jQuery('#paymentwall-brick-payment-c'), function (cart) {
            instance.payPaymentwallBrickPayment(function (res, data) {
                paymentwallBlock.hide();

                if(res){
                    if(data['success'] == 1){
                        instance.ident.find('.stage_3').find('.result').text(instance.loc.gettext("You've successfully reserved the service. Please check your email to see the notification!"));
                    } else { // if data['success'] == 2
                        instance.ident.find('.stage_3').find('.result').text(instance.loc.gettext("We haven't received the payment from payment system yet. If you made a payment, please wait until it is processed. Booking confirmation will be sent to your email. Sorry for inconveniences."));
                    }
                } else {
                    instance.ident.find('.stage_3').find('.result').text(instance.loc.gettext("Error! Payment failed."));
                }
            });
        });
    },


    showPaymentwallPayment : function(){
        var instance = this;

        this.ident.find('.methods').hide();
        var paymentwallBlock = this.ident.find('.paymentwall_payment');
        paymentwallBlock.show();

        this.initPaymentwallPayment(this._getBookingIds(), function () {

        });

        this.payPaymentwallPayment(function (res) {
            paymentwallBlock.hide();
            if(res){
                instance.ident.find('.stage_3').find('.result').text(instance.loc.gettext("You've successfully reserved the service. Please check your email to see the notification!"));
            } else {
                instance.ident.find('.stage_3').find('.result').text(instance.loc.gettext("We haven't received the payment from payment system yet. If you made a payment, please wait until it is processed. Booking confirmation will be sent to your email. Sorry for inconveniences."));
            }
        });
    },


    showAdyenPayment : function(){
        var instance = this;

        this.ident.find('.methods').hide();
        var adyenBlock = this.ident.find('.adyen_payment');
        adyenBlock.show();

        this.initAdyenPayment(this._getBookingIds(), this.cache['clientData'], jQuery('#adyen-payment-c'), function (cart) {

            instance.payAdyenPayment(function (status, statusId) {
                adyenBlock.hide();
                if(status && statusId==1){
                    instance.ident.find('.stage_3').find('.result').text(instance.loc.gettext("You've successfully reserved the service. Please check your email to see the notification!"));
                }else
                if(status && statusId==2){
                    instance.ident.find('.stage_3').find('.result').text(instance.loc.gettext("We haven't received the payment from payment system yet. If you made a payment, please wait until it is processed. Booking confirmation will be sent to your email. Sorry for inconveniences."));
                }else{
                    instance.ident.find('.stage_3').find('.result').text(instance.loc.gettext("Error! Payment failed."));
                }
            });
        });
    },


    clearText : function (text) {
        var node = jQuery('<div>' + text + '</div>');

        ///remove h1-9, body, head, html, canvas
        node.find("h1, h2, h3, h4, h5, h6, body, head, html, canvas").each(function () {
            jQuery(this).replaceWith('<span>' + jQuery(this).html() +'</span>');
        });

        //remove all classes and styles, scripts
        node.find('*').removeAttr('class').removeAttr('id').removeAttr('style').removeAttr("onclick").removeAttr("onmouseout").removeAttr("onmouseover");
        node.find('script, style').remove();

        return node.html();
    }

});
