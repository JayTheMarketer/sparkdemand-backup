<?php
?>

<div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">{{_t "Membership is required"}}</h4>
        </div>
        <div class="modal-body">
            {{_t "To book this service membership is required. Would you like to buy the membership?"}}
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">{{_t "No"}}</button>
            <button type="button" class="btn btn-primary" id="sb_buy_membership_btn" data-id="{{eventId}}">{{_t "Yes"}}</button>
        </div>
    </div>

</div>

