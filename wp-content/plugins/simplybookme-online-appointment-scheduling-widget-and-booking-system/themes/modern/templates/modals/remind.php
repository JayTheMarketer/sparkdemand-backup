<?php
?>

<div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">{{_t "Remind password"}}</h4>
        </div>
        <div class="modal-body">

            <div class="remind_info">
                <span class="error"></span>
                <span class="mess"></span>
            </div>

            <div class="form-group row">
                <label class="col-sm-offset-2 col-sm-2 control-label" for="sb_remind_email">
                    {{_t "Email"}}
                </label>
                <div class="col-sm-6">
                    <input type="email" class="form-control" id="sb_remind_email" name="email" value="" placeholder="{{_t "Email"}}">
                    <p class="help-block"></p>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">{{_t "Back"}}</button>
            <button type="button" class="btn btn-primary" id="sb_remind_btn">{{_t "Remind"}}</button>
        </div>
    </div>

</div>

