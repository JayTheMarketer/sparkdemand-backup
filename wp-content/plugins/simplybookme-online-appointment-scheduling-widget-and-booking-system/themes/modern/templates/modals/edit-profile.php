<?php
?>



<div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">{{_t "Edit profile"}}</h4>
        </div>
        <div class="modal-body">

            <div class="edit_profile_info">
                <span class="error"></span>
                <span class="mess"></span>
            </div>

            <div class="form-group row">
                <label class="col-sm-offset-2 col-sm-2 control-label" for="sb_edit_profile_name">
                    {{_t "Name"}}
                </label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" id="sb_edit_profile_name" name="name" value="{{name}}" placeholder="{{_t "Name"}}">
                    <p class="help-block"></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-sm-offset-2 col-sm-2 control-label" for="sb_edit_profile_email">
                    {{_t "Email"}}
                </label>
                <div class="col-sm-6">
                    <input type="email" class="form-control" id="sb_edit_profile_email" name="email" value="{{email}}" placeholder="{{_t "Email"}}">
                    <p class="help-block"></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-sm-offset-2 col-sm-2 control-label" for="sb_edit_profile_phone">
                    {{_t "Phone"}}
                </label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" id="sb_edit_profile_phone" name="phone" value="{{phone}}" placeholder="{{_t "Phone"}}">
                    <p class="help-block"></p>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-sm-offset-2 col-sm-2 control-label" for="sb_edit_profile_password">
                    {{_t "Password"}}
                </label>
                <div class="col-sm-6">
                    <input type="password" class="form-control" id="sb_edit_profile_password" name="password" value="" placeholder="{{_t "Password"}}">
                    <p class="help-block"></p>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">{{_t "Back"}}</button>
            <button type="button" class="btn btn-primary" id="sb_edit_profile_btn">{{_t "Edit"}}</button>
        </div>
    </div>

</div>

