<?php
?>


<div class="tab-pd">

	<div class="clearfix">
        <div class="container-fluid">
            <div class="row">


                {{#each memberships}}
                <div class="sb-membership-item col-lg-6 col-sm-6 col-xs-12">
                    <div class="sb-membership-item-container">

                        <div class="sb-membeship-photo">
                            <div class="image" style="background-image: url('{{formatImagePath picture_path}}')"></div>
                        </div>

                        <div class="sb-membership-name">
                            {{name}}
                        </div>

                        <div class="sb-membership-description">
                            {{#if description}}
                                {{description}}
                            {{else}}
                                {{_t "No description."}}
                            {{/if}}
                        </div>

                        <div class="sb-membership-duration row">
                            <div class="col-lg-6 col-sm-6 col-xs-12">
                                <span class="glyphicon glyphicon-calendar" aria-hidden="true"></span>
                                {{_t "From"}}: <br> {{formatDateTime period_start 'date'}}
                            </div>

                            <div class="col-lg-6 col-sm-6 col-xs-12">
                                <span class="glyphicon glyphicon-calendar" aria-hidden="true"></span>
                                {{_t "To"}}: <br> {{formatDateTime period_end 'date'}}
                            </div>
                        </div>

                        <div class="row sb-membership-actions">
                            <div class="sb-membership-action col-sm-12">
                                <div class="active-container" data-id="{{id}}">
                                    <div class="wrapper btn price-formatter">
                                        {{_t "Renew for"}}
                                        {{recurring_price}}
                                        <span class="pr-currency">{{currency}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                {{/each}}


                {{#ifCondLength memberships '==' 0}}
                <div class="col-sm-12">
                    <div class="alert alert-info">
                        {{_t "list_empty"}}
                    </div>
                </div>
                {{/ifCondLength}}

            </div>
        </div>
	</div>
</div>