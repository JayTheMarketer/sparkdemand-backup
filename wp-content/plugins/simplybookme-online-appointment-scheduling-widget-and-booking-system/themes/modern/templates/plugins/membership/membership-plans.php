<?php
?>

<div class="tab-pd">

	<div class="clearfix">
        <div class="container-fluid">
            <div class="row">

                {{#ifCondLength memberships '>' 0}}
                <strong class="text-center col-sm-12 col-xs-12">{{_t "Service filter"}}</strong>

                <div id="sb-membership-filter" class="menu col-sm-12 col-xs-12">
                    <ul class="list-inline">
                        <li>
                            <a href="#" data-service="0">{{_t "All"}}</a>
                        </li>

                        {{#each event_list}}
                            {{#ifCondLength memberships '>' 0}}
                                <li>
                                    <a href="#" data-service="{{id}}">{{name}}</a>
                                </li>
                            {{/ifCondLength}}
                        {{/each}}
                    </ul>
                </div>
                {{/ifCondLength}}

                {{#each memberships}}
                <div class="sb-membership-item col-lg-6 col-sm-6 col-xs-12" data-id="{{id}}">
                    <div class="sb-membership-item-container">

                        <div class="sb-membeship-photo">
                            <div class="image" style="background-image: url('{{formatImagePath picture_path}}')"></div>
                        </div>

                        <div class="sb-membership-name">
                            {{name}}
                        </div>

                        <div class="sb-membership-description">
                            {{#if description}}
                                {{description}}
                            {{else}}
                                {{_t "No description."}}
                            {{/if}}
                        </div>

                        <div class="sb-membership-duration">
                            <span class="glyphicon glyphicon-time" aria-hidden="true"></span>
                            {{formatDuration duration type=duration_type}}
                        </div>

                        {{#ifIn id ../clientMembershipIds}}

                        <div class="row sb-membership-actions">
                            <div class="sb-membership-action col-sm-12">
                                <div class="active-container" data-id="{{id}}">
                                    <div class="wrapper btn price-formatter">
                                        {{_t "Renew for"}}
                                        {{recurring_price}}
                                        <span class="pr-currency">{{currency}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{else}}

                        <div class="row sb-membership-actions">
                            <div class="sb-membership-action col-sm-6">
                                <div class="info-container">
                                    <div class="wrapper price-formatter">
                                        {{_t "Renew for"}}
                                        {{recurring_price}}
                                        <span class="pr-currency">{{currency}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="sb-membership-action col-sm-6">
                                <div class="active-container" data-id="{{id}}">
                                    <a href="#" class="wrapper btn price-formatter">
                                        {{_t "Buy for"}}
                                        {{first_price}}
                                        <span class="pr-currency">{{currency}}</span>
                                    </a>
                                </div>
                            </div>
                        </div>

                        {{/ifIn}}



                    </div>
                </div>
                {{/each}}

                {{#ifCondLength memberships '==' 0}}
                <div class="col-sm-12">
                    <div class="alert alert-info">
                        {{_t "list_empty"}}
                    </div>
                </div>
                {{/ifCondLength}}

            </div>
        </div>
	</div>
</div>