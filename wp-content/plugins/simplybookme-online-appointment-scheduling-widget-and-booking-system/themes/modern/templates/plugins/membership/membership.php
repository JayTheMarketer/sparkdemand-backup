<?php
?>

<div class="tab-pd">
	<div class="title-main">
		{{_t "Membership"}}
	</div>

	<div class="col-lg-12 col-md-12 col-sm-12 plugin_info">
		<span class="error"></span>
		<span class="mess"></span>
	</div>

	<div class="menu" id="sb-membership-menu">
		<ul class="list-inline">
			<li><a href="#" class="sb-membership-menu-plans">{{_t "Memberships plans"}}</a></li>
			<li><a href="#" class="sb-membership-menu-purchased">{{_t "Purchased plans"}}</a></li>
			<li><a href="#" class="sb-membership-menu-history">{{_t "Payments history"}}</a></li>
		</ul>
	</div>

	<div class="custom-form clearfix">
		<div id="sb-membership-content">

		</div>
<!--
		<div class="col-xs-12 col-sm-12 text-center page-actions">
			<button class="btn btn-primary" id="sb-bookings-back">
				{{_t "Back"}}
			</button>
		</div>
-->
	</div>
</div>