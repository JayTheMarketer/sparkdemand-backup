<?php
?>


<div class="tab-pd">

	<div class="clearfix">
        <div class="container-fluid">
            <div class="row">


                <table class="table table-striped tab">
                    <thead class="table-header">
                    <tr>
                        <th>{{_t "Transaction date"}}</th>
                        <th>{{_t "Item title"}}</th>
                        <th>{{_t "Amount"}}</th>
                        <th>{{_t "Payment processor"}}</th>
                        <th>{{_t "Next payment date"}}</th>
                        <th>{{_t "Status"}}</th>
                    </tr>
                    </thead>
                    <tbody id="sb_membership_payment_history">

                    {{#each payments}}
                    <tr class="paid-row">
                        <td>
                            {{formatDateTime payment_date 'datetime'}}
                        </td>
                        <td>
                            {{membership_name}}<br>
                            {{formatDateTime period_start 'date'}} - {{formatDateTime period_end 'date'}}
                        </td>
                        <td>
                            {{payed_amount}} {{currency}}
                        </td>
                        <td>
                            {{_t payment_processor}}
                        </td>
                        <td>
                            {{formatDateTime next_payment_datetime 'datetime'}}
                        </td>
                        <td class="paid">
                            <span class="">{{_t status}}</span>
                        </td>
                    </tr>
                    {{/each}}

                    </tbody>
                </table>

            </div>
        </div>
	</div>
</div>