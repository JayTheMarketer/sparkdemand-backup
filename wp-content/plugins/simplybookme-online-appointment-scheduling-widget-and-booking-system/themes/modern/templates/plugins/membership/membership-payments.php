<?php
?>

<div class="tab-pd">
	<div class="title-sub-main">
		{{_t "Instructions"}}:
	</div>
	<div class="clearfix">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-12">
					{{_t "To buy membership, please choose a payment method"}}
				</div>

                <div id="sb-membership-payment-result">
                    <div class="methods col-sm-12">
                        {{#if status.stripe}}
                        <button class="method_status" data-payment="stripe">{{_t "Stripe Payment System"}}</button>
                        {{/if}}
                        {{#if status.paypal}}
                        <button class="method_status" data-payment="paypal">{{_t "Paypal payment"}}</button>
                        {{/if}}
                        {{#if status.adyen}}
                        <button class="method_status" data-payment="adyen">{{_t "Adyen payment"}}</button>
                        {{/if}}
                        {{#if status.paymentwall}}
                        <button class="method_status" data-payment="paymentwall">{{_t "Paymentwall payment"}}</button>
                        {{/if}}
                        {{#if status.paymentwall_brick}}
                        <button class="method_status" data-payment="paymentwall_brick">{{_t "Paymentwall payment"}}</button>
                        {{/if}}
                        {{#if (_toInt (Config 'allow_delay_payment'))}}
                        <button class="method_status" data-payment="delay">{{_t "Delay payment"}}</button>
                        {{/if}}
                    </div>
                </div>
			</div>
		</div>
	</div>
</div>