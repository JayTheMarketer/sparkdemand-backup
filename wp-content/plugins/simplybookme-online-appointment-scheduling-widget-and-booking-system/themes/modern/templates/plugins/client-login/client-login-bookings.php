<?php
?>

<div class="tab-pd">
    <div class="title-main">
		{{_t "Bookings history"}}
    </div>

    <div class="col-lg-12 col-md-12 col-sm-12 plugin_info">
        <span class="error"></span>
        <span class="mess"></span>
    </div>

    <div class="custom-form clearfix">

        {{#each bookings}}
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 sb-bookings-item">
            <div class="row sb-bookings-item-container">
                <div class="col-sm-12 service-name">
                    {{#ifIn this.service_id ../event_list}}
                        {{#object ../event_list key=service_id}} {{name}} {{/object}}
                    {{else}}
                        {{_t "Deleted"}}
                    {{/ifIn}}
                </div>
                <div class="col-sm-6 label">{{_t "Date"}}:</div>
                <div class="col-sm-6">{{formatDateTime start_date 'date'}}</div>

                <div class="col-sm-6 label">{{_t "Starts at"}}:</div>
                <div class="col-sm-6">{{formatDateTime (concat start_date ' ' start_time) 'time'}}</div>

                <div class="col-sm-6 label">{{_t "Ends at"}}:</div>
                <div class="col-sm-6">{{formatDateTime (concat end_date ' ' end_time) 'time'}}</div>

                <div class="col-sm-6 label">{{_t "Service provider"}}:</div>
                <div class="col-sm-6">
                    {{#ifIn this.provider_id ../unit_list}}
                        {{#object ../unit_list key=provider_id}} {{name}} {{/object}}
                    {{else}}
                        {{_t "Deleted"}}
                    {{/ifIn}}
                </div>

                <div class="col-sm-6 label">{{_t "Booking code"}}:</div>
                <div class="col-sm-6">{{code}}</div>

                <div class="col-sm-6 label">{{_t "Status"}}:</div>
                <div class="col-sm-6">{{_t status}}</div>

                <div class="col-sm-12 actions">
                    <button class="btn btn-danger {{#if is_cancellable}}{{else}}disabled{{/if}}" id="sb-bookings-cancel-booking" {{#if is_cancellable}}data-hash="{{hash}}" data-id="{{id}}"{{else}} disabled {{/if}}>
                        <span class="glyphicon glyphicon-remove"></span>
                        {{_t "Cancel"}}
                    </button>
                </div>
            </div>
        </div>
        {{/each}}


        <div class="col-xs-12 col-sm-12 text-center page-actions">
            <button class="btn btn-primary" id="sb-bookings-back">
                {{_t "Back"}}
            </button>
        </div>

    </div>
</div>