<?php
?>


<div class="col-sm-offset-7 col-sm-5">
	<div class="media">
		<!--<a class="pull-right" href="#">
			<img class="media-object dp img-circle avatar" src="#" >
		</a>-->
		<div class="media-body">
			<h5>{{_t "Hello"}}, <a href="#" class="user_name">{{name}}</a></h5>
			<hr style="margin:8px auto">
		</div>
	</div>
</div>

<div class="profile">
	<div class="col-md-12">
		<div class="profile-sidebar">

			<!-- <div class="profile-userpic">
				<img src="http://keenthemes.com/preview/metronic/theme/assets/admin/pages/media/profile/profile_user.jpg" class="img-responsive" alt="">
			</div>
			-->
			<div class="profile-usertitle">
				<div class="profile-usertitle-name user_name">
					{{name}}
				</div>

			</div>

			<div class="profile-usermenu">
				<ul class="nav">
					<li class="">
					    <span class="menuitem" id="sb-home-booking">
							<i class="glyphicon glyphicon-home"></i>
							{{_t "Home"}} ({{_t "book now"}})
						</span>
					</li>
					<li>
                        <span class="menuitem" id="sb-profile-edit">
                            <i class="glyphicon glyphicon-user"></i>
                            {{_t "Edit profile"}}
                        </span>
					</li>
                    <li>
                        <span class="menuitem" id="sb-profile-history">
                            <i class="glyphicon glyphicon-calendar"></i>
                            {{_t "Booking history"}}
                        </span>
					</li>
                    {{#ifPluginActive 'membership'}}
                    <li>
                        <span class="menuitem" id="sb-profile-membership">
                            <i class="glyphicon glyphicon-ok-sign"></i>
                            {{_t "Membership"}}
                        </span>
					</li>
                    {{/ifPluginActive}}
					<li>
                        <span class="menuitem" style="color:red;" id="sb-profile-logout">
                            <i class="glyphicon glyphicon-log-out"></i>
                            {{_t "Logout"}}
                        </span>
					</li>

				</ul>
			</div>


		</div>
	</div>

</div>



