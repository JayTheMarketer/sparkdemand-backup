<?php
?>


<div class="tab-pd">
    <div class="title-main">
        <?php sb_show_text("Please sign in to continue")?>
    </div>

    <div class="col-lg-12 col-md-12 col-sm-12 login_info">
        <span class="error"></span>
        <span class="mess"></span>
    </div>

    <div class="custom-form clearfix">
        <div class="col-sm-6">
            <div class="form-horizontal" id="sb_sign_in_form">
                <div class="cap">
                    <?php sb_show_text("sign in existing client")?>
                </div>
                <div class="form-group row">
                    <label for="sb_sign_in_email" class="col-sm-5 control-label"><?php sb_show_text("email")?></label>
                    <div class="col-sm-7">
                        <input type="email" class="form-control" id="sb_sign_in_email" name="email" value="" placeholder="<?php sb_show_text("email")?>">
                        <p class="help-block"></p>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="sb_sign_in_password" class="col-sm-5 control-label"><?php sb_show_text("password")?></label>
                    <div class="col-sm-7">
                        <input type="password" class="form-control" id="sb_sign_in_password" name="password" value="" placeholder="<?php sb_show_text("password")?>">
                        <p class="help-block"></p>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-offset-3 col-sm-9">
                        <a class="txt-right remind-pass" href="#"  data-toggle="modal" data-target="#sb-remind-password-c"><?php sb_show_text("remind_password")?></a>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12">
                        <button type="button" class="btn btn-primary custom" id="sb_sign_in_btn" ><?php sb_show_text("sign_in_and_continue")?></button>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="border hidden-xs hidden-sm"></div>
            <div class="form-horizontal" id="sb_sign_up_form">
                <div class="cap">
                    <?php sb_show_text("sign up new client")?>
                </div>
                <div class="form-group row">
                    <label for="sb_sign_up_name" class="col-sm-5 control-label"><?php sb_show_text("name")?></label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control" id="sb_sign_up_name" name="name" value="" placeholder="<?php sb_show_text("name")?>">
                        <p class="help-block"></p>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="sb_sign_up_email" class="col-sm-5 control-label"><?php sb_show_text("email")?></label>
                    <div class="col-sm-7">
                        <input type="email" class="form-control" id="sb_sign_up_email" name="email" value="" placeholder="<?php sb_show_text("email")?>">
                        <p class="help-block"></p>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="sb_sign_up_phone" class="col-sm-5 control-label"><?php sb_show_text("phone")?></label>
                    <div class="col-sm-7">
                        <input type="tel" class="form-control " id="sb_sign_up_phone" name="phone" value="" placeholder="<?php sb_show_text("phone")?>">
                        <p class="help-block"></p>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="sb_sign_up_password" class="col-sm-5 control-label"><?php sb_show_text("password")?></label>
                    <div class="col-sm-7">
                        <input type="password" class="form-control" id="sb_sign_up_password" name="password" value="" placeholder="<?php sb_show_text("password")?>">
                        <p class="help-block"></p>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12">
                        <button type="button" class="btn btn-primary custom" id="sb_sign_up_btn"><?php sb_show_text("sign_up")?></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



