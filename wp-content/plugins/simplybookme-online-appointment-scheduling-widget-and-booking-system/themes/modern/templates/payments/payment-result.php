<?php
?>

<div class="col-sm-12">
    {{#if payed_amount}}
        {{var "amount" payed_amount}}
    {{/if}}

	{{#ifCond result '==' 1}}
		<!-- success -->
        <div class="alert alert-success">
            {{#if message}}
                {{_t message}}
            {{else}}
                {{#if membership}}
                    {{_t "Congratulations! You've successfully paid %amount% %currency% for the %name% membership for the period from %period_start% to %period_end%" amount=amount currency=currency name=name period_start=(formatDateTime period_start 'date') period_end=(formatDateTime period_end 'date') }}
                {{else}}
                    {{_t "You've successfully reserved the service. Please check your email to see the notification!"}}
                {{/if}}
            {{/if}}
        </div>
	{{else}}
		{{#ifCond result '==' 2}}
			<!-- wait -->
            <div class="alert alert-info">
                {{#if message}}
                    {{_t message}}
                {{else}}
                    {{#if membership}}
                        {{_t "We haven't received the payment from payment system yet. If you made a payment, please wait until it is processed. Booking confirmation will be sent to your email. Sorry for inconveniences."}}
                    {{else}}
                        {{_t "We haven't received the payment from payment system yet. If you made a payment, please wait until it is processed. Booking confirmation will be sent to your email. Sorry for inconveniences."}}
                    {{/if}}
                {{/if}}
            </div>
        {{else}}
			<!-- error -->
            <div class="alert alert-danger">
                {{_t "Error! Payment failed."}}
                {{#if message}}
                    </br>Error: "{{message}}"
                {{/if}}
            </div>
		{{/ifCond}}
	{{/ifCond}}

</div>
