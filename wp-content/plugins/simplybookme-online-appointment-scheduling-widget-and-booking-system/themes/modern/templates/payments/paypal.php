<?php
?>

<form action="" method="POST" class="paypal_form" target="_top">
	<input type="hidden" name="upload" value="1">
	<input type="hidden" name="cmd" value="_cart">
	<input type="hidden" name="business" value="">

	<input type="hidden" name="item_name_1" value="IDs: {{bookingIds}}">
	<input type="hidden" name="quantity_1" value="1">
	<input type="hidden" name="amount_1" value="{{cart.amount}}">

	<input type="hidden" name="currency_code" value="{{cart.currency}}">
	<input type="hidden" name="no_shipping" value="1">
	<input type="hidden" name="no_note" value="1">

	<input type="hidden" name="shopping_url" value="">
	<input type="hidden" name="cancel_return" value="">
	<input type="hidden" name="notify_url" value="">
	<input type="hidden" name="return" value="">

	<!--<input type="hidden" name="lc" value="en">-->
	<input type="hidden" name="first_name" value="{{#if clientData.name}}{{clientData.name}}{{else}}ClientId{{clientData.client_id}}{{/if}}">
	<input type="hidden" name="last_name" value="">
	<input type="hidden" name="address1" value="">
	<input type="hidden" name="address2" value="">
	<input type="hidden" name="city" value="">
	<input type="hidden" name="country" value="">
	<input type="hidden" name="zip" value="">

	<input type="hidden" name="email" value="">
	<input type="hidden" name="custom" value="">

	<input type="hidden" name="charset" value="UTF-8">

	<input type="submit" class="btn btn-success" value="{{_t 'Pay'}} {{cart.amount}} {{cart.currency}}" id="pay_btn">
</form>
