<?php

?>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">

            <div id="simplybook_view" class="row">

                <div class="col-sm-12 booking_info">
                    <span class="error"></span>
                    <span class="mess"></span>
                </div>

                <!-- Stage 1 - booking process (select location, category, service, provider, date and time) -->
                <div class="stage_1">
                    <div class="">

                        <div class="location_block">
                            <h3>{{_t "Location"}}:</h3>
                            <div class="content_block"></div>
                            <!--<select class="form-control" name="location"></select>-->
                        </div>

                        <div class="category_block">
                            <h3>{{_t "Category"}}:</h3>
                            <div class="content_block"></div>
                            <!--<select class="form-control" name="category"></select>-->
                        </div>

                        <div class="service_block">
                            <h3>{{_t "Services"}}:</h3>
                            <div class="content_block"></div>
                            <!--<select class="form-control" name="service"></select>-->
                        </div>

                        <div class="group_bookings_block">
                            <h3>{{_t "Participants count"}}:</h3>
                            <select class="form-control" name="group-booking-count"></select>
                        </div>

                        <div class="provider_block">
                            <h3>{{_t "Provider"}}:</h3>
                            <div class="content_block"></div>
                            <!--<select class="form-control" name="provider"></select>-->
                        </div>

                    </div>

                    <div>

                        <div class="date_block">
                            <h3>{{_t "Select date and time"}}:</h3>

                            <div class="row">
                                <div id="calendar" class=""></div>
                                <div id="starttime" class=""></div>
                            </div>
                        </div>

                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <center>
                            <button class="btn btn-primary btn-back">{{_t "Back"}}</button>
                        </center>
                    </div>
                </div>

                <!-- Stage 2 - booking additional -->

                <form class="form_field" method="POST" novalidate>

                <div class="stage_2">

                    <div class="detailed_block">

                        <div class="detailed_item">
                            <h3 class="title_item">Item title:</h3>
                            <div class="image_item">
                                <img src="" alt=""/>
                            </div>
                            <h2 class="name_item">Name item</h2>
                            <span class="description_item"></span>
                        </div>

                    </div>


                    <div class=" fields">

                        <div class="col-lg-12 col-md-12 col-sm-12"> <!-- if multiple plugin enable  -->
                            <div class="row booked_list">
                                <div class="col-lg-6 col-md-6 booked_item">
                                    <span class="name">Service Name</span>
                                <span class="time">
                                    <span class="glyphicon glyphicon-time"></span>
                                    dd.mm.yyyy H:i
                                </span>
                                <span class="provider">
                                    <span class="glyphicon glyphicon-user"></span>
                                    <span class="tx">ProvName</span>
                                </span>
                                    <span class="close">x</span>
                                </div>
                            </div>
                        </div>

                        <div class="authorize_block">
                            {{_t "You are logged in as "}} <span class="user_name"></span>
                            <br>
                            <div class="btn btn-danger" id="sb_sign_out_btn">{{_t "Logout"}}</div>
                        </div>

                        <div class="fields_base">

                            <h3 class="client_name">{{_t "Your name"}}: <span class="required_p">*</span> </h3>
                            <input type="text" class="form-control" name="name" required/>

                            <h3 class="client_email">{{_t "E-mail"}}:</h3>
                            <input type="text" class="form-control" name="email" data-rule-email="true" />

                            <h3 class="client_phone">{{_t "Phone"}}:</h3>
                            <div class="form-group">
                                    <select id="countries_phone1" name="phone_code" class="form-control bfh-countries"></select>
                                    <input type="text" class="form-control bfh-phone" data-rule-digits="true" name="phone" data-country="countries_phone1">
                            </div>
                        </div>


                        <div class="fields_adds">

                        </div>

                        <div class="fields_promo">

                        </div>

                    </div>


                    <div class="col-lg-12 col-md-12 col-sm-12 cancelation_policy">

                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 user_license">
                        {{{_t "By clicking the button, you agree to the <a href=\"#\">Terms & Conditions</a>."}}}
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 button_block">
                        <center>
                            <button type="button" class="btn btn-primary btn-back">{{_t "Back"}}</button>
                            <button type="button" class="btn btn-primary center-block multiple-book">{{_t "Add more bookings"}}</button>
                            <button type="submit" class="btn btn-primary center-block btn-book2">{{_t "Book now"}}</button>
                        </center>
                    </div>

                </div>
                </form>

                <!-- Stage 3 - result -->
                <div class="stage_3">
                    <div class="col-lg-12 col-md-12 col-sm-12">

                        <div class="row result_booked_list">
                            <div class="result_booked_item">
                                <span class="time">

                                </span>
                                <span class="name">Service Name</span>

                                <span class="provider">
                                    <span class="tx">ProvName</span>
                                </span>
                            </div>
                        </div>

                        <span class="methods">
                            <button class="method_status stripe">{{_t "Stripe Payment System"}}</button>
                            <button class="method_status paypal">{{_t "Paypal payment"}}</button>
                            <button class="method_status adyen">{{_t "Adyen payment"}}</button>
                            <button class="method_status paymentwall">{{_t "Paymentwall payment"}}</button>
                            <button class="method_status paymentwall_brick">{{_t "Paymentwall Brick payment"}}</button>
                            <button class="method_status delay">{{_t "Delay payment"}}</button>
                        </span>

                        <span class="stripe_payment">
                            <button id="stripePayButton">{{_t "Pay"}}</button>
                        </span>

                        <span class="paypal_payment" id="paypal-payment-c">

                        </span>

                        <span class="paymentwall_payment">

                        </span>

                        <span class="paymentwall_brick_payment" id="paymentwall-brick-payment-c">

                        </span>

                        <span class="adyen_payment" id="adyen-payment-c">

                        </span>

                        <span class="result"></span>
                    </div>
                </div>


                <div class="user_license_popup">

                </div>

                <div class="cancelation_policy_popup">

                </div>

            </div>

        </div>
    </div>
</div>


