<?php
/**
*   Default template SimplyBook
**/
?>


<div class="container-fluid body_ex bootstrap-iso" id="sb-plugin-container">

    <div id="sb-account-panel-c"></div>

    <div id="client-login-plugin-c" class="row section column"></div>
    <div id="client-login-bookings-c" class="row section column"></div>

    <div id="membership-plugin-c" class="row section column"></div>

    <?php
    // TODO : refactor code
    sb_include('full', 'full-booking-t'); ?>

    <div class="modal fade" id="sb-edit-profile-c" role="dialog"></div>
    <div class="modal fade" id="sb-remind-password-c" role="dialog"></div>
    <div class="modal fade" id="sb-membership-buy-c" role="dialog"></div>

    <div class="simplybook_preloader">
        <div class='uil-ring-css'><div></div></div>
    </div>

</div> <!-- /container -->


<!-- Client Login Plugin -->
<?php sb_include('plugins/client-login/client-login', 'client-login-plugin-t'); ?>
<?php sb_include('plugins/client-login/client-login-bookings', 'client-login-booking-history-t'); ?>
<?php sb_include('plugins/client-login/client-login-panel', 'client-login-plugin-panel-t'); ?>
<!-- Modal Edit profile -->
<?php sb_include('modals/edit-profile', 'client-login-plugin-edit-profile-t'); ?>
<!-- Modal Remind password -->
<?php sb_include('modals/remind', 'client-login-plugin-remind-password-t'); ?>



<!-- Membership Plugin -->
<?php sb_include('plugins/membership/membership', 'membership-plugin-t'); ?>
<?php sb_include('plugins/membership/membership-plans', 'membership-plugin-plans-t'); ?>
<?php sb_include('plugins/membership/membership-purchased', 'membership-plugin-purchased-t'); ?>
<?php sb_include('plugins/membership/membership-history', 'membership-plugin-history-t'); ?>
<?php sb_include('plugins/membership/membership-payments', 'membership-plugin-payments-t'); ?>
    <!-- Modal Buy membership -->
<?php sb_include('modals/buy-membership', 'membership-plugin-buy-membership-t'); ?>


<!-- Payments -->
<?php sb_include('payments/paypal', 'paypal-payment-t'); ?>
<?php sb_include('payments/paymentwall-brick', 'paymentwall-brick-payment-t'); ?>
<?php sb_include('payments/adyen', 'adyen-payment-t'); ?>
<?php sb_include('payments/payment-result', 'payment-result-t'); ?>