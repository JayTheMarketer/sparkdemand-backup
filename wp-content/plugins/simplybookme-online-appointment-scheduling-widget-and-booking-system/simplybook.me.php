<?php
/*
Plugin Name: Online Appointment Scheduling Widget and Booking System
Plugin URI: https://simplybook.me/index/wp-plugin
Description: Easy to use online booking solution for you and your clients.
Version: 3.0.5
Author: Simplybook Inc.
Author URI: http://simplybook.me/
*/
session_start();

$simplybook_domain = 'simplybook_me';
$simplybook_plugin_dir =  plugin_basename(__DIR__);

define("SIMPLYBOOK_DIR",  WP_PLUGIN_DIR . '/' .$simplybook_plugin_dir);
define("SIMPLYBOOK_URL",  plugins_url() . '/' .$simplybook_plugin_dir);

$simplybook_cfg = simplybook_get_config();

function simplybook_get_config(){
	global $simplybook_domain;

	$default_options = array(
		'url' => 'user-api.simplybook.me',
		'stripe_url' => 'api.stripe.com',
		'paypal_url' => 'www.paypal.com',
		'login' => 'company_login',
		'api_key' => 'your_api_key',
		'api_secret_key' => 'your_api_secret_key',
		'template' => 'modern',
		'cached_methods' => array('available_methods', 'company_timezone', 'getUnitList', 'getEventList', 'getCategoriesList', 'getLocationsList', 'getAnyUnitData', 'getCompanyParam', 'getCompanyParams', /*'getPluginStatuses',*/ 'getAdditionalFields', 'getCountryPhoneCodes', 'getBookingCart')
	);

	//update_option($simplybook_domain, $default_options); //uncomment if new option param

	$options = get_option($simplybook_domain);

	if(!$options){
		add_option($simplybook_domain, $default_options); //set default value
		$options = $default_options;
	}

	$cached_diff = array_diff($default_options['cached_methods'], $options['cached_methods']);
	$cached_diff2 = array_diff($options['cached_methods'], $default_options['cached_methods']);

	if(count($cached_diff) || count($cached_diff2)){
		$options['cached_methods'] = $default_options['cached_methods'];
		update_option($simplybook_domain, $options);
	}

	if($options['template'] == 'default'){  //TODO: temp if
		$options['template'] = 'modern';
		update_option($simplybook_domain, $options);
	}

	return $options;
}


function simplybook_admin(){  //if load wp-admin
    global $simplybook_domain, $simplybook_cfg;
    add_options_page(sb_get_text("Simplybook Plugin Settings"), sb_get_text("Simplybook plugin"), 'manage_options', $simplybook_domain, 'simplybook_admin_page');
}

function simplybook_admin_page(){ //page content
	simplybook_init_process('admin_page');
}

function simplybook_add_action_links ( $links ) { // add settings links in plugin list
    global $simplybook_domain;

    $mylinks = array(
        '<a href="' . admin_url( 'options-general.php?page='.$simplybook_domain ) . '">'.sb_get_text("Settings").'</a>'
    );
    return array_merge( $links, $mylinks );
}

function simplybook_add_translations(){
    global $simplybook_domain, $simplybook_plugin_dir;

    if (function_exists('load_plugin_textdomain')) {
        $locale = get_locale();
        //print_r($locale);
        load_plugin_textdomain($simplybook_domain, false, './'.$simplybook_plugin_dir.'/langs/');
    }
}

function sb_get_text($text){
    global $simplybook_domain;
    return __($text,$simplybook_domain);
}

function sb_show_text($text){
    global $simplybook_domain;
    echo __($text,$simplybook_domain);
}

function simplybook_run(){
    global $simplybook_domain, $simplybook_cfg;
    simplybook_add_translations();
}

function simplybook_booking_content(){
	return simplybook_init_process('show');
}

function simplybook_init_process($do){
	global $simplybook_domain, $simplybook_cfg, $simplybook_plugin_dir;
	$api_url = 	admin_url('admin-ajax.php')."?action=sb_api";

	if($do == 'show'){

		//get transliatons
		$translations = get_translations_for_domain($simplybook_domain);
		$translations_keys = array();
		foreach($translations->entries as $key=>$val){
			$translations_keys[$key] = $val->translations[0];
		}

		// Register the script
		/*wp_register_script( $simplybook_domain.'_translation', plugins_url($simplybook_plugin_dir.'/js/locale.js') );
		wp_register_script( $simplybook_domain.'_flexible_timeline', plugins_url($simplybook_plugin_dir.'/js/flexible-timeline.js') );
		wp_register_script( $simplybook_domain.'_api_client', plugins_url($simplybook_plugin_dir.'/js/api-client.js') );
		wp_register_script( $simplybook_domain.'_api_model', plugins_url($simplybook_plugin_dir.'/js/api-model.js') );
		wp_register_script( $simplybook_domain.'_login_plugin', plugins_url($simplybook_plugin_dir. '/js/client-login.js') );*/
		wp_register_script( $simplybook_domain.'_api_view', plugins_url($simplybook_plugin_dir.'/themes/'.$simplybook_cfg['template'].'/js/booking.view.min.js') );
		wp_register_script( $simplybook_domain.'_init', plugins_url($simplybook_plugin_dir. '/js/init.min.js') );

		wp_enqueue_script('jquery');
		wp_enqueue_script('jquery-ui-core');
		wp_enqueue_script('jquery-ui-dialog');
		wp_enqueue_script('jquery-ui-datepicker');
		wp_enqueue_script( $simplybook_domain.'_init', array('jquery', 'jquery-ui-core', 'jquery-ui-datepicker') );
		wp_enqueue_script( $simplybook_domain.'_api_view', array('jquery', 'jquery-ui-core', 'jquery-ui-datepicker') );

/*		wp_enqueue_script( $simplybook_domain.'_translation', array('jquery', 'jquery-ui-core', 'jquery-ui-datepicker') );
		wp_enqueue_script( $simplybook_domain.'_flexible_timeline', array('jquery', 'jquery-ui-core', 'jquery-ui-datepicker') );
		wp_enqueue_script( $simplybook_domain.'_api_client', array('jquery', 'jquery-ui-core', 'jquery-ui-datepicker') );
		wp_enqueue_script( $simplybook_domain.'_api_model', array('jquery', 'jquery-ui-core', 'jquery-ui-datepicker') );
		wp_enqueue_script( $simplybook_domain.'_login_plugin', array('jquery', 'jquery-ui-core', 'jquery-ui-datepicker') );*/

		wp_localize_script( $simplybook_domain.'_init', 'simplybook_translation', $translations_keys );
		wp_localize_script( $simplybook_domain.'_init', 'simplybook_api_url', $api_url );
		wp_localize_script( $simplybook_domain.'_init', 'simplybook_stripe_status', (((int)@$simplybook_cfg['stripe_status'])?'1':'0') );
		wp_localize_script( $simplybook_domain.'_init', 'simplybook_paypal_status', (((int)@$simplybook_cfg['paypal_status'])?'1':'0') );
		wp_localize_script( $simplybook_domain.'_init', 'simplybook_adyen_status', (((int)@$simplybook_cfg['adyen_status'])?'1':'0') );
		wp_localize_script( $simplybook_domain.'_init', 'simplybook_paymentwall_status', (((int)@$simplybook_cfg['paymentwall_status'])?$simplybook_cfg['paymentwall_status']:'0') );
		wp_localize_script( $simplybook_domain.'_init', 'simplybook_url', '//' . substr(@$simplybook_cfg['url'] , strpos(@$simplybook_cfg['url'], '.')+1) );  //if api_url = user-api.simplybook.me than return //simplybook.me


		if((int)@$simplybook_cfg['stripe_status']){
			wp_enqueue_script( 'stripe-checkout', '//checkout.stripe.com/checkout.js', array( 'jquery' ));

			wp_localize_script( $simplybook_domain.'_init', 'simplybook_stripe_key', @$simplybook_cfg['stripe_api_key'] );
			wp_localize_script( $simplybook_domain.'_init', 'simplybook_stripe_name', @$simplybook_cfg['stripe_name'] );
			wp_localize_script( $simplybook_domain.'_init', 'simplybook_stripe_description', @$simplybook_cfg['stripe_description'] );
		}

		if((int)@$simplybook_cfg['paymentwall_status']){
			wp_enqueue_script( 'paymentwall-brick', 'https://api.paymentwall.com/brick/brick.1.4.js', array( 'jquery' ));
			//wp_enqueue_script( 'paymentwall-class', plugins_url($simplybook_plugin_dir.'/js/view/paymentwall.js'), array( 'jquery' )) ;
			wp_enqueue_style('paypal-checkout-style', plugins_url($simplybook_plugin_dir.'/css/paymentwall.css') );

			wp_localize_script( $simplybook_domain.'_init', 'simplybook_paymentwall_key', @$simplybook_cfg['paymentwall_api_key'] );
			//wp_localize_script( $simplybook_domain.'_init', 'simplybook_paymentwall_name', @$simplybook_cfg['paymentwall_name'] );
			//wp_localize_script( $simplybook_domain.'_init', 'simplybook_paymentwall_description', @$simplybook_cfg['paymentwall_description'] );
		}

		if((int)@$simplybook_cfg['paypal_status']){
			//wp_enqueue_script( 'paypal-checkout', plugins_url($simplybook_plugin_dir.'/js/view/paypal.js'), array( 'jquery' )) ;
			wp_enqueue_style('paypal-checkout-style', plugins_url($simplybook_plugin_dir.'/css/paypal.css') );

			wp_localize_script( $simplybook_domain.'_init', 'simplybook_paypal_url', @$simplybook_cfg['paypal_url'] );
			wp_localize_script( $simplybook_domain.'_init', 'simplybook_paypal_account', @$simplybook_cfg['paypal_account'] );
		}

		if((int)@$simplybook_cfg['adyen_status']){
			//wp_enqueue_script( 'adyen-checkout', plugins_url($simplybook_plugin_dir.'/js/view/adyen.js'), array( 'jquery' )) ;
			wp_enqueue_style('adyen-checkout-style', plugins_url($simplybook_plugin_dir.'/css/adyen.css') );

			wp_localize_script( $simplybook_domain.'_init', 'simplybook_adyen_mode', @$simplybook_cfg['adyen_mode'] );
			//wp_localize_script( $simplybook_domain.'_init', 'simplybook_paypal_account', @$simplybook_cfg['paypal_account'] );
		}


		$simplybook_params = (array)simplybook_get_api(
			'getCompanyParams',
			array(
				'time_format',
				'date_format',
				'monday_is_first_day',
				'show_booking_page_in_client_timezone',
				'require_fields',
				'fixed_country_prefix',
				'changable_prefix_by_client',
				'allow_delay_payment',
				'user_public_timeline_type',
                'cancelation_policy_label',
                'cancelation_policy_text'
			)
		);

		wp_localize_script( $simplybook_domain.'_init', 'simplybook_params',$simplybook_params);
		wp_localize_script( $simplybook_domain.'_init', 'simplybook_loader_mode', $simplybook_cfg['loader_mode']);

		//wp_enqueue_style("wp-jquery-ui-dialog");
		wp_register_style( $simplybook_domain.'_style', plugins_url($simplybook_plugin_dir.'/themes/'.$simplybook_cfg['template'].'/css/style.css') );
		wp_enqueue_style( $simplybook_domain.'_style' );

		wp_register_style( $simplybook_domain.'_flexible_timeline', plugins_url($simplybook_plugin_dir.'/css/flexible-timeline.css') );
		wp_enqueue_style( $simplybook_domain.'_flexible_timeline' );

		wp_register_style( $simplybook_domain.'_bootstrap', plugins_url($simplybook_plugin_dir.'/libs/bootstrap/css/bootstrap-iso.css') ); //Isolate Bootstrap CSS to Avoid Conflicts
		wp_enqueue_style( $simplybook_domain.'_bootstrap' );


		$minify = function($str){
			return trim(preg_replace('/[[:^print:]]/', '', $str));
		};
		$custom_css = implode( "\n", array_map( $minify, explode( "\n", $simplybook_cfg['custom_css'] ) ) );
		$custom_css = preg_replace('/^\s+|\n|\r|\t|\s+$/m', '', $custom_css); //remove  all whitespace and line-breaks;

		wp_add_inline_style( $simplybook_domain.'_style', $custom_css );
		wp_enqueue_style('jquery-style', plugins_url($simplybook_plugin_dir.'/css/jquery-ui.css') );
	}

	$content = '';
	include SIMPLYBOOK_DIR . '/controller.php';
	return $content;
}

function simplybook_api_action(){
	global $simplybook_cfg;

	$map = array(
		'paymentwall_' => array( 'paymentwall_api', @$simplybook_cfg['paymentwall_status'] ),
		'adyen_' => array( 'adyen_api', @$simplybook_cfg['adyen_status'] ),
		'stripe_' => array( 'stripe_api', @$simplybook_cfg['stripe_status'] ),
		'paypal_' => array( 'paypal_api',@$simplybook_cfg['paypal_status'] )
	);

	foreach($map as $key=>$item) {
		list($api_name, $api_status) = $item;

		if ( strpos( @$_REQUEST['method'], $key ) !== false && $api_status ) {
			$_REQUEST['method'] = str_replace( $key, '', $_REQUEST['method'] );
			simplybook_init_process( $api_name );
			wp_die();
			return;
		}
	}
	simplybook_init_process('api');
	wp_die();
}


function simplybook_get_theme_list(){
	$directory = SIMPLYBOOK_DIR.'/themes/';
	$scanned_directory = array_diff(scandir($directory), array( '..', '.' ));
	$list = array();

	foreach($scanned_directory as $el){
		$list[] = array(
			'name' => $el,
			'dir' => SIMPLYBOOK_DIR.'/themes/'.$el,
			'url' => SIMPLYBOOK_URL.'/themes/'.$el
		);
	}
	return $list;
}

if (!function_exists('array_map_recursive')) {

	function array_map_recursive( $callback, $array ) {
		foreach ( $array as $key => $value ) {
			if ( is_array( $array[ $key ] ) ) {
				$array[ $key ] = array_map_recursive( $callback, $array[ $key ] );
			} else {
				$array[ $key ] = call_user_func( $callback, $array[ $key ] );
			}
		}
		return $array;
	}
}

function simplybook_get_api(){
	$args = func_get_args();
	$_REQUEST['method'] = array_shift($args);
	$_REQUEST['params'] = $args;

	ob_start();
	simplybook_init_process('api');
	$out = ob_get_contents();
	ob_end_clean();

	//return $out;
	return json_decode($out, true);
}

function simplybook_save_log($file_name, $data, $append = true){
	$log_dir = SIMPLYBOOK_DIR.'/logs/';

	if($append){
		file_put_contents($log_dir.$file_name.'.txt', $data, FILE_APPEND | LOCK_EX );
	} else {
		file_put_contents($log_dir.$file_name.'.txt', $data );
	}

}

function simplybook_clear_cache(){
	global $simplybook_cfg;

	foreach($simplybook_cfg['cached_methods'] as $method){
		delete_transient( 'simplybook_'.$method );
	}
}

function _is_curl_installed() {
	if  (in_array  ('curl', get_loaded_extensions())) {
		return true;
	}
	else {
		return false;
	}
}

function curl_load($url){
	curl_setopt($ch=curl_init(), CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$response = curl_exec($ch);
	curl_close($ch);
	return $response;
}

function sb_include($path, $template_name){
	global $simplybook_cfg;
	ob_start();
	include SIMPLYBOOK_DIR.'/themes/'.$simplybook_cfg['template'].'/templates/' . $path . '.php';
	$content = sb_sanitize_html_output(ob_get_contents());
	ob_end_clean();

	$result = "\n<script id=\"{$template_name}\" type=\"text/x-handlebars-template\">{$content}</script>\n";
	echo $result;
}

function sb_sanitize_html_output($buffer) {
	$search = array(
		'/\>[^\S ]+/s',     // strip whitespaces after tags, except space
		'/[^\S ]+\</s',     // strip whitespaces before tags, except space
		'/(\s)+/s',         // shorten multiple whitespace sequences
		'/<!--(.|\s)*?-->/' // Remove HTML comments
	);
	$replace = array('>','<','\\1',	'');
	$buffer = preg_replace($search, $replace, $buffer);
	return $buffer;
}


function sb_get_api_type($ids){
	$currentType = 'booking'; //default type
	$types = array(
		'm' => 'membership',
	);

	if(!is_array($ids)){
		$ids = array($ids);
	}

	foreach ($types as $typeCode=>$type){
		for($i=0; $i<count($ids); $i++){
			if(strpos(strval($ids[$i]), $typeCode)!== false){
				$currentType = $type;
				$ids[$i] = intval(str_replace($typeCode, '', $ids[$i]));
			} else{
				$ids[$i] = intval($ids[$i]);
			}
		}
	}

	return array($currentType, $ids);
}

add_action('admin_menu', 'simplybook_admin');
add_filter( 'plugin_action_links_' . plugin_basename(__FILE__), 'simplybook_add_action_links' ); // add settings links in plugin list
add_action('init', 'simplybook_run'); // add translation
//add_action('the_content', 'simplybook_booking_content');
add_shortcode( 'simplybook', 'simplybook_booking_content' );

///API hook
add_action('wp_ajax_sb_api', 'simplybook_api_action');
add_action('wp_ajax_nopriv_sb_api', 'simplybook_api_action');




?>
