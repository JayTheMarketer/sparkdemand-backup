<?php

/**
 * JSON-RPC CLient class 
 */
class JsonRpcClient {

    protected $_requestId = 1;
    protected $_contextOptions;
    protected $_url;
    
    /**
     * Constructor. Takes the connection parameters
     * 
     * @param String $url
     */
    public function __construct($url, $options = array()) {
        $headers = array('Content-type: application/json');
        if (isset($options['headers'])) {
            $headers = array_merge($headers, $options['headers']);
        }
        $this->_contextOptions = array(
            'http' => array(
                'method' => 'POST',
                'header' => implode("\r\n", $headers) . "\r\n"
            )
        );

        $this->_url = $url;
    }
    
    /**
     * Performs a jsonRCP request and return result
     *
     * @param String $method
     * @param Array $params
     * @return mixed
     */
	public function __call($method, $params) {
		$type = 'curl';

		$requests = array();
		if(!is_array($method)){
			$method = array($method);
			$params = array($params);
		}

		for($i = 0; $i<count($method); $i++){
			$currentId = $this->_requestId++;
			$requests[] = array(
				'method' => $method[$i],
				'params' => array_values($params[$i]),
				'id'     => $currentId
			);
		}

		if($type == 'curl'){
			$response = $this->curGet($this->_url, $requests);
		} else {
			foreach($requests as $request) {
				$method = $request['method'];
				$request = json_encode( $request );
				$this->_contextOptions['http']['content'] = $request;
				//var_dump($request);
				$response[$method] = file_get_contents( $this->_url, false, stream_context_create( $this->_contextOptions ) );
			}
		}

		//echo $ip = gethostbyname('user-api.simplybook.me');
		//var_dump($this->_url, $method, $params, $response);
		//die();

/*	    if($method == 'getPluginStatuses'){
		    var_dump($this->_url, $method, $params, $response);
			die($method);
		}*/

		$res = array();

		foreach($response as $key=>$respEl) {
			$result = json_decode( $respEl, false );

			/*if ( $result->id != $currentId ) {
				//throw new Exception("Incorrect response id (request id: {$currentId}, response id: {$result->id})" . "\n\nResponse: " . $response);
				throw new Exception( $respEl );
			}*/
			if ( isset( $result->error ) && $result->error ) {
				//throw new Exception("Request error: {$result->error->message}");
				throw new Exception( $respEl );
			}

			$res[$key] = $result->result;
		}


		//print_r($res);

		if(count($res)<=1) {
			//return $result->result;
			return array_pop($res);
		} else {
			return $res;
		}
	}


	function curGet($url, $requests) {
		$mh = curl_multi_init(); // init the curl Multi
		$aCurlHandles = array(); // create an array for the individual curl handles
		$headers = array_map('trim', explode("\n", $this->_contextOptions['http']['header']) );

		foreach($requests as $request) {
			$method = $request['method'];

			$ch = curl_init();

			if($request != null){
				curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request));
			}

			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers );
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); //ONLY FOR TEST API
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false); //ONLY FOR TEST API
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_VERBOSE, 0);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_USERAGENT, "Worpdress/Joomla module API request, Mozilla/4.0 (compatible;)");
			curl_setopt($ch, CURLOPT_URL, $url);

			$aCurlHandles[$method] = $ch;
			curl_multi_add_handle($mh, $ch);
		}

		$active = null;

		do {
			//curl_multi_select($mh);
			$mrc = curl_multi_exec($mh, $active);
		} while ($active);

		$response = array();
		// iterate through the handles and get your content
		foreach ($aCurlHandles as $method=>$ch) {
			$response[$method] = curl_multi_getcontent($ch); // get the content
			curl_multi_remove_handle($mh, $ch); // remove the handle (assuming  you are done with it);
		}

		curl_multi_close($mh);

		return $response;
	}
}