<?php

/**
 * User: vetal
 * Date: 30.12.15
 * Time: 16:09
 */



class ApiClient {

	protected $_token = null;
	protected $_url = null;
	protected $_company_login = null;
	protected $_api_key = null;
	protected $_options = null;

	protected $_client = null;

	public function __construct($url, $login, $api_key) {
		$this->_url = $url;
		$this->_company_login = $login;
		$this->_api_key = $api_key;
		$this->_login();
	}

	public function __call($method, $args) {
		try {
			return $this->_client->__call($method, $args);
		} catch (Exception $e) {
			if ($e->getCode() == -32600) {
				$this->_login(true);

				return $this->_client->__call($method, $args);
			} else {
				throw $e;
			}
		}
	}

	protected function _login($forceLogin = false) {

		$token = get_transient( 'simplybook_token' );

		if ($forceLogin || $token === false) {
			$loginClient = new JsonRpcClient( $this->_url. '/login/' );
			$token       = $loginClient->getToken( $this->_company_login, $this->_api_key );
			set_transient( 'simplybook_token', $token, HOUR_IN_SECONDS-60 ); //59 min
		}

		$this->_client = new JsonRpcClient( $this->_url, array(
			'headers' => array(
				'X-Company-Login: ' . $this->_company_login,
				'X-Token: ' . $token
			)
		) );

	}


}