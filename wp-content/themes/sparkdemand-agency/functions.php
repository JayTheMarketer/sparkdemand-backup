<?php

/**
 * Functions - Child theme custom functions
 */


/*************************************************************************************************************************
****************************** CAUTION: do not remove or edit anything within this section ******************************/

/**
 * Enqueue styles for child theme.
 * Do not remove this, or your child theme will not work unless you include a @import rule in your child stylesheet.
 */
function dce_enqueue_styles()
{
    wp_enqueue_style('divi-parent-styles', get_template_directory_uri() . '/style.css');
    wp_enqueue_style('child-theme-styles', get_stylesheet_directory_uri() . '/style.css', array( 'divi-parent-styles' ));
    wp_enqueue_style('font-awesome', get_stylesheet_directory_uri() . '/includes/resources/font-awesome-4.7.0/css/font-awesome.min.css');
}
add_action('wp_enqueue_scripts', 'dce_enqueue_styles');

/**
 * Makes the Divi Children Engine available for the child theme.
 * Do not remove this, or you will lose all the customization capabilities created by Divi Children Engine.
 */
require_once('divi-children-engine/divi_children_engine.php');

/***********************************************************************************************************************/

/*- You can include any custom code for your child theme below this line -*/

wp_register_style( 'socicon', 'https://d1azc1qln24ryf.cloudfront.net/114779/Socicon/style-cf.css' );
wp_enqueue_style('socicon');

add_action('init', 'stop_heartbeat', 1);
function stop_heartbeat()
{
    wp_deregister_script('heartbeat');
}

add_filter('gform_field_value_gacid', 'getgacid');
function getgacid($value) {
    $tracker = ga.getAll()[0];
    try {
        return $tracker.get('clientId');
    } catch (Exception $e) {
        return 'n/a';
    }
}


/**
 * Load stylesheet
 */

function mmm_elegant_enqueue_css()
{
    wp_enqueue_style('parent-style', get_template_directory_uri() . '/style.css');
}
add_action('wp_enqueue_scripts', 'mmm_elegant_enqueue_css');

/**
 * Fix “Full Width Menu” module
 * By default, when using the full width menu module in Divi, the Primary Menu will always be displayed
 * regardless of the menu selected in the module settings. To fix this add the following to the bottom
 * of your themes functions.php file:
**/
function mmm_remove_theme_location($args)
{
    if (isset($args['theme_location'])) {
        unset($args['theme_location']);
    }

    return $args;
}
add_filter('et_fullwidth_menu_args', 'mmm_remove_theme_location');

/**
 * Create a new "Divi" Mega Menu theme
 */
function load_divi_theme($themes)
{
    $themes["divi"] = array(
        'title' => 'Divi',
        'container_background_from' => 'rgba(255, 255, 255, 0)',
        'container_background_to' => 'rgba(255, 255, 255, 0)',
        'arrow_up' => 'dash-f343',
        'arrow_down' => 'dash-f347',
        'arrow_left' => 'dash-f341',
        'arrow_right' => 'dash-f345',
        'menu_item_background_hover_from' => 'rgba(0, 0, 0, 0.03)',
        'menu_item_background_hover_to' => 'rgba(0, 0, 0, 0.03)',
        'menu_item_link_height' => '83px',
        'menu_item_link_color' => 'rgb(51, 51, 51)',
        'menu_item_link_color_hover' => 'rgb(34, 34, 34)',
        'menu_item_highlight_current' => 'on',
        'panel_background_from' => 'rgb(250, 250, 250)',
        'panel_background_to' => 'rgb(250, 250, 250)',
        'panel_width' => 'body',
        'panel_inner_width' => '.container',
        'panel_header_color' => 'rgb(51, 51, 51)',
        'panel_header_font_weight' => 'normal',
        'panel_header_padding_bottom' => '0px',
        'panel_header_margin_bottom' => '10px',
        'panel_header_border_color' => 'rgba(0, 0, 0, 0.2)',
        'panel_header_border_bottom' => '1px',
        'panel_font_size' => '14px',
        'panel_font_color' => 'rgb(51, 51, 51)',
        'panel_font_family' => 'inherit',
        'panel_second_level_font_color' => 'rgb(51, 51, 51)',
        'panel_second_level_font_color_hover' => 'rgb(51, 51, 51)',
        'panel_second_level_text_transform' => 'uppercase',
        'panel_second_level_font' => 'inherit',
        'panel_second_level_font_size' => '16px',
        'panel_second_level_font_weight' => 'normal',
        'panel_second_level_font_weight_hover' => 'normal',
        'panel_second_level_text_decoration' => 'none',
        'panel_second_level_text_decoration_hover' => 'none',
        'panel_second_level_margin_bottom' => '10px',
        'panel_second_level_border_color' => 'rgb(221, 221, 221)',
        'panel_second_level_border_bottom' => '1px',
        'panel_third_level_font_color' => '#666',
        'panel_third_level_font_color_hover' => '#666',
        'panel_third_level_font' => 'inherit',
        'panel_third_level_font_size' => '14px',
        'flyout_menu_background_from' => 'rgb(250, 250, 250)',
        'flyout_menu_background_to' => 'rgb(250, 250, 250)',
        'flyout_menu_item_divider_color' => 'rgba(0, 0, 0, 0.1)',
        'flyout_background_from' => 'rgba(255, 255, 255, 0)',
        'flyout_background_to' => 'rgba(255, 255, 255, 0)',
        'flyout_background_hover_from' => 'rgba(0, 0, 0, 0.03)',
        'flyout_background_hover_to' => 'rgba(0, 0, 0, 0.03)',
        'flyout_link_size' => '14px',
        'flyout_link_color' => '#666',
        'flyout_link_color_hover' => '#666',
        'flyout_link_family' => 'inherit',
        'responsive_breakpoint' => '980px',
        'shadow_vertical' => '2px',
        'transitions' => 'on',
        'mobile_columns' => '1',
        'toggle_background_from' => 'rgb(225, 225, 225)',
        'toggle_background_to' => 'rgb(225, 225, 225)',
        'toggle_font_color' => 'rgb(51, 51, 51)',
        'mobile_background_from' => 'rgb(225, 225, 225)',
        'mobile_background_to' => 'rgb(225, 225, 225)',
        'custom_css' => '',
    );

    return $themes;
}

add_filter('megamenu_themes', 'load_divi_theme');
