<?php

// BEGIN iThemes Security - Do not modify or remove this line
// iThemes Security Config Details: 2
define( 'FORCE_SSL_ADMIN', true ); // Redirect All HTTP Page Requests to HTTPS - Security > Settings > Secure Socket Layers (SSL) > SSL for Dashboard
// END iThemes Security - Do not modify or remove this line

/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'sparkdem_wp384');

/** MySQL database username */
define('DB_USER', 'sparkdem_wp384');

/** MySQL database password */
define('DB_PASSWORD', '82g5pD]J[S');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'oz3jof1pvhztkkacba6ejbgdp0bytb3zwbo5skhtbdyth3d6yas8zavfvl27fw1z');
define('SECURE_AUTH_KEY', 'h3jv7pvg3okow8zqjvgxl5kzph0qzlzhxzvlyowhdyhmyxqlkvay0tn0oh2p8lej');
define('LOGGED_IN_KEY', 'hrvlpuwntr29utqda6nqdxgrutdbzf5jghmw6svnp8fwgprr99wdgxdmtqllozit');
define('NONCE_KEY', 'gcg8z7w9f99lmrepxuyy1mzdiylxxbj6ebnu2qllagosdry5a31zmhgkw3mhalow');
define('AUTH_SALT', 'zy1slerk6uijaiippqrvmwuqrydtnxhcddiyxrbfgbdilbzgxgw3lekcmn8nqaks');
define('SECURE_AUTH_SALT', 'xoracq4l3w4zov7thqwkwdtugbyk2blhlqqslhosai4w61femuvww6mi3i6dsnu4');
define('LOGGED_IN_SALT', 'ncsxepavx5iebav5kobfrc2bginvulozd3gogfdivftscqurun5bippi8ppx1tog');
define('NONCE_SALT', '4q0jwsgwwmd608r42bcs3ufnjmnkk8tgfnmziq2ndh5zph3szriuxxugh4k7dke1');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'sparkdem88_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);
define('WP_MEMORY_LIMIT', '128M');
define('DISABLE_WP_CRON', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if (!defined('ABSPATH')) {
    define('ABSPATH', dirname(__FILE__) . '/');
}

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');



# Disables all core updates. Added by SiteGround Autoupdate:
define( 'WP_AUTO_UPDATE_CORE', false );
